/**
 * This program and the accompanying materials
 * are made available under the terms of the License
 * which accompanies this distribution in the file LICENSE.txt
 */
package com.archimatetool.model;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>VsmSrmRoleFunction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.archimatetool.model.IArchimatePackage#getVsmSrmRoleFunction()
 * @model
 * @generated
 */
public interface IVsmSrmRoleFunction extends IVsmSrmElement, IBehaviorElement {
} // IVsmSrmRoleFunction
