/**
 * This program and the accompanying materials
 * are made available under the terms of the License
 * which accompanies this distribution in the file LICENSE.txt
 */
package com.archimatetool.model;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>VsmViaB2aUi</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.archimatetool.model.IArchimatePackage#getVsmViaB2aUi()
 * @model
 * @generated
 */
public interface IVsmViaB2aUi extends IVsmViaElement, IBehaviorElement {
} // IVsmViaB2aUi
