/**
 * This program and the accompanying materials
 * are made available under the terms of the License
 * which accompanies this distribution in the file LICENSE.txt
 */
package com.archimatetool.model.impl;

import org.eclipse.emf.ecore.EClass;

import com.archimatetool.model.IArchimatePackage;
import com.archimatetool.model.IVsmTechnologyK8sContainer;


/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Business Function</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class VsmTechnologyK8sContainer extends ArchimateElement implements IVsmTechnologyK8sContainer {
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected VsmTechnologyK8sContainer() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return IArchimatePackage.Literals.VSM_TECHNOLOGY_K8S_CONTAINER;
    }

} //VsmTechnologyK8sContainer
