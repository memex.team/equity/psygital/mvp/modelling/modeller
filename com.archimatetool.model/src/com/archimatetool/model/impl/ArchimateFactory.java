/**
 * This program and the accompanying materials
 * are made available under the terms of the License
 * which accompanies this distribution in the file LICENSE.txt
 */
package com.archimatetool.model.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;

import com.archimatetool.model.FolderType;
import com.archimatetool.model.IAccessRelationship;
import com.archimatetool.model.IAggregationRelationship;
import com.archimatetool.model.IApplicationCollaboration;
import com.archimatetool.model.IApplicationComponent;
import com.archimatetool.model.IApplicationEvent;
import com.archimatetool.model.IApplicationFunction;
import com.archimatetool.model.IVsmViaB2aUi;
import com.archimatetool.model.IVsmViaB2aIntegration;
import com.archimatetool.model.IVsmViaB2aDs;
import com.archimatetool.model.IVsmViaA2dData;
import com.archimatetool.model.IVsmViaA2tYaml;
import com.archimatetool.model.IVsmApplicationTask;
import com.archimatetool.model.IVsmDataGitRepo;
import com.archimatetool.model.IVsmDataGitBranch;
import com.archimatetool.model.IVsmDataSrcDir;
import com.archimatetool.model.IVsmDataSrcFile;
import com.archimatetool.model.IVsmApplicationClass;
import com.archimatetool.model.IVsmApplicationModule;
import com.archimatetool.model.IVsmApplicationMethod;
import com.archimatetool.model.IVsmSrmStatus;
import com.archimatetool.model.IVsmSrmAction;
import com.archimatetool.model.IVsmSrmRoleFunction;
import com.archimatetool.model.IVsmSrmRoleUser;
import com.archimatetool.model.IVsmSrmUser;
import com.archimatetool.model.IApplicationInteraction;
import com.archimatetool.model.IApplicationInterface;
import com.archimatetool.model.IApplicationProcess;
import com.archimatetool.model.IApplicationService;
import com.archimatetool.model.IArchimateDiagramModel;
import com.archimatetool.model.IArchimateFactory;
import com.archimatetool.model.IArchimateModel;
import com.archimatetool.model.IArchimatePackage;
import com.archimatetool.model.IArtifact;
import com.archimatetool.model.IAssessment;
import com.archimatetool.model.IAssignmentRelationship;
import com.archimatetool.model.IAssociationRelationship;
import com.archimatetool.model.IBounds;
import com.archimatetool.model.IBusinessActor;
import com.archimatetool.model.IBusinessCollaboration;
import com.archimatetool.model.IBusinessEvent;
import com.archimatetool.model.IBusinessFunction;
import com.archimatetool.model.IVsmBusinessEpic;
import com.archimatetool.model.IVsmBusinessUs;
import com.archimatetool.model.IVsmBusinessTask;
import com.archimatetool.model.IVsmBusinessAc;
import com.archimatetool.model.IVsmBusinessUc;
import com.archimatetool.model.IVsmBusinessFr;
import com.archimatetool.model.IVsmBusinessNfr;
import com.archimatetool.model.IBusinessInteraction;
import com.archimatetool.model.IBusinessInterface;
import com.archimatetool.model.IBusinessObject;
import com.archimatetool.model.IBusinessProcess;
import com.archimatetool.model.IBusinessRole;
import com.archimatetool.model.IBusinessService;
import com.archimatetool.model.ICapability;
import com.archimatetool.model.ICommunicationNetwork;
import com.archimatetool.model.ICompositionRelationship;
import com.archimatetool.model.IConstraint;
import com.archimatetool.model.IContract;
import com.archimatetool.model.ICourseOfAction;
import com.archimatetool.model.IDataObject;
import com.archimatetool.model.IDeliverable;
import com.archimatetool.model.IDevice;
import com.archimatetool.model.IDiagramModelArchimateConnection;
import com.archimatetool.model.IDiagramModelArchimateObject;
import com.archimatetool.model.IDiagramModelBendpoint;
import com.archimatetool.model.IDiagramModelConnection;
import com.archimatetool.model.IDiagramModelGroup;
import com.archimatetool.model.IDiagramModelImage;
import com.archimatetool.model.IDiagramModelNote;
import com.archimatetool.model.IDiagramModelReference;
import com.archimatetool.model.IDistributionNetwork;
import com.archimatetool.model.IDriver;
import com.archimatetool.model.IEquipment;
import com.archimatetool.model.IFacility;
import com.archimatetool.model.IFeature;
import com.archimatetool.model.IFlowRelationship;
import com.archimatetool.model.IFolder;
import com.archimatetool.model.IGap;
import com.archimatetool.model.IGoal;
import com.archimatetool.model.IVsmDataDatabase;
import com.archimatetool.model.IVsmDataSchema;
import com.archimatetool.model.IVsmDataView;
import com.archimatetool.model.IVsmDataTable;
import com.archimatetool.model.IVsmDataColumn;
import com.archimatetool.model.IVsmDataTask;
import com.archimatetool.model.IVsmLandscapeInfrastructureOctagon;
import com.archimatetool.model.IVsmLandscapeProductOctagon;
import com.archimatetool.model.IVsmLandscapeLayerGeneral;
import com.archimatetool.model.IVsmLandscapeLayerBusiness;
import com.archimatetool.model.IVsmLandscapeLayerApplication;
import com.archimatetool.model.IVsmLandscapeLayerTechnology;
import com.archimatetool.model.IVsmLandscapeLayerData;
import com.archimatetool.model.IVsmLandscapeLayerB2a;
import com.archimatetool.model.IVsmLandscapeLayerA2d;
import com.archimatetool.model.IVsmLandscapeLayerA2t;
import com.archimatetool.model.IDynamicsPersonalFeeling;
import com.archimatetool.model.IDynamicsPersonalPrediction;
import com.archimatetool.model.IDynamicsPersonalPredictionRelationship;
import com.archimatetool.model.IDynamicsPersonalCorrection;
import com.archimatetool.model.IDynamicsPersonalByproduct;
import com.archimatetool.model.IDynamicsTeamPrediction;
import com.archimatetool.model.IDynamicsTeamPredictionRelationship;
import com.archimatetool.model.IDynamicsTeamFeeling;
import com.archimatetool.model.IDynamicsOrgPrediction;
import com.archimatetool.model.IDynamicsOrgPredictionRelationship;
import com.archimatetool.model.IDynamicsEnvPrediction;
import com.archimatetool.model.IDynamicsEnvPredictionRelationship;
import com.archimatetool.model.IContextmeBasicConcept;
import com.archimatetool.model.IContextmeBasicSituation;
import com.archimatetool.model.IContextmeBasicQuestioning;
import com.archimatetool.model.IContextmeBasicCurious;
import com.archimatetool.model.IContextmeBasicLink;
import com.archimatetool.model.IContextmeBasicClarify;
import com.archimatetool.model.IContextmeBasicExpress;
import com.archimatetool.model.IContextmeBasicDiscuss;
import com.archimatetool.model.IContextmeBasicNote;
import com.archimatetool.model.IGrouping;
import com.archimatetool.model.IImplementationEvent;
import com.archimatetool.model.IInfluenceRelationship;
import com.archimatetool.model.IJunction;
import com.archimatetool.model.ILocation;
import com.archimatetool.model.IMaterial;
import com.archimatetool.model.IMeaning;
import com.archimatetool.model.IMetadata;
import com.archimatetool.model.INode;
import com.archimatetool.model.IOutcome;
import com.archimatetool.model.IPath;
import com.archimatetool.model.IPlateau;
import com.archimatetool.model.IPrinciple;
import com.archimatetool.model.IProduct;
import com.archimatetool.model.IProperty;
import com.archimatetool.model.IRealizationRelationship;
import com.archimatetool.model.IRepresentation;
import com.archimatetool.model.IRequirement;
import com.archimatetool.model.IResource;
import com.archimatetool.model.IServingRelationship;
import com.archimatetool.model.ISketchModel;
import com.archimatetool.model.ISketchModelActor;
import com.archimatetool.model.ISketchModelSticky;
import com.archimatetool.model.ISpecializationRelationship;
import com.archimatetool.model.IStakeholder;
import com.archimatetool.model.ISystemSoftware;
import com.archimatetool.model.ITechnologyCollaboration;
import com.archimatetool.model.ITechnologyEvent;
import com.archimatetool.model.ITechnologyFunction;
import com.archimatetool.model.IVsmTechnologyTask;
import com.archimatetool.model.IVsmTechnologyK8sPod;
import com.archimatetool.model.IVsmTechnologyK8sContainer;
import com.archimatetool.model.IVsmTechnologyK8sController;
import com.archimatetool.model.IVsmTechnologyStorage;
import com.archimatetool.model.ITechnologyInteraction;
import com.archimatetool.model.ITechnologyInterface;
import com.archimatetool.model.ITechnologyProcess;
import com.archimatetool.model.ITechnologyService;
import com.archimatetool.model.ITriggeringRelationship;
import com.archimatetool.model.IProductAppOwnsRelationship;
import com.archimatetool.model.IProductBizOwnsRelationship;
import com.archimatetool.model.IProductDataOwnsRelationship;
import com.archimatetool.model.IProductTechOwnsRelationship;
import com.archimatetool.model.IProductTechOSIRelationship;
import com.archimatetool.model.IValue;
import com.archimatetool.model.IValueStream;
import com.archimatetool.model.IWorkPackage;


/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ArchimateFactory extends EFactoryImpl implements IArchimateFactory {
    /**
     * Creates the default factory implementation.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public static IArchimateFactory init() {
        try {
            IArchimateFactory theArchimateFactory = (IArchimateFactory)EPackage.Registry.INSTANCE.getEFactory(IArchimatePackage.eNS_URI);
            if (theArchimateFactory != null) {
                return theArchimateFactory;
            }
        }
        catch (Exception exception) {
            EcorePlugin.INSTANCE.log(exception);
        }
        return new ArchimateFactory();
    }

    /**
     * Creates an instance of the factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public ArchimateFactory() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EObject create(EClass eClass) {
        switch (eClass.getClassifierID()) {
            case IArchimatePackage.PROPERTY: return createProperty();
            case IArchimatePackage.FEATURE: return createFeature();
            case IArchimatePackage.METADATA: return createMetadata();
            case IArchimatePackage.FOLDER: return createFolder();
            case IArchimatePackage.ARCHIMATE_MODEL: return createArchimateModel();
            case IArchimatePackage.JUNCTION: return createJunction();
            case IArchimatePackage.APPLICATION_COLLABORATION: return createApplicationCollaboration();
            case IArchimatePackage.APPLICATION_COMPONENT: return createApplicationComponent();
            case IArchimatePackage.APPLICATION_EVENT: return createApplicationEvent();
            case IArchimatePackage.APPLICATION_FUNCTION: return createApplicationFunction();
            
            case IArchimatePackage.VSM_VIA_B2A_UI: return createVsmViaB2aUi();
            case IArchimatePackage.VSM_VIA_B2A_INTEGRATION: return createVsmViaB2aIntegration();
            case IArchimatePackage.VSM_VIA_B2A_DS: return createVsmViaB2aDs();
            case IArchimatePackage.VSM_VIA_A2D_DATA: return createVsmViaA2dData();
            case IArchimatePackage.VSM_VIA_A2T_YAML: return createVsmViaA2tYaml();
            
            case IArchimatePackage.VSM_APPLICATION_TASK: return createVsmApplicationTask();
            case IArchimatePackage.VSM_DATA_GITREPO: return createVsmDataGitRepo();
            case IArchimatePackage.VSM_DATA_GITBRANCH: return createVsmDataGitBranch();
            case IArchimatePackage.VSM_DATA_SRCDIR: return createVsmDataSrcDir();
            case IArchimatePackage.VSM_DATA_SRCFILE: return createVsmDataSrcFile();
            case IArchimatePackage.VSM_APPLICATION_CLASS: return createVsmApplicationClass();
            case IArchimatePackage.VSM_APPLICATION_MODULE: return createVsmApplicationModule();
            case IArchimatePackage.VSM_APPLICATION_METHOD: return createVsmApplicationMethod();
            
            case IArchimatePackage.APPLICATION_INTERACTION: return createApplicationInteraction();
            case IArchimatePackage.APPLICATION_INTERFACE: return createApplicationInterface();
            case IArchimatePackage.APPLICATION_PROCESS: return createApplicationProcess();
            case IArchimatePackage.APPLICATION_SERVICE: return createApplicationService();
            case IArchimatePackage.ARTIFACT: return createArtifact();
            case IArchimatePackage.ASSESSMENT: return createAssessment();
            case IArchimatePackage.BUSINESS_ACTOR: return createBusinessActor();
            case IArchimatePackage.BUSINESS_COLLABORATION: return createBusinessCollaboration();
            case IArchimatePackage.BUSINESS_EVENT: return createBusinessEvent();
            case IArchimatePackage.BUSINESS_FUNCTION: return createBusinessFunction();
            case IArchimatePackage.VSM_BUSINESS_EPIC: return createVsmBusinessEpic();
            case IArchimatePackage.VSM_BUSINESS_US: return createVsmBusinessUs();
            case IArchimatePackage.VSM_BUSINESS_TASK: return createVsmBusinessTask();
            case IArchimatePackage.VSM_BUSINESS_AC: return createVsmBusinessAc();
            case IArchimatePackage.VSM_BUSINESS_UC: return createVsmBusinessUc();
            case IArchimatePackage.VSM_BUSINESS_FR: return createVsmBusinessFr();
            case IArchimatePackage.VSM_BUSINESS_NFR: return createVsmBusinessNfr();
            case IArchimatePackage.BUSINESS_INTERACTION: return createBusinessInteraction();
            case IArchimatePackage.BUSINESS_INTERFACE: return createBusinessInterface();
            case IArchimatePackage.BUSINESS_OBJECT: return createBusinessObject();
            case IArchimatePackage.BUSINESS_PROCESS: return createBusinessProcess();
            case IArchimatePackage.BUSINESS_ROLE: return createBusinessRole();
            case IArchimatePackage.BUSINESS_SERVICE: return createBusinessService();
            case IArchimatePackage.CAPABILITY: return createCapability();
            case IArchimatePackage.COMMUNICATION_NETWORK: return createCommunicationNetwork();
            case IArchimatePackage.CONTRACT: return createContract();
            case IArchimatePackage.CONSTRAINT: return createConstraint();
            case IArchimatePackage.COURSE_OF_ACTION: return createCourseOfAction();
            case IArchimatePackage.DATA_OBJECT: return createDataObject();
            case IArchimatePackage.DELIVERABLE: return createDeliverable();
            case IArchimatePackage.DEVICE: return createDevice();
            case IArchimatePackage.DISTRIBUTION_NETWORK: return createDistributionNetwork();
            case IArchimatePackage.DRIVER: return createDriver();
            case IArchimatePackage.EQUIPMENT: return createEquipment();
            case IArchimatePackage.FACILITY: return createFacility();
            case IArchimatePackage.GAP: return createGap();
            case IArchimatePackage.GOAL: return createGoal();
            case IArchimatePackage.DYNAMICS_PERSONAL_FEELING: return createDynamicsPersonalFeeling();
            case IArchimatePackage.DYNAMICS_PERSONAL_PREDICTION: return createDynamicsPersonalPrediction();
            case IArchimatePackage.DYNAMICS_PERSONAL_CORRECTION: return createDynamicsPersonalCorrection();
            case IArchimatePackage.DYNAMICS_PERSONAL_BYPRODUCT: return createDynamicsPersonalByproduct();
            case IArchimatePackage.DYNAMICS_TEAM_PREDICTION: return createDynamicsTeamPrediction();
            case IArchimatePackage.DYNAMICS_TEAM_FEELING: return createDynamicsTeamFeeling();
            case IArchimatePackage.DYNAMICS_ORG_PREDICTION: return createDynamicsOrgPrediction();
            case IArchimatePackage.DYNAMICS_ENV_PREDICTION: return createDynamicsEnvPrediction();
            
            case IArchimatePackage.VSM_SRM_STATUS: return createVsmSrmStatus();
            case IArchimatePackage.VSM_SRM_ACTION: return createVsmSrmAction();
            case IArchimatePackage.VSM_SRM_ROLEFUNCTION: return createVsmSrmRoleFunction();
            case IArchimatePackage.VSM_SRM_ROLEUSER: return createVsmSrmRoleUser();
            case IArchimatePackage.VSM_SRM_USER: return createVsmSrmUser();
            
            case IArchimatePackage.CONTEXTME_BASIC_CONCEPT: return createContextmeBasicConcept();
            case IArchimatePackage.CONTEXTME_BASIC_SITUATION: return createContextmeBasicSituation();
            case IArchimatePackage.CONTEXTME_BASIC_QUESTIONING: return createContextmeBasicQuestioning();
            case IArchimatePackage.CONTEXTME_BASIC_CURIOUS: return createContextmeBasicCurious();
            case IArchimatePackage.CONTEXTME_BASIC_LINK: return createContextmeBasicLink();
            case IArchimatePackage.CONTEXTME_BASIC_CLARIFY: return createContextmeBasicClarify();
            case IArchimatePackage.CONTEXTME_BASIC_EXPRESS: return createContextmeBasicExpress();
            case IArchimatePackage.CONTEXTME_BASIC_DISCUSS: return createContextmeBasicDiscuss();
            case IArchimatePackage.CONTEXTME_BASIC_NOTE: return createContextmeBasicNote();

            case IArchimatePackage.VSM_DATA_DATABASE: return createVsmDataDatabase();
            case IArchimatePackage.VSM_DATA_SCHEMA: return createVsmDataSchema();
            case IArchimatePackage.VSM_DATA_VIEW: return createVsmDataView();
            case IArchimatePackage.VSM_DATA_TABLE: return createVsmDataTable();
            case IArchimatePackage.VSM_DATA_COLUMN: return createVsmDataColumn();
            case IArchimatePackage.VSM_DATA_TASK: return createVsmDataTask();                       
            case IArchimatePackage.VSM_LANDSCAPE_INFRASTRUCTURE_OCTAGON: return createVsmLandscapeInfrastructureOctagon();
            case IArchimatePackage.VSM_LANDSCAPE_PRODUCT_OCTAGON: return createVsmLandscapeProductOctagon();
            case IArchimatePackage.VSM_LANDSCAPE_LAYER_GENERAL: return createVsmLandscapeLayerGeneral();
            case IArchimatePackage.VSM_LANDSCAPE_LAYER_BUSINESS: return createVsmLandscapeLayerBusiness();
            case IArchimatePackage.VSM_LANDSCAPE_LAYER_APPLICATION: return createVsmLandscapeLayerApplication();
            case IArchimatePackage.VSM_LANDSCAPE_LAYER_TECHNOLOGY: return createVsmLandscapeLayerTechnology();
            case IArchimatePackage.VSM_LANDSCAPE_LAYER_DATA: return createVsmLandscapeLayerData();
            case IArchimatePackage.VSM_LANDSCAPE_LAYER_B2A: return createVsmLandscapeLayerB2a();
            case IArchimatePackage.VSM_LANDSCAPE_LAYER_A2D: return createVsmLandscapeLayerA2d();
            case IArchimatePackage.VSM_LANDSCAPE_LAYER_A2T: return createVsmLandscapeLayerA2t();
            case IArchimatePackage.GROUPING: return createGrouping();
            case IArchimatePackage.IMPLEMENTATION_EVENT: return createImplementationEvent();
            case IArchimatePackage.LOCATION: return createLocation();
            case IArchimatePackage.MATERIAL: return createMaterial();
            case IArchimatePackage.MEANING: return createMeaning();
            case IArchimatePackage.NODE: return createNode();
            case IArchimatePackage.OUTCOME: return createOutcome();
            case IArchimatePackage.PATH: return createPath();
            case IArchimatePackage.PLATEAU: return createPlateau();
            case IArchimatePackage.PRINCIPLE: return createPrinciple();
            case IArchimatePackage.PRODUCT: return createProduct();
            case IArchimatePackage.REPRESENTATION: return createRepresentation();
            case IArchimatePackage.RESOURCE: return createResource();
            case IArchimatePackage.REQUIREMENT: return createRequirement();
            case IArchimatePackage.STAKEHOLDER: return createStakeholder();
            case IArchimatePackage.SYSTEM_SOFTWARE: return createSystemSoftware();
            case IArchimatePackage.TECHNOLOGY_COLLABORATION: return createTechnologyCollaboration();
            case IArchimatePackage.TECHNOLOGY_EVENT: return createTechnologyEvent();
            case IArchimatePackage.TECHNOLOGY_FUNCTION: return createTechnologyFunction();
            case IArchimatePackage.VSM_TECHNOLOGY_TASK: return createVsmTechnologyTask();
            case IArchimatePackage.VSM_TECHNOLOGY_K8S_POD: return createVsmTechnologyK8sPod();
            case IArchimatePackage.VSM_TECHNOLOGY_K8S_CONTAINER: return createVsmTechnologyK8sContainer();
            case IArchimatePackage.VSM_TECHNOLOGY_K8S_CONTROLLER: return createVsmTechnologyK8sController();
            case IArchimatePackage.VSM_TECHNOLOGY_STORAGE: return createVsmTechnologyStorage();
            case IArchimatePackage.TECHNOLOGY_INTERFACE: return createTechnologyInterface();
            case IArchimatePackage.TECHNOLOGY_INTERACTION: return createTechnologyInteraction();
            case IArchimatePackage.TECHNOLOGY_PROCESS: return createTechnologyProcess();
            case IArchimatePackage.TECHNOLOGY_SERVICE: return createTechnologyService();
            case IArchimatePackage.VALUE: return createValue();
            case IArchimatePackage.VALUE_STREAM: return createValueStream();
            case IArchimatePackage.WORK_PACKAGE: return createWorkPackage();
            case IArchimatePackage.ACCESS_RELATIONSHIP: return createAccessRelationship();
            case IArchimatePackage.AGGREGATION_RELATIONSHIP: return createAggregationRelationship();
            case IArchimatePackage.ASSIGNMENT_RELATIONSHIP: return createAssignmentRelationship();
            case IArchimatePackage.ASSOCIATION_RELATIONSHIP: return createAssociationRelationship();
            case IArchimatePackage.COMPOSITION_RELATIONSHIP: return createCompositionRelationship();
            case IArchimatePackage.FLOW_RELATIONSHIP: return createFlowRelationship();
            case IArchimatePackage.INFLUENCE_RELATIONSHIP: return createInfluenceRelationship();
            case IArchimatePackage.REALIZATION_RELATIONSHIP: return createRealizationRelationship();
            case IArchimatePackage.SERVING_RELATIONSHIP: return createServingRelationship();
            case IArchimatePackage.SPECIALIZATION_RELATIONSHIP: return createSpecializationRelationship();
            case IArchimatePackage.TRIGGERING_RELATIONSHIP: return createTriggeringRelationship();
            case IArchimatePackage.PRODUCTAPPOWNS_RELATIONSHIP: return createProductAppOwnsRelationship();
            case IArchimatePackage.PRODUCTBIZOWNS_RELATIONSHIP: return createProductBizOwnsRelationship();
            case IArchimatePackage.PRODUCTDATAOWNS_RELATIONSHIP: return createProductDataOwnsRelationship();
            case IArchimatePackage.PRODUCTTECHOWNS_RELATIONSHIP: return createProductTechOwnsRelationship();
            
            case IArchimatePackage.DYNAMICS_PERSONAL_PREDICTION_RELATIONSHIP: return createDynamicsPersonalPredictionRelationship();
            case IArchimatePackage.DYNAMICS_TEAM_PREDICTION_RELATIONSHIP: return createDynamicsTeamPredictionRelationship();
            case IArchimatePackage.DYNAMICS_ORG_PREDICTION_RELATIONSHIP: return createDynamicsOrgPredictionRelationship();
            case IArchimatePackage.DYNAMICS_ENV_PREDICTION_RELATIONSHIP: return createDynamicsEnvPredictionRelationship();
            
            case IArchimatePackage.PRODUCTTECHOSI_RELATIONSHIP: return createProductTechOSIRelationship();                                    
            case IArchimatePackage.DIAGRAM_MODEL_REFERENCE: return createDiagramModelReference();
            case IArchimatePackage.DIAGRAM_MODEL_GROUP: return createDiagramModelGroup();
            case IArchimatePackage.DIAGRAM_MODEL_NOTE: return createDiagramModelNote();
            case IArchimatePackage.DIAGRAM_MODEL_IMAGE: return createDiagramModelImage();
            case IArchimatePackage.DIAGRAM_MODEL_CONNECTION: return createDiagramModelConnection();
            case IArchimatePackage.DIAGRAM_MODEL_BENDPOINT: return createDiagramModelBendpoint();
            case IArchimatePackage.BOUNDS: return createBounds();
            case IArchimatePackage.ARCHIMATE_DIAGRAM_MODEL: return createArchimateDiagramModel();
            case IArchimatePackage.DIAGRAM_MODEL_ARCHIMATE_OBJECT: return createDiagramModelArchimateObject();
            case IArchimatePackage.DIAGRAM_MODEL_ARCHIMATE_CONNECTION: return createDiagramModelArchimateConnection();
            case IArchimatePackage.SKETCH_MODEL: return createSketchModel();
            case IArchimatePackage.SKETCH_MODEL_STICKY: return createSketchModelSticky();
            case IArchimatePackage.SKETCH_MODEL_ACTOR: return createSketchModelActor();
            default:
                throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier"); //$NON-NLS-1$ //$NON-NLS-2$
        }
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Object createFromString(EDataType eDataType, String initialValue) {
        switch (eDataType.getClassifierID()) {
            case IArchimatePackage.FOLDER_TYPE:
                return createFolderTypeFromString(eDataType, initialValue);
            default:
                throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier"); //$NON-NLS-1$ //$NON-NLS-2$
        }
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public String convertToString(EDataType eDataType, Object instanceValue) {
        switch (eDataType.getClassifierID()) {
            case IArchimatePackage.FOLDER_TYPE:
                return convertFolderTypeToString(eDataType, instanceValue);
            default:
                throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier"); //$NON-NLS-1$ //$NON-NLS-2$
        }
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IProperty createProperty() {
        Property property = new Property();
        return property;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IFeature createFeature() {
        Feature feature = new Feature();
        return feature;
    }

    /* (non-Javadoc)
     * @see com.archimatetool.model.IArchimateFactory#createProperty(java.lang.String, java.lang.String)
     */
    @Override
    public IProperty createProperty(String key, String value) {
        IProperty property = createProperty();
        property.setKey(key);
        property.setValue(value);
        return property;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IMetadata createMetadata() {
        Metadata metadata = new Metadata();
        return metadata;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IArchimateModel createArchimateModel() {
        ArchimateModel archimateModel = new ArchimateModel();
        return archimateModel;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IJunction createJunction() {
        Junction junction = new Junction();
        return junction;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IResource createResource() {
        Resource resource = new Resource();
        return resource;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IFolder createFolder() {
        Folder folder = new Folder();
        return folder;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IBusinessActor createBusinessActor() {
        BusinessActor businessActor = new BusinessActor();
        return businessActor;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IBusinessCollaboration createBusinessCollaboration() {
        BusinessCollaboration businessCollaboration = new BusinessCollaboration();
        return businessCollaboration;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IContract createContract() {
        Contract contract = new Contract();
        return contract;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IBusinessEvent createBusinessEvent() {
        BusinessEvent businessEvent = new BusinessEvent();
        return businessEvent;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IBusinessFunction createBusinessFunction() {
        BusinessFunction businessFunction = new BusinessFunction();
        return businessFunction;
    }

    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IVsmBusinessTask createVsmBusinessTask() {
        VsmBusinessTask vSMBusinessTask = new VsmBusinessTask();
        return vSMBusinessTask;
    }
    
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IVsmBusinessAc createVsmBusinessAc() {
        VsmBusinessAc vSMBusinessAC = new VsmBusinessAc();
        return vSMBusinessAC;
    }
    
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IVsmBusinessUc createVsmBusinessUc() {
        VsmBusinessUc vSMBusinessUC = new VsmBusinessUc();
        return vSMBusinessUC;
    }
    
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IVsmBusinessFr createVsmBusinessFr() {
        VsmBusinessFr vSMBusinessFR = new VsmBusinessFr();
        return vSMBusinessFR;
    }
    
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IVsmBusinessNfr createVsmBusinessNfr() {
        VsmBusinessNfr vSMBusinessNFR = new VsmBusinessNfr();
        return vSMBusinessNFR;
    }    
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IVsmBusinessUs createVsmBusinessUs() {
        VsmBusinessUs vSMBusinessUS = new VsmBusinessUs();
        return vSMBusinessUS;
    }
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IVsmBusinessEpic createVsmBusinessEpic() {
        VsmBusinessEpic vSMBusinessEpic = new VsmBusinessEpic();
        return vSMBusinessEpic;
    }
    
    

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IBusinessInteraction createBusinessInteraction() {
        BusinessInteraction businessInteraction = new BusinessInteraction();
        return businessInteraction;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IBusinessInterface createBusinessInterface() {
        BusinessInterface businessInterface = new BusinessInterface();
        return businessInterface;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IMeaning createMeaning() {
        Meaning meaning = new Meaning();
        return meaning;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IBusinessObject createBusinessObject() {
        BusinessObject businessObject = new BusinessObject();
        return businessObject;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IBusinessProcess createBusinessProcess() {
        BusinessProcess businessProcess = new BusinessProcess();
        return businessProcess;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IProduct createProduct() {
        Product product = new Product();
        return product;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IRepresentation createRepresentation() {
        Representation representation = new Representation();
        return representation;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IBusinessRole createBusinessRole() {
        BusinessRole businessRole = new BusinessRole();
        return businessRole;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IBusinessService createBusinessService() {
        BusinessService businessService = new BusinessService();
        return businessService;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public ICapability createCapability() {
        Capability capability = new Capability();
        return capability;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public ICommunicationNetwork createCommunicationNetwork() {
        CommunicationNetwork communicationNetwork = new CommunicationNetwork();
        return communicationNetwork;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IValue createValue() {
        Value value = new Value();
        return value;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IValueStream createValueStream() {
        ValueStream valueStream = new ValueStream();
        return valueStream;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public ILocation createLocation() {
        Location location = new Location();
        return location;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IMaterial createMaterial() {
        Material material = new Material();
        return material;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IApplicationComponent createApplicationComponent() {
        ApplicationComponent applicationComponent = new ApplicationComponent();
        return applicationComponent;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IApplicationEvent createApplicationEvent() {
        ApplicationEvent applicationEvent = new ApplicationEvent();
        return applicationEvent;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IApplicationFunction createApplicationFunction() {
        ApplicationFunction applicationFunction = new ApplicationFunction();
        return applicationFunction;
    }

    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IVsmViaB2aUi createVsmViaB2aUi() {
        VsmViaB2aUi vSMViaB2AUI = new VsmViaB2aUi();
        return vSMViaB2AUI;
    }
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IVsmViaB2aIntegration createVsmViaB2aIntegration() {
        VsmViaB2aIntegration vSMViaB2AIntegration = new VsmViaB2aIntegration();
        return vSMViaB2AIntegration;
    }
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IVsmViaB2aDs createVsmViaB2aDs() {
        VsmViaB2aDs vSMViaB2ADs = new VsmViaB2aDs();
        return vSMViaB2ADs;
    }
    
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IVsmViaA2dData createVsmViaA2dData() {
        VsmViaA2dData vSMViaA2DData = new VsmViaA2dData();
        return vSMViaA2DData;
    }
    

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IVsmViaA2tYaml createVsmViaA2tYaml() {
        VsmViaA2tYaml vSMViaA2TTech = new VsmViaA2tYaml();
        return vSMViaA2TTech;
    }
    

    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IVsmApplicationTask createVsmApplicationTask() {
        VsmApplicationTask vSMApplicationTask = new VsmApplicationTask();
        return vSMApplicationTask;
    }
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IVsmDataGitRepo createVsmDataGitRepo() {
        VsmDataGitRepo vSMDataGitRepo = new VsmDataGitRepo();
        return vSMDataGitRepo;
    }
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IVsmDataGitBranch createVsmDataGitBranch() {
        VsmDataGitBranch vSMDataGitBranch = new VsmDataGitBranch();
        return vSMDataGitBranch;
    }
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IVsmDataSrcDir createVsmDataSrcDir() {
        VsmDataSrcDir vSMDataSrcDir = new VsmDataSrcDir();
        return vSMDataSrcDir;
    }
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IVsmDataSrcFile createVsmDataSrcFile() {
        VsmDataSrcFile vSMDataSrcFile = new VsmDataSrcFile();
        return vSMDataSrcFile;
    }
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IVsmApplicationClass createVsmApplicationClass() {
        VsmApplicationClass vSMApplicationClass = new VsmApplicationClass();
        return vSMApplicationClass;
    }
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IVsmApplicationModule createVsmApplicationModule() {
        VsmApplicationModule vSMApplicationModule = new VsmApplicationModule();
        return vSMApplicationModule;
    }
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IVsmApplicationMethod createVsmApplicationMethod() {
        VsmApplicationMethod vSMApplicationMethod = new VsmApplicationMethod();
        return vSMApplicationMethod;
    }
    
       

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IApplicationInteraction createApplicationInteraction() {
        ApplicationInteraction applicationInteraction = new ApplicationInteraction();
        return applicationInteraction;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IApplicationInterface createApplicationInterface() {
        ApplicationInterface applicationInterface = new ApplicationInterface();
        return applicationInterface;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IApplicationProcess createApplicationProcess() {
        ApplicationProcess applicationProcess = new ApplicationProcess();
        return applicationProcess;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IDataObject createDataObject() {
        DataObject dataObject = new DataObject();
        return dataObject;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IApplicationService createApplicationService() {
        ApplicationService applicationService = new ApplicationService();
        return applicationService;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IArtifact createArtifact() {
        Artifact artifact = new Artifact();
        return artifact;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public INode createNode() {
        Node node = new Node();
        return node;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IOutcome createOutcome() {
        Outcome outcome = new Outcome();
        return outcome;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public ISystemSoftware createSystemSoftware() {
        SystemSoftware systemSoftware = new SystemSoftware();
        return systemSoftware;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public ITechnologyCollaboration createTechnologyCollaboration() {
        TechnologyCollaboration technologyCollaboration = new TechnologyCollaboration();
        return technologyCollaboration;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public ITechnologyEvent createTechnologyEvent() {
        TechnologyEvent technologyEvent = new TechnologyEvent();
        return technologyEvent;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public ITechnologyFunction createTechnologyFunction() {
        TechnologyFunction technologyFunction = new TechnologyFunction();
        return technologyFunction;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IVsmTechnologyTask createVsmTechnologyTask() {
        VsmTechnologyTask vSMTechnologyTask = new VsmTechnologyTask();
        return vSMTechnologyTask;
    }
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IVsmTechnologyK8sPod createVsmTechnologyK8sPod() {
        VsmTechnologyK8sPod vSMTechnologyK8sPod = new VsmTechnologyK8sPod();
        return vSMTechnologyK8sPod;
    }
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IVsmTechnologyK8sContainer createVsmTechnologyK8sContainer() {
        VsmTechnologyK8sContainer vSMTechnologyK8sContainer = new VsmTechnologyK8sContainer();
        return vSMTechnologyK8sContainer;
    }
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IVsmTechnologyK8sController createVsmTechnologyK8sController() {
        VsmTechnologyK8sController vSMTechnologyK8sController = new VsmTechnologyK8sController();
        return vSMTechnologyK8sController;
    }
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IVsmTechnologyStorage createVsmTechnologyStorage() {
        VsmTechnologyStorage vSMTechnologyStorage = new VsmTechnologyStorage();
        return vSMTechnologyStorage;
    }
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public ITechnologyInterface createTechnologyInterface() {
        TechnologyInterface technologyInterface = new TechnologyInterface();
        return technologyInterface;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public ITechnologyInteraction createTechnologyInteraction() {
        TechnologyInteraction technologyInteraction = new TechnologyInteraction();
        return technologyInteraction;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public ITechnologyProcess createTechnologyProcess() {
        TechnologyProcess technologyProcess = new TechnologyProcess();
        return technologyProcess;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public ITechnologyService createTechnologyService() {
        TechnologyService technologyService = new TechnologyService();
        return technologyService;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IDevice createDevice() {
        Device device = new Device();
        return device;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IDistributionNetwork createDistributionNetwork() {
        DistributionNetwork distributionNetwork = new DistributionNetwork();
        return distributionNetwork;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IEquipment createEquipment() {
        Equipment equipment = new Equipment();
        return equipment;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IFacility createFacility() {
        Facility facility = new Facility();
        return facility;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IStakeholder createStakeholder() {
        Stakeholder stakeholder = new Stakeholder();
        return stakeholder;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IDriver createDriver() {
        Driver driver = new Driver();
        return driver;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IAssessment createAssessment() {
        Assessment assessment = new Assessment();
        return assessment;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IGoal createGoal() {
        Goal goal = new Goal();
        return goal;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IDynamicsPersonalFeeling createDynamicsPersonalFeeling() {
    	DynamicsPersonalFeeling dynamicsPersonalFeeling = new DynamicsPersonalFeeling();
        return dynamicsPersonalFeeling;
    }   
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IDynamicsPersonalPrediction createDynamicsPersonalPrediction() {
    	DynamicsPersonalPrediction dynamicsPersonalPrediction = new DynamicsPersonalPrediction();
        return dynamicsPersonalPrediction;
    }       
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IDynamicsPersonalCorrection createDynamicsPersonalCorrection() {
    	DynamicsPersonalCorrection dynamicsPersonalCorrection = new DynamicsPersonalCorrection();
        return dynamicsPersonalCorrection;
    }   
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IDynamicsPersonalByproduct createDynamicsPersonalByproduct() {
    	DynamicsPersonalByproduct dynamicsPersonalByproduct = new DynamicsPersonalByproduct();
        return dynamicsPersonalByproduct;
    }   
    
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IDynamicsTeamPrediction createDynamicsTeamPrediction() {
    	DynamicsTeamPrediction dynamicsTeamPrediction = new DynamicsTeamPrediction();
        return dynamicsTeamPrediction;
    }       
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IDynamicsTeamFeeling createDynamicsTeamFeeling() {
    	DynamicsTeamFeeling dynamicsTeamFeeling = new DynamicsTeamFeeling();
        return dynamicsTeamFeeling;
    }   
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IDynamicsOrgPrediction createDynamicsOrgPrediction() {
    	DynamicsOrgPrediction dynamicsOrgPrediction = new DynamicsOrgPrediction();
        return dynamicsOrgPrediction;
    }       
        
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IDynamicsEnvPrediction createDynamicsEnvPrediction() {
    	DynamicsEnvPrediction dynamicsEnvPrediction = new DynamicsEnvPrediction();
        return dynamicsEnvPrediction;
    }       
    
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IVsmSrmStatus createVsmSrmStatus() {
    	VsmSrmStatus vsmSrmStatus = new VsmSrmStatus();
        return vsmSrmStatus;
    } 
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IVsmSrmAction createVsmSrmAction() {
    	VsmSrmAction vsmSrmAction = new VsmSrmAction();
        return vsmSrmAction;
    } 
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IVsmSrmRoleFunction createVsmSrmRoleFunction() {
    	VsmSrmRoleFunction vsmSrmRoleFunction = new VsmSrmRoleFunction();
        return vsmSrmRoleFunction;
    } 
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IVsmSrmRoleUser createVsmSrmRoleUser() {
    	VsmSrmRoleUser vsmSrmRoleUser = new VsmSrmRoleUser();
        return vsmSrmRoleUser;
    } 
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IVsmSrmUser createVsmSrmUser() {
    	VsmSrmUser vsmSrmUser = new VsmSrmUser();
        return vsmSrmUser;
    } 
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IContextmeBasicConcept createContextmeBasicConcept() {
    	ContextmeBasicConcept contextmeBasicConcept = new ContextmeBasicConcept();
        return contextmeBasicConcept;
    } 
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IContextmeBasicSituation createContextmeBasicSituation() {
    	ContextmeBasicSituation contextmeBasicSituation = new ContextmeBasicSituation();
        return contextmeBasicSituation;
    } 
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IContextmeBasicQuestioning createContextmeBasicQuestioning() {
    	ContextmeBasicQuestioning contextmeBasicQuestioning = new ContextmeBasicQuestioning();
        return contextmeBasicQuestioning;
    } 
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IContextmeBasicCurious createContextmeBasicCurious() {
    	ContextmeBasicCurious contextmeBasicCurious = new ContextmeBasicCurious();
        return contextmeBasicCurious;
    } 
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IContextmeBasicLink createContextmeBasicLink() {
    	ContextmeBasicLink contextmeBasicLink = new ContextmeBasicLink();
        return contextmeBasicLink;
    } 
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IContextmeBasicClarify createContextmeBasicClarify() {
    	ContextmeBasicClarify contextmeBasicClarify = new ContextmeBasicClarify();
        return contextmeBasicClarify;
    } 
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IContextmeBasicExpress createContextmeBasicExpress() {
    	ContextmeBasicExpress contextmeBasicExpress = new ContextmeBasicExpress();
        return contextmeBasicExpress;
    } 
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IContextmeBasicDiscuss createContextmeBasicDiscuss() {
    	ContextmeBasicDiscuss contextmeBasicDiscuss = new ContextmeBasicDiscuss();
        return contextmeBasicDiscuss;
    } 
    

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IContextmeBasicNote createContextmeBasicNote() {
    	ContextmeBasicNote contextmeBasicNote = new ContextmeBasicNote();
        return contextmeBasicNote;
    } 
 
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IVsmDataTask createVsmDataTask() {
        VsmDataTask vSMDataTask = new VsmDataTask();
        return vSMDataTask;
    }
    
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IVsmDataDatabase createVsmDataDatabase() {
        VsmDataDatabase vSMDataDatabase = new VsmDataDatabase();
        return vSMDataDatabase;
    }
    
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IVsmDataSchema createVsmDataSchema() {
        VsmDataSchema vSMDataSchema = new VsmDataSchema();
        return vSMDataSchema;
    }
    
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IVsmDataView createVsmDataView() {
        VsmDataView vSMDataView = new VsmDataView();
        return vSMDataView;
    }
    
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IVsmDataTable createVsmDataTable() {
        VsmDataTable vSMDataTable = new VsmDataTable();
        return vSMDataTable;
    }
    
    
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IVsmDataColumn createVsmDataColumn() {
        VsmDataColumn vSMDataColumn = new VsmDataColumn();
        return vSMDataColumn;
    }
    
   /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IVsmLandscapeInfrastructureOctagon createVsmLandscapeInfrastructureOctagon() {
        VsmLandscapeInfrastructureOctagon vsmLandscapeInfrastructureOctagon = new VsmLandscapeInfrastructureOctagon();
        return vsmLandscapeInfrastructureOctagon;
    } 
    
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IVsmLandscapeProductOctagon createVsmLandscapeProductOctagon() {
        VsmLandscapeProductOctagon vsmLandscapeProductOctagon = new VsmLandscapeProductOctagon();
        return vsmLandscapeProductOctagon;
    }
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IVsmLandscapeLayerGeneral createVsmLandscapeLayerGeneral() {
        VsmLandscapeLayerGeneral vsmLandscapeLayerGeneral = new VsmLandscapeLayerGeneral();
        return vsmLandscapeLayerGeneral;
    }
    
    @Override
    public IVsmLandscapeLayerBusiness createVsmLandscapeLayerBusiness() {
        VsmLandscapeLayerBusiness vsmLandscapeLayerBusiness = new VsmLandscapeLayerBusiness();
        return vsmLandscapeLayerBusiness;
    }
    
    @Override
    public IVsmLandscapeLayerApplication createVsmLandscapeLayerApplication() {
        VsmLandscapeLayerApplication vsmLandscapeLayerApplication = new VsmLandscapeLayerApplication();
        return vsmLandscapeLayerApplication;
    }
    
    @Override
    public IVsmLandscapeLayerTechnology createVsmLandscapeLayerTechnology() {
        VsmLandscapeLayerTechnology vsmLandscapeLayerTechnology = new VsmLandscapeLayerTechnology();
        return vsmLandscapeLayerTechnology;
    }
    
    @Override
    public IVsmLandscapeLayerData createVsmLandscapeLayerData() {
        VsmLandscapeLayerData vsmLandscapeLayerData = new VsmLandscapeLayerData();
        return vsmLandscapeLayerData;
    }    
    
    @Override
    public IVsmLandscapeLayerB2a createVsmLandscapeLayerB2a() {
        VsmLandscapeLayerB2a vsmLandscapeLayerB2a = new VsmLandscapeLayerB2a();
        return vsmLandscapeLayerB2a;
    }
    
    @Override
    public IVsmLandscapeLayerA2d createVsmLandscapeLayerA2d() {
        VsmLandscapeLayerA2d vsmLandscapeLayerA2d = new VsmLandscapeLayerA2d();
        return vsmLandscapeLayerA2d;
    }
    
    @Override
    public IVsmLandscapeLayerA2t createVsmLandscapeLayerA2t() {
        VsmLandscapeLayerA2t vsmLandscapeLayerA2t = new VsmLandscapeLayerA2t();
        return vsmLandscapeLayerA2t;
    }
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IGrouping createGrouping() {
        Grouping grouping = new Grouping();
        return grouping;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IImplementationEvent createImplementationEvent() {
        ImplementationEvent implementationEvent = new ImplementationEvent();
        return implementationEvent;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IRequirement createRequirement() {
        Requirement requirement = new Requirement();
        return requirement;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IConstraint createConstraint() {
        Constraint constraint = new Constraint();
        return constraint;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public ICourseOfAction createCourseOfAction() {
        CourseOfAction courseOfAction = new CourseOfAction();
        return courseOfAction;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IPrinciple createPrinciple() {
        Principle principle = new Principle();
        return principle;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IWorkPackage createWorkPackage() {
        WorkPackage workPackage = new WorkPackage();
        return workPackage;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IDeliverable createDeliverable() {
        Deliverable deliverable = new Deliverable();
        return deliverable;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IPlateau createPlateau() {
        Plateau plateau = new Plateau();
        return plateau;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IGap createGap() {
        Gap gap = new Gap();
        return gap;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IArchimateDiagramModel createArchimateDiagramModel() {
        ArchimateDiagramModel archimateDiagramModel = new ArchimateDiagramModel();
        return archimateDiagramModel;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IDiagramModelReference createDiagramModelReference() {
        DiagramModelReference diagramModelReference = new DiagramModelReference();
        return diagramModelReference;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IDiagramModelArchimateObject createDiagramModelArchimateObject() {
        DiagramModelArchimateObject diagramModelArchimateObject = new DiagramModelArchimateObject();
        return diagramModelArchimateObject;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IDiagramModelGroup createDiagramModelGroup() {
        DiagramModelGroup diagramModelGroup = new DiagramModelGroup();
        return diagramModelGroup;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IDiagramModelNote createDiagramModelNote() {
        DiagramModelNote diagramModelNote = new DiagramModelNote();
        return diagramModelNote;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IDiagramModelImage createDiagramModelImage() {
        DiagramModelImage diagramModelImage = new DiagramModelImage();
        return diagramModelImage;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IDiagramModelConnection createDiagramModelConnection() {
        DiagramModelConnection diagramModelConnection = new DiagramModelConnection();
        return diagramModelConnection;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IDiagramModelArchimateConnection createDiagramModelArchimateConnection() {
        DiagramModelArchimateConnection diagramModelArchimateConnection = new DiagramModelArchimateConnection();
        return diagramModelArchimateConnection;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IDiagramModelBendpoint createDiagramModelBendpoint() {
        DiagramModelBendpoint diagramModelBendpoint = new DiagramModelBendpoint();
        return diagramModelBendpoint;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IBounds createBounds() {
        Bounds bounds = new Bounds();
        return bounds;
    }
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public ISketchModel createSketchModel() {
        SketchModel sketchModel = new SketchModel();
        return sketchModel;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public ISketchModelSticky createSketchModelSticky() {
        SketchModelSticky sketchModelSticky = new SketchModelSticky();
        return sketchModelSticky;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public ISketchModelActor createSketchModelActor() {
        SketchModelActor sketchModelActor = new SketchModelActor();
        return sketchModelActor;
    }

    /* (non-Javadoc)
     * @see com.archimatetool.model.IArchimateFactory#createBounds(int, int, int, int)
     */
    @Override
    public IBounds createBounds(int x, int y, int width, int height) {
        IBounds bounds = createBounds();
        bounds.setX(x);
        bounds.setY(y);
        bounds.setWidth(width);
        bounds.setHeight(height);
        return bounds;
    }
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public FolderType createFolderTypeFromString(EDataType eDataType, String initialValue) {
        FolderType result = FolderType.get(initialValue);
        if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
        return result;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public String convertFolderTypeToString(EDataType eDataType, Object instanceValue) {
        return instanceValue == null ? null : instanceValue.toString();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IApplicationCollaboration createApplicationCollaboration() {
        ApplicationCollaboration applicationCollaboration = new ApplicationCollaboration();
        return applicationCollaboration;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IPath createPath() {
        Path path = new Path();
        return path;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IAccessRelationship createAccessRelationship() {
        AccessRelationship accessRelationship = new AccessRelationship();
        return accessRelationship;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IAggregationRelationship createAggregationRelationship() {
        AggregationRelationship aggregationRelationship = new AggregationRelationship();
        return aggregationRelationship;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IAssignmentRelationship createAssignmentRelationship() {
        AssignmentRelationship assignmentRelationship = new AssignmentRelationship();
        return assignmentRelationship;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IAssociationRelationship createAssociationRelationship() {
        AssociationRelationship associationRelationship = new AssociationRelationship();
        return associationRelationship;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public ICompositionRelationship createCompositionRelationship() {
        CompositionRelationship compositionRelationship = new CompositionRelationship();
        return compositionRelationship;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IFlowRelationship createFlowRelationship() {
        FlowRelationship flowRelationship = new FlowRelationship();
        return flowRelationship;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public ITriggeringRelationship createTriggeringRelationship() {
        TriggeringRelationship triggeringRelationship = new TriggeringRelationship();
        return triggeringRelationship;
    }
    
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    
    @Override
    public IProductAppOwnsRelationship createProductAppOwnsRelationship() {
        ProductAppOwnsRelationship productAppOwnsRelationship = new ProductAppOwnsRelationship();
        return productAppOwnsRelationship;
    }  
    
    @Override
    public IProductBizOwnsRelationship createProductBizOwnsRelationship() {
        ProductBizOwnsRelationship productBizOwnsRelationship = new ProductBizOwnsRelationship();
        return productBizOwnsRelationship;
    }  
    
    @Override
    public IProductDataOwnsRelationship createProductDataOwnsRelationship() {
        ProductDataOwnsRelationship productDataOwnsRelationship = new ProductDataOwnsRelationship();
        return productDataOwnsRelationship;
    }  
    
    @Override
    public IProductTechOwnsRelationship createProductTechOwnsRelationship() {
        ProductTechOwnsRelationship productTechOwnsRelationship = new ProductTechOwnsRelationship();
        return productTechOwnsRelationship;
    }  
    
    
    @Override
    public IDynamicsPersonalPredictionRelationship createDynamicsPersonalPredictionRelationship() {
        DynamicsPersonalPredictionRelationship dynamicsPersonalPredictionRelationship = new DynamicsPersonalPredictionRelationship();
        return dynamicsPersonalPredictionRelationship;
    }      
    
    @Override
    public IDynamicsTeamPredictionRelationship createDynamicsTeamPredictionRelationship() {
        DynamicsTeamPredictionRelationship dynamicsTeamPredictionRelationship = new DynamicsTeamPredictionRelationship();
        return dynamicsTeamPredictionRelationship;
    } 
    
    @Override
    public IDynamicsOrgPredictionRelationship createDynamicsOrgPredictionRelationship() {
        DynamicsOrgPredictionRelationship dynamicsOrgPredictionRelationship = new DynamicsOrgPredictionRelationship();
        return dynamicsOrgPredictionRelationship;
    } 
    
    @Override
    public IDynamicsEnvPredictionRelationship createDynamicsEnvPredictionRelationship() {
        DynamicsEnvPredictionRelationship dynamicsEnvPredictionRelationship = new DynamicsEnvPredictionRelationship();
        return dynamicsEnvPredictionRelationship;
    } 
    
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IProductTechOSIRelationship createProductTechOSIRelationship() {
        ProductTechOSIRelationship productTechOSIRelationship = new ProductTechOSIRelationship();
        return productTechOSIRelationship;
    }     
    

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IInfluenceRelationship createInfluenceRelationship() {
        InfluenceRelationship influenceRelationship = new InfluenceRelationship();
        return influenceRelationship;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IRealizationRelationship createRealizationRelationship() {
        RealizationRelationship realizationRelationship = new RealizationRelationship();
        return realizationRelationship;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IServingRelationship createServingRelationship() {
        ServingRelationship servingRelationship = new ServingRelationship();
        return servingRelationship;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public ISpecializationRelationship createSpecializationRelationship() {
        SpecializationRelationship specializationRelationship = new SpecializationRelationship();
        return specializationRelationship;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IArchimatePackage getArchimatePackage() {
        return (IArchimatePackage)getEPackage();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @deprecated
     * @generated
     */
    @Deprecated
    public static IArchimatePackage getPackage() {
        return IArchimatePackage.eINSTANCE;
    }

} //ArchimateFactory
