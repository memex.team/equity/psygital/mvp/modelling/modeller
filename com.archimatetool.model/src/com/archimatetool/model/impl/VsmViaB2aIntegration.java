/**
 * This program and the accompanying materials
 * are made available under the terms of the License
 * which accompanies this distribution in the file LICENSE.txt
 */
package com.archimatetool.model.impl;

import org.eclipse.emf.ecore.EClass;

import com.archimatetool.model.IArchimatePackage;
import com.archimatetool.model.IVsmViaB2aIntegration;


/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>VsmViaB2aIntegration</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class VsmViaB2aIntegration extends ArchimateElement implements IVsmViaB2aIntegration {
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected VsmViaB2aIntegration() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return IArchimatePackage.Literals.VSM_VIA_B2A_INTEGRATION;
    }

} //VsmViaB2aIntegration
