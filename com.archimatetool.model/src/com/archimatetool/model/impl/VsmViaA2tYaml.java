/**
 * This program and the accompanying materials
 * are made available under the terms of the License
 * which accompanies this distribution in the file LICENSE.txt
 */
package com.archimatetool.model.impl;

import org.eclipse.emf.ecore.EClass;

import com.archimatetool.model.IArchimatePackage;
import com.archimatetool.model.IVsmViaA2tYaml;


/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>VsmViaA2tYaml</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class VsmViaA2tYaml extends ArchimateElement implements IVsmViaA2tYaml {
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected VsmViaA2tYaml() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return IArchimatePackage.Literals.VSM_VIA_A2T_YAML;
    }

} //VsmViaA2tYaml
