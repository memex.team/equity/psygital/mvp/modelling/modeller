/**
 * This program and the accompanying materials
 * are made available under the terms of the License
 * which accompanies this distribution in the file LICENSE.txt
 */
package com.archimatetool.model.impl;

import org.eclipse.emf.ecore.EClass;

import com.archimatetool.model.IArchimatePackage;
import com.archimatetool.model.IVsmLandscapeLayerA2d;


/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>VsmLandscape LayerA2d</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class VsmLandscapeLayerA2d extends ArchimateElement implements IVsmLandscapeLayerA2d {
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected VsmLandscapeLayerA2d() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return IArchimatePackage.Literals.VSM_LANDSCAPE_LAYER_A2D;
    }

} //LayerA2d
