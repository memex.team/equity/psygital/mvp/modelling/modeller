/**
 * This program and the accompanying materials
 * are made available under the terms of the License
 * which accompanies this distribution in the file LICENSE.txt
 */
package com.archimatetool.model.impl;

import org.eclipse.emf.ecore.EClass;

import com.archimatetool.model.IArchimatePackage;
import com.archimatetool.model.IVsmDataSrcDir;


/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>VSM Application SrcDir</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class VsmDataSrcDir extends ArchimateElement implements IVsmDataSrcDir {
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected VsmDataSrcDir() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return IArchimatePackage.Literals.VSM_DATA_SRCDIR;
    }

} //VsmDataSrcDir
