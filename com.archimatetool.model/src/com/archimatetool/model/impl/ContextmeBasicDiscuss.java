/**
 * This program and the accompanying materials
 * are made available under the terms of the License
 * which accompanies this distribution in the file LICENSE.txt
 */
package com.archimatetool.model.impl;

import org.eclipse.emf.ecore.EClass;

import com.archimatetool.model.IArchimatePackage;
import com.archimatetool.model.IContextmeBasicDiscuss;


/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Contextme Basic Discuss</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ContextmeBasicDiscuss extends ArchimateElement implements IContextmeBasicDiscuss {
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected ContextmeBasicDiscuss() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return IArchimatePackage.Literals.CONTEXTME_BASIC_DISCUSS;
    }

} //ContextmeBasicDiscuss
