/**
 * This program and the accompanying materials
 * are made available under the terms of the License
 * which accompanies this distribution in the file LICENSE.txt
 */
package com.archimatetool.model.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.UniqueEList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

import com.archimatetool.model.IAdapter;
import com.archimatetool.model.IArchimateConcept;
import com.archimatetool.model.IArchimateModel;
import com.archimatetool.model.IArchimateModelObject;
import com.archimatetool.model.IArchimatePackage;
import com.archimatetool.model.IArchimateRelationship;
import com.archimatetool.model.ICloneable;
import com.archimatetool.model.IDocumentable;
import com.archimatetool.model.IArchestry;
import com.archimatetool.model.IFeature;
import com.archimatetool.model.IFeatures;
import com.archimatetool.model.IIdentifier;
import com.archimatetool.model.INameable;
import com.archimatetool.model.IProperties;
import com.archimatetool.model.IFeaturesEList;
import com.archimatetool.model.IProperty;
import com.archimatetool.model.util.UUIDFactory;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Concept</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.archimatetool.model.impl.ArchimateConcept#getName <em>Name</em>}</li>
 *   <li>{@link com.archimatetool.model.impl.ArchimateConcept#getId <em>Id</em>}</li>
 *   <li>{@link com.archimatetool.model.impl.ArchimateConcept#getFeatures <em>Features</em>}</li>
 *   <li>{@link com.archimatetool.model.impl.ArchimateConcept#getDocumentation <em>Documentation</em>}</li>
 *   <li>{@link com.archimatetool.model.impl.ArchimateConcept#getProperties <em>Properties</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class ArchimateConcept extends EObjectImpl implements IArchimateConcept {
    /**
     * The default value of the '{@link #getName() <em>Name</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getName()
     * @generated
     * @ordered
     */
    protected static final String NAME_EDEFAULT = ""; //$NON-NLS-1$

    /**
     * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getName()
     * @generated
     * @ordered
     */
    protected String name = NAME_EDEFAULT;

    /**
     * The default value of the '{@link #getId() <em>Id</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getId()
     * @generated
     * @ordered
     */
    protected static final String ID_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getId()
     * @generated
     * @ordered
     */
    protected String id = ID_EDEFAULT;

    /**
     * The cached value of the '{@link #getFeatures() <em>Features</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getFeatures()
     * @generated
     * @ordered
     */
    protected EList<IFeature> features;

    /**
     * The default value of the '{@link #getDocumentation() <em>Documentation</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getDocumentation()
     * @generated
     * @ordered
     */
    protected static final String DOCUMENTATION_EDEFAULT = ""; //$NON-NLS-1$

    /**
     * The cached value of the '{@link #getDocumentation() <em>Documentation</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getDocumentation()
     * @generated
     * @ordered
     */
    protected String documentation = DOCUMENTATION_EDEFAULT;

    
    protected static final String ARCHESTRY_ORDER_LABEL_EDEFAULT = ""; //$NON-NLS-1$
    protected String archestry_OrderLabel = ARCHESTRY_ORDER_LABEL_EDEFAULT;
    protected static final String ARCHESTRY_ORDER_FONTCOLOR_EDEFAULT = ""; //$NON-NLS-1$
    protected String archestry_OrderFontColor = ARCHESTRY_ORDER_FONTCOLOR_EDEFAULT;
    protected static final String ARCHESTRY_ORDER_AREACOLOR_EDEFAULT = ""; //$NON-NLS-1$
    protected String archestry_OrderAreaColor = ARCHESTRY_ORDER_AREACOLOR_EDEFAULT;
    
    protected static final String ARCHESTRY_FAMILY_LABEL_EDEFAULT = ""; //$NON-NLS-1$
    protected String archestry_FamilyLabel = ARCHESTRY_FAMILY_LABEL_EDEFAULT;
    protected static final String ARCHESTRY_FAMILY_FONTCOLOR_EDEFAULT = ""; //$NON-NLS-1$
    protected String archestry_FamilyFontColor = ARCHESTRY_FAMILY_FONTCOLOR_EDEFAULT;
    protected static final String ARCHESTRY_FAMILY_AREACOLOR_EDEFAULT = ""; //$NON-NLS-1$
    protected String archestry_FamilyAreaColor = ARCHESTRY_FAMILY_AREACOLOR_EDEFAULT;
    
    protected static final String ARCHESTRY_GENUS_LABEL_EDEFAULT = ""; //$NON-NLS-1$
    protected String archestry_GenusLabel = ARCHESTRY_GENUS_LABEL_EDEFAULT;
    protected static final String ARCHESTRY_GENUS_FONTCOLOR_EDEFAULT = ""; //$NON-NLS-1$
    protected String archestry_GenusFontColor = ARCHESTRY_GENUS_FONTCOLOR_EDEFAULT;
    protected static final String ARCHESTRY_GENUS_AREACOLOR_EDEFAULT = ""; //$NON-NLS-1$
    protected String archestry_GenusAreaColor = ARCHESTRY_GENUS_AREACOLOR_EDEFAULT;
    
    protected static final String ARCHESTRY_EXTUUID_LABEL_EDEFAULT = ""; //$NON-NLS-1$
    protected String archestry_ExtUUIDLabel = ARCHESTRY_EXTUUID_LABEL_EDEFAULT;
    protected static final String ARCHESTRY_EXTUUID_FONTCOLOR_EDEFAULT = ""; //$NON-NLS-1$
    protected String archestry_ExtUUIDFontColor = ARCHESTRY_EXTUUID_FONTCOLOR_EDEFAULT;
    protected static final String ARCHESTRY_EXTUUID_AREACOLOR_EDEFAULT = ""; //$NON-NLS-1$
    protected String archestry_ExtUUIDAreaColor = ARCHESTRY_EXTUUID_AREACOLOR_EDEFAULT;
    
    protected static final String ARCHESTRY_REFID_LABEL_EDEFAULT = ""; //$NON-NLS-1$
    protected String archestry_RefIDLabel = ARCHESTRY_REFID_LABEL_EDEFAULT;
    protected static final String ARCHESTRY_REFID_FONTCOLOR_EDEFAULT = ""; //$NON-NLS-1$
    protected String archestry_RefIDFontColor = ARCHESTRY_REFID_FONTCOLOR_EDEFAULT;
    protected static final String ARCHESTRY_REFID_AREACOLOR_EDEFAULT = ""; //$NON-NLS-1$
    protected String archestry_RefIDAreaColor = ARCHESTRY_REFID_AREACOLOR_EDEFAULT;
    
    protected static final String ARCHESTRY_STATUS_LABEL_EDEFAULT = ""; //$NON-NLS-1$
    protected String archestry_StatusLabel = ARCHESTRY_STATUS_LABEL_EDEFAULT;
    protected static final String ARCHESTRY_STATUS_FONTCOLOR_EDEFAULT = ""; //$NON-NLS-1$
    protected String archestry_StatusFontColor = ARCHESTRY_STATUS_FONTCOLOR_EDEFAULT;
    protected static final String ARCHESTRY_STATUS_AREACOLOR_EDEFAULT = ""; //$NON-NLS-1$
    protected String archestry_StatusAreaColor = ARCHESTRY_STATUS_AREACOLOR_EDEFAULT;
    
    protected static final String ARCHESTRY_STAGE_LABEL_EDEFAULT = ""; //$NON-NLS-1$
    protected String archestry_StageLabel = ARCHESTRY_STAGE_LABEL_EDEFAULT;
    protected static final String ARCHESTRY_STAGE_FONTCOLOR_EDEFAULT = ""; //$NON-NLS-1$
    protected String archestry_StageFontColor = ARCHESTRY_STAGE_FONTCOLOR_EDEFAULT;
    protected static final String ARCHESTRY_STAGE_AREACOLOR_EDEFAULT = ""; //$NON-NLS-1$
    protected String archestry_StageAreaColor = ARCHESTRY_STAGE_AREACOLOR_EDEFAULT;
    
    protected static final String ARCHESTRY_COMMWITH_LABEL_EDEFAULT = ""; //$NON-NLS-1$
    protected String archestry_CommWithLabel = ARCHESTRY_COMMWITH_LABEL_EDEFAULT;
    protected static final String ARCHESTRY_COMMWITH_FONTCOLOR_EDEFAULT = ""; //$NON-NLS-1$
    protected String archestry_CommWithFontColor = ARCHESTRY_COMMWITH_FONTCOLOR_EDEFAULT;
    protected static final String ARCHESTRY_COMMWITH_AREACOLOR_EDEFAULT = ""; //$NON-NLS-1$
    protected String archestry_CommWithAreaColor = ARCHESTRY_COMMWITH_AREACOLOR_EDEFAULT;
    
    protected static final String ARCHESTRY_VERSION_LABEL_EDEFAULT = ""; //$NON-NLS-1$
    protected String archestry_VersionLabel = ARCHESTRY_VERSION_LABEL_EDEFAULT;
    protected static final String ARCHESTRY_VERSION_FONTCOLOR_EDEFAULT = ""; //$NON-NLS-1$
    protected String archestry_VersionFontColor = ARCHESTRY_VERSION_FONTCOLOR_EDEFAULT;
    protected static final String ARCHESTRY_VERSION_AREACOLOR_EDEFAULT = ""; //$NON-NLS-1$
    protected String archestry_VersionAreaColor = ARCHESTRY_VERSION_AREACOLOR_EDEFAULT;
    
    protected static final String ARCHESTRY_DATE_LABEL_EDEFAULT = ""; //$NON-NLS-1$
    protected String archestry_DateLabel = ARCHESTRY_DATE_LABEL_EDEFAULT;
    protected static final String ARCHESTRY_DATE_FONTCOLOR_EDEFAULT = ""; //$NON-NLS-1$
    protected String archestry_DateFontColor = ARCHESTRY_DATE_FONTCOLOR_EDEFAULT;
    protected static final String ARCHESTRY_DATE_AREACOLOR_EDEFAULT = ""; //$NON-NLS-1$
    protected String archestry_DateAreaColor = ARCHESTRY_DATE_AREACOLOR_EDEFAULT;


    /**
     * The cached value of the '{@link #getProperties() <em>Properties</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getProperties()
     * @generated
     * @ordered
     */
    protected EList<IProperty> properties;

    /**
     * Adapter Map for arbitrary objects
     */
    private Map<Object, Object> fAdapterMap = new HashMap<Object, Object>();

    /**
     * Stored references to connected relationships
     */
    protected EList<IArchimateRelationship> sourceRelationships, targetRelationships;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated NOT
     */
    protected ArchimateConcept() {
        super();
        id = UUIDFactory.createID(this);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return IArchimatePackage.Literals.ARCHIMATE_CONCEPT;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public String getId() {
        return id;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void setId(String newId) {
        String oldId = id;
        id = newId;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, IArchimatePackage.ARCHIMATE_CONCEPT__ID, oldId, id));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated NOT
     */
    @Override
    public IFeaturesEList getFeatures() {
        if (features == null) {
            features = new FeaturesEList(IFeature.class, this, IArchimatePackage.ARCHIMATE_CONCEPT__FEATURES);
        }
        return (IFeaturesEList)features;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void setName(String newName) {
        String oldName = name;
        name = newName;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, IArchimatePackage.ARCHIMATE_CONCEPT__NAME, oldName, name));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public String getDocumentation() {
        return documentation;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void setDocumentation(String newDocumentation) {
        String oldDocumentation = documentation;
        documentation = newDocumentation;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, IArchimatePackage.ARCHIMATE_CONCEPT__DOCUMENTATION, oldDocumentation, documentation));
    }

    
    
    
    @Override
    public String getArchestry_OrderLabel() {
        return archestry_OrderLabel;
    }    
    @Override
    public void setArchestry_OrderLabel(String newArchestry_OrderLabel) {
        String oldArchestry_OrderLabel = archestry_OrderLabel;
        archestry_OrderLabel = newArchestry_OrderLabel;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_ORDER_LABEL, oldArchestry_OrderLabel, archestry_OrderLabel));
    }    
    @Override
    public String getArchestry_OrderFontColor() {
        return archestry_OrderFontColor;
    }    
    @Override
    public void setArchestry_OrderFontColor(String newArchestry_OrderFontColor) {
        String oldArchestry_OrderFontColor = archestry_OrderFontColor;
        archestry_OrderFontColor = newArchestry_OrderFontColor;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_ORDER_FONTCOLOR, oldArchestry_OrderFontColor, archestry_OrderFontColor));
    }  
    @Override
    public String getArchestry_OrderAreaColor() {
        return archestry_OrderAreaColor;
    }    
    @Override
    public void setArchestry_OrderAreaColor(String newArchestry_OrderAreaColor) {
        String oldArchestry_OrderAreaColor = archestry_OrderAreaColor;
        archestry_OrderAreaColor = newArchestry_OrderAreaColor;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_ORDER_AREACOLOR, oldArchestry_OrderAreaColor, archestry_OrderAreaColor));
    }  
    
    
    @Override
    public String getArchestry_FamilyLabel() {
        return archestry_FamilyLabel;
    }    
    @Override
    public void setArchestry_FamilyLabel(String newArchestry_FamilyLabel) {
        String oldArchestry_FamilyLabel = archestry_FamilyLabel;
        archestry_FamilyLabel = newArchestry_FamilyLabel;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_FAMILY_LABEL, oldArchestry_FamilyLabel, archestry_FamilyLabel));
    }    
    @Override
    public String getArchestry_FamilyFontColor() {
        return archestry_FamilyFontColor;
    }    
    @Override
    public void setArchestry_FamilyFontColor(String newArchestry_FamilyFontColor) {
        String oldArchestry_FamilyFontColor = archestry_FamilyFontColor;
        archestry_FamilyFontColor = newArchestry_FamilyFontColor;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_FAMILY_FONTCOLOR, oldArchestry_FamilyFontColor, archestry_FamilyFontColor));
    }  
    @Override
    public String getArchestry_FamilyAreaColor() {
        return archestry_FamilyAreaColor;
    }    
    @Override
    public void setArchestry_FamilyAreaColor(String newArchestry_FamilyAreaColor) {
        String oldArchestry_FamilyAreaColor = archestry_FamilyAreaColor;
        archestry_FamilyAreaColor = newArchestry_FamilyAreaColor;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_FAMILY_AREACOLOR, oldArchestry_FamilyAreaColor, archestry_FamilyAreaColor));
    }  
    
    @Override
    public String getArchestry_GenusLabel() {
        return archestry_GenusLabel;
    }    
    @Override
    public void setArchestry_GenusLabel(String newArchestry_GenusLabel) {
        String oldArchestry_GenusLabel = archestry_GenusLabel;
        archestry_GenusLabel = newArchestry_GenusLabel;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_GENUS_LABEL, oldArchestry_GenusLabel, archestry_GenusLabel));
    }    
    @Override
    public String getArchestry_GenusFontColor() {
        return archestry_GenusFontColor;
    }    
    @Override
    public void setArchestry_GenusFontColor(String newArchestry_GenusFontColor) {
        String oldArchestry_GenusFontColor = archestry_GenusFontColor;
        archestry_GenusFontColor = newArchestry_GenusFontColor;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_GENUS_FONTCOLOR, oldArchestry_GenusFontColor, archestry_GenusFontColor));
    }  
    @Override
    public String getArchestry_GenusAreaColor() {
        return archestry_GenusAreaColor;
    }    
    @Override
    public void setArchestry_GenusAreaColor(String newArchestry_GenusAreaColor) {
        String oldArchestry_GenusAreaColor = archestry_GenusAreaColor;
        archestry_GenusAreaColor = newArchestry_GenusAreaColor;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_GENUS_AREACOLOR, oldArchestry_GenusAreaColor, archestry_GenusAreaColor));
    }  
    
    @Override
    public String getArchestry_ExtUUIDLabel() {
        return archestry_ExtUUIDLabel;
    }    
    @Override
    public void setArchestry_ExtUUIDLabel(String newArchestry_ExtUUIDLabel) {
        String oldArchestry_ExtUUIDLabel = archestry_ExtUUIDLabel;
        archestry_ExtUUIDLabel = newArchestry_ExtUUIDLabel;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_EXTUUID_LABEL, oldArchestry_ExtUUIDLabel, archestry_ExtUUIDLabel));
    }    
    @Override
    public String getArchestry_ExtUUIDFontColor() {
        return archestry_ExtUUIDFontColor;
    }    
    @Override
    public void setArchestry_ExtUUIDFontColor(String newArchestry_ExtUUIDFontColor) {
        String oldArchestry_ExtUUIDFontColor = archestry_ExtUUIDFontColor;
        archestry_ExtUUIDFontColor = newArchestry_ExtUUIDFontColor;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_EXTUUID_FONTCOLOR, oldArchestry_ExtUUIDFontColor, archestry_ExtUUIDFontColor));
    }  
    @Override
    public String getArchestry_ExtUUIDAreaColor() {
        return archestry_ExtUUIDAreaColor;
    }    
    @Override
    public void setArchestry_ExtUUIDAreaColor(String newArchestry_ExtUUIDAreaColor) {
        String oldArchestry_ExtUUIDAreaColor = archestry_ExtUUIDAreaColor;
        archestry_ExtUUIDAreaColor = newArchestry_ExtUUIDAreaColor;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_EXTUUID_AREACOLOR, oldArchestry_ExtUUIDAreaColor, archestry_ExtUUIDAreaColor));
    }  
    
    @Override
    public String getArchestry_RefIDLabel() {
        return archestry_RefIDLabel;
    }    
    @Override
    public void setArchestry_RefIDLabel(String newArchestry_RefIDLabel) {
        String oldArchestry_RefIDLabel = archestry_RefIDLabel;
        archestry_RefIDLabel = newArchestry_RefIDLabel;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_REFID_LABEL, oldArchestry_RefIDLabel, archestry_RefIDLabel));
    }    
    @Override
    public String getArchestry_RefIDFontColor() {
        return archestry_RefIDFontColor;
    }    
    @Override
    public void setArchestry_RefIDFontColor(String newArchestry_RefIDFontColor) {
        String oldArchestry_RefIDFontColor = archestry_RefIDFontColor;
        archestry_RefIDFontColor = newArchestry_RefIDFontColor;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_REFID_FONTCOLOR, oldArchestry_RefIDFontColor, archestry_RefIDFontColor));
    }  
    @Override
    public String getArchestry_RefIDAreaColor() {
        return archestry_RefIDAreaColor;
    }    
    @Override
    public void setArchestry_RefIDAreaColor(String newArchestry_RefIDAreaColor) {
        String oldArchestry_RefIDAreaColor = archestry_RefIDAreaColor;
        archestry_RefIDAreaColor = newArchestry_RefIDAreaColor;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_REFID_AREACOLOR, oldArchestry_RefIDAreaColor, archestry_RefIDAreaColor));
    }  
    
    @Override
    public String getArchestry_StatusLabel() {
        return archestry_StatusLabel;
    }    
    @Override
    public void setArchestry_StatusLabel(String newArchestry_StatusLabel) {
        String oldArchestry_StatusLabel = archestry_StatusLabel;
        archestry_StatusLabel = newArchestry_StatusLabel;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_STATUS_LABEL, oldArchestry_StatusLabel, archestry_StatusLabel));
    }    
    @Override
    public String getArchestry_StatusFontColor() {
        return archestry_StatusFontColor;
    }    
    @Override
    public void setArchestry_StatusFontColor(String newArchestry_StatusFontColor) {
        String oldArchestry_StatusFontColor = archestry_StatusFontColor;
        archestry_StatusFontColor = newArchestry_StatusFontColor;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_STATUS_FONTCOLOR, oldArchestry_StatusFontColor, archestry_StatusFontColor));
    }  
    @Override
    public String getArchestry_StatusAreaColor() {
        return archestry_StatusAreaColor;
    }    
    @Override
    public void setArchestry_StatusAreaColor(String newArchestry_StatusAreaColor) {
        String oldArchestry_StatusAreaColor = archestry_StatusAreaColor;
        archestry_StatusAreaColor = newArchestry_StatusAreaColor;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_STATUS_AREACOLOR, oldArchestry_StatusAreaColor, archestry_StatusAreaColor));
    }  
   
   
    @Override
    public String getArchestry_StageLabel() {
        return archestry_StageLabel;
    }    
    @Override
    public void setArchestry_StageLabel(String newArchestry_StageLabel) {
        String oldArchestry_StageLabel = archestry_StageLabel;
        archestry_StageLabel = newArchestry_StageLabel;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_STAGE_LABEL, oldArchestry_StageLabel, archestry_StageLabel));
    }    
    @Override
    public String getArchestry_StageFontColor() {
        return archestry_StageFontColor;
    }    
    @Override
    public void setArchestry_StageFontColor(String newArchestry_StageFontColor) {
        String oldArchestry_StageFontColor = archestry_StageFontColor;
        archestry_StageFontColor = newArchestry_StageFontColor;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_STAGE_FONTCOLOR, oldArchestry_StageFontColor, archestry_StageFontColor));
    }  
    @Override
    public String getArchestry_StageAreaColor() {
        return archestry_StageAreaColor;
    }    
    @Override
    public void setArchestry_StageAreaColor(String newArchestry_StageAreaColor) {
        String oldArchestry_StageAreaColor = archestry_StageAreaColor;
        archestry_StageAreaColor = newArchestry_StageAreaColor;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_STAGE_AREACOLOR, oldArchestry_StageAreaColor, archestry_StageAreaColor));
    }  
    
    
    @Override
    public String getArchestry_CommWithLabel() {
        return archestry_CommWithLabel;
    }    
    @Override
    public void setArchestry_CommWithLabel(String newArchestry_CommWithLabel) {
        String oldArchestry_CommWithLabel = archestry_CommWithLabel;
        archestry_CommWithLabel = newArchestry_CommWithLabel;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_COMMWITH_LABEL, oldArchestry_CommWithLabel, archestry_CommWithLabel));
    }    
    @Override
    public String getArchestry_CommWithFontColor() {
        return archestry_CommWithFontColor;
    }    
    @Override
    public void setArchestry_CommWithFontColor(String newArchestry_CommWithFontColor) {
        String oldArchestry_CommWithFontColor = archestry_CommWithFontColor;
        archestry_CommWithFontColor = newArchestry_CommWithFontColor;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_COMMWITH_FONTCOLOR, oldArchestry_CommWithFontColor, archestry_CommWithFontColor));
    }  
    @Override
    public String getArchestry_CommWithAreaColor() {
        return archestry_CommWithAreaColor;
    }    
    @Override
    public void setArchestry_CommWithAreaColor(String newArchestry_CommWithAreaColor) {
        String oldArchestry_CommWithAreaColor = archestry_CommWithAreaColor;
        archestry_CommWithAreaColor = newArchestry_CommWithAreaColor;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_COMMWITH_AREACOLOR, oldArchestry_CommWithAreaColor, archestry_CommWithAreaColor));
    }  
    
    @Override
    public String getArchestry_VersionLabel() {
        return archestry_VersionLabel;
    }    
    @Override
    public void setArchestry_VersionLabel(String newArchestry_VersionLabel) {
        String oldArchestry_VersionLabel = archestry_VersionLabel;
        archestry_VersionLabel = newArchestry_VersionLabel;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_VERSION_LABEL, oldArchestry_VersionLabel, archestry_VersionLabel));
    }    
    @Override
    public String getArchestry_VersionFontColor() {
        return archestry_VersionFontColor;
    }    
    @Override
    public void setArchestry_VersionFontColor(String newArchestry_VersionFontColor) {
        String oldArchestry_VersionFontColor = archestry_VersionFontColor;
        archestry_VersionFontColor = newArchestry_VersionFontColor;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_VERSION_FONTCOLOR, oldArchestry_VersionFontColor, archestry_VersionFontColor));
    }  
    @Override
    public String getArchestry_VersionAreaColor() {
        return archestry_VersionAreaColor;
    }    
    @Override
    public void setArchestry_VersionAreaColor(String newArchestry_VersionAreaColor) {
        String oldArchestry_VersionAreaColor = archestry_VersionAreaColor;
        archestry_VersionAreaColor = newArchestry_VersionAreaColor;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_VERSION_AREACOLOR, oldArchestry_VersionAreaColor, archestry_VersionAreaColor));
    }  
    
    @Override
    public String getArchestry_DateLabel() {
        return archestry_DateLabel;
    }    
    @Override
    public void setArchestry_DateLabel(String newArchestry_DateLabel) {
        String oldArchestry_DateLabel = archestry_DateLabel;
        archestry_DateLabel = newArchestry_DateLabel;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_DATE_LABEL, oldArchestry_DateLabel, archestry_DateLabel));
    }    
    @Override
    public String getArchestry_DateFontColor() {
        return archestry_DateFontColor;
    }    
    @Override
    public void setArchestry_DateFontColor(String newArchestry_DateFontColor) {
        String oldArchestry_DateFontColor = archestry_DateFontColor;
        archestry_DateFontColor = newArchestry_DateFontColor;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_DATE_FONTCOLOR, oldArchestry_DateFontColor, archestry_DateFontColor));
    }  
    @Override
    public String getArchestry_DateAreaColor() {
        return archestry_DateAreaColor;
    }    
    @Override
    public void setArchestry_DateAreaColor(String newArchestry_DateAreaColor) {
        String oldArchestry_DateAreaColor = archestry_DateAreaColor;
        archestry_DateAreaColor = newArchestry_DateAreaColor;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_DATE_AREACOLOR, oldArchestry_DateAreaColor, archestry_DateAreaColor));
    }  
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EList<IProperty> getProperties() {
        if (properties == null) {
            properties = new EObjectContainmentEList<IProperty>(IProperty.class, this, IArchimatePackage.ARCHIMATE_CONCEPT__PROPERTIES);
        }
        return properties;
    }

    @Override
    public EList<IArchimateRelationship> getSourceRelationships() {
        if(sourceRelationships == null) {
            sourceRelationships = new UniqueEList<IArchimateRelationship>();
        }
        return sourceRelationships;
    }
    
    @Override
    public EList<IArchimateRelationship> getTargetRelationships() {
        if(targetRelationships == null) {
            targetRelationships = new UniqueEList<IArchimateRelationship>();
        }
        return targetRelationships;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated NOT
     */
    @Override
    public EObject getCopy() {
        IArchimateConcept newObject = EcoreUtil.copy(this);
        newObject.setId(UUIDFactory.createID(newObject)); // need a new ID
        return newObject;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated NOT
     */
    @Override
    public IArchimateModel getArchimateModel() {
        if(eContainer() == null) {
            return null;
        }
        return ((IArchimateModelObject)eContainer()).getArchimateModel();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated NOT
     */
    @Override
    public Object getAdapter(Object adapter) {
        if(!fAdapterMap.containsKey(adapter) && eContainer() instanceof IAdapter) {
            return ((IAdapter)eContainer()).getAdapter(adapter);
        }
        
        return fAdapterMap.get(adapter);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated NOT
     */
    @Override
    public void setAdapter(Object adapter, Object object) {
        fAdapterMap.put(adapter, object);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case IArchimatePackage.ARCHIMATE_CONCEPT__FEATURES:
                return ((InternalEList<?>)getFeatures()).basicRemove(otherEnd, msgs);
            case IArchimatePackage.ARCHIMATE_CONCEPT__PROPERTIES:
                return ((InternalEList<?>)getProperties()).basicRemove(otherEnd, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case IArchimatePackage.ARCHIMATE_CONCEPT__NAME:
                return getName();
            case IArchimatePackage.ARCHIMATE_CONCEPT__ID:
                return getId();
            case IArchimatePackage.ARCHIMATE_CONCEPT__FEATURES:
                return getFeatures();
            case IArchimatePackage.ARCHIMATE_CONCEPT__DOCUMENTATION:
                return getDocumentation();
                
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_ORDER_LABEL:
                return getArchestry_OrderLabel();
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_ORDER_FONTCOLOR:
                return getArchestry_OrderFontColor();
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_ORDER_AREACOLOR:
                return getArchestry_OrderAreaColor();
                
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_FAMILY_LABEL:
                return getArchestry_FamilyLabel();
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_FAMILY_FONTCOLOR:
                return getArchestry_FamilyFontColor();
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_FAMILY_AREACOLOR:
                return getArchestry_FamilyAreaColor();
                
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_GENUS_LABEL:
                return getArchestry_GenusLabel();
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_GENUS_FONTCOLOR:
                return getArchestry_GenusFontColor();
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_GENUS_AREACOLOR:
                return getArchestry_GenusAreaColor();
                
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_EXTUUID_LABEL:
                return getArchestry_ExtUUIDLabel();
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_EXTUUID_FONTCOLOR:
                return getArchestry_ExtUUIDFontColor();
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_EXTUUID_AREACOLOR:
                return getArchestry_ExtUUIDAreaColor();
                
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_REFID_LABEL:
                return getArchestry_RefIDLabel();
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_REFID_FONTCOLOR:
                return getArchestry_RefIDFontColor();
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_REFID_AREACOLOR:
                return getArchestry_RefIDAreaColor();
                
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_STATUS_LABEL:
                return getArchestry_StatusLabel();
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_STATUS_FONTCOLOR:
                return getArchestry_StatusFontColor();
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_STATUS_AREACOLOR:
                return getArchestry_StatusAreaColor();
                
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_STAGE_LABEL:
                return getArchestry_StageLabel();
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_STAGE_FONTCOLOR:
                return getArchestry_StageFontColor();
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_STAGE_AREACOLOR:
                return getArchestry_StageAreaColor();
                
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_COMMWITH_LABEL:
                return getArchestry_CommWithLabel();
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_COMMWITH_FONTCOLOR:
                return getArchestry_CommWithFontColor();
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_COMMWITH_AREACOLOR:
                return getArchestry_CommWithAreaColor();
                
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_VERSION_LABEL:
                return getArchestry_VersionLabel();
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_VERSION_FONTCOLOR:
                return getArchestry_VersionFontColor();
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_VERSION_AREACOLOR:
                return getArchestry_VersionAreaColor();
                
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_DATE_LABEL:
                return getArchestry_DateLabel();
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_DATE_FONTCOLOR:
                return getArchestry_DateFontColor();
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_DATE_AREACOLOR:
                return getArchestry_DateAreaColor();
                
            case IArchimatePackage.ARCHIMATE_CONCEPT__PROPERTIES:
                return getProperties();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @SuppressWarnings("unchecked")
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case IArchimatePackage.ARCHIMATE_CONCEPT__NAME:
                setName((String)newValue);
                return;
            case IArchimatePackage.ARCHIMATE_CONCEPT__ID:
                setId((String)newValue);
                return;
            case IArchimatePackage.ARCHIMATE_CONCEPT__FEATURES:
                getFeatures().clear();
                getFeatures().addAll((Collection<? extends IFeature>)newValue);
                return;
            case IArchimatePackage.ARCHIMATE_CONCEPT__DOCUMENTATION:
                setDocumentation((String)newValue);
                return;
            
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_ORDER_LABEL:
                setArchestry_OrderLabel((String)newValue);
                return;
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_ORDER_FONTCOLOR:
                setArchestry_OrderFontColor((String)newValue);
                return;
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_ORDER_AREACOLOR:
                setArchestry_OrderAreaColor((String)newValue);
                return;
                
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_FAMILY_LABEL:
                setArchestry_FamilyLabel((String)newValue);
                return;
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_FAMILY_FONTCOLOR:
                setArchestry_FamilyFontColor((String)newValue);
                return;
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_FAMILY_AREACOLOR:
                setArchestry_FamilyAreaColor((String)newValue);
                return;
                
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_GENUS_LABEL:
                setArchestry_GenusLabel((String)newValue);
                return;
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_GENUS_FONTCOLOR:
                setArchestry_GenusFontColor((String)newValue);
                return;
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_GENUS_AREACOLOR:
                setArchestry_GenusAreaColor((String)newValue);
                return;
                
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_EXTUUID_LABEL:
                setArchestry_ExtUUIDLabel((String)newValue);
                return;
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_EXTUUID_FONTCOLOR:
                setArchestry_ExtUUIDFontColor((String)newValue);
                return;
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_EXTUUID_AREACOLOR:
                setArchestry_ExtUUIDAreaColor((String)newValue);
                return;
                
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_REFID_LABEL:
                setArchestry_RefIDLabel((String)newValue);
                return;
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_REFID_FONTCOLOR:
                setArchestry_RefIDFontColor((String)newValue);
                return;
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_REFID_AREACOLOR:
                setArchestry_RefIDAreaColor((String)newValue);
                return;
                
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_STATUS_LABEL:
                setArchestry_StatusLabel((String)newValue);
                return;
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_STATUS_FONTCOLOR:
                setArchestry_StatusFontColor((String)newValue);
                return;
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_STATUS_AREACOLOR:
                setArchestry_StatusAreaColor((String)newValue);
                return;
                
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_STAGE_LABEL:
                setArchestry_StageLabel((String)newValue);
                return;
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_STAGE_FONTCOLOR:
                setArchestry_StageFontColor((String)newValue);
                return;
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_STAGE_AREACOLOR:
                setArchestry_StageAreaColor((String)newValue);
                return;
                
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_COMMWITH_LABEL:
                setArchestry_CommWithLabel((String)newValue);
                return;
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_COMMWITH_FONTCOLOR:
                setArchestry_CommWithFontColor((String)newValue);
                return;
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_COMMWITH_AREACOLOR:
                setArchestry_CommWithAreaColor((String)newValue);
                return;
                
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_VERSION_LABEL:
                setArchestry_VersionLabel((String)newValue);
                return;
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_VERSION_FONTCOLOR:
                setArchestry_VersionFontColor((String)newValue);
                return;
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_VERSION_AREACOLOR:
                setArchestry_VersionAreaColor((String)newValue);
                return;
                
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_DATE_LABEL:
                setArchestry_DateLabel((String)newValue);
                return;
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_DATE_FONTCOLOR:
                setArchestry_DateFontColor((String)newValue);
                return;
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_DATE_AREACOLOR:
                setArchestry_DateAreaColor((String)newValue);
                return;
            
            
            case IArchimatePackage.ARCHIMATE_CONCEPT__PROPERTIES:
                getProperties().clear();
                getProperties().addAll((Collection<? extends IProperty>)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case IArchimatePackage.ARCHIMATE_CONCEPT__NAME:
                setName(NAME_EDEFAULT);
                return;
            case IArchimatePackage.ARCHIMATE_CONCEPT__ID:
                setId(ID_EDEFAULT);
                return;
            case IArchimatePackage.ARCHIMATE_CONCEPT__FEATURES:
                getFeatures().clear();
                return;
            case IArchimatePackage.ARCHIMATE_CONCEPT__DOCUMENTATION:
                setDocumentation(DOCUMENTATION_EDEFAULT);
                return;
                
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_ORDER_LABEL:
                setArchestry_OrderLabel(ARCHESTRY_ORDER_LABEL_EDEFAULT);
                return;
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_ORDER_FONTCOLOR:
                setArchestry_OrderFontColor(ARCHESTRY_ORDER_FONTCOLOR_EDEFAULT);
                return;
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_ORDER_AREACOLOR:
                setArchestry_OrderAreaColor(ARCHESTRY_ORDER_AREACOLOR_EDEFAULT);
                return;
                
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_FAMILY_LABEL:
                setArchestry_FamilyLabel(ARCHESTRY_FAMILY_LABEL_EDEFAULT);
                return;
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_FAMILY_FONTCOLOR:
                setArchestry_FamilyFontColor(ARCHESTRY_FAMILY_FONTCOLOR_EDEFAULT);
                return;
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_FAMILY_AREACOLOR:
                setArchestry_FamilyAreaColor(ARCHESTRY_FAMILY_AREACOLOR_EDEFAULT);
                return;
                
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_GENUS_LABEL:
                setArchestry_GenusLabel(ARCHESTRY_GENUS_LABEL_EDEFAULT);
                return;
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_GENUS_FONTCOLOR:
                setArchestry_GenusFontColor(ARCHESTRY_GENUS_FONTCOLOR_EDEFAULT);
                return;
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_GENUS_AREACOLOR:
                setArchestry_GenusAreaColor(ARCHESTRY_GENUS_AREACOLOR_EDEFAULT);
                return;

            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_EXTUUID_LABEL:
                setArchestry_ExtUUIDLabel(ARCHESTRY_EXTUUID_LABEL_EDEFAULT);
                return;
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_EXTUUID_FONTCOLOR:
                setArchestry_ExtUUIDFontColor(ARCHESTRY_EXTUUID_FONTCOLOR_EDEFAULT);
                return;
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_EXTUUID_AREACOLOR:
                setArchestry_ExtUUIDAreaColor(ARCHESTRY_EXTUUID_AREACOLOR_EDEFAULT);
                return;

            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_REFID_LABEL:
                setArchestry_RefIDLabel(ARCHESTRY_REFID_LABEL_EDEFAULT);
                return;
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_REFID_FONTCOLOR:
                setArchestry_RefIDFontColor(ARCHESTRY_REFID_FONTCOLOR_EDEFAULT);
                return;
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_REFID_AREACOLOR:
                setArchestry_RefIDAreaColor(ARCHESTRY_REFID_AREACOLOR_EDEFAULT);
                return;
                
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_STATUS_LABEL:
                setArchestry_StatusLabel(ARCHESTRY_STATUS_LABEL_EDEFAULT);
                return;
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_STATUS_FONTCOLOR:
                setArchestry_StatusFontColor(ARCHESTRY_STATUS_FONTCOLOR_EDEFAULT);
                return;
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_STATUS_AREACOLOR:
                setArchestry_StatusAreaColor(ARCHESTRY_STATUS_AREACOLOR_EDEFAULT);
                return;
                
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_STAGE_LABEL:
                setArchestry_StageLabel(ARCHESTRY_STAGE_LABEL_EDEFAULT);
                return;
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_STAGE_FONTCOLOR:
                setArchestry_StageFontColor(ARCHESTRY_STAGE_FONTCOLOR_EDEFAULT);
                return;
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_STAGE_AREACOLOR:
                setArchestry_StageAreaColor(ARCHESTRY_STAGE_AREACOLOR_EDEFAULT);
                return;
                
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_COMMWITH_LABEL:
                setArchestry_CommWithLabel(ARCHESTRY_COMMWITH_LABEL_EDEFAULT);
                return;
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_COMMWITH_FONTCOLOR:
                setArchestry_CommWithFontColor(ARCHESTRY_COMMWITH_FONTCOLOR_EDEFAULT);
                return;
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_COMMWITH_AREACOLOR:
                setArchestry_CommWithAreaColor(ARCHESTRY_COMMWITH_AREACOLOR_EDEFAULT);
                return;
                
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_VERSION_LABEL:
                setArchestry_VersionLabel(ARCHESTRY_VERSION_LABEL_EDEFAULT);
                return;
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_VERSION_FONTCOLOR:
                setArchestry_VersionFontColor(ARCHESTRY_VERSION_FONTCOLOR_EDEFAULT);
                return;
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_VERSION_AREACOLOR:
                setArchestry_VersionAreaColor(ARCHESTRY_VERSION_AREACOLOR_EDEFAULT);
                return;
                
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_DATE_LABEL:
                setArchestry_DateLabel(ARCHESTRY_DATE_LABEL_EDEFAULT);
                return;
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_DATE_FONTCOLOR:
                setArchestry_DateFontColor(ARCHESTRY_DATE_FONTCOLOR_EDEFAULT);
                return;
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_DATE_AREACOLOR:
                setArchestry_DateAreaColor(ARCHESTRY_DATE_AREACOLOR_EDEFAULT);
                return;
                
            case IArchimatePackage.ARCHIMATE_CONCEPT__PROPERTIES:
                getProperties().clear();
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case IArchimatePackage.ARCHIMATE_CONCEPT__NAME:
                return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
            case IArchimatePackage.ARCHIMATE_CONCEPT__ID:
                return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
            case IArchimatePackage.ARCHIMATE_CONCEPT__FEATURES:
                return features != null && !features.isEmpty();
            case IArchimatePackage.ARCHIMATE_CONCEPT__DOCUMENTATION:
                return DOCUMENTATION_EDEFAULT == null ? documentation != null : !DOCUMENTATION_EDEFAULT.equals(documentation);
                
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_ORDER_LABEL:
                return ARCHESTRY_ORDER_LABEL_EDEFAULT == null ? archestry_OrderLabel != null : !ARCHESTRY_ORDER_LABEL_EDEFAULT.equals(archestry_OrderLabel);
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_ORDER_FONTCOLOR:
                return ARCHESTRY_ORDER_FONTCOLOR_EDEFAULT == null ? archestry_OrderFontColor != null : !ARCHESTRY_ORDER_FONTCOLOR_EDEFAULT.equals(archestry_OrderFontColor);
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_ORDER_AREACOLOR:
                return ARCHESTRY_ORDER_AREACOLOR_EDEFAULT == null ? archestry_OrderAreaColor != null : !ARCHESTRY_ORDER_AREACOLOR_EDEFAULT.equals(archestry_OrderAreaColor);
                
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_FAMILY_LABEL:
                return ARCHESTRY_FAMILY_LABEL_EDEFAULT == null ? archestry_FamilyLabel != null : !ARCHESTRY_FAMILY_LABEL_EDEFAULT.equals(archestry_FamilyLabel);
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_FAMILY_FONTCOLOR:
                return ARCHESTRY_FAMILY_FONTCOLOR_EDEFAULT == null ? archestry_FamilyFontColor != null : !ARCHESTRY_FAMILY_FONTCOLOR_EDEFAULT.equals(archestry_FamilyFontColor);
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_FAMILY_AREACOLOR:
                return ARCHESTRY_FAMILY_AREACOLOR_EDEFAULT == null ? archestry_FamilyAreaColor != null : !ARCHESTRY_FAMILY_AREACOLOR_EDEFAULT.equals(archestry_FamilyAreaColor);
                
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_GENUS_LABEL:
                return ARCHESTRY_GENUS_LABEL_EDEFAULT == null ? archestry_GenusLabel != null : !ARCHESTRY_GENUS_LABEL_EDEFAULT.equals(archestry_GenusLabel);
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_GENUS_FONTCOLOR:
                return ARCHESTRY_GENUS_FONTCOLOR_EDEFAULT == null ? archestry_GenusFontColor != null : !ARCHESTRY_GENUS_FONTCOLOR_EDEFAULT.equals(archestry_GenusFontColor);
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_GENUS_AREACOLOR:
                return ARCHESTRY_GENUS_AREACOLOR_EDEFAULT == null ? archestry_GenusAreaColor != null : !ARCHESTRY_GENUS_AREACOLOR_EDEFAULT.equals(archestry_GenusAreaColor);
                
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_EXTUUID_LABEL:
                return ARCHESTRY_EXTUUID_LABEL_EDEFAULT == null ? archestry_ExtUUIDLabel != null : !ARCHESTRY_EXTUUID_LABEL_EDEFAULT.equals(archestry_ExtUUIDLabel);
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_EXTUUID_FONTCOLOR:
                return ARCHESTRY_EXTUUID_FONTCOLOR_EDEFAULT == null ? archestry_ExtUUIDFontColor != null : !ARCHESTRY_EXTUUID_FONTCOLOR_EDEFAULT.equals(archestry_ExtUUIDFontColor);
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_EXTUUID_AREACOLOR:
                return ARCHESTRY_EXTUUID_AREACOLOR_EDEFAULT == null ? archestry_ExtUUIDAreaColor != null : !ARCHESTRY_EXTUUID_AREACOLOR_EDEFAULT.equals(archestry_ExtUUIDAreaColor);
                
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_REFID_LABEL:
                return ARCHESTRY_REFID_LABEL_EDEFAULT == null ? archestry_RefIDLabel != null : !ARCHESTRY_REFID_LABEL_EDEFAULT.equals(archestry_RefIDLabel);
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_REFID_FONTCOLOR:
                return ARCHESTRY_REFID_FONTCOLOR_EDEFAULT == null ? archestry_RefIDFontColor != null : !ARCHESTRY_REFID_FONTCOLOR_EDEFAULT.equals(archestry_RefIDFontColor);
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_REFID_AREACOLOR:
                return ARCHESTRY_REFID_AREACOLOR_EDEFAULT == null ? archestry_RefIDAreaColor != null : !ARCHESTRY_REFID_AREACOLOR_EDEFAULT.equals(archestry_RefIDAreaColor);
                
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_STATUS_LABEL:
                return ARCHESTRY_STATUS_LABEL_EDEFAULT == null ? archestry_StatusLabel != null : !ARCHESTRY_STATUS_LABEL_EDEFAULT.equals(archestry_StatusLabel);
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_STATUS_FONTCOLOR:
                return ARCHESTRY_STATUS_FONTCOLOR_EDEFAULT == null ? archestry_StatusFontColor != null : !ARCHESTRY_STATUS_FONTCOLOR_EDEFAULT.equals(archestry_StatusFontColor);
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_STATUS_AREACOLOR:
                return ARCHESTRY_STATUS_AREACOLOR_EDEFAULT == null ? archestry_StatusAreaColor != null : !ARCHESTRY_STATUS_AREACOLOR_EDEFAULT.equals(archestry_StatusAreaColor);
                
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_STAGE_LABEL:
                return ARCHESTRY_STAGE_LABEL_EDEFAULT == null ? archestry_StageLabel != null : !ARCHESTRY_STAGE_LABEL_EDEFAULT.equals(archestry_StageLabel);
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_STAGE_FONTCOLOR:
                return ARCHESTRY_STAGE_FONTCOLOR_EDEFAULT == null ? archestry_StageFontColor != null : !ARCHESTRY_STAGE_FONTCOLOR_EDEFAULT.equals(archestry_StageFontColor);
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_STAGE_AREACOLOR:
                return ARCHESTRY_STAGE_AREACOLOR_EDEFAULT == null ? archestry_StageAreaColor != null : !ARCHESTRY_STAGE_AREACOLOR_EDEFAULT.equals(archestry_StageAreaColor);
                
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_COMMWITH_LABEL:
                return ARCHESTRY_COMMWITH_LABEL_EDEFAULT == null ? archestry_CommWithLabel != null : !ARCHESTRY_COMMWITH_LABEL_EDEFAULT.equals(archestry_CommWithLabel);
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_COMMWITH_FONTCOLOR:
                return ARCHESTRY_COMMWITH_FONTCOLOR_EDEFAULT == null ? archestry_CommWithFontColor != null : !ARCHESTRY_COMMWITH_FONTCOLOR_EDEFAULT.equals(archestry_CommWithFontColor);
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_COMMWITH_AREACOLOR:
                return ARCHESTRY_COMMWITH_AREACOLOR_EDEFAULT == null ? archestry_CommWithAreaColor != null : !ARCHESTRY_COMMWITH_AREACOLOR_EDEFAULT.equals(archestry_CommWithAreaColor);
                
                
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_VERSION_LABEL:
                return ARCHESTRY_VERSION_LABEL_EDEFAULT == null ? archestry_VersionLabel != null : !ARCHESTRY_VERSION_LABEL_EDEFAULT.equals(archestry_VersionLabel);
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_VERSION_FONTCOLOR:
                return ARCHESTRY_VERSION_FONTCOLOR_EDEFAULT == null ? archestry_VersionFontColor != null : !ARCHESTRY_VERSION_FONTCOLOR_EDEFAULT.equals(archestry_VersionFontColor);
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_VERSION_AREACOLOR:
                return ARCHESTRY_VERSION_AREACOLOR_EDEFAULT == null ? archestry_VersionAreaColor != null : !ARCHESTRY_VERSION_AREACOLOR_EDEFAULT.equals(archestry_VersionAreaColor);
                
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_DATE_LABEL:
                return ARCHESTRY_DATE_LABEL_EDEFAULT == null ? archestry_DateLabel != null : !ARCHESTRY_DATE_LABEL_EDEFAULT.equals(archestry_DateLabel);
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_DATE_FONTCOLOR:
                return ARCHESTRY_DATE_FONTCOLOR_EDEFAULT == null ? archestry_DateFontColor != null : !ARCHESTRY_DATE_FONTCOLOR_EDEFAULT.equals(archestry_DateFontColor);
            case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_DATE_AREACOLOR:
                return ARCHESTRY_DATE_AREACOLOR_EDEFAULT == null ? archestry_DateAreaColor != null : !ARCHESTRY_DATE_AREACOLOR_EDEFAULT.equals(archestry_DateAreaColor);
                

            case IArchimatePackage.ARCHIMATE_CONCEPT__PROPERTIES:
                return properties != null && !properties.isEmpty();
        }

        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
        if (baseClass == INameable.class) {
            switch (derivedFeatureID) {
                case IArchimatePackage.ARCHIMATE_CONCEPT__NAME: return IArchimatePackage.NAMEABLE__NAME;
                default: return -1;
            }
        }
        if (baseClass == IIdentifier.class) {
            switch (derivedFeatureID) {
                case IArchimatePackage.ARCHIMATE_CONCEPT__ID: return IArchimatePackage.IDENTIFIER__ID;
                default: return -1;
            }
        }
        if (baseClass == IFeatures.class) {
            switch (derivedFeatureID) {
                case IArchimatePackage.ARCHIMATE_CONCEPT__FEATURES: return IArchimatePackage.FEATURES__FEATURES;
                default: return -1;
            }
        }
        if (baseClass == ICloneable.class) {
            switch (derivedFeatureID) {
                default: return -1;
            }
        }
        if (baseClass == IDocumentable.class) {
            switch (derivedFeatureID) {
                case IArchimatePackage.ARCHIMATE_CONCEPT__DOCUMENTATION: return IArchimatePackage.DOCUMENTABLE__DOCUMENTATION;
                default: return -1;
            }
        }
        if (baseClass == IArchestry.class) {
            switch (derivedFeatureID) {
                case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_ORDER_LABEL: return IArchimatePackage.ARCHESTRY__ORDER_LABEL;
                case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_ORDER_FONTCOLOR: return IArchimatePackage.ARCHESTRY__ORDER_FONTCOLOR;
                case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_ORDER_AREACOLOR: return IArchimatePackage.ARCHESTRY__ORDER_AREACOLOR;
                
                case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_FAMILY_LABEL: return IArchimatePackage.ARCHESTRY__FAMILY_LABEL;
                case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_FAMILY_FONTCOLOR: return IArchimatePackage.ARCHESTRY__FAMILY_FONTCOLOR;
                case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_FAMILY_AREACOLOR: return IArchimatePackage.ARCHESTRY__FAMILY_AREACOLOR;
                
                case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_GENUS_LABEL: return IArchimatePackage.ARCHESTRY__GENUS_LABEL;
                case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_GENUS_FONTCOLOR: return IArchimatePackage.ARCHESTRY__GENUS_FONTCOLOR;
                case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_GENUS_AREACOLOR: return IArchimatePackage.ARCHESTRY__GENUS_AREACOLOR;
                
                case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_EXTUUID_LABEL: return IArchimatePackage.ARCHESTRY__EXTUUID_LABEL;
                case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_EXTUUID_FONTCOLOR: return IArchimatePackage.ARCHESTRY__EXTUUID_FONTCOLOR;
                case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_EXTUUID_AREACOLOR: return IArchimatePackage.ARCHESTRY__EXTUUID_AREACOLOR;
                
                case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_REFID_LABEL: return IArchimatePackage.ARCHESTRY__REFID_LABEL;
                case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_REFID_FONTCOLOR: return IArchimatePackage.ARCHESTRY__REFID_FONTCOLOR;
                case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_REFID_AREACOLOR: return IArchimatePackage.ARCHESTRY__REFID_AREACOLOR;
                
                case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_STATUS_LABEL: return IArchimatePackage.ARCHESTRY__STATUS_LABEL;
                case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_STATUS_FONTCOLOR: return IArchimatePackage.ARCHESTRY__STATUS_FONTCOLOR;
                case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_STATUS_AREACOLOR: return IArchimatePackage.ARCHESTRY__STATUS_AREACOLOR;
                
                case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_STAGE_LABEL: return IArchimatePackage.ARCHESTRY__STAGE_LABEL;
                case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_STAGE_FONTCOLOR: return IArchimatePackage.ARCHESTRY__STAGE_FONTCOLOR;
                case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_STAGE_AREACOLOR: return IArchimatePackage.ARCHESTRY__STAGE_AREACOLOR;
                
                case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_COMMWITH_LABEL: return IArchimatePackage.ARCHESTRY__COMMWITH_LABEL;
                case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_COMMWITH_FONTCOLOR: return IArchimatePackage.ARCHESTRY__COMMWITH_FONTCOLOR;
                case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_COMMWITH_AREACOLOR: return IArchimatePackage.ARCHESTRY__COMMWITH_AREACOLOR;
                
                case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_VERSION_LABEL: return IArchimatePackage.ARCHESTRY__VERSION_LABEL;
                case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_VERSION_FONTCOLOR: return IArchimatePackage.ARCHESTRY__VERSION_FONTCOLOR;
                case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_VERSION_AREACOLOR: return IArchimatePackage.ARCHESTRY__VERSION_AREACOLOR;
                
                case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_DATE_LABEL: return IArchimatePackage.ARCHESTRY__DATE_LABEL;
                case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_DATE_FONTCOLOR: return IArchimatePackage.ARCHESTRY__DATE_FONTCOLOR;
                case IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_DATE_AREACOLOR: return IArchimatePackage.ARCHESTRY__DATE_AREACOLOR;
                
                
                default: return -1;
            }
        }        
        if (baseClass == IProperties.class) {
            switch (derivedFeatureID) {
                case IArchimatePackage.ARCHIMATE_CONCEPT__PROPERTIES: return IArchimatePackage.PROPERTIES__PROPERTIES;
                default: return -1;
            }
        }
        return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
        if (baseClass == INameable.class) {
            switch (baseFeatureID) {
                case IArchimatePackage.NAMEABLE__NAME: return IArchimatePackage.ARCHIMATE_CONCEPT__NAME;
                default: return -1;
            }
        }
        if (baseClass == IIdentifier.class) {
            switch (baseFeatureID) {
                case IArchimatePackage.IDENTIFIER__ID: return IArchimatePackage.ARCHIMATE_CONCEPT__ID;
                default: return -1;
            }
        }
        if (baseClass == IFeatures.class) {
            switch (baseFeatureID) {
                case IArchimatePackage.FEATURES__FEATURES: return IArchimatePackage.ARCHIMATE_CONCEPT__FEATURES;
                default: return -1;
            }
        }
        if (baseClass == ICloneable.class) {
            switch (baseFeatureID) {
                default: return -1;
            }
        }
        if (baseClass == IDocumentable.class) {
            switch (baseFeatureID) {
                case IArchimatePackage.DOCUMENTABLE__DOCUMENTATION: return IArchimatePackage.ARCHIMATE_CONCEPT__DOCUMENTATION;
                default: return -1;
            }
        }
        
        if (baseClass == IArchestry.class) {
            switch (baseFeatureID) {
                case IArchimatePackage.ARCHESTRY__ORDER_LABEL: return IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_ORDER_LABEL;
                case IArchimatePackage.ARCHESTRY__ORDER_FONTCOLOR: return IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_ORDER_FONTCOLOR;
                case IArchimatePackage.ARCHESTRY__ORDER_AREACOLOR: return IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_ORDER_AREACOLOR;
                
                case IArchimatePackage.ARCHESTRY__FAMILY_LABEL: return IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_FAMILY_LABEL;
                case IArchimatePackage.ARCHESTRY__FAMILY_FONTCOLOR: return IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_FAMILY_FONTCOLOR;
                case IArchimatePackage.ARCHESTRY__FAMILY_AREACOLOR: return IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_FAMILY_AREACOLOR;
                
                case IArchimatePackage.ARCHESTRY__GENUS_LABEL: return IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_GENUS_LABEL;
                case IArchimatePackage.ARCHESTRY__GENUS_FONTCOLOR: return IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_GENUS_FONTCOLOR;
                case IArchimatePackage.ARCHESTRY__GENUS_AREACOLOR: return IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_GENUS_AREACOLOR;
                
                case IArchimatePackage.ARCHESTRY__EXTUUID_LABEL: return IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_EXTUUID_LABEL;
                case IArchimatePackage.ARCHESTRY__EXTUUID_FONTCOLOR: return IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_EXTUUID_FONTCOLOR;
                case IArchimatePackage.ARCHESTRY__EXTUUID_AREACOLOR: return IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_EXTUUID_AREACOLOR;
                
                case IArchimatePackage.ARCHESTRY__REFID_LABEL: return IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_REFID_LABEL;
                case IArchimatePackage.ARCHESTRY__REFID_FONTCOLOR: return IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_REFID_FONTCOLOR;
                case IArchimatePackage.ARCHESTRY__REFID_AREACOLOR: return IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_REFID_AREACOLOR;
                
                case IArchimatePackage.ARCHESTRY__STATUS_LABEL: return IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_STATUS_LABEL;
                case IArchimatePackage.ARCHESTRY__STATUS_FONTCOLOR: return IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_STATUS_FONTCOLOR;
                case IArchimatePackage.ARCHESTRY__STATUS_AREACOLOR: return IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_STATUS_AREACOLOR;
                
                case IArchimatePackage.ARCHESTRY__STAGE_LABEL: return IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_STAGE_LABEL;
                case IArchimatePackage.ARCHESTRY__STAGE_FONTCOLOR: return IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_STAGE_FONTCOLOR;
                case IArchimatePackage.ARCHESTRY__STAGE_AREACOLOR: return IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_STAGE_AREACOLOR;
                
                case IArchimatePackage.ARCHESTRY__COMMWITH_LABEL: return IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_COMMWITH_LABEL;
                case IArchimatePackage.ARCHESTRY__COMMWITH_FONTCOLOR: return IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_COMMWITH_FONTCOLOR;
                case IArchimatePackage.ARCHESTRY__COMMWITH_AREACOLOR: return IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_COMMWITH_AREACOLOR;
                
                case IArchimatePackage.ARCHESTRY__VERSION_LABEL: return IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_VERSION_LABEL;
                case IArchimatePackage.ARCHESTRY__VERSION_FONTCOLOR: return IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_VERSION_FONTCOLOR;
                case IArchimatePackage.ARCHESTRY__VERSION_AREACOLOR: return IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_VERSION_AREACOLOR;
                
                case IArchimatePackage.ARCHESTRY__DATE_LABEL: return IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_DATE_LABEL;
                case IArchimatePackage.ARCHESTRY__DATE_FONTCOLOR: return IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_DATE_FONTCOLOR;
                case IArchimatePackage.ARCHESTRY__DATE_AREACOLOR: return IArchimatePackage.ARCHIMATE_CONCEPT__ARCHESTRY_DATE_AREACOLOR;
                
                default: return -1;
            }        
        }
        if (baseClass == IProperties.class) {
            switch (baseFeatureID) {
                case IArchimatePackage.PROPERTIES__PROPERTIES: return IArchimatePackage.ARCHIMATE_CONCEPT__PROPERTIES;
                default: return -1;
            }
        }
        return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (name: "); //$NON-NLS-1$
        result.append(name);
        result.append(", id: "); //$NON-NLS-1$
        result.append(id);
        result.append(", documentation: "); //$NON-NLS-1$
        result.append(documentation);
        
        result.append(", archestryOrderLabel: "); //$NON-NLS-1$
        result.append(archestry_OrderLabel);        
        result.append(", archestryOrderFontColor: "); //$NON-NLS-1$
        result.append(archestry_OrderFontColor);  
        result.append(", archestryOrderAreaColor: "); //$NON-NLS-1$
        result.append(archestry_OrderAreaColor);  
        
        result.append(", archestryFamilyLabel: "); //$NON-NLS-1$
        result.append(archestry_FamilyLabel);    
        result.append(", archestryFamilyFontColor: "); //$NON-NLS-1$
        result.append(archestry_FamilyFontColor);    
        result.append(", archestryFamilyAreaColor: "); //$NON-NLS-1$
        result.append(archestry_FamilyAreaColor);    

        result.append(", archestryGenusLabel: "); //$NON-NLS-1$
        result.append(archestry_GenusLabel);    
        result.append(", archestryGenusFontColor: "); //$NON-NLS-1$
        result.append(archestry_GenusFontColor);    
        result.append(", archestryGenusAreaColor: "); //$NON-NLS-1$
        result.append(archestry_GenusAreaColor);   
        
        result.append(", archestryExtUUIDLabel: "); //$NON-NLS-1$
        result.append(archestry_ExtUUIDLabel);
        result.append(", archestryExtUUIDFontColor: "); //$NON-NLS-1$
        result.append(archestry_ExtUUIDFontColor);
        result.append(", archestryExtUUIDAreaColor: "); //$NON-NLS-1$
        result.append(archestry_ExtUUIDAreaColor);
        
        result.append(", archestryRefIDLabel: "); //$NON-NLS-1$
        result.append(archestry_RefIDLabel);
        result.append(", archestryRefIDFontColor: "); //$NON-NLS-1$
        result.append(archestry_RefIDFontColor);    
        result.append(", archestryRefIDAreaColor: "); //$NON-NLS-1$
        result.append(archestry_RefIDAreaColor);    
        
        result.append(", archestryStatusLabel: "); //$NON-NLS-1$
        result.append(archestry_StatusLabel);
        result.append(", archestryStatusFontColor: "); //$NON-NLS-1$
        result.append(archestry_StatusFontColor);    
        result.append(", archestryStatusAreaColor: "); //$NON-NLS-1$
        result.append(archestry_StatusAreaColor);    
        
        result.append(", archestryStageLabel: "); //$NON-NLS-1$
        result.append(archestry_StageLabel);
        result.append(", archestryStageFontColor: "); //$NON-NLS-1$
        result.append(archestry_StageFontColor);    
        result.append(", archestryStageAreaColor: "); //$NON-NLS-1$
        result.append(archestry_StageAreaColor);    
        
        result.append(", archestryCommWithLabel: "); //$NON-NLS-1$
        result.append(archestry_CommWithLabel);
        result.append(", archestryCommWithFontColor: "); //$NON-NLS-1$
        result.append(archestry_CommWithFontColor);    
        result.append(", archestryCommWithAreaColor: "); //$NON-NLS-1$
        result.append(archestry_CommWithAreaColor);    
        
        result.append(", archestryVersionLabel: "); //$NON-NLS-1$
        result.append(archestry_VersionLabel);    
        result.append(", archestryVersionFontColor: "); //$NON-NLS-1$
        result.append(archestry_VersionFontColor);    
        result.append(", archestryVersionAreaColor: "); //$NON-NLS-1$
        result.append(archestry_VersionAreaColor);    
        
        result.append(", archestryDateLabel: "); //$NON-NLS-1$
        result.append(archestry_DateLabel);
        result.append(", archestryDateFontColor: "); //$NON-NLS-1$
        result.append(archestry_DateFontColor); 
        result.append(", archestryDateAreaColor: "); //$NON-NLS-1$
        result.append(archestry_DateAreaColor); 
        
        result.append(')');
        return result.toString();
    }

} //ArchimateConcept
