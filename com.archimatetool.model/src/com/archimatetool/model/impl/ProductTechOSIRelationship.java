/**
 * This program and the accompanying materials
 * are made available under the terms of the License
 * which accompanies this distribution in the file LICENSE.txt
 */
package com.archimatetool.model.impl;

import org.eclipse.emf.ecore.EClass;

import com.archimatetool.model.IArchimatePackage;
import com.archimatetool.model.IProductTechOSIRelationship;


/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ProductTechOSI Relationship</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ProductTechOSIRelationship extends ArchimateRelationship implements IProductTechOSIRelationship {
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected ProductTechOSIRelationship() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return IArchimatePackage.Literals.PRODUCTTECHOSI_RELATIONSHIP;
    }

} //ProductTechOSIRelationship
