/**
 * This program and the accompanying materials
 * are made available under the terms of the License
 * which accompanies this distribution in the file LICENSE.txt
 */
package com.archimatetool.model.impl;

import org.eclipse.emf.ecore.EClass;

import com.archimatetool.model.IArchimatePackage;
import com.archimatetool.model.IContextmeBasicClarify;


/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Contextme Basic Clarify</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ContextmeBasicClarify extends ArchimateElement implements IContextmeBasicClarify {
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected ContextmeBasicClarify() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return IArchimatePackage.Literals.CONTEXTME_BASIC_CLARIFY;
    }

} //ContextmeBasicClarify
