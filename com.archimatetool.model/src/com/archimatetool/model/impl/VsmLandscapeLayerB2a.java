/**
 * This program and the accompanying materials
 * are made available under the terms of the License
 * which accompanies this distribution in the file LICENSE.txt
 */
package com.archimatetool.model.impl;

import org.eclipse.emf.ecore.EClass;

import com.archimatetool.model.IArchimatePackage;
import com.archimatetool.model.IVsmLandscapeLayerB2a;


/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>VsmLandscape LayerB2a</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class VsmLandscapeLayerB2a extends ArchimateElement implements IVsmLandscapeLayerB2a {
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected VsmLandscapeLayerB2a() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return IArchimatePackage.Literals.VSM_LANDSCAPE_LAYER_B2A;
    }

} //LayerB2a
