package com.archimatetool.model;


import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Folder Type</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see com.archimatetool.model.IArchimatePackage#getFolderType()
 * @model
 * @generated
 */
public enum FolderType implements Enumerator {
    /**
     * The '<em><b>User</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #USER_VALUE
     * @generated NOT
     * @ordered
     */
    USER(0, "user", "user", Messages.FolderType_0), //$NON-NLS-1$ //$NON-NLS-2$

    /**
     * The '<em><b>Strategy</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #STRATEGY_VALUE
     * @generated NOT
     * @ordered
     */
    STRATEGY(1, "strategy", "strategy", Messages.FolderType_1), //$NON-NLS-1$ //$NON-NLS-2$

    /**
     * The '<em><b>Business</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #BUSINESS_VALUE
     * @generated NOT
     * @ordered
     */
    BUSINESS(2, "business", "business", Messages.FolderType_2), //$NON-NLS-1$ //$NON-NLS-2$

    /**
     * The '<em><b>Application</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #APPLICATION_VALUE
     * @generated NOT
     * @ordered
     */
    APPLICATION(3, "application", "application", Messages.FolderType_3), //$NON-NLS-1$ //$NON-NLS-2$

    /**
     * The '<em><b>Technology</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #TECHNOLOGY_VALUE
     * @generated NOT
     * @ordered
     */
    TECHNOLOGY(4, "technology", "technology", Messages.FolderType_4), //$NON-NLS-1$ //$NON-NLS-2$

    /**
     * The '<em><b>Relations</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #RELATIONS_VALUE
     * @generated NOT
     * @ordered
     */
    RELATIONS(5, "relations", "relations", Messages.FolderType_5), //$NON-NLS-1$ //$NON-NLS-2$

    /**
     * The '<em><b>Other</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #OTHER_VALUE
     * @generated NOT
     * @ordered
     */
    OTHER(6, "other", "other", Messages.FolderType_6), //$NON-NLS-1$ //$NON-NLS-2$

    /**
     * The '<em><b>Diagrams</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #DIAGRAMS_VALUE
     * @generated NOT
     * @ordered
     */
    DIAGRAMS(7, "diagrams", "diagrams", Messages.FolderType_7), //$NON-NLS-1$ //$NON-NLS-2$

    /**
     * The '<em><b>Motivation</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #MOTIVATION_VALUE
     * @generated NOT
     * @ordered
     */
    MOTIVATION(8, "motivation", "motivation", Messages.FolderType_8), //$NON-NLS-1$ //$NON-NLS-2$

    /**
     * The '<em><b>Implementation migration</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #IMPLEMENTATION_MIGRATION_VALUE
     * @generated NOT
     * @ordered
     */
    IMPLEMENTATION_MIGRATION(9, "implementation_migration", "implementation_migration", Messages.FolderType_9), //$NON-NLS-1$ //$NON-NLS-2$    

    /**
     * The '<em><b>Data</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #MOTIVATIO#MOTIVATION_VALUE
     * @generated
     * @ordered
     */
    DATA(20, "data", "data", Messages.FolderType_20), //$NON-NLS-1$ //$NON-NLS-2$    
    
    
    /**
     * The '<em><b>Archestry</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #MOTIVATIO#MOTIVATION_VALUE
     * @generated
     * @ordered
     */
    VSM_LANDSCAPE(30, "vsm_landscape", "vsm_landscape", Messages.FolderType_30), //$NON-NLS-1$ //$NON-NLS-2$
    
    
    /**
     * The '<em><b>Personal dynamics</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #MOTIVATIO#MOTIVATION_VALUE
     * @generated
     * @ordered
     */
    DYNAMICS_PERSONAL(40, "dynamics_personal", "dynamics_personal", Messages.FolderType_40), //$NON-NLS-1$ //$NON-NLS-2$
    DYNAMICS_TEAM(50, "dynamics_team", "dynamics_team", Messages.FolderType_50), //$NON-NLS-1$ //$NON-NLS-2$    
    DYNAMICS_ORG(60, "dynamics_org", "dynamics_org", Messages.FolderType_60), //$NON-NLS-1$ //$NON-NLS-2$
    DYNAMICS_ENV(70, "dynamics_env", "dynamics_env", Messages.FolderType_70), //$NON-NLS-1$ //$NON-NLS-2$
    
    VSM_BUSINESS(81, "vsm_business", "vsm_business", Messages.FolderType_81), //$NON-NLS-1$ //$NON-NLS-2$
    VSM_APPLICATION(82, "vsm_application", "vsm_application", Messages.FolderType_82), //$NON-NLS-1$ //$NON-NLS-2$
    VSM_DATA(83, "vsm_data", "vsm_data", Messages.FolderType_83), //$NON-NLS-1$ //$NON-NLS-2$    
    VSM_TECHNOLOGY(84, "vsm_technology", "vsm_technology", Messages.FolderType_84), //$NON-NLS-1$ //$NON-NLS-2$
    VSM_VIA(85, "vsm_via", "vsm_via", Messages.FolderType_85), //$NON-NLS-1$ //$NON-NLS-2$
      
    CONTEXTME_BASIC(90, "contextme_basic", "contextme_basic", Messages.FolderType_90), //$NON-NLS-1$ //$NON-NLS-2$
    VSM_SRM(100, "vsm_srm", "vsm_srm", Messages.FolderType_100); //$NON-NLS-1$ //$NON-NLS-2$
          

    /**
     * The '<em><b>User</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of '<em><b>User</b></em>' literal object isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @see #USER
     * @model name="user"
     * @generated
     * @ordered
     */
    public static final int USER_VALUE = 0;

    /**
     * The '<em><b>Strategy</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of '<em><b>Strategy</b></em>' literal object isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @see #STRATEGY
     * @model name="strategy"
     * @generated
     * @ordered
     */
    public static final int STRATEGY_VALUE = 1;

    /**
     * The '<em><b>Business</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of '<em><b>Business</b></em>' literal object isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @see #BUSINESS
     * @model name="business"
     * @generated
     * @ordered
     */
    public static final int BUSINESS_VALUE = 2;

    /**
     * The '<em><b>Application</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of '<em><b>Application</b></em>' literal object isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @see #APPLICATION
     * @model name="application"
     * @generated
     * @ordered
     */
    public static final int APPLICATION_VALUE = 3;

    /**
     * The '<em><b>Technology</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of '<em><b>Technology</b></em>' literal object isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @see #TECHNOLOGY
     * @model name="technology"
     * @generated
     * @ordered
     */
    public static final int TECHNOLOGY_VALUE = 4;

    /**
     * The '<em><b>Relations</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of '<em><b>Relations</b></em>' literal object isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @see #RELATIONS
     * @model name="relations"
     * @generated
     * @ordered
     */
    public static final int RELATIONS_VALUE = 5;

    /**
     * The '<em><b>Other</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of '<em><b>Other</b></em>' literal object isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @see #OTHER
     * @model name="other"
     * @generated
     * @ordered
     */
    public static final int OTHER_VALUE = 6;

    /**
     * The '<em><b>Diagrams</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of '<em><b>Diagrams</b></em>' literal object isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @see #DIAGRAMS
     * @model name="diagrams"
     * @generated
     * @ordered
     */
    public static final int DIAGRAMS_VALUE = 7;

    /**
     * The '<em><b>Motivation</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of '<em><b>Motivation</b></em>' literal object isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @see #MOTIVATION
     * @model name="motivation"
     * @generated
     * @ordered
     */
    public static final int MOTIVATION_VALUE = 8;

    
    /**
     * The '<em><b>Data</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of '<em><b>Data</b></em>' literal object isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @see #DATA
     * @model name="data"
     * @generated
     * @ordered
     */
    public static final int DATA_VALUE = 20;
    
    /**
     * The '<em><b>VSM Landscape</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of '<em><b>VSM Landscape</b></em>' literal object isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @see #VSM_LANDSCAPE
     * @model name="VSM Landscape"
     * @generated
     * @ordered
     */
    public static final int VSM_LANDSCAPE_VALUE = 30;    
    
    /**
     * The '<em><b>Personaldynamics</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of '<em><b>Personaldynamics</b></em>' literal object isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @see #DYNAMICS_PERSONAL
     * @model name="personaldynamics"
     * @generated
     * @ordered
     */
    public static final int DYNAMICS_PERSONAL_VALUE = 40;
    
    /**
     * The '<em><b>Teamdynamics</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of '<em><b>Personaldynamics</b></em>' literal object isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @see #DYNAMICS_TEAM
     * @model name="teamdynamics"
     * @generated
     * @ordered
     */
    public static final int DYNAMICS_TEAM_VALUE = 50;
    
    public static final int DYNAMICS_ORG_VALUE = 60;    
    
    public static final int DYNAMICS_ENV_VALUE = 70;    
    
    public static final int VSM_BUSINESS_VALUE = 81; 
    
    public static final int VSM_APPLICATION_VALUE = 82; 
    
    public static final int VSM_DATA_VALUE = 83; 
    
    public static final int VSM_TECHNOLOGY_VALUE = 84; 
    
    public static final int VSM_VIA_VALUE = 85; 
    
    public static final int CONTEXTME_BASIC_VALUE = 90; 
    
    public static final int VSM_SRM_VALUE = 100; 

    /**
     * The '<em><b>Implementation migration</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of '<em><b>Implementation migration</b></em>' literal object isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @see #IMPLEMENTATION_MIGRATION
     * @model name="implementation_migration"
     * @generated
     * @ordered
     */
    public static final int IMPLEMENTATION_MIGRATION_VALUE = 9;

    /**
     * An array of all the '<em><b>Folder Type</b></em>' enumerators.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private static final FolderType[] VALUES_ARRAY =
        new FolderType[] {
            USER,
            STRATEGY,
            BUSINESS,
            APPLICATION,
            DATA,
            TECHNOLOGY,
            VSM_BUSINESS,
            VSM_APPLICATION,
            VSM_DATA,
            VSM_TECHNOLOGY,            
            RELATIONS,
            OTHER,
            DIAGRAMS,
            MOTIVATION,
            IMPLEMENTATION_MIGRATION,
            VSM_LANDSCAPE,
            DYNAMICS_PERSONAL,
            DYNAMICS_TEAM,
            DYNAMICS_ORG,
            DYNAMICS_ENV,
            CONTEXTME_BASIC,
            VSM_SRM,
            VSM_VIA,
        };

    /**
     * A public read-only list of all the '<em><b>Folder Type</b></em>' enumerators.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public static final List<FolderType> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

    /**
     * Returns the '<em><b>Folder Type</b></em>' literal with the specified literal value.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param literal the literal.
     * @return the matching enumerator or <code>null</code>.
     * @generated
     */
    public static FolderType get(String literal) {
        for (int i = 0; i < VALUES_ARRAY.length; ++i) {
            FolderType result = VALUES_ARRAY[i];
            if (result.toString().equals(literal)) {
                return result;
            }
        }
        return null;
    }

    /**
     * Returns the '<em><b>Folder Type</b></em>' literal with the specified name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param name the name.
     * @return the matching enumerator or <code>null</code>.
     * @generated
     */
    public static FolderType getByName(String name) {
        for (int i = 0; i < VALUES_ARRAY.length; ++i) {
            FolderType result = VALUES_ARRAY[i];
            if (result.getName().equals(name)) {
                return result;
            }
        }
        return null;
    }

    /**
     * Returns the '<em><b>Folder Type</b></em>' literal with the specified integer value.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the integer value.
     * @return the matching enumerator or <code>null</code>.
     * @generated
     */
    public static FolderType get(int value) {
        switch (value) {
            case USER_VALUE: return USER;
            case STRATEGY_VALUE: return STRATEGY;
            case BUSINESS_VALUE: return BUSINESS;
            case VSM_VIA_VALUE: return VSM_VIA;
            case APPLICATION_VALUE: return APPLICATION;
            case DATA_VALUE: return DATA;
            case TECHNOLOGY_VALUE: return VSM_TECHNOLOGY;
            case VSM_BUSINESS_VALUE: return VSM_BUSINESS;
            case VSM_APPLICATION_VALUE: return VSM_APPLICATION;
            case VSM_DATA_VALUE: return VSM_DATA;
            case VSM_TECHNOLOGY_VALUE: return VSM_TECHNOLOGY;
            case RELATIONS_VALUE: return RELATIONS;
            case OTHER_VALUE: return OTHER;
            case DIAGRAMS_VALUE: return DIAGRAMS;
            case MOTIVATION_VALUE: return MOTIVATION;
            case IMPLEMENTATION_MIGRATION_VALUE: return IMPLEMENTATION_MIGRATION;
            case VSM_LANDSCAPE_VALUE: return VSM_LANDSCAPE;
            case DYNAMICS_PERSONAL_VALUE: return DYNAMICS_PERSONAL;
            case DYNAMICS_TEAM_VALUE: return DYNAMICS_TEAM;
            case DYNAMICS_ORG_VALUE: return DYNAMICS_ORG;
            case DYNAMICS_ENV_VALUE: return DYNAMICS_ENV;
            case CONTEXTME_BASIC_VALUE: return CONTEXTME_BASIC;
            case VSM_SRM_VALUE: return VSM_SRM;
        }
        return null;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private final int value;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private final String name;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private final String literal;
    
    private String label;

    /**
     * Only this class can construct instances.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private FolderType(int value, String name, String literal) {
        this.value = value;
        this.name = name;
        this.literal = literal;
    }

    private FolderType(int value, String name, String literal, String label) {
        this.value = value;
        this.name = name;
        this.literal = literal;
        this.label = label;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public int getValue() {
      return value;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public String getName() {
      return name;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public String getLiteral() {
      return literal;
    }
    
    public String getLabel() {
        return label;
    }

    /**
     * Returns the literal value of the enumerator, which is its string representation.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public String toString() {
        return literal;
    }
    
} //FolderType
