/**
 * This program and the accompanying materials
 * are made available under the terms of the License
 * which accompanies this distribution in the file LICENSE.txt
 */
package com.archimatetool.model;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>VsmViaA2dData</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.archimatetool.model.IArchimatePackage#getVsmViaA2dData()
 * @model
 * @generated
 */
public interface IVsmViaA2dData extends IVsmViaElement, IBehaviorElement {
} // IVsmViaA2dData
