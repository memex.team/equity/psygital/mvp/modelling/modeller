/**
 * This program and the accompanying materials
 * are made available under the terms of the License
 * which accompanies this distribution in the file LICENSE.txt
 */
package com.archimatetool.model;

import org.eclipse.emf.ecore.EObject;

public interface IArchestry extends EObject {

    String getArchestry_OrderLabel();
    void setArchestry_OrderLabel(String value);
    String getArchestry_OrderFontColor();
    void setArchestry_OrderFontColor(String value);
    String getArchestry_OrderAreaColor();
    void setArchestry_OrderAreaColor(String value);
    
    String getArchestry_FamilyLabel();
    void setArchestry_FamilyLabel(String value);
    String getArchestry_FamilyFontColor();
    void setArchestry_FamilyFontColor(String value);
    String getArchestry_FamilyAreaColor();
    void setArchestry_FamilyAreaColor(String value);
    
    String getArchestry_GenusLabel();
    void setArchestry_GenusLabel(String value);
    String getArchestry_GenusFontColor();
    void setArchestry_GenusFontColor(String value);
    String getArchestry_GenusAreaColor();
    void setArchestry_GenusAreaColor(String value);
        
    
    String getArchestry_ExtUUIDLabel();
    void setArchestry_ExtUUIDLabel(String value);
    String getArchestry_ExtUUIDFontColor();
    void setArchestry_ExtUUIDFontColor(String value);
    String getArchestry_ExtUUIDAreaColor();
    void setArchestry_ExtUUIDAreaColor(String value);
    
    String getArchestry_RefIDLabel();
    void setArchestry_RefIDLabel(String value);
    String getArchestry_RefIDFontColor();
    void setArchestry_RefIDFontColor(String value);
    String getArchestry_RefIDAreaColor();
    void setArchestry_RefIDAreaColor(String value);
    
    String getArchestry_StatusLabel();
    void setArchestry_StatusLabel(String value);
    String getArchestry_StatusFontColor();
    void setArchestry_StatusFontColor(String value);
    String getArchestry_StatusAreaColor();
    void setArchestry_StatusAreaColor(String value);
    
    String getArchestry_StageLabel();
    void setArchestry_StageLabel(String value);
    String getArchestry_StageFontColor();
    void setArchestry_StageFontColor(String value);
    String getArchestry_StageAreaColor();
    void setArchestry_StageAreaColor(String value);
    
    String getArchestry_CommWithLabel();
    void setArchestry_CommWithLabel(String value);
    String getArchestry_CommWithFontColor();
    void setArchestry_CommWithFontColor(String value);
    String getArchestry_CommWithAreaColor();
    void setArchestry_CommWithAreaColor(String value);
    
    String getArchestry_VersionLabel();
    void setArchestry_VersionLabel(String value);
    String getArchestry_VersionFontColor();
    void setArchestry_VersionFontColor(String value);
    String getArchestry_VersionAreaColor();
    void setArchestry_VersionAreaColor(String value);
    
    String getArchestry_DateLabel();
    void setArchestry_DateLabel(String value);
    String getArchestry_DateFontColor();
    void setArchestry_DateFontColor(String value);
    String getArchestry_DateAreaColor();
    void setArchestry_DateAreaColor(String value);

} // IArchestry

