/**
 * This program and the accompanying materials
 * are made available under the terms of the License
 * which accompanies this distribution in the file LICENSE.txt
 */
package com.archimatetool.model.util;

import com.archimatetool.model.IAccessRelationship;
import com.archimatetool.model.IActiveStructureElement;
import com.archimatetool.model.IAdapter;
import com.archimatetool.model.IAggregationRelationship;
import com.archimatetool.model.IApplicationCollaboration;
import com.archimatetool.model.IApplicationComponent;
import com.archimatetool.model.IVsmViaElement;
import com.archimatetool.model.IVsmViaB2aUi;
import com.archimatetool.model.IVsmViaB2aIntegration;
import com.archimatetool.model.IVsmViaB2aDs;
import com.archimatetool.model.IVsmViaA2dData;
import com.archimatetool.model.IVsmViaA2tYaml;
import com.archimatetool.model.IVsmApplicationElement;
import com.archimatetool.model.IApplicationElement;
import com.archimatetool.model.IApplicationEvent;
import com.archimatetool.model.IApplicationFunction;
import com.archimatetool.model.IVsmApplicationTask;
import com.archimatetool.model.IVsmDataGitRepo;
import com.archimatetool.model.IVsmDataGitBranch;
import com.archimatetool.model.IVsmDataSrcDir;
import com.archimatetool.model.IVsmDataSrcFile;
import com.archimatetool.model.IVsmApplicationClass;
import com.archimatetool.model.IVsmApplicationModule;
import com.archimatetool.model.IVsmApplicationMethod;
import com.archimatetool.model.IVsmSrmElement;
import com.archimatetool.model.IVsmSrmStatus;
import com.archimatetool.model.IVsmSrmAction;
import com.archimatetool.model.IVsmSrmRoleFunction;
import com.archimatetool.model.IVsmSrmRoleUser;
import com.archimatetool.model.IVsmSrmUser;
import com.archimatetool.model.IApplicationInteraction;
import com.archimatetool.model.IApplicationInterface;
import com.archimatetool.model.IApplicationProcess;
import com.archimatetool.model.IApplicationService;
import com.archimatetool.model.IArchimateConcept;
import com.archimatetool.model.IArchimateDiagramModel;
import com.archimatetool.model.IArchimateElement;
import com.archimatetool.model.IArchimateModel;
import com.archimatetool.model.IArchimateModelObject;
import com.archimatetool.model.IArchimatePackage;
import com.archimatetool.model.IArchimateRelationship;
import com.archimatetool.model.IArtifact;
import com.archimatetool.model.IAssessment;
import com.archimatetool.model.IAssignmentRelationship;
import com.archimatetool.model.IAssociationRelationship;
import com.archimatetool.model.IBehaviorElement;
import com.archimatetool.model.IBorderObject;
import com.archimatetool.model.IBorderType;
import com.archimatetool.model.IBounds;
import com.archimatetool.model.IBusinessActor;
import com.archimatetool.model.IBusinessCollaboration;
import com.archimatetool.model.IVsmBusinessElement;
import com.archimatetool.model.IBusinessElement;
import com.archimatetool.model.IBusinessEvent;
import com.archimatetool.model.IBusinessFunction;
import com.archimatetool.model.IVsmBusinessEpic;
import com.archimatetool.model.IVsmBusinessUs;
import com.archimatetool.model.IVsmBusinessTask;
import com.archimatetool.model.IVsmBusinessAc;
import com.archimatetool.model.IVsmBusinessUc;
import com.archimatetool.model.IVsmBusinessFr;
import com.archimatetool.model.IVsmBusinessNfr;
import com.archimatetool.model.IBusinessInteraction;
import com.archimatetool.model.IBusinessInterface;
import com.archimatetool.model.IBusinessObject;
import com.archimatetool.model.IBusinessProcess;
import com.archimatetool.model.IBusinessRole;
import com.archimatetool.model.IBusinessService;
import com.archimatetool.model.ICapability;
import com.archimatetool.model.ICloneable;
import com.archimatetool.model.ICommunicationNetwork;
import com.archimatetool.model.ICompositeElement;
import com.archimatetool.model.ICompositionRelationship;
import com.archimatetool.model.IConnectable;
import com.archimatetool.model.IConstraint;
import com.archimatetool.model.IContract;
import com.archimatetool.model.ICourseOfAction;
import com.archimatetool.model.IDataObject;
import com.archimatetool.model.IDeliverable;
import com.archimatetool.model.IDependendencyRelationship;
import com.archimatetool.model.IDevice;
import com.archimatetool.model.IDiagramModel;
import com.archimatetool.model.IDiagramModelArchimateComponent;
import com.archimatetool.model.IDiagramModelArchimateConnection;
import com.archimatetool.model.IDiagramModelArchimateObject;
import com.archimatetool.model.IDiagramModelBendpoint;
import com.archimatetool.model.IDiagramModelComponent;
import com.archimatetool.model.IDiagramModelConnection;
import com.archimatetool.model.IDiagramModelContainer;
import com.archimatetool.model.IDiagramModelGroup;
import com.archimatetool.model.IDiagramModelImage;
import com.archimatetool.model.IDiagramModelImageProvider;
import com.archimatetool.model.IDiagramModelNote;
import com.archimatetool.model.IDiagramModelObject;
import com.archimatetool.model.IDiagramModelReference;
import com.archimatetool.model.IDistributionNetwork;
import com.archimatetool.model.IDocumentable;
import com.archimatetool.model.IDriver;
import com.archimatetool.model.IDynamicRelationship;
import com.archimatetool.model.IEquipment;
import com.archimatetool.model.IFacility;
import com.archimatetool.model.IFeature;
import com.archimatetool.model.IFeatures;
import com.archimatetool.model.IFlowRelationship;
import com.archimatetool.model.IFolder;
import com.archimatetool.model.IFolderContainer;
import com.archimatetool.model.IFontAttribute;
import com.archimatetool.model.IGap;
import com.archimatetool.model.IGoal;
import com.archimatetool.model.IDynamicsPersonalFeeling;
import com.archimatetool.model.IDynamicsPersonalPrediction;
import com.archimatetool.model.IDynamicsPersonalCorrection;
import com.archimatetool.model.IDynamicsPersonalByproduct;
import com.archimatetool.model.IDynamicsTeamPrediction;
import com.archimatetool.model.IDynamicsTeamFeeling;
import com.archimatetool.model.IDynamicsOrgPrediction;
import com.archimatetool.model.IDynamicsEnvPrediction;
import com.archimatetool.model.IContextmeBasicElement;
import com.archimatetool.model.IContextmeBasicConcept;
import com.archimatetool.model.IContextmeBasicSituation;
import com.archimatetool.model.IContextmeBasicQuestioning;
import com.archimatetool.model.IContextmeBasicCurious;
import com.archimatetool.model.IContextmeBasicLink;
import com.archimatetool.model.IContextmeBasicClarify;
import com.archimatetool.model.IContextmeBasicExpress;
import com.archimatetool.model.IContextmeBasicDiscuss;
import com.archimatetool.model.IContextmeBasicNote;
import com.archimatetool.model.IVsmDataElement;
import com.archimatetool.model.IDataElement;
import com.archimatetool.model.IVsmDataDatabase;
import com.archimatetool.model.IVsmDataSchema;
import com.archimatetool.model.IVsmDataView;
import com.archimatetool.model.IVsmDataTable;
import com.archimatetool.model.IVsmDataColumn;
import com.archimatetool.model.IVsmDataTask;
import com.archimatetool.model.IVsmLandscapeInfrastructureOctagon;
import com.archimatetool.model.IVsmLandscapeProductOctagon;
import com.archimatetool.model.IVsmLandscapeLayerGeneral;
import com.archimatetool.model.IVsmLandscapeLayerBusiness;
import com.archimatetool.model.IVsmLandscapeLayerApplication;
import com.archimatetool.model.IVsmLandscapeLayerTechnology;
import com.archimatetool.model.IVsmLandscapeLayerData;
import com.archimatetool.model.IVsmLandscapeLayerB2a;
import com.archimatetool.model.IVsmLandscapeLayerA2d;
import com.archimatetool.model.IVsmLandscapeLayerA2t;
import com.archimatetool.model.IGrouping;
import com.archimatetool.model.IIdentifier;
import com.archimatetool.model.IImplementationEvent;
import com.archimatetool.model.IImplementationMigrationElement;
import com.archimatetool.model.IInfluenceRelationship;
import com.archimatetool.model.IJunction;
import com.archimatetool.model.ILineObject;
import com.archimatetool.model.ILocation;
import com.archimatetool.model.ILockable;
import com.archimatetool.model.IMaterial;
import com.archimatetool.model.IMeaning;
import com.archimatetool.model.IMetadata;
import com.archimatetool.model.IMotivationElement;
import com.archimatetool.model.IVsmLandscapeElement;
import com.archimatetool.model.IDynamicsPersonalElement;
import com.archimatetool.model.IDynamicsTeamElement;
import com.archimatetool.model.IDynamicsOrgElement;
import com.archimatetool.model.IDynamicsEnvElement;
import com.archimatetool.model.INameable;
import com.archimatetool.model.INode;
import com.archimatetool.model.IOtherRelationship;
import com.archimatetool.model.IOutcome;
import com.archimatetool.model.IPassiveStructureElement;
import com.archimatetool.model.IPath;
import com.archimatetool.model.IPhysicalElement;
import com.archimatetool.model.IPlateau;
import com.archimatetool.model.IPrinciple;
import com.archimatetool.model.IProduct;
import com.archimatetool.model.IProperties;
import com.archimatetool.model.IProperty;
import com.archimatetool.model.IRealizationRelationship;
import com.archimatetool.model.IRepresentation;
import com.archimatetool.model.IRequirement;
import com.archimatetool.model.IResource;
import com.archimatetool.model.IServingRelationship;
import com.archimatetool.model.ISketchModel;
import com.archimatetool.model.ISketchModelActor;
import com.archimatetool.model.ISketchModelSticky;
import com.archimatetool.model.ISpecializationRelationship;
import com.archimatetool.model.IStakeholder;
import com.archimatetool.model.IStrategyBehaviorElement;
import com.archimatetool.model.IStrategyElement;
import com.archimatetool.model.IStructuralRelationship;
import com.archimatetool.model.IStructureElement;
import com.archimatetool.model.ISystemSoftware;
import com.archimatetool.model.ITechnologyCollaboration;
import com.archimatetool.model.IVsmTechnologyElement;
import com.archimatetool.model.ITechnologyElement;
import com.archimatetool.model.ITechnologyEvent;
import com.archimatetool.model.ITechnologyFunction;
import com.archimatetool.model.IVsmTechnologyTask;
import com.archimatetool.model.IVsmTechnologyK8sPod;
import com.archimatetool.model.IVsmTechnologyK8sContainer;
import com.archimatetool.model.IVsmTechnologyK8sController;
import com.archimatetool.model.IVsmTechnologyStorage;
import com.archimatetool.model.ITechnologyInteraction;
import com.archimatetool.model.ITechnologyInterface;
import com.archimatetool.model.ITechnologyObject;
import com.archimatetool.model.ITechnologyProcess;
import com.archimatetool.model.ITechnologyService;
import com.archimatetool.model.ITextAlignment;
import com.archimatetool.model.ITextContent;
import com.archimatetool.model.ITextPosition;
import com.archimatetool.model.ITriggeringRelationship;
import com.archimatetool.model.IProductAppOwnsRelationship;
import com.archimatetool.model.IProductBizOwnsRelationship;
import com.archimatetool.model.IProductDataOwnsRelationship;
import com.archimatetool.model.IProductTechOwnsRelationship;
import com.archimatetool.model.IProductTechOSIRelationship;
import com.archimatetool.model.IValue;
import com.archimatetool.model.IValueStream;
import com.archimatetool.model.IWorkPackage;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.util.Switch;


/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see com.archimatetool.model.IArchimatePackage
 * @generated
 */
public class ArchimateSwitch<T> extends Switch<T> {
    /**
     * The cached model package
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected static IArchimatePackage modelPackage;

    /**
     * Creates an instance of the switch.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public ArchimateSwitch() {
        if (modelPackage == null) {
            modelPackage = IArchimatePackage.eINSTANCE;
        }
    }

    /**
     * Checks whether this is a switch for the given package.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param ePackage the package in question.
     * @return whether this is a switch for the given package.
     * @generated
     */
    @Override
    protected boolean isSwitchFor(EPackage ePackage) {
        return ePackage == modelPackage;
    }

    /**
     * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the first non-null result returned by a <code>caseXXX</code> call.
     * @generated
     */
    @Override
    protected T doSwitch(int classifierID, EObject theEObject) {
        switch (classifierID) {
            case IArchimatePackage.ADAPTER: {
                IAdapter adapter = (IAdapter)theEObject;
                T result = caseAdapter(adapter);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.IDENTIFIER: {
                IIdentifier identifier = (IIdentifier)theEObject;
                T result = caseIdentifier(identifier);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.PROPERTY: {
                IProperty property = (IProperty)theEObject;
                T result = caseProperty(property);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.PROPERTIES: {
                IProperties properties = (IProperties)theEObject;
                T result = caseProperties(properties);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.FEATURE: {
                IFeature feature = (IFeature)theEObject;
                T result = caseFeature(feature);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.FEATURES: {
                IFeatures features = (IFeatures)theEObject;
                T result = caseFeatures(features);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.METADATA: {
                IMetadata metadata = (IMetadata)theEObject;
                T result = caseMetadata(metadata);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.NAMEABLE: {
                INameable nameable = (INameable)theEObject;
                T result = caseNameable(nameable);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.TEXT_CONTENT: {
                ITextContent textContent = (ITextContent)theEObject;
                T result = caseTextContent(textContent);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.DOCUMENTABLE: {
                IDocumentable documentable = (IDocumentable)theEObject;
                T result = caseDocumentable(documentable);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.CLONEABLE: {
                ICloneable cloneable = (ICloneable)theEObject;
                T result = caseCloneable(cloneable);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.FOLDER_CONTAINER: {
                IFolderContainer folderContainer = (IFolderContainer)theEObject;
                T result = caseFolderContainer(folderContainer);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.FOLDER: {
                IFolder folder = (IFolder)theEObject;
                T result = caseFolder(folder);
                if (result == null) result = caseArchimateModelObject(folder);
                if (result == null) result = caseFolderContainer(folder);
                if (result == null) result = caseDocumentable(folder);
                if (result == null) result = caseProperties(folder);
                if (result == null) result = caseAdapter(folder);
                if (result == null) result = caseNameable(folder);
                if (result == null) result = caseIdentifier(folder);
                if (result == null) result = caseFeatures(folder);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.ARCHIMATE_MODEL_OBJECT: {
                IArchimateModelObject archimateModelObject = (IArchimateModelObject)theEObject;
                T result = caseArchimateModelObject(archimateModelObject);
                if (result == null) result = caseAdapter(archimateModelObject);
                if (result == null) result = caseNameable(archimateModelObject);
                if (result == null) result = caseIdentifier(archimateModelObject);
                if (result == null) result = caseFeatures(archimateModelObject);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.ARCHIMATE_CONCEPT: {
                IArchimateConcept archimateConcept = (IArchimateConcept)theEObject;
                T result = caseArchimateConcept(archimateConcept);
                if (result == null) result = caseArchimateModelObject(archimateConcept);
                if (result == null) result = caseCloneable(archimateConcept);
                if (result == null) result = caseDocumentable(archimateConcept);
                if (result == null) result = caseProperties(archimateConcept);
                if (result == null) result = caseAdapter(archimateConcept);
                if (result == null) result = caseNameable(archimateConcept);
                if (result == null) result = caseIdentifier(archimateConcept);
                if (result == null) result = caseFeatures(archimateConcept);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.ARCHIMATE_ELEMENT: {
                IArchimateElement archimateElement = (IArchimateElement)theEObject;
                T result = caseArchimateElement(archimateElement);
                if (result == null) result = caseArchimateConcept(archimateElement);
                if (result == null) result = caseArchimateModelObject(archimateElement);
                if (result == null) result = caseCloneable(archimateElement);
                if (result == null) result = caseDocumentable(archimateElement);
                if (result == null) result = caseProperties(archimateElement);
                if (result == null) result = caseAdapter(archimateElement);
                if (result == null) result = caseNameable(archimateElement);
                if (result == null) result = caseIdentifier(archimateElement);
                if (result == null) result = caseFeatures(archimateElement);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.ARCHIMATE_RELATIONSHIP: {
                IArchimateRelationship archimateRelationship = (IArchimateRelationship)theEObject;
                T result = caseArchimateRelationship(archimateRelationship);
                if (result == null) result = caseArchimateConcept(archimateRelationship);
                if (result == null) result = caseArchimateModelObject(archimateRelationship);
                if (result == null) result = caseCloneable(archimateRelationship);
                if (result == null) result = caseDocumentable(archimateRelationship);
                if (result == null) result = caseProperties(archimateRelationship);
                if (result == null) result = caseAdapter(archimateRelationship);
                if (result == null) result = caseNameable(archimateRelationship);
                if (result == null) result = caseIdentifier(archimateRelationship);
                if (result == null) result = caseFeatures(archimateRelationship);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.STRATEGY_ELEMENT: {
                IStrategyElement strategyElement = (IStrategyElement)theEObject;
                T result = caseStrategyElement(strategyElement);
                if (result == null) result = caseArchimateElement(strategyElement);
                if (result == null) result = caseArchimateConcept(strategyElement);
                if (result == null) result = caseArchimateModelObject(strategyElement);
                if (result == null) result = caseCloneable(strategyElement);
                if (result == null) result = caseDocumentable(strategyElement);
                if (result == null) result = caseProperties(strategyElement);
                if (result == null) result = caseAdapter(strategyElement);
                if (result == null) result = caseNameable(strategyElement);
                if (result == null) result = caseIdentifier(strategyElement);
                if (result == null) result = caseFeatures(strategyElement);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.BUSINESS_ELEMENT: {
                IBusinessElement businessElement = (IBusinessElement)theEObject;
                T result = caseBusinessElement(businessElement);
                if (result == null) result = caseArchimateElement(businessElement);
                if (result == null) result = caseArchimateConcept(businessElement);
                if (result == null) result = caseArchimateModelObject(businessElement);
                if (result == null) result = caseCloneable(businessElement);
                if (result == null) result = caseDocumentable(businessElement);
                if (result == null) result = caseProperties(businessElement);
                if (result == null) result = caseAdapter(businessElement);
                if (result == null) result = caseNameable(businessElement);
                if (result == null) result = caseIdentifier(businessElement);
                if (result == null) result = caseFeatures(businessElement);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.APPLICATION_ELEMENT: {
                IApplicationElement applicationElement = (IApplicationElement)theEObject;
                T result = caseApplicationElement(applicationElement);
                if (result == null) result = caseArchimateElement(applicationElement);
                if (result == null) result = caseArchimateConcept(applicationElement);
                if (result == null) result = caseArchimateModelObject(applicationElement);
                if (result == null) result = caseCloneable(applicationElement);
                if (result == null) result = caseDocumentable(applicationElement);
                if (result == null) result = caseProperties(applicationElement);
                if (result == null) result = caseAdapter(applicationElement);
                if (result == null) result = caseNameable(applicationElement);
                if (result == null) result = caseIdentifier(applicationElement);
                if (result == null) result = caseFeatures(applicationElement);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.TECHNOLOGY_ELEMENT: {
                ITechnologyElement technologyElement = (ITechnologyElement)theEObject;
                T result = caseTechnologyElement(technologyElement);
                if (result == null) result = caseArchimateElement(technologyElement);
                if (result == null) result = caseArchimateConcept(technologyElement);
                if (result == null) result = caseArchimateModelObject(technologyElement);
                if (result == null) result = caseCloneable(technologyElement);
                if (result == null) result = caseDocumentable(technologyElement);
                if (result == null) result = caseProperties(technologyElement);
                if (result == null) result = caseAdapter(technologyElement);
                if (result == null) result = caseNameable(technologyElement);
                if (result == null) result = caseIdentifier(technologyElement);
                if (result == null) result = caseFeatures(technologyElement);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.TECHNOLOGY_OBJECT: {
                ITechnologyObject technologyObject = (ITechnologyObject)theEObject;
                T result = caseTechnologyObject(technologyObject);
                if (result == null) result = caseTechnologyElement(technologyObject);
                if (result == null) result = casePassiveStructureElement(technologyObject);
                if (result == null) result = caseStructureElement(technologyObject);
                if (result == null) result = caseArchimateElement(technologyObject);
                if (result == null) result = caseArchimateConcept(technologyObject);
                if (result == null) result = caseArchimateModelObject(technologyObject);
                if (result == null) result = caseCloneable(technologyObject);
                if (result == null) result = caseDocumentable(technologyObject);
                if (result == null) result = caseProperties(technologyObject);
                if (result == null) result = caseAdapter(technologyObject);
                if (result == null) result = caseNameable(technologyObject);
                if (result == null) result = caseIdentifier(technologyObject);
                if (result == null) result = caseFeatures(technologyObject);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.PHYSICAL_ELEMENT: {
                IPhysicalElement physicalElement = (IPhysicalElement)theEObject;
                T result = casePhysicalElement(physicalElement);
                if (result == null) result = caseArchimateElement(physicalElement);
                if (result == null) result = caseArchimateConcept(physicalElement);
                if (result == null) result = caseArchimateModelObject(physicalElement);
                if (result == null) result = caseCloneable(physicalElement);
                if (result == null) result = caseDocumentable(physicalElement);
                if (result == null) result = caseProperties(physicalElement);
                if (result == null) result = caseAdapter(physicalElement);
                if (result == null) result = caseNameable(physicalElement);
                if (result == null) result = caseIdentifier(physicalElement);
                if (result == null) result = caseFeatures(physicalElement);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.MOTIVATION_ELEMENT: {
                IMotivationElement motivationElement = (IMotivationElement)theEObject;
                T result = caseMotivationElement(motivationElement);
                if (result == null) result = caseArchimateElement(motivationElement);
                if (result == null) result = caseArchimateConcept(motivationElement);
                if (result == null) result = caseArchimateModelObject(motivationElement);
                if (result == null) result = caseCloneable(motivationElement);
                if (result == null) result = caseDocumentable(motivationElement);
                if (result == null) result = caseProperties(motivationElement);
                if (result == null) result = caseAdapter(motivationElement);
                if (result == null) result = caseNameable(motivationElement);
                if (result == null) result = caseIdentifier(motivationElement);
                if (result == null) result = caseFeatures(motivationElement);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.IMPLEMENTATION_MIGRATION_ELEMENT: {
                IImplementationMigrationElement implementationMigrationElement = (IImplementationMigrationElement)theEObject;
                T result = caseImplementationMigrationElement(implementationMigrationElement);
                if (result == null) result = caseArchimateElement(implementationMigrationElement);
                if (result == null) result = caseArchimateConcept(implementationMigrationElement);
                if (result == null) result = caseArchimateModelObject(implementationMigrationElement);
                if (result == null) result = caseCloneable(implementationMigrationElement);
                if (result == null) result = caseDocumentable(implementationMigrationElement);
                if (result == null) result = caseProperties(implementationMigrationElement);
                if (result == null) result = caseAdapter(implementationMigrationElement);
                if (result == null) result = caseNameable(implementationMigrationElement);
                if (result == null) result = caseIdentifier(implementationMigrationElement);
                if (result == null) result = caseFeatures(implementationMigrationElement);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.DATA_ELEMENT: {
                IDataElement dataElement = (IDataElement)theEObject;
                T result = caseDataElement(dataElement);
                if (result == null) result = caseArchimateElement(dataElement);
                if (result == null) result = caseArchimateConcept(dataElement);
                if (result == null) result = caseArchimateModelObject(dataElement);
                if (result == null) result = caseCloneable(dataElement);
                if (result == null) result = caseDocumentable(dataElement);
                if (result == null) result = caseProperties(dataElement);
                if (result == null) result = caseAdapter(dataElement);
                if (result == null) result = caseNameable(dataElement);
                if (result == null) result = caseIdentifier(dataElement);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }  
            case IArchimatePackage.VSM_LANDSCAPE_ELEMENT: {
                IVsmLandscapeElement vsmLandscapeElement = (IVsmLandscapeElement)theEObject;
                T result = caseVsmLandscapeElement(vsmLandscapeElement);
                if (result == null) result = caseArchimateElement(vsmLandscapeElement);
                if (result == null) result = caseArchimateConcept(vsmLandscapeElement);
                if (result == null) result = caseArchimateModelObject(vsmLandscapeElement);
                if (result == null) result = caseCloneable(vsmLandscapeElement);
                if (result == null) result = caseDocumentable(vsmLandscapeElement);
                if (result == null) result = caseProperties(vsmLandscapeElement);
                if (result == null) result = caseAdapter(vsmLandscapeElement);
                if (result == null) result = caseNameable(vsmLandscapeElement);
                if (result == null) result = caseIdentifier(vsmLandscapeElement);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }   
            
            case IArchimatePackage.DYNAMICS_PERSONAL_ELEMENT: {
                IDynamicsPersonalElement dynamicsPersonalElement = (IDynamicsPersonalElement)theEObject;
                T result = caseDynamicsPersonalElement(dynamicsPersonalElement);
                if (result == null) result = caseArchimateElement(dynamicsPersonalElement);
                if (result == null) result = caseArchimateConcept(dynamicsPersonalElement);
                if (result == null) result = caseArchimateModelObject(dynamicsPersonalElement);
                if (result == null) result = caseCloneable(dynamicsPersonalElement);
                if (result == null) result = caseDocumentable(dynamicsPersonalElement);
                if (result == null) result = caseProperties(dynamicsPersonalElement);
                if (result == null) result = caseAdapter(dynamicsPersonalElement);
                if (result == null) result = caseNameable(dynamicsPersonalElement);
                if (result == null) result = caseIdentifier(dynamicsPersonalElement);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }

            case IArchimatePackage.DYNAMICS_TEAM_ELEMENT: {
                IDynamicsTeamElement dynamicsTeamElement = (IDynamicsTeamElement)theEObject;
                T result = caseDynamicsTeamElement(dynamicsTeamElement);
                if (result == null) result = caseArchimateElement(dynamicsTeamElement);
                if (result == null) result = caseArchimateConcept(dynamicsTeamElement);
                if (result == null) result = caseArchimateModelObject(dynamicsTeamElement);
                if (result == null) result = caseCloneable(dynamicsTeamElement);
                if (result == null) result = caseDocumentable(dynamicsTeamElement);
                if (result == null) result = caseProperties(dynamicsTeamElement);
                if (result == null) result = caseAdapter(dynamicsTeamElement);
                if (result == null) result = caseNameable(dynamicsTeamElement);
                if (result == null) result = caseIdentifier(dynamicsTeamElement);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            
            case IArchimatePackage.DYNAMICS_ORG_ELEMENT: {
                IDynamicsOrgElement dynamicsOrgElement = (IDynamicsOrgElement)theEObject;
                T result = caseDynamicsOrgElement(dynamicsOrgElement);
                if (result == null) result = caseArchimateElement(dynamicsOrgElement);
                if (result == null) result = caseArchimateConcept(dynamicsOrgElement);
                if (result == null) result = caseArchimateModelObject(dynamicsOrgElement);
                if (result == null) result = caseCloneable(dynamicsOrgElement);
                if (result == null) result = caseDocumentable(dynamicsOrgElement);
                if (result == null) result = caseProperties(dynamicsOrgElement);
                if (result == null) result = caseAdapter(dynamicsOrgElement);
                if (result == null) result = caseNameable(dynamicsOrgElement);
                if (result == null) result = caseIdentifier(dynamicsOrgElement);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            
            case IArchimatePackage.DYNAMICS_ENV_ELEMENT: {
                IDynamicsEnvElement dynamicsEnvElement = (IDynamicsEnvElement)theEObject;
                T result = caseDynamicsEnvElement(dynamicsEnvElement);
                if (result == null) result = caseArchimateElement(dynamicsEnvElement);
                if (result == null) result = caseArchimateConcept(dynamicsEnvElement);
                if (result == null) result = caseArchimateModelObject(dynamicsEnvElement);
                if (result == null) result = caseCloneable(dynamicsEnvElement);
                if (result == null) result = caseDocumentable(dynamicsEnvElement);
                if (result == null) result = caseProperties(dynamicsEnvElement);
                if (result == null) result = caseAdapter(dynamicsEnvElement);
                if (result == null) result = caseNameable(dynamicsEnvElement);
                if (result == null) result = caseIdentifier(dynamicsEnvElement);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }       
            
            case IArchimatePackage.CONTEXTME_BASIC_ELEMENT: {
                IContextmeBasicElement contextmeBasicElement = (IContextmeBasicElement)theEObject;
                T result = caseContextmeBasicElement(contextmeBasicElement);
                if (result == null) result = caseArchimateElement(contextmeBasicElement);
                if (result == null) result = caseArchimateConcept(contextmeBasicElement);
                if (result == null) result = caseArchimateModelObject(contextmeBasicElement);
                if (result == null) result = caseCloneable(contextmeBasicElement);
                if (result == null) result = caseDocumentable(contextmeBasicElement);
                if (result == null) result = caseProperties(contextmeBasicElement);
                if (result == null) result = caseAdapter(contextmeBasicElement);
                if (result == null) result = caseNameable(contextmeBasicElement);
                if (result == null) result = caseIdentifier(contextmeBasicElement);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }       
            
            case IArchimatePackage.VSM_SRM_ELEMENT: {
                IVsmSrmElement vsmSrmElement = (IVsmSrmElement)theEObject;
                T result = caseVsmSrmElement(vsmSrmElement);
                if (result == null) result = caseArchimateElement(vsmSrmElement);
                if (result == null) result = caseArchimateConcept(vsmSrmElement);
                if (result == null) result = caseArchimateModelObject(vsmSrmElement);
                if (result == null) result = caseCloneable(vsmSrmElement);
                if (result == null) result = caseDocumentable(vsmSrmElement);
                if (result == null) result = caseProperties(vsmSrmElement);
                if (result == null) result = caseAdapter(vsmSrmElement);
                if (result == null) result = caseNameable(vsmSrmElement);
                if (result == null) result = caseIdentifier(vsmSrmElement);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }             
            
            case IArchimatePackage.COMPOSITE_ELEMENT: {
                ICompositeElement compositeElement = (ICompositeElement)theEObject;
                T result = caseCompositeElement(compositeElement);
                if (result == null) result = caseArchimateElement(compositeElement);
                if (result == null) result = caseArchimateConcept(compositeElement);
                if (result == null) result = caseArchimateModelObject(compositeElement);
                if (result == null) result = caseCloneable(compositeElement);
                if (result == null) result = caseDocumentable(compositeElement);
                if (result == null) result = caseProperties(compositeElement);
                if (result == null) result = caseAdapter(compositeElement);
                if (result == null) result = caseNameable(compositeElement);
                if (result == null) result = caseIdentifier(compositeElement);
                if (result == null) result = caseFeatures(compositeElement);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.BEHAVIOR_ELEMENT: {
                IBehaviorElement behaviorElement = (IBehaviorElement)theEObject;
                T result = caseBehaviorElement(behaviorElement);
                if (result == null) result = caseArchimateElement(behaviorElement);
                if (result == null) result = caseArchimateConcept(behaviorElement);
                if (result == null) result = caseArchimateModelObject(behaviorElement);
                if (result == null) result = caseCloneable(behaviorElement);
                if (result == null) result = caseDocumentable(behaviorElement);
                if (result == null) result = caseProperties(behaviorElement);
                if (result == null) result = caseAdapter(behaviorElement);
                if (result == null) result = caseNameable(behaviorElement);
                if (result == null) result = caseIdentifier(behaviorElement);
                if (result == null) result = caseFeatures(behaviorElement);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.STRATEGY_BEHAVIOR_ELEMENT: {
                IStrategyBehaviorElement strategyBehaviorElement = (IStrategyBehaviorElement)theEObject;
                T result = caseStrategyBehaviorElement(strategyBehaviorElement);
                if (result == null) result = caseBehaviorElement(strategyBehaviorElement);
                if (result == null) result = caseStrategyElement(strategyBehaviorElement);
                if (result == null) result = caseArchimateElement(strategyBehaviorElement);
                if (result == null) result = caseArchimateConcept(strategyBehaviorElement);
                if (result == null) result = caseArchimateModelObject(strategyBehaviorElement);
                if (result == null) result = caseCloneable(strategyBehaviorElement);
                if (result == null) result = caseDocumentable(strategyBehaviorElement);
                if (result == null) result = caseProperties(strategyBehaviorElement);
                if (result == null) result = caseAdapter(strategyBehaviorElement);
                if (result == null) result = caseNameable(strategyBehaviorElement);
                if (result == null) result = caseIdentifier(strategyBehaviorElement);
                if (result == null) result = caseFeatures(strategyBehaviorElement);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.STRUCTURE_ELEMENT: {
                IStructureElement structureElement = (IStructureElement)theEObject;
                T result = caseStructureElement(structureElement);
                if (result == null) result = caseArchimateElement(structureElement);
                if (result == null) result = caseArchimateConcept(structureElement);
                if (result == null) result = caseArchimateModelObject(structureElement);
                if (result == null) result = caseCloneable(structureElement);
                if (result == null) result = caseDocumentable(structureElement);
                if (result == null) result = caseProperties(structureElement);
                if (result == null) result = caseAdapter(structureElement);
                if (result == null) result = caseNameable(structureElement);
                if (result == null) result = caseIdentifier(structureElement);
                if (result == null) result = caseFeatures(structureElement);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.ACTIVE_STRUCTURE_ELEMENT: {
                IActiveStructureElement activeStructureElement = (IActiveStructureElement)theEObject;
                T result = caseActiveStructureElement(activeStructureElement);
                if (result == null) result = caseStructureElement(activeStructureElement);
                if (result == null) result = caseArchimateElement(activeStructureElement);
                if (result == null) result = caseArchimateConcept(activeStructureElement);
                if (result == null) result = caseArchimateModelObject(activeStructureElement);
                if (result == null) result = caseCloneable(activeStructureElement);
                if (result == null) result = caseDocumentable(activeStructureElement);
                if (result == null) result = caseProperties(activeStructureElement);
                if (result == null) result = caseAdapter(activeStructureElement);
                if (result == null) result = caseNameable(activeStructureElement);
                if (result == null) result = caseIdentifier(activeStructureElement);
                if (result == null) result = caseFeatures(activeStructureElement);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.PASSIVE_STRUCTURE_ELEMENT: {
                IPassiveStructureElement passiveStructureElement = (IPassiveStructureElement)theEObject;
                T result = casePassiveStructureElement(passiveStructureElement);
                if (result == null) result = caseStructureElement(passiveStructureElement);
                if (result == null) result = caseArchimateElement(passiveStructureElement);
                if (result == null) result = caseArchimateConcept(passiveStructureElement);
                if (result == null) result = caseArchimateModelObject(passiveStructureElement);
                if (result == null) result = caseCloneable(passiveStructureElement);
                if (result == null) result = caseDocumentable(passiveStructureElement);
                if (result == null) result = caseProperties(passiveStructureElement);
                if (result == null) result = caseAdapter(passiveStructureElement);
                if (result == null) result = caseNameable(passiveStructureElement);
                if (result == null) result = caseIdentifier(passiveStructureElement);
                if (result == null) result = caseFeatures(passiveStructureElement);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.STRUCTURAL_RELATIONSHIP: {
                IStructuralRelationship structuralRelationship = (IStructuralRelationship)theEObject;
                T result = caseStructuralRelationship(structuralRelationship);
                if (result == null) result = caseArchimateRelationship(structuralRelationship);
                if (result == null) result = caseArchimateConcept(structuralRelationship);
                if (result == null) result = caseArchimateModelObject(structuralRelationship);
                if (result == null) result = caseCloneable(structuralRelationship);
                if (result == null) result = caseDocumentable(structuralRelationship);
                if (result == null) result = caseProperties(structuralRelationship);
                if (result == null) result = caseAdapter(structuralRelationship);
                if (result == null) result = caseNameable(structuralRelationship);
                if (result == null) result = caseIdentifier(structuralRelationship);
                if (result == null) result = caseFeatures(structuralRelationship);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.DEPENDENDENCY_RELATIONSHIP: {
                IDependendencyRelationship dependendencyRelationship = (IDependendencyRelationship)theEObject;
                T result = caseDependendencyRelationship(dependendencyRelationship);
                if (result == null) result = caseArchimateRelationship(dependendencyRelationship);
                if (result == null) result = caseArchimateConcept(dependendencyRelationship);
                if (result == null) result = caseArchimateModelObject(dependendencyRelationship);
                if (result == null) result = caseCloneable(dependendencyRelationship);
                if (result == null) result = caseDocumentable(dependendencyRelationship);
                if (result == null) result = caseProperties(dependendencyRelationship);
                if (result == null) result = caseAdapter(dependendencyRelationship);
                if (result == null) result = caseNameable(dependendencyRelationship);
                if (result == null) result = caseIdentifier(dependendencyRelationship);
                if (result == null) result = caseFeatures(dependendencyRelationship);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.DYNAMIC_RELATIONSHIP: {
                IDynamicRelationship dynamicRelationship = (IDynamicRelationship)theEObject;
                T result = caseDynamicRelationship(dynamicRelationship);
                if (result == null) result = caseArchimateRelationship(dynamicRelationship);
                if (result == null) result = caseArchimateConcept(dynamicRelationship);
                if (result == null) result = caseArchimateModelObject(dynamicRelationship);
                if (result == null) result = caseCloneable(dynamicRelationship);
                if (result == null) result = caseDocumentable(dynamicRelationship);
                if (result == null) result = caseProperties(dynamicRelationship);
                if (result == null) result = caseAdapter(dynamicRelationship);
                if (result == null) result = caseNameable(dynamicRelationship);
                if (result == null) result = caseIdentifier(dynamicRelationship);
                if (result == null) result = caseFeatures(dynamicRelationship);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.OTHER_RELATIONSHIP: {
                IOtherRelationship otherRelationship = (IOtherRelationship)theEObject;
                T result = caseOtherRelationship(otherRelationship);
                if (result == null) result = caseArchimateRelationship(otherRelationship);
                if (result == null) result = caseArchimateConcept(otherRelationship);
                if (result == null) result = caseArchimateModelObject(otherRelationship);
                if (result == null) result = caseCloneable(otherRelationship);
                if (result == null) result = caseDocumentable(otherRelationship);
                if (result == null) result = caseProperties(otherRelationship);
                if (result == null) result = caseAdapter(otherRelationship);
                if (result == null) result = caseNameable(otherRelationship);
                if (result == null) result = caseIdentifier(otherRelationship);
                if (result == null) result = caseFeatures(otherRelationship);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.ARCHIMATE_MODEL: {
                IArchimateModel archimateModel = (IArchimateModel)theEObject;
                T result = caseArchimateModel(archimateModel);
                if (result == null) result = caseFolderContainer(archimateModel);
                if (result == null) result = caseArchimateModelObject(archimateModel);
                if (result == null) result = caseProperties(archimateModel);
                if (result == null) result = caseAdapter(archimateModel);
                if (result == null) result = caseNameable(archimateModel);
                if (result == null) result = caseIdentifier(archimateModel);
                if (result == null) result = caseFeatures(archimateModel);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.JUNCTION: {
                IJunction junction = (IJunction)theEObject;
                T result = caseJunction(junction);
                if (result == null) result = caseArchimateElement(junction);
                if (result == null) result = caseArchimateConcept(junction);
                if (result == null) result = caseArchimateModelObject(junction);
                if (result == null) result = caseCloneable(junction);
                if (result == null) result = caseDocumentable(junction);
                if (result == null) result = caseProperties(junction);
                if (result == null) result = caseAdapter(junction);
                if (result == null) result = caseNameable(junction);
                if (result == null) result = caseIdentifier(junction);
                if (result == null) result = caseFeatures(junction);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.APPLICATION_COLLABORATION: {
                IApplicationCollaboration applicationCollaboration = (IApplicationCollaboration)theEObject;
                T result = caseApplicationCollaboration(applicationCollaboration);
                if (result == null) result = caseApplicationElement(applicationCollaboration);
                if (result == null) result = caseActiveStructureElement(applicationCollaboration);
                if (result == null) result = caseStructureElement(applicationCollaboration);
                if (result == null) result = caseArchimateElement(applicationCollaboration);
                if (result == null) result = caseArchimateConcept(applicationCollaboration);
                if (result == null) result = caseArchimateModelObject(applicationCollaboration);
                if (result == null) result = caseCloneable(applicationCollaboration);
                if (result == null) result = caseDocumentable(applicationCollaboration);
                if (result == null) result = caseProperties(applicationCollaboration);
                if (result == null) result = caseAdapter(applicationCollaboration);
                if (result == null) result = caseNameable(applicationCollaboration);
                if (result == null) result = caseIdentifier(applicationCollaboration);
                if (result == null) result = caseFeatures(applicationCollaboration);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.APPLICATION_COMPONENT: {
                IApplicationComponent applicationComponent = (IApplicationComponent)theEObject;
                T result = caseApplicationComponent(applicationComponent);
                if (result == null) result = caseApplicationElement(applicationComponent);
                if (result == null) result = caseActiveStructureElement(applicationComponent);
                if (result == null) result = caseStructureElement(applicationComponent);
                if (result == null) result = caseArchimateElement(applicationComponent);
                if (result == null) result = caseArchimateConcept(applicationComponent);
                if (result == null) result = caseArchimateModelObject(applicationComponent);
                if (result == null) result = caseCloneable(applicationComponent);
                if (result == null) result = caseDocumentable(applicationComponent);
                if (result == null) result = caseProperties(applicationComponent);
                if (result == null) result = caseAdapter(applicationComponent);
                if (result == null) result = caseNameable(applicationComponent);
                if (result == null) result = caseIdentifier(applicationComponent);
                if (result == null) result = caseFeatures(applicationComponent);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.APPLICATION_EVENT: {
                IApplicationEvent applicationEvent = (IApplicationEvent)theEObject;
                T result = caseApplicationEvent(applicationEvent);
                if (result == null) result = caseApplicationElement(applicationEvent);
                if (result == null) result = caseBehaviorElement(applicationEvent);
                if (result == null) result = caseArchimateElement(applicationEvent);
                if (result == null) result = caseArchimateConcept(applicationEvent);
                if (result == null) result = caseArchimateModelObject(applicationEvent);
                if (result == null) result = caseCloneable(applicationEvent);
                if (result == null) result = caseDocumentable(applicationEvent);
                if (result == null) result = caseProperties(applicationEvent);
                if (result == null) result = caseAdapter(applicationEvent);
                if (result == null) result = caseNameable(applicationEvent);
                if (result == null) result = caseIdentifier(applicationEvent);
                if (result == null) result = caseFeatures(applicationEvent);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.APPLICATION_FUNCTION: {
                IApplicationFunction applicationFunction = (IApplicationFunction)theEObject;
                T result = caseApplicationFunction(applicationFunction);
                if (result == null) result = caseApplicationElement(applicationFunction);
                if (result == null) result = caseBehaviorElement(applicationFunction);
                if (result == null) result = caseArchimateElement(applicationFunction);
                if (result == null) result = caseArchimateConcept(applicationFunction);
                if (result == null) result = caseArchimateModelObject(applicationFunction);
                if (result == null) result = caseCloneable(applicationFunction);
                if (result == null) result = caseDocumentable(applicationFunction);
                if (result == null) result = caseProperties(applicationFunction);
                if (result == null) result = caseAdapter(applicationFunction);
                if (result == null) result = caseNameable(applicationFunction);
                if (result == null) result = caseIdentifier(applicationFunction);
                if (result == null) result = caseFeatures(applicationFunction);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.VSM_VIA_B2A_UI: {
                IVsmViaB2aUi vSMViaB2AUI = (IVsmViaB2aUi)theEObject;
                T result = caseVsmViaB2aUi(vSMViaB2AUI);
                if (result == null) result = caseVsmViaElement(vSMViaB2AUI);
                if (result == null) result = caseBehaviorElement(vSMViaB2AUI);
                if (result == null) result = caseArchimateElement(vSMViaB2AUI);
                if (result == null) result = caseArchimateConcept(vSMViaB2AUI);
                if (result == null) result = caseArchimateModelObject(vSMViaB2AUI);
                if (result == null) result = caseCloneable(vSMViaB2AUI);
                if (result == null) result = caseDocumentable(vSMViaB2AUI);
                if (result == null) result = caseProperties(vSMViaB2AUI);
                if (result == null) result = caseAdapter(vSMViaB2AUI);
                if (result == null) result = caseNameable(vSMViaB2AUI);
                if (result == null) result = caseIdentifier(vSMViaB2AUI);
                if (result == null) result = caseFeatures(vSMViaB2AUI);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.VSM_VIA_B2A_INTEGRATION: {
                IVsmViaB2aIntegration vSMViaB2AIntegration = (IVsmViaB2aIntegration)theEObject;
                T result = caseVsmViaB2aIntegration(vSMViaB2AIntegration);
                if (result == null) result = caseVsmViaElement(vSMViaB2AIntegration);
                if (result == null) result = caseBehaviorElement(vSMViaB2AIntegration);
                if (result == null) result = caseArchimateElement(vSMViaB2AIntegration);
                if (result == null) result = caseArchimateConcept(vSMViaB2AIntegration);
                if (result == null) result = caseArchimateModelObject(vSMViaB2AIntegration);
                if (result == null) result = caseCloneable(vSMViaB2AIntegration);
                if (result == null) result = caseDocumentable(vSMViaB2AIntegration);
                if (result == null) result = caseProperties(vSMViaB2AIntegration);
                if (result == null) result = caseAdapter(vSMViaB2AIntegration);
                if (result == null) result = caseNameable(vSMViaB2AIntegration);
                if (result == null) result = caseIdentifier(vSMViaB2AIntegration);
                if (result == null) result = caseFeatures(vSMViaB2AIntegration);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.VSM_VIA_B2A_DS: {
                IVsmViaB2aDs vSMViaB2ADS = (IVsmViaB2aDs)theEObject;
                T result = caseVsmViaB2aDs(vSMViaB2ADS);
                if (result == null) result = caseVsmViaElement(vSMViaB2ADS);
                if (result == null) result = caseBehaviorElement(vSMViaB2ADS);
                if (result == null) result = caseArchimateElement(vSMViaB2ADS);
                if (result == null) result = caseArchimateConcept(vSMViaB2ADS);
                if (result == null) result = caseArchimateModelObject(vSMViaB2ADS);
                if (result == null) result = caseCloneable(vSMViaB2ADS);
                if (result == null) result = caseDocumentable(vSMViaB2ADS);
                if (result == null) result = caseProperties(vSMViaB2ADS);
                if (result == null) result = caseAdapter(vSMViaB2ADS);
                if (result == null) result = caseNameable(vSMViaB2ADS);
                if (result == null) result = caseIdentifier(vSMViaB2ADS);
                if (result == null) result = caseFeatures(vSMViaB2ADS);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.VSM_VIA_A2D_DATA: {
                IVsmViaA2dData vSMViaA2DData = (IVsmViaA2dData)theEObject;
                T result = caseVsmViaA2dData(vSMViaA2DData);
                if (result == null) result = caseVsmViaElement(vSMViaA2DData);
                if (result == null) result = caseBehaviorElement(vSMViaA2DData);
                if (result == null) result = caseArchimateElement(vSMViaA2DData);
                if (result == null) result = caseArchimateConcept(vSMViaA2DData);
                if (result == null) result = caseArchimateModelObject(vSMViaA2DData);
                if (result == null) result = caseCloneable(vSMViaA2DData);
                if (result == null) result = caseDocumentable(vSMViaA2DData);
                if (result == null) result = caseProperties(vSMViaA2DData);
                if (result == null) result = caseAdapter(vSMViaA2DData);
                if (result == null) result = caseNameable(vSMViaA2DData);
                if (result == null) result = caseIdentifier(vSMViaA2DData);
                if (result == null) result = caseFeatures(vSMViaA2DData);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.VSM_VIA_A2T_YAML: {
                IVsmViaA2tYaml vSMViaA2TTech = (IVsmViaA2tYaml)theEObject;
                T result = caseVsmViaA2tYaml(vSMViaA2TTech);
                if (result == null) result = caseVsmViaElement(vSMViaA2TTech);
                if (result == null) result = caseBehaviorElement(vSMViaA2TTech);
                if (result == null) result = caseArchimateElement(vSMViaA2TTech);
                if (result == null) result = caseArchimateConcept(vSMViaA2TTech);
                if (result == null) result = caseArchimateModelObject(vSMViaA2TTech);
                if (result == null) result = caseCloneable(vSMViaA2TTech);
                if (result == null) result = caseDocumentable(vSMViaA2TTech);
                if (result == null) result = caseProperties(vSMViaA2TTech);
                if (result == null) result = caseAdapter(vSMViaA2TTech);
                if (result == null) result = caseNameable(vSMViaA2TTech);
                if (result == null) result = caseIdentifier(vSMViaA2TTech);
                if (result == null) result = caseFeatures(vSMViaA2TTech);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.VSM_APPLICATION_TASK: {
                IVsmApplicationTask vSMApplicationTask = (IVsmApplicationTask)theEObject;
                T result = caseVsmApplicationTask(vSMApplicationTask);
                if (result == null) result = caseVsmApplicationElement(vSMApplicationTask);
                if (result == null) result = caseBehaviorElement(vSMApplicationTask);
                if (result == null) result = caseArchimateElement(vSMApplicationTask);
                if (result == null) result = caseArchimateConcept(vSMApplicationTask);
                if (result == null) result = caseArchimateModelObject(vSMApplicationTask);
                if (result == null) result = caseCloneable(vSMApplicationTask);
                if (result == null) result = caseDocumentable(vSMApplicationTask);
                if (result == null) result = caseProperties(vSMApplicationTask);
                if (result == null) result = caseAdapter(vSMApplicationTask);
                if (result == null) result = caseNameable(vSMApplicationTask);
                if (result == null) result = caseIdentifier(vSMApplicationTask);
                if (result == null) result = caseFeatures(vSMApplicationTask);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.VSM_DATA_GITREPO: {
                IVsmDataGitRepo vSMDataGitRepo = (IVsmDataGitRepo)theEObject;
                T result = caseVsmDataGitRepo(vSMDataGitRepo);
                if (result == null) result = caseVsmDataElement(vSMDataGitRepo);
                if (result == null) result = casePassiveStructureElement(vSMDataGitRepo);
                if (result == null) result = caseArchimateElement(vSMDataGitRepo);
                if (result == null) result = caseArchimateConcept(vSMDataGitRepo);
                if (result == null) result = caseArchimateModelObject(vSMDataGitRepo);
                if (result == null) result = caseCloneable(vSMDataGitRepo);
                if (result == null) result = caseDocumentable(vSMDataGitRepo);
                if (result == null) result = caseProperties(vSMDataGitRepo);
                if (result == null) result = caseAdapter(vSMDataGitRepo);
                if (result == null) result = caseNameable(vSMDataGitRepo);
                if (result == null) result = caseIdentifier(vSMDataGitRepo);
                if (result == null) result = caseFeatures(vSMDataGitRepo);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.VSM_DATA_GITBRANCH: {
                IVsmDataGitBranch vSMDataGitBranch = (IVsmDataGitBranch)theEObject;
                T result = caseVsmDataGitBranch(vSMDataGitBranch);
                if (result == null) result = caseVsmDataElement(vSMDataGitBranch);
                if (result == null) result = casePassiveStructureElement(vSMDataGitBranch);
                if (result == null) result = caseArchimateElement(vSMDataGitBranch);
                if (result == null) result = caseArchimateConcept(vSMDataGitBranch);
                if (result == null) result = caseArchimateModelObject(vSMDataGitBranch);
                if (result == null) result = caseCloneable(vSMDataGitBranch);
                if (result == null) result = caseDocumentable(vSMDataGitBranch);
                if (result == null) result = caseProperties(vSMDataGitBranch);
                if (result == null) result = caseAdapter(vSMDataGitBranch);
                if (result == null) result = caseNameable(vSMDataGitBranch);
                if (result == null) result = caseIdentifier(vSMDataGitBranch);
                if (result == null) result = caseFeatures(vSMDataGitBranch);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.VSM_DATA_SRCDIR: {
                IVsmDataSrcDir vSMDataSrcDir = (IVsmDataSrcDir)theEObject;
                T result = caseVsmDataSrcDir(vSMDataSrcDir);
                if (result == null) result = caseVsmDataElement(vSMDataSrcDir);
                if (result == null) result = casePassiveStructureElement(vSMDataSrcDir);
                if (result == null) result = caseArchimateElement(vSMDataSrcDir);
                if (result == null) result = caseArchimateConcept(vSMDataSrcDir);
                if (result == null) result = caseArchimateModelObject(vSMDataSrcDir);
                if (result == null) result = caseCloneable(vSMDataSrcDir);
                if (result == null) result = caseDocumentable(vSMDataSrcDir);
                if (result == null) result = caseProperties(vSMDataSrcDir);
                if (result == null) result = caseAdapter(vSMDataSrcDir);
                if (result == null) result = caseNameable(vSMDataSrcDir);
                if (result == null) result = caseIdentifier(vSMDataSrcDir);
                if (result == null) result = caseFeatures(vSMDataSrcDir);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.VSM_DATA_SRCFILE: {
                IVsmDataSrcFile vSMDataSrcFile = (IVsmDataSrcFile)theEObject;
                T result = caseVsmDataSrcFile(vSMDataSrcFile);
                if (result == null) result = caseVsmDataElement(vSMDataSrcFile);
                if (result == null) result = casePassiveStructureElement(vSMDataSrcFile);
                if (result == null) result = caseArchimateElement(vSMDataSrcFile);
                if (result == null) result = caseArchimateConcept(vSMDataSrcFile);
                if (result == null) result = caseArchimateModelObject(vSMDataSrcFile);
                if (result == null) result = caseCloneable(vSMDataSrcFile);
                if (result == null) result = caseDocumentable(vSMDataSrcFile);
                if (result == null) result = caseProperties(vSMDataSrcFile);
                if (result == null) result = caseAdapter(vSMDataSrcFile);
                if (result == null) result = caseNameable(vSMDataSrcFile);
                if (result == null) result = caseIdentifier(vSMDataSrcFile);
                if (result == null) result = caseFeatures(vSMDataSrcFile);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.VSM_APPLICATION_CLASS: {
                IVsmApplicationClass vSMApplicationClass = (IVsmApplicationClass)theEObject;
                T result = caseVsmApplicationClass(vSMApplicationClass);
                if (result == null) result = caseVsmApplicationElement(vSMApplicationClass);
                if (result == null) result = caseBehaviorElement(vSMApplicationClass);
                if (result == null) result = caseArchimateElement(vSMApplicationClass);
                if (result == null) result = caseArchimateConcept(vSMApplicationClass);
                if (result == null) result = caseArchimateModelObject(vSMApplicationClass);
                if (result == null) result = caseCloneable(vSMApplicationClass);
                if (result == null) result = caseDocumentable(vSMApplicationClass);
                if (result == null) result = caseProperties(vSMApplicationClass);
                if (result == null) result = caseAdapter(vSMApplicationClass);
                if (result == null) result = caseNameable(vSMApplicationClass);
                if (result == null) result = caseIdentifier(vSMApplicationClass);
                if (result == null) result = caseFeatures(vSMApplicationClass);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            
            case IArchimatePackage.VSM_APPLICATION_MODULE: {
                IVsmApplicationModule vSMApplicationModule = (IVsmApplicationModule)theEObject;
                T result = caseVsmApplicationModule(vSMApplicationModule);
                if (result == null) result = caseVsmApplicationElement(vSMApplicationModule);
                if (result == null) result = caseBehaviorElement(vSMApplicationModule);
                if (result == null) result = caseArchimateElement(vSMApplicationModule);
                if (result == null) result = caseArchimateConcept(vSMApplicationModule);
                if (result == null) result = caseArchimateModelObject(vSMApplicationModule);
                if (result == null) result = caseCloneable(vSMApplicationModule);
                if (result == null) result = caseDocumentable(vSMApplicationModule);
                if (result == null) result = caseProperties(vSMApplicationModule);
                if (result == null) result = caseAdapter(vSMApplicationModule);
                if (result == null) result = caseNameable(vSMApplicationModule);
                if (result == null) result = caseIdentifier(vSMApplicationModule);
                if (result == null) result = caseFeatures(vSMApplicationModule);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.VSM_APPLICATION_METHOD: {
                IVsmApplicationMethod vSMApplicationMethod = (IVsmApplicationMethod)theEObject;
                T result = caseVsmApplicationMethod(vSMApplicationMethod);
                if (result == null) result = caseVsmApplicationElement(vSMApplicationMethod);
                if (result == null) result = caseBehaviorElement(vSMApplicationMethod);
                if (result == null) result = caseArchimateElement(vSMApplicationMethod);
                if (result == null) result = caseArchimateConcept(vSMApplicationMethod);
                if (result == null) result = caseArchimateModelObject(vSMApplicationMethod);
                if (result == null) result = caseCloneable(vSMApplicationMethod);
                if (result == null) result = caseDocumentable(vSMApplicationMethod);
                if (result == null) result = caseProperties(vSMApplicationMethod);
                if (result == null) result = caseAdapter(vSMApplicationMethod);
                if (result == null) result = caseNameable(vSMApplicationMethod);
                if (result == null) result = caseIdentifier(vSMApplicationMethod);
                if (result == null) result = caseFeatures(vSMApplicationMethod);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.APPLICATION_INTERACTION: {
                IApplicationInteraction applicationInteraction = (IApplicationInteraction)theEObject;
                T result = caseApplicationInteraction(applicationInteraction);
                if (result == null) result = caseApplicationElement(applicationInteraction);
                if (result == null) result = caseBehaviorElement(applicationInteraction);
                if (result == null) result = caseArchimateElement(applicationInteraction);
                if (result == null) result = caseArchimateConcept(applicationInteraction);
                if (result == null) result = caseArchimateModelObject(applicationInteraction);
                if (result == null) result = caseCloneable(applicationInteraction);
                if (result == null) result = caseDocumentable(applicationInteraction);
                if (result == null) result = caseProperties(applicationInteraction);
                if (result == null) result = caseAdapter(applicationInteraction);
                if (result == null) result = caseNameable(applicationInteraction);
                if (result == null) result = caseIdentifier(applicationInteraction);
                if (result == null) result = caseFeatures(applicationInteraction);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.APPLICATION_INTERFACE: {
                IApplicationInterface applicationInterface = (IApplicationInterface)theEObject;
                T result = caseApplicationInterface(applicationInterface);
                if (result == null) result = caseApplicationElement(applicationInterface);
                if (result == null) result = caseActiveStructureElement(applicationInterface);
                if (result == null) result = caseStructureElement(applicationInterface);
                if (result == null) result = caseArchimateElement(applicationInterface);
                if (result == null) result = caseArchimateConcept(applicationInterface);
                if (result == null) result = caseArchimateModelObject(applicationInterface);
                if (result == null) result = caseCloneable(applicationInterface);
                if (result == null) result = caseDocumentable(applicationInterface);
                if (result == null) result = caseProperties(applicationInterface);
                if (result == null) result = caseAdapter(applicationInterface);
                if (result == null) result = caseNameable(applicationInterface);
                if (result == null) result = caseIdentifier(applicationInterface);
                if (result == null) result = caseFeatures(applicationInterface);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.APPLICATION_PROCESS: {
                IApplicationProcess applicationProcess = (IApplicationProcess)theEObject;
                T result = caseApplicationProcess(applicationProcess);
                if (result == null) result = caseApplicationElement(applicationProcess);
                if (result == null) result = caseBehaviorElement(applicationProcess);
                if (result == null) result = caseArchimateElement(applicationProcess);
                if (result == null) result = caseArchimateConcept(applicationProcess);
                if (result == null) result = caseArchimateModelObject(applicationProcess);
                if (result == null) result = caseCloneable(applicationProcess);
                if (result == null) result = caseDocumentable(applicationProcess);
                if (result == null) result = caseProperties(applicationProcess);
                if (result == null) result = caseAdapter(applicationProcess);
                if (result == null) result = caseNameable(applicationProcess);
                if (result == null) result = caseIdentifier(applicationProcess);
                if (result == null) result = caseFeatures(applicationProcess);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.APPLICATION_SERVICE: {
                IApplicationService applicationService = (IApplicationService)theEObject;
                T result = caseApplicationService(applicationService);
                if (result == null) result = caseApplicationElement(applicationService);
                if (result == null) result = caseBehaviorElement(applicationService);
                if (result == null) result = caseArchimateElement(applicationService);
                if (result == null) result = caseArchimateConcept(applicationService);
                if (result == null) result = caseArchimateModelObject(applicationService);
                if (result == null) result = caseCloneable(applicationService);
                if (result == null) result = caseDocumentable(applicationService);
                if (result == null) result = caseProperties(applicationService);
                if (result == null) result = caseAdapter(applicationService);
                if (result == null) result = caseNameable(applicationService);
                if (result == null) result = caseIdentifier(applicationService);
                if (result == null) result = caseFeatures(applicationService);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.ARTIFACT: {
                IArtifact artifact = (IArtifact)theEObject;
                T result = caseArtifact(artifact);
                if (result == null) result = caseTechnologyObject(artifact);
                if (result == null) result = caseTechnologyElement(artifact);
                if (result == null) result = casePassiveStructureElement(artifact);
                if (result == null) result = caseStructureElement(artifact);
                if (result == null) result = caseArchimateElement(artifact);
                if (result == null) result = caseArchimateConcept(artifact);
                if (result == null) result = caseArchimateModelObject(artifact);
                if (result == null) result = caseCloneable(artifact);
                if (result == null) result = caseDocumentable(artifact);
                if (result == null) result = caseProperties(artifact);
                if (result == null) result = caseAdapter(artifact);
                if (result == null) result = caseNameable(artifact);
                if (result == null) result = caseIdentifier(artifact);
                if (result == null) result = caseFeatures(artifact);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.ASSESSMENT: {
                IAssessment assessment = (IAssessment)theEObject;
                T result = caseAssessment(assessment);
                if (result == null) result = caseMotivationElement(assessment);
                if (result == null) result = caseArchimateElement(assessment);
                if (result == null) result = caseArchimateConcept(assessment);
                if (result == null) result = caseArchimateModelObject(assessment);
                if (result == null) result = caseCloneable(assessment);
                if (result == null) result = caseDocumentable(assessment);
                if (result == null) result = caseProperties(assessment);
                if (result == null) result = caseAdapter(assessment);
                if (result == null) result = caseNameable(assessment);
                if (result == null) result = caseIdentifier(assessment);
                if (result == null) result = caseFeatures(assessment);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.BUSINESS_ACTOR: {
                IBusinessActor businessActor = (IBusinessActor)theEObject;
                T result = caseBusinessActor(businessActor);
                if (result == null) result = caseBusinessElement(businessActor);
                if (result == null) result = caseActiveStructureElement(businessActor);
                if (result == null) result = caseStructureElement(businessActor);
                if (result == null) result = caseArchimateElement(businessActor);
                if (result == null) result = caseArchimateConcept(businessActor);
                if (result == null) result = caseArchimateModelObject(businessActor);
                if (result == null) result = caseCloneable(businessActor);
                if (result == null) result = caseDocumentable(businessActor);
                if (result == null) result = caseProperties(businessActor);
                if (result == null) result = caseAdapter(businessActor);
                if (result == null) result = caseNameable(businessActor);
                if (result == null) result = caseIdentifier(businessActor);
                if (result == null) result = caseFeatures(businessActor);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.BUSINESS_COLLABORATION: {
                IBusinessCollaboration businessCollaboration = (IBusinessCollaboration)theEObject;
                T result = caseBusinessCollaboration(businessCollaboration);
                if (result == null) result = caseBusinessElement(businessCollaboration);
                if (result == null) result = caseActiveStructureElement(businessCollaboration);
                if (result == null) result = caseStructureElement(businessCollaboration);
                if (result == null) result = caseArchimateElement(businessCollaboration);
                if (result == null) result = caseArchimateConcept(businessCollaboration);
                if (result == null) result = caseArchimateModelObject(businessCollaboration);
                if (result == null) result = caseCloneable(businessCollaboration);
                if (result == null) result = caseDocumentable(businessCollaboration);
                if (result == null) result = caseProperties(businessCollaboration);
                if (result == null) result = caseAdapter(businessCollaboration);
                if (result == null) result = caseNameable(businessCollaboration);
                if (result == null) result = caseIdentifier(businessCollaboration);
                if (result == null) result = caseFeatures(businessCollaboration);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.BUSINESS_EVENT: {
                IBusinessEvent businessEvent = (IBusinessEvent)theEObject;
                T result = caseBusinessEvent(businessEvent);
                if (result == null) result = caseBusinessElement(businessEvent);
                if (result == null) result = caseBehaviorElement(businessEvent);
                if (result == null) result = caseArchimateElement(businessEvent);
                if (result == null) result = caseArchimateConcept(businessEvent);
                if (result == null) result = caseArchimateModelObject(businessEvent);
                if (result == null) result = caseCloneable(businessEvent);
                if (result == null) result = caseDocumentable(businessEvent);
                if (result == null) result = caseProperties(businessEvent);
                if (result == null) result = caseAdapter(businessEvent);
                if (result == null) result = caseNameable(businessEvent);
                if (result == null) result = caseIdentifier(businessEvent);
                if (result == null) result = caseFeatures(businessEvent);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.BUSINESS_FUNCTION: {
                IBusinessFunction businessFunction = (IBusinessFunction)theEObject;
                T result = caseBusinessFunction(businessFunction);
                if (result == null) result = caseBusinessElement(businessFunction);
                if (result == null) result = caseBehaviorElement(businessFunction);
                if (result == null) result = caseArchimateElement(businessFunction);
                if (result == null) result = caseArchimateConcept(businessFunction);
                if (result == null) result = caseArchimateModelObject(businessFunction);
                if (result == null) result = caseCloneable(businessFunction);
                if (result == null) result = caseDocumentable(businessFunction);
                if (result == null) result = caseProperties(businessFunction);
                if (result == null) result = caseAdapter(businessFunction);
                if (result == null) result = caseNameable(businessFunction);
                if (result == null) result = caseIdentifier(businessFunction);
                if (result == null) result = caseFeatures(businessFunction);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.VSM_BUSINESS_EPIC: {
                IVsmBusinessEpic vSMBusinessEpic = (IVsmBusinessEpic)theEObject;
                T result = caseVsmBusinessEpic(vSMBusinessEpic);
                if (result == null) result = caseVsmBusinessElement(vSMBusinessEpic);
                if (result == null) result = caseBehaviorElement(vSMBusinessEpic);
                if (result == null) result = caseArchimateElement(vSMBusinessEpic);
                if (result == null) result = caseArchimateConcept(vSMBusinessEpic);
                if (result == null) result = caseArchimateModelObject(vSMBusinessEpic);
                if (result == null) result = caseCloneable(vSMBusinessEpic);
                if (result == null) result = caseDocumentable(vSMBusinessEpic);
                if (result == null) result = caseProperties(vSMBusinessEpic);
                if (result == null) result = caseAdapter(vSMBusinessEpic);
                if (result == null) result = caseNameable(vSMBusinessEpic);
                if (result == null) result = caseIdentifier(vSMBusinessEpic);
                if (result == null) result = caseFeatures(vSMBusinessEpic);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.VSM_BUSINESS_US: {
                IVsmBusinessUs vSMBusinessUS = (IVsmBusinessUs)theEObject;
                T result = caseVsmBusinessUs(vSMBusinessUS);
                if (result == null) result = caseVsmBusinessElement(vSMBusinessUS);
                if (result == null) result = caseBehaviorElement(vSMBusinessUS);
                if (result == null) result = caseArchimateElement(vSMBusinessUS);
                if (result == null) result = caseArchimateConcept(vSMBusinessUS);
                if (result == null) result = caseArchimateModelObject(vSMBusinessUS);
                if (result == null) result = caseCloneable(vSMBusinessUS);
                if (result == null) result = caseDocumentable(vSMBusinessUS);
                if (result == null) result = caseProperties(vSMBusinessUS);
                if (result == null) result = caseAdapter(vSMBusinessUS);
                if (result == null) result = caseNameable(vSMBusinessUS);
                if (result == null) result = caseIdentifier(vSMBusinessUS);
                if (result == null) result = caseFeatures(vSMBusinessUS);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.VSM_BUSINESS_TASK: {
                IVsmBusinessTask vSMBusinessTask = (IVsmBusinessTask)theEObject;
                T result = caseVsmBusinessTask(vSMBusinessTask);
                if (result == null) result = caseVsmBusinessElement(vSMBusinessTask);
                if (result == null) result = caseBehaviorElement(vSMBusinessTask);
                if (result == null) result = caseArchimateElement(vSMBusinessTask);
                if (result == null) result = caseArchimateConcept(vSMBusinessTask);
                if (result == null) result = caseArchimateModelObject(vSMBusinessTask);
                if (result == null) result = caseCloneable(vSMBusinessTask);
                if (result == null) result = caseDocumentable(vSMBusinessTask);
                if (result == null) result = caseProperties(vSMBusinessTask);
                if (result == null) result = caseAdapter(vSMBusinessTask);
                if (result == null) result = caseNameable(vSMBusinessTask);
                if (result == null) result = caseIdentifier(vSMBusinessTask);
                if (result == null) result = caseFeatures(vSMBusinessTask);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.VSM_BUSINESS_AC: {
                IVsmBusinessAc vSMBusinessAC = (IVsmBusinessAc)theEObject;
                T result = caseVsmBusinessAc(vSMBusinessAC);
                if (result == null) result = caseVsmBusinessElement(vSMBusinessAC);
                if (result == null) result = caseBehaviorElement(vSMBusinessAC);
                if (result == null) result = caseArchimateElement(vSMBusinessAC);
                if (result == null) result = caseArchimateConcept(vSMBusinessAC);
                if (result == null) result = caseArchimateModelObject(vSMBusinessAC);
                if (result == null) result = caseCloneable(vSMBusinessAC);
                if (result == null) result = caseDocumentable(vSMBusinessAC);
                if (result == null) result = caseProperties(vSMBusinessAC);
                if (result == null) result = caseAdapter(vSMBusinessAC);
                if (result == null) result = caseNameable(vSMBusinessAC);
                if (result == null) result = caseIdentifier(vSMBusinessAC);
                if (result == null) result = caseFeatures(vSMBusinessAC);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.VSM_BUSINESS_UC: {
                IVsmBusinessUc vSMBusinessUC = (IVsmBusinessUc)theEObject;
                T result = caseVsmBusinessUc(vSMBusinessUC);
                if (result == null) result = caseVsmBusinessElement(vSMBusinessUC);
                if (result == null) result = caseBehaviorElement(vSMBusinessUC);
                if (result == null) result = caseArchimateElement(vSMBusinessUC);
                if (result == null) result = caseArchimateConcept(vSMBusinessUC);
                if (result == null) result = caseArchimateModelObject(vSMBusinessUC);
                if (result == null) result = caseCloneable(vSMBusinessUC);
                if (result == null) result = caseDocumentable(vSMBusinessUC);
                if (result == null) result = caseProperties(vSMBusinessUC);
                if (result == null) result = caseAdapter(vSMBusinessUC);
                if (result == null) result = caseNameable(vSMBusinessUC);
                if (result == null) result = caseIdentifier(vSMBusinessUC);
                if (result == null) result = caseFeatures(vSMBusinessUC);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.VSM_BUSINESS_FR: {
                IVsmBusinessFr vSMBusinessFR = (IVsmBusinessFr)theEObject;
                T result = caseVsmBusinessFr(vSMBusinessFR);
                if (result == null) result = caseVsmBusinessElement(vSMBusinessFR);
                if (result == null) result = caseBehaviorElement(vSMBusinessFR);
                if (result == null) result = caseArchimateElement(vSMBusinessFR);
                if (result == null) result = caseArchimateConcept(vSMBusinessFR);
                if (result == null) result = caseArchimateModelObject(vSMBusinessFR);
                if (result == null) result = caseCloneable(vSMBusinessFR);
                if (result == null) result = caseDocumentable(vSMBusinessFR);
                if (result == null) result = caseProperties(vSMBusinessFR);
                if (result == null) result = caseAdapter(vSMBusinessFR);
                if (result == null) result = caseNameable(vSMBusinessFR);
                if (result == null) result = caseIdentifier(vSMBusinessFR);
                if (result == null) result = caseFeatures(vSMBusinessFR);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.VSM_BUSINESS_NFR: {
                IVsmBusinessNfr vSMBusinessNFR = (IVsmBusinessNfr)theEObject;
                T result = caseVsmBusinessNfr(vSMBusinessNFR);
                if (result == null) result = caseVsmBusinessElement(vSMBusinessNFR);
                if (result == null) result = caseBehaviorElement(vSMBusinessNFR);
                if (result == null) result = caseArchimateElement(vSMBusinessNFR);
                if (result == null) result = caseArchimateConcept(vSMBusinessNFR);
                if (result == null) result = caseArchimateModelObject(vSMBusinessNFR);
                if (result == null) result = caseCloneable(vSMBusinessNFR);
                if (result == null) result = caseDocumentable(vSMBusinessNFR);
                if (result == null) result = caseProperties(vSMBusinessNFR);
                if (result == null) result = caseAdapter(vSMBusinessNFR);
                if (result == null) result = caseNameable(vSMBusinessNFR);
                if (result == null) result = caseIdentifier(vSMBusinessNFR);
                if (result == null) result = caseFeatures(vSMBusinessNFR);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.BUSINESS_INTERACTION: {
                IBusinessInteraction businessInteraction = (IBusinessInteraction)theEObject;
                T result = caseBusinessInteraction(businessInteraction);
                if (result == null) result = caseBusinessElement(businessInteraction);
                if (result == null) result = caseBehaviorElement(businessInteraction);
                if (result == null) result = caseArchimateElement(businessInteraction);
                if (result == null) result = caseArchimateConcept(businessInteraction);
                if (result == null) result = caseArchimateModelObject(businessInteraction);
                if (result == null) result = caseCloneable(businessInteraction);
                if (result == null) result = caseDocumentable(businessInteraction);
                if (result == null) result = caseProperties(businessInteraction);
                if (result == null) result = caseAdapter(businessInteraction);
                if (result == null) result = caseNameable(businessInteraction);
                if (result == null) result = caseIdentifier(businessInteraction);
                if (result == null) result = caseFeatures(businessInteraction);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.BUSINESS_INTERFACE: {
                IBusinessInterface businessInterface = (IBusinessInterface)theEObject;
                T result = caseBusinessInterface(businessInterface);
                if (result == null) result = caseBusinessElement(businessInterface);
                if (result == null) result = caseActiveStructureElement(businessInterface);
                if (result == null) result = caseStructureElement(businessInterface);
                if (result == null) result = caseArchimateElement(businessInterface);
                if (result == null) result = caseArchimateConcept(businessInterface);
                if (result == null) result = caseArchimateModelObject(businessInterface);
                if (result == null) result = caseCloneable(businessInterface);
                if (result == null) result = caseDocumentable(businessInterface);
                if (result == null) result = caseProperties(businessInterface);
                if (result == null) result = caseAdapter(businessInterface);
                if (result == null) result = caseNameable(businessInterface);
                if (result == null) result = caseIdentifier(businessInterface);
                if (result == null) result = caseFeatures(businessInterface);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.BUSINESS_OBJECT: {
                IBusinessObject businessObject = (IBusinessObject)theEObject;
                T result = caseBusinessObject(businessObject);
                if (result == null) result = caseBusinessElement(businessObject);
                if (result == null) result = casePassiveStructureElement(businessObject);
                if (result == null) result = caseStructureElement(businessObject);
                if (result == null) result = caseArchimateElement(businessObject);
                if (result == null) result = caseArchimateConcept(businessObject);
                if (result == null) result = caseArchimateModelObject(businessObject);
                if (result == null) result = caseCloneable(businessObject);
                if (result == null) result = caseDocumentable(businessObject);
                if (result == null) result = caseProperties(businessObject);
                if (result == null) result = caseAdapter(businessObject);
                if (result == null) result = caseNameable(businessObject);
                if (result == null) result = caseIdentifier(businessObject);
                if (result == null) result = caseFeatures(businessObject);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.BUSINESS_PROCESS: {
                IBusinessProcess businessProcess = (IBusinessProcess)theEObject;
                T result = caseBusinessProcess(businessProcess);
                if (result == null) result = caseBusinessElement(businessProcess);
                if (result == null) result = caseBehaviorElement(businessProcess);
                if (result == null) result = caseArchimateElement(businessProcess);
                if (result == null) result = caseArchimateConcept(businessProcess);
                if (result == null) result = caseArchimateModelObject(businessProcess);
                if (result == null) result = caseCloneable(businessProcess);
                if (result == null) result = caseDocumentable(businessProcess);
                if (result == null) result = caseProperties(businessProcess);
                if (result == null) result = caseAdapter(businessProcess);
                if (result == null) result = caseNameable(businessProcess);
                if (result == null) result = caseIdentifier(businessProcess);
                if (result == null) result = caseFeatures(businessProcess);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.BUSINESS_ROLE: {
                IBusinessRole businessRole = (IBusinessRole)theEObject;
                T result = caseBusinessRole(businessRole);
                if (result == null) result = caseBusinessElement(businessRole);
                if (result == null) result = caseActiveStructureElement(businessRole);
                if (result == null) result = caseStructureElement(businessRole);
                if (result == null) result = caseArchimateElement(businessRole);
                if (result == null) result = caseArchimateConcept(businessRole);
                if (result == null) result = caseArchimateModelObject(businessRole);
                if (result == null) result = caseCloneable(businessRole);
                if (result == null) result = caseDocumentable(businessRole);
                if (result == null) result = caseProperties(businessRole);
                if (result == null) result = caseAdapter(businessRole);
                if (result == null) result = caseNameable(businessRole);
                if (result == null) result = caseIdentifier(businessRole);
                if (result == null) result = caseFeatures(businessRole);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.BUSINESS_SERVICE: {
                IBusinessService businessService = (IBusinessService)theEObject;
                T result = caseBusinessService(businessService);
                if (result == null) result = caseBusinessElement(businessService);
                if (result == null) result = caseBehaviorElement(businessService);
                if (result == null) result = caseArchimateElement(businessService);
                if (result == null) result = caseArchimateConcept(businessService);
                if (result == null) result = caseArchimateModelObject(businessService);
                if (result == null) result = caseCloneable(businessService);
                if (result == null) result = caseDocumentable(businessService);
                if (result == null) result = caseProperties(businessService);
                if (result == null) result = caseAdapter(businessService);
                if (result == null) result = caseNameable(businessService);
                if (result == null) result = caseIdentifier(businessService);
                if (result == null) result = caseFeatures(businessService);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.CAPABILITY: {
                ICapability capability = (ICapability)theEObject;
                T result = caseCapability(capability);
                if (result == null) result = caseStrategyBehaviorElement(capability);
                if (result == null) result = caseBehaviorElement(capability);
                if (result == null) result = caseStrategyElement(capability);
                if (result == null) result = caseArchimateElement(capability);
                if (result == null) result = caseArchimateConcept(capability);
                if (result == null) result = caseArchimateModelObject(capability);
                if (result == null) result = caseCloneable(capability);
                if (result == null) result = caseDocumentable(capability);
                if (result == null) result = caseProperties(capability);
                if (result == null) result = caseAdapter(capability);
                if (result == null) result = caseNameable(capability);
                if (result == null) result = caseIdentifier(capability);
                if (result == null) result = caseFeatures(capability);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.COMMUNICATION_NETWORK: {
                ICommunicationNetwork communicationNetwork = (ICommunicationNetwork)theEObject;
                T result = caseCommunicationNetwork(communicationNetwork);
                if (result == null) result = caseTechnologyElement(communicationNetwork);
                if (result == null) result = caseActiveStructureElement(communicationNetwork);
                if (result == null) result = caseStructureElement(communicationNetwork);
                if (result == null) result = caseArchimateElement(communicationNetwork);
                if (result == null) result = caseArchimateConcept(communicationNetwork);
                if (result == null) result = caseArchimateModelObject(communicationNetwork);
                if (result == null) result = caseCloneable(communicationNetwork);
                if (result == null) result = caseDocumentable(communicationNetwork);
                if (result == null) result = caseProperties(communicationNetwork);
                if (result == null) result = caseAdapter(communicationNetwork);
                if (result == null) result = caseNameable(communicationNetwork);
                if (result == null) result = caseIdentifier(communicationNetwork);
                if (result == null) result = caseFeatures(communicationNetwork);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.CONTRACT: {
                IContract contract = (IContract)theEObject;
                T result = caseContract(contract);
                if (result == null) result = caseBusinessObject(contract);
                if (result == null) result = caseBusinessElement(contract);
                if (result == null) result = casePassiveStructureElement(contract);
                if (result == null) result = caseStructureElement(contract);
                if (result == null) result = caseArchimateElement(contract);
                if (result == null) result = caseArchimateConcept(contract);
                if (result == null) result = caseArchimateModelObject(contract);
                if (result == null) result = caseCloneable(contract);
                if (result == null) result = caseDocumentable(contract);
                if (result == null) result = caseProperties(contract);
                if (result == null) result = caseAdapter(contract);
                if (result == null) result = caseNameable(contract);
                if (result == null) result = caseIdentifier(contract);
                if (result == null) result = caseFeatures(contract);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.CONSTRAINT: {
                IConstraint constraint = (IConstraint)theEObject;
                T result = caseConstraint(constraint);
                if (result == null) result = caseMotivationElement(constraint);
                if (result == null) result = caseArchimateElement(constraint);
                if (result == null) result = caseArchimateConcept(constraint);
                if (result == null) result = caseArchimateModelObject(constraint);
                if (result == null) result = caseCloneable(constraint);
                if (result == null) result = caseDocumentable(constraint);
                if (result == null) result = caseProperties(constraint);
                if (result == null) result = caseAdapter(constraint);
                if (result == null) result = caseNameable(constraint);
                if (result == null) result = caseIdentifier(constraint);
                if (result == null) result = caseFeatures(constraint);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.COURSE_OF_ACTION: {
                ICourseOfAction courseOfAction = (ICourseOfAction)theEObject;
                T result = caseCourseOfAction(courseOfAction);
                if (result == null) result = caseStrategyElement(courseOfAction);
                if (result == null) result = caseBehaviorElement(courseOfAction);
                if (result == null) result = caseArchimateElement(courseOfAction);
                if (result == null) result = caseArchimateConcept(courseOfAction);
                if (result == null) result = caseArchimateModelObject(courseOfAction);
                if (result == null) result = caseCloneable(courseOfAction);
                if (result == null) result = caseDocumentable(courseOfAction);
                if (result == null) result = caseProperties(courseOfAction);
                if (result == null) result = caseAdapter(courseOfAction);
                if (result == null) result = caseNameable(courseOfAction);
                if (result == null) result = caseIdentifier(courseOfAction);
                if (result == null) result = caseFeatures(courseOfAction);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.DATA_OBJECT: {
                IDataObject dataObject = (IDataObject)theEObject;
                T result = caseDataObject(dataObject);
                if (result == null) result = caseDataElement(dataObject);
                if (result == null) result = casePassiveStructureElement(dataObject);
                if (result == null) result = caseStructureElement(dataObject);
                if (result == null) result = caseArchimateElement(dataObject);
                if (result == null) result = caseArchimateConcept(dataObject);
                if (result == null) result = caseArchimateModelObject(dataObject);
                if (result == null) result = caseCloneable(dataObject);
                if (result == null) result = caseDocumentable(dataObject);
                if (result == null) result = caseProperties(dataObject);
                if (result == null) result = caseAdapter(dataObject);
                if (result == null) result = caseNameable(dataObject);
                if (result == null) result = caseIdentifier(dataObject);
                if (result == null) result = caseFeatures(dataObject);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.DELIVERABLE: {
                IDeliverable deliverable = (IDeliverable)theEObject;
                T result = caseDeliverable(deliverable);
                if (result == null) result = caseImplementationMigrationElement(deliverable);
                if (result == null) result = casePassiveStructureElement(deliverable);
                if (result == null) result = caseStructureElement(deliverable);
                if (result == null) result = caseArchimateElement(deliverable);
                if (result == null) result = caseArchimateConcept(deliverable);
                if (result == null) result = caseArchimateModelObject(deliverable);
                if (result == null) result = caseCloneable(deliverable);
                if (result == null) result = caseDocumentable(deliverable);
                if (result == null) result = caseProperties(deliverable);
                if (result == null) result = caseAdapter(deliverable);
                if (result == null) result = caseNameable(deliverable);
                if (result == null) result = caseIdentifier(deliverable);
                if (result == null) result = caseFeatures(deliverable);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.DEVICE: {
                IDevice device = (IDevice)theEObject;
                T result = caseDevice(device);
                if (result == null) result = caseTechnologyElement(device);
                if (result == null) result = caseActiveStructureElement(device);
                if (result == null) result = caseStructureElement(device);
                if (result == null) result = caseArchimateElement(device);
                if (result == null) result = caseArchimateConcept(device);
                if (result == null) result = caseArchimateModelObject(device);
                if (result == null) result = caseCloneable(device);
                if (result == null) result = caseDocumentable(device);
                if (result == null) result = caseProperties(device);
                if (result == null) result = caseAdapter(device);
                if (result == null) result = caseNameable(device);
                if (result == null) result = caseIdentifier(device);
                if (result == null) result = caseFeatures(device);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.DISTRIBUTION_NETWORK: {
                IDistributionNetwork distributionNetwork = (IDistributionNetwork)theEObject;
                T result = caseDistributionNetwork(distributionNetwork);
                if (result == null) result = casePhysicalElement(distributionNetwork);
                if (result == null) result = caseActiveStructureElement(distributionNetwork);
                if (result == null) result = caseStructureElement(distributionNetwork);
                if (result == null) result = caseArchimateElement(distributionNetwork);
                if (result == null) result = caseArchimateConcept(distributionNetwork);
                if (result == null) result = caseArchimateModelObject(distributionNetwork);
                if (result == null) result = caseCloneable(distributionNetwork);
                if (result == null) result = caseDocumentable(distributionNetwork);
                if (result == null) result = caseProperties(distributionNetwork);
                if (result == null) result = caseAdapter(distributionNetwork);
                if (result == null) result = caseNameable(distributionNetwork);
                if (result == null) result = caseIdentifier(distributionNetwork);
                if (result == null) result = caseFeatures(distributionNetwork);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.DRIVER: {
                IDriver driver = (IDriver)theEObject;
                T result = caseDriver(driver);
                if (result == null) result = caseMotivationElement(driver);
                if (result == null) result = caseArchimateElement(driver);
                if (result == null) result = caseArchimateConcept(driver);
                if (result == null) result = caseArchimateModelObject(driver);
                if (result == null) result = caseCloneable(driver);
                if (result == null) result = caseDocumentable(driver);
                if (result == null) result = caseProperties(driver);
                if (result == null) result = caseAdapter(driver);
                if (result == null) result = caseNameable(driver);
                if (result == null) result = caseIdentifier(driver);
                if (result == null) result = caseFeatures(driver);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.EQUIPMENT: {
                IEquipment equipment = (IEquipment)theEObject;
                T result = caseEquipment(equipment);
                if (result == null) result = casePhysicalElement(equipment);
                if (result == null) result = caseActiveStructureElement(equipment);
                if (result == null) result = caseStructureElement(equipment);
                if (result == null) result = caseArchimateElement(equipment);
                if (result == null) result = caseArchimateConcept(equipment);
                if (result == null) result = caseArchimateModelObject(equipment);
                if (result == null) result = caseCloneable(equipment);
                if (result == null) result = caseDocumentable(equipment);
                if (result == null) result = caseProperties(equipment);
                if (result == null) result = caseAdapter(equipment);
                if (result == null) result = caseNameable(equipment);
                if (result == null) result = caseIdentifier(equipment);
                if (result == null) result = caseFeatures(equipment);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.FACILITY: {
                IFacility facility = (IFacility)theEObject;
                T result = caseFacility(facility);
                if (result == null) result = casePhysicalElement(facility);
                if (result == null) result = caseActiveStructureElement(facility);
                if (result == null) result = caseStructureElement(facility);
                if (result == null) result = caseArchimateElement(facility);
                if (result == null) result = caseArchimateConcept(facility);
                if (result == null) result = caseArchimateModelObject(facility);
                if (result == null) result = caseCloneable(facility);
                if (result == null) result = caseDocumentable(facility);
                if (result == null) result = caseProperties(facility);
                if (result == null) result = caseAdapter(facility);
                if (result == null) result = caseNameable(facility);
                if (result == null) result = caseIdentifier(facility);
                if (result == null) result = caseFeatures(facility);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.GAP: {
                IGap gap = (IGap)theEObject;
                T result = caseGap(gap);
                if (result == null) result = caseImplementationMigrationElement(gap);
                if (result == null) result = casePassiveStructureElement(gap);
                if (result == null) result = caseStructureElement(gap);
                if (result == null) result = caseArchimateElement(gap);
                if (result == null) result = caseArchimateConcept(gap);
                if (result == null) result = caseArchimateModelObject(gap);
                if (result == null) result = caseCloneable(gap);
                if (result == null) result = caseDocumentable(gap);
                if (result == null) result = caseProperties(gap);
                if (result == null) result = caseAdapter(gap);
                if (result == null) result = caseNameable(gap);
                if (result == null) result = caseIdentifier(gap);
                if (result == null) result = caseFeatures(gap);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            
            case IArchimatePackage.DYNAMICS_PERSONAL_FEELING: {
                IDynamicsPersonalFeeling dynamicsPersonalFeeling = (IDynamicsPersonalFeeling)theEObject;
                T result = caseDynamicsPersonalFeeling(dynamicsPersonalFeeling);
                if (result == null) result = caseDynamicsPersonalElement(dynamicsPersonalFeeling);
                if (result == null) result = caseArchimateElement(dynamicsPersonalFeeling);
                if (result == null) result = caseArchimateConcept(dynamicsPersonalFeeling);
                if (result == null) result = caseArchimateModelObject(dynamicsPersonalFeeling);
                if (result == null) result = caseCloneable(dynamicsPersonalFeeling);
                if (result == null) result = caseDocumentable(dynamicsPersonalFeeling);
                if (result == null) result = caseProperties(dynamicsPersonalFeeling);
                if (result == null) result = caseAdapter(dynamicsPersonalFeeling);
                if (result == null) result = caseNameable(dynamicsPersonalFeeling);
                if (result == null) result = caseIdentifier(dynamicsPersonalFeeling);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }   
            
            case IArchimatePackage.DYNAMICS_PERSONAL_CORRECTION: {
                IDynamicsPersonalCorrection dynamicsPersonalCorrection = (IDynamicsPersonalCorrection)theEObject;
                T result = caseDynamicsPersonalCorrection(dynamicsPersonalCorrection);
                if (result == null) result = caseDynamicsPersonalElement(dynamicsPersonalCorrection);
                if (result == null) result = caseArchimateElement(dynamicsPersonalCorrection);
                if (result == null) result = caseArchimateConcept(dynamicsPersonalCorrection);
                if (result == null) result = caseArchimateModelObject(dynamicsPersonalCorrection);
                if (result == null) result = caseCloneable(dynamicsPersonalCorrection);
                if (result == null) result = caseDocumentable(dynamicsPersonalCorrection);
                if (result == null) result = caseProperties(dynamicsPersonalCorrection);
                if (result == null) result = caseAdapter(dynamicsPersonalCorrection);
                if (result == null) result = caseNameable(dynamicsPersonalCorrection);
                if (result == null) result = caseIdentifier(dynamicsPersonalCorrection);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }   
            
            
            case IArchimatePackage.DYNAMICS_PERSONAL_PREDICTION: {
                IDynamicsPersonalPrediction dynamicsPersonalPrediction = (IDynamicsPersonalPrediction)theEObject;
                T result = caseDynamicsPersonalPrediction(dynamicsPersonalPrediction);
                if (result == null) result = caseDynamicsPersonalElement(dynamicsPersonalPrediction);
                if (result == null) result = caseArchimateElement(dynamicsPersonalPrediction);
                if (result == null) result = caseArchimateConcept(dynamicsPersonalPrediction);
                if (result == null) result = caseArchimateModelObject(dynamicsPersonalPrediction);
                if (result == null) result = caseCloneable(dynamicsPersonalPrediction);
                if (result == null) result = caseDocumentable(dynamicsPersonalPrediction);
                if (result == null) result = caseProperties(dynamicsPersonalPrediction);
                if (result == null) result = caseAdapter(dynamicsPersonalPrediction);
                if (result == null) result = caseNameable(dynamicsPersonalPrediction);
                if (result == null) result = caseIdentifier(dynamicsPersonalPrediction);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }   
            
            case IArchimatePackage.DYNAMICS_PERSONAL_BYPRODUCT: {
                IDynamicsPersonalByproduct dynamicsPersonalByproduct = (IDynamicsPersonalByproduct)theEObject;
                T result = caseDynamicsPersonalByproduct(dynamicsPersonalByproduct);
                if (result == null) result = caseDynamicsPersonalElement(dynamicsPersonalByproduct);
                if (result == null) result = caseArchimateElement(dynamicsPersonalByproduct);
                if (result == null) result = caseArchimateConcept(dynamicsPersonalByproduct);
                if (result == null) result = caseArchimateModelObject(dynamicsPersonalByproduct);
                if (result == null) result = caseCloneable(dynamicsPersonalByproduct);
                if (result == null) result = caseDocumentable(dynamicsPersonalByproduct);
                if (result == null) result = caseProperties(dynamicsPersonalByproduct);
                if (result == null) result = caseAdapter(dynamicsPersonalByproduct);
                if (result == null) result = caseNameable(dynamicsPersonalByproduct);
                if (result == null) result = caseIdentifier(dynamicsPersonalByproduct);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }   
            
            case IArchimatePackage.DYNAMICS_TEAM_PREDICTION: {
                IDynamicsTeamPrediction dynamicsTeamPrediction = (IDynamicsTeamPrediction)theEObject;
                T result = caseDynamicsTeamPrediction(dynamicsTeamPrediction);
                if (result == null) result = caseDynamicsTeamElement(dynamicsTeamPrediction);
                if (result == null) result = caseArchimateElement(dynamicsTeamPrediction);
                if (result == null) result = caseArchimateConcept(dynamicsTeamPrediction);
                if (result == null) result = caseArchimateModelObject(dynamicsTeamPrediction);
                if (result == null) result = caseCloneable(dynamicsTeamPrediction);
                if (result == null) result = caseDocumentable(dynamicsTeamPrediction);
                if (result == null) result = caseProperties(dynamicsTeamPrediction);
                if (result == null) result = caseAdapter(dynamicsTeamPrediction);
                if (result == null) result = caseNameable(dynamicsTeamPrediction);
                if (result == null) result = caseIdentifier(dynamicsTeamPrediction);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }   
            
            
            case IArchimatePackage.DYNAMICS_TEAM_FEELING: {
                IDynamicsTeamFeeling dynamicsTeamFeeling = (IDynamicsTeamFeeling)theEObject;
                T result = caseDynamicsTeamFeeling(dynamicsTeamFeeling);
                if (result == null) result = caseDynamicsTeamElement(dynamicsTeamFeeling);
                if (result == null) result = caseArchimateElement(dynamicsTeamFeeling);
                if (result == null) result = caseArchimateConcept(dynamicsTeamFeeling);
                if (result == null) result = caseArchimateModelObject(dynamicsTeamFeeling);
                if (result == null) result = caseCloneable(dynamicsTeamFeeling);
                if (result == null) result = caseDocumentable(dynamicsTeamFeeling);
                if (result == null) result = caseProperties(dynamicsTeamFeeling);
                if (result == null) result = caseAdapter(dynamicsTeamFeeling);
                if (result == null) result = caseNameable(dynamicsTeamFeeling);
                if (result == null) result = caseIdentifier(dynamicsTeamFeeling);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }       
            
            case IArchimatePackage.DYNAMICS_ORG_PREDICTION: {
                IDynamicsOrgPrediction dynamicsOrgPrediction = (IDynamicsOrgPrediction)theEObject;
                T result = caseDynamicsOrgPrediction(dynamicsOrgPrediction);
                if (result == null) result = caseDynamicsOrgElement(dynamicsOrgPrediction);
                if (result == null) result = caseArchimateElement(dynamicsOrgPrediction);
                if (result == null) result = caseArchimateConcept(dynamicsOrgPrediction);
                if (result == null) result = caseArchimateModelObject(dynamicsOrgPrediction);
                if (result == null) result = caseCloneable(dynamicsOrgPrediction);
                if (result == null) result = caseDocumentable(dynamicsOrgPrediction);
                if (result == null) result = caseProperties(dynamicsOrgPrediction);
                if (result == null) result = caseAdapter(dynamicsOrgPrediction);
                if (result == null) result = caseNameable(dynamicsOrgPrediction);
                if (result == null) result = caseIdentifier(dynamicsOrgPrediction);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }   
            
            case IArchimatePackage.DYNAMICS_ENV_PREDICTION: {
                IDynamicsEnvPrediction dynamicsEnvPrediction = (IDynamicsEnvPrediction)theEObject;
                T result = caseDynamicsEnvPrediction(dynamicsEnvPrediction);
                if (result == null) result = caseDynamicsEnvElement(dynamicsEnvPrediction);
                if (result == null) result = caseArchimateElement(dynamicsEnvPrediction);
                if (result == null) result = caseArchimateConcept(dynamicsEnvPrediction);
                if (result == null) result = caseArchimateModelObject(dynamicsEnvPrediction);
                if (result == null) result = caseCloneable(dynamicsEnvPrediction);
                if (result == null) result = caseDocumentable(dynamicsEnvPrediction);
                if (result == null) result = caseProperties(dynamicsEnvPrediction);
                if (result == null) result = caseAdapter(dynamicsEnvPrediction);
                if (result == null) result = caseNameable(dynamicsEnvPrediction);
                if (result == null) result = caseIdentifier(dynamicsEnvPrediction);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }   
            
            case IArchimatePackage.VSM_SRM_STATUS: {
                IVsmSrmStatus vsmSrmStatus = (IVsmSrmStatus)theEObject;
                T result = caseVsmSrmStatus(vsmSrmStatus);
                if (result == null) result = caseVsmSrmElement(vsmSrmStatus);
                if (result == null) result = caseArchimateElement(vsmSrmStatus);
                if (result == null) result = caseArchimateConcept(vsmSrmStatus);
                if (result == null) result = caseArchimateModelObject(vsmSrmStatus);
                if (result == null) result = caseCloneable(vsmSrmStatus);
                if (result == null) result = caseDocumentable(vsmSrmStatus);
                if (result == null) result = caseProperties(vsmSrmStatus);
                if (result == null) result = caseAdapter(vsmSrmStatus);
                if (result == null) result = caseNameable(vsmSrmStatus);
                if (result == null) result = caseIdentifier(vsmSrmStatus);
                if (result == null) result = defaultCase(theEObject);
                return result;
            } 
            
            case IArchimatePackage.VSM_SRM_ACTION: {
                IVsmSrmAction vsmSrmAction = (IVsmSrmAction)theEObject;
                T result = caseVsmSrmAction(vsmSrmAction);
                if (result == null) result = caseVsmSrmElement(vsmSrmAction);
                if (result == null) result = caseArchimateElement(vsmSrmAction);
                if (result == null) result = caseArchimateConcept(vsmSrmAction);
                if (result == null) result = caseArchimateModelObject(vsmSrmAction);
                if (result == null) result = caseCloneable(vsmSrmAction);
                if (result == null) result = caseDocumentable(vsmSrmAction);
                if (result == null) result = caseProperties(vsmSrmAction);
                if (result == null) result = caseAdapter(vsmSrmAction);
                if (result == null) result = caseNameable(vsmSrmAction);
                if (result == null) result = caseIdentifier(vsmSrmAction);
                if (result == null) result = defaultCase(theEObject);
                return result;
            } 
            
            
            case IArchimatePackage.VSM_SRM_ROLEFUNCTION: {
                IVsmSrmRoleFunction vsmSrmRoleFunction = (IVsmSrmRoleFunction)theEObject;
                T result = caseVsmSrmRoleFunction(vsmSrmRoleFunction);
                if (result == null) result = caseVsmSrmElement(vsmSrmRoleFunction);
                if (result == null) result = caseArchimateElement(vsmSrmRoleFunction);
                if (result == null) result = caseArchimateConcept(vsmSrmRoleFunction);
                if (result == null) result = caseArchimateModelObject(vsmSrmRoleFunction);
                if (result == null) result = caseCloneable(vsmSrmRoleFunction);
                if (result == null) result = caseDocumentable(vsmSrmRoleFunction);
                if (result == null) result = caseProperties(vsmSrmRoleFunction);
                if (result == null) result = caseAdapter(vsmSrmRoleFunction);
                if (result == null) result = caseNameable(vsmSrmRoleFunction);
                if (result == null) result = caseIdentifier(vsmSrmRoleFunction);
                if (result == null) result = defaultCase(theEObject);
                return result;
            } 
            

            case IArchimatePackage.VSM_SRM_ROLEUSER: {
                IVsmSrmRoleUser vsmSrmRoleUser = (IVsmSrmRoleUser)theEObject;
                T result = caseVsmSrmRoleUser(vsmSrmRoleUser);
                if (result == null) result = caseVsmSrmElement(vsmSrmRoleUser);
                if (result == null) result = caseArchimateElement(vsmSrmRoleUser);
                if (result == null) result = caseArchimateConcept(vsmSrmRoleUser);
                if (result == null) result = caseArchimateModelObject(vsmSrmRoleUser);
                if (result == null) result = caseCloneable(vsmSrmRoleUser);
                if (result == null) result = caseDocumentable(vsmSrmRoleUser);
                if (result == null) result = caseProperties(vsmSrmRoleUser);
                if (result == null) result = caseAdapter(vsmSrmRoleUser);
                if (result == null) result = caseNameable(vsmSrmRoleUser);
                if (result == null) result = caseIdentifier(vsmSrmRoleUser);
                if (result == null) result = defaultCase(theEObject);
                return result;
            } 
            
            
            case IArchimatePackage.VSM_SRM_USER: {
                IVsmSrmUser vsmSrmUser = (IVsmSrmUser)theEObject;
                T result = caseVsmSrmUser(vsmSrmUser);
                if (result == null) result = caseVsmSrmElement(vsmSrmUser);
                if (result == null) result = caseArchimateElement(vsmSrmUser);
                if (result == null) result = caseArchimateConcept(vsmSrmUser);
                if (result == null) result = caseArchimateModelObject(vsmSrmUser);
                if (result == null) result = caseCloneable(vsmSrmUser);
                if (result == null) result = caseDocumentable(vsmSrmUser);
                if (result == null) result = caseProperties(vsmSrmUser);
                if (result == null) result = caseAdapter(vsmSrmUser);
                if (result == null) result = caseNameable(vsmSrmUser);
                if (result == null) result = caseIdentifier(vsmSrmUser);
                if (result == null) result = defaultCase(theEObject);
                return result;
            } 
            
            
            case IArchimatePackage.CONTEXTME_BASIC_CONCEPT: {
                IContextmeBasicConcept contextmeBasicConcept = (IContextmeBasicConcept)theEObject;
                T result = caseContextmeBasicConcept(contextmeBasicConcept);
                if (result == null) result = caseContextmeBasicElement(contextmeBasicConcept);
                if (result == null) result = caseArchimateElement(contextmeBasicConcept);
                if (result == null) result = caseArchimateConcept(contextmeBasicConcept);
                if (result == null) result = caseArchimateModelObject(contextmeBasicConcept);
                if (result == null) result = caseCloneable(contextmeBasicConcept);
                if (result == null) result = caseDocumentable(contextmeBasicConcept);
                if (result == null) result = caseProperties(contextmeBasicConcept);
                if (result == null) result = caseAdapter(contextmeBasicConcept);
                if (result == null) result = caseNameable(contextmeBasicConcept);
                if (result == null) result = caseIdentifier(contextmeBasicConcept);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }  
            
            
            case IArchimatePackage.CONTEXTME_BASIC_SITUATION: {
                IContextmeBasicSituation contextmeBasicSituation = (IContextmeBasicSituation)theEObject;
                T result = caseContextmeBasicSituation(contextmeBasicSituation);
                if (result == null) result = caseContextmeBasicElement(contextmeBasicSituation);
                if (result == null) result = caseArchimateElement(contextmeBasicSituation);
                if (result == null) result = caseArchimateConcept(contextmeBasicSituation);
                if (result == null) result = caseArchimateModelObject(contextmeBasicSituation);
                if (result == null) result = caseCloneable(contextmeBasicSituation);
                if (result == null) result = caseDocumentable(contextmeBasicSituation);
                if (result == null) result = caseProperties(contextmeBasicSituation);
                if (result == null) result = caseAdapter(contextmeBasicSituation);
                if (result == null) result = caseNameable(contextmeBasicSituation);
                if (result == null) result = caseIdentifier(contextmeBasicSituation);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }  
             
            case IArchimatePackage.CONTEXTME_BASIC_QUESTIONING: {
                IContextmeBasicQuestioning contextmeBasicQuestioning = (IContextmeBasicQuestioning)theEObject;
                T result = caseContextmeBasicQuestioning(contextmeBasicQuestioning);
                if (result == null) result = caseContextmeBasicElement(contextmeBasicQuestioning);
                if (result == null) result = caseArchimateElement(contextmeBasicQuestioning);
                if (result == null) result = caseArchimateConcept(contextmeBasicQuestioning);
                if (result == null) result = caseArchimateModelObject(contextmeBasicQuestioning);
                if (result == null) result = caseCloneable(contextmeBasicQuestioning);
                if (result == null) result = caseDocumentable(contextmeBasicQuestioning);
                if (result == null) result = caseProperties(contextmeBasicQuestioning);
                if (result == null) result = caseAdapter(contextmeBasicQuestioning);
                if (result == null) result = caseNameable(contextmeBasicQuestioning);
                if (result == null) result = caseIdentifier(contextmeBasicQuestioning);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }  
            
            case IArchimatePackage.CONTEXTME_BASIC_CURIOUS: {
                IContextmeBasicCurious contextmeBasicCurious = (IContextmeBasicCurious)theEObject;
                T result = caseContextmeBasicCurious(contextmeBasicCurious);
                if (result == null) result = caseContextmeBasicElement(contextmeBasicCurious);
                if (result == null) result = caseArchimateElement(contextmeBasicCurious);
                if (result == null) result = caseArchimateConcept(contextmeBasicCurious);
                if (result == null) result = caseArchimateModelObject(contextmeBasicCurious);
                if (result == null) result = caseCloneable(contextmeBasicCurious);
                if (result == null) result = caseDocumentable(contextmeBasicCurious);
                if (result == null) result = caseProperties(contextmeBasicCurious);
                if (result == null) result = caseAdapter(contextmeBasicCurious);
                if (result == null) result = caseNameable(contextmeBasicCurious);
                if (result == null) result = caseIdentifier(contextmeBasicCurious);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }  
            
            case IArchimatePackage.CONTEXTME_BASIC_LINK: {
                IContextmeBasicLink contextmeBasicLink = (IContextmeBasicLink)theEObject;
                T result = caseContextmeBasicLink(contextmeBasicLink);
                if (result == null) result = caseContextmeBasicElement(contextmeBasicLink);
                if (result == null) result = caseArchimateElement(contextmeBasicLink);
                if (result == null) result = caseArchimateConcept(contextmeBasicLink);
                if (result == null) result = caseArchimateModelObject(contextmeBasicLink);
                if (result == null) result = caseCloneable(contextmeBasicLink);
                if (result == null) result = caseDocumentable(contextmeBasicLink);
                if (result == null) result = caseProperties(contextmeBasicLink);
                if (result == null) result = caseAdapter(contextmeBasicLink);
                if (result == null) result = caseNameable(contextmeBasicLink);
                if (result == null) result = caseIdentifier(contextmeBasicLink);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }  
            
            
            case IArchimatePackage.CONTEXTME_BASIC_CLARIFY: {
                IContextmeBasicClarify contextmeBasicClarify = (IContextmeBasicClarify)theEObject;
                T result = caseContextmeBasicClarify(contextmeBasicClarify);
                if (result == null) result = caseContextmeBasicElement(contextmeBasicClarify);
                if (result == null) result = caseArchimateElement(contextmeBasicClarify);
                if (result == null) result = caseArchimateConcept(contextmeBasicClarify);
                if (result == null) result = caseArchimateModelObject(contextmeBasicClarify);
                if (result == null) result = caseCloneable(contextmeBasicClarify);
                if (result == null) result = caseDocumentable(contextmeBasicClarify);
                if (result == null) result = caseProperties(contextmeBasicClarify);
                if (result == null) result = caseAdapter(contextmeBasicClarify);
                if (result == null) result = caseNameable(contextmeBasicClarify);
                if (result == null) result = caseIdentifier(contextmeBasicClarify);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }  
            
            case IArchimatePackage.CONTEXTME_BASIC_EXPRESS: {
                IContextmeBasicExpress contextmeBasicExpress = (IContextmeBasicExpress)theEObject;
                T result = caseContextmeBasicExpress(contextmeBasicExpress);
                if (result == null) result = caseContextmeBasicElement(contextmeBasicExpress);
                if (result == null) result = caseArchimateElement(contextmeBasicExpress);
                if (result == null) result = caseArchimateConcept(contextmeBasicExpress);
                if (result == null) result = caseArchimateModelObject(contextmeBasicExpress);
                if (result == null) result = caseCloneable(contextmeBasicExpress);
                if (result == null) result = caseDocumentable(contextmeBasicExpress);
                if (result == null) result = caseProperties(contextmeBasicExpress);
                if (result == null) result = caseAdapter(contextmeBasicExpress);
                if (result == null) result = caseNameable(contextmeBasicExpress);
                if (result == null) result = caseIdentifier(contextmeBasicExpress);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }  
            
            case IArchimatePackage.CONTEXTME_BASIC_DISCUSS: {
                IContextmeBasicDiscuss contextmeBasicDiscuss = (IContextmeBasicDiscuss)theEObject;
                T result = caseContextmeBasicDiscuss(contextmeBasicDiscuss);
                if (result == null) result = caseContextmeBasicElement(contextmeBasicDiscuss);
                if (result == null) result = caseArchimateElement(contextmeBasicDiscuss);
                if (result == null) result = caseArchimateConcept(contextmeBasicDiscuss);
                if (result == null) result = caseArchimateModelObject(contextmeBasicDiscuss);
                if (result == null) result = caseCloneable(contextmeBasicDiscuss);
                if (result == null) result = caseDocumentable(contextmeBasicDiscuss);
                if (result == null) result = caseProperties(contextmeBasicDiscuss);
                if (result == null) result = caseAdapter(contextmeBasicDiscuss);
                if (result == null) result = caseNameable(contextmeBasicDiscuss);
                if (result == null) result = caseIdentifier(contextmeBasicDiscuss);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }  
            
            case IArchimatePackage.CONTEXTME_BASIC_NOTE: {
                IContextmeBasicNote contextmeBasicNote = (IContextmeBasicNote)theEObject;
                T result = caseContextmeBasicNote(contextmeBasicNote);
                if (result == null) result = caseContextmeBasicElement(contextmeBasicNote);
                if (result == null) result = caseArchimateElement(contextmeBasicNote);
                if (result == null) result = caseArchimateConcept(contextmeBasicNote);
                if (result == null) result = caseArchimateModelObject(contextmeBasicNote);
                if (result == null) result = caseCloneable(contextmeBasicNote);
                if (result == null) result = caseDocumentable(contextmeBasicNote);
                if (result == null) result = caseProperties(contextmeBasicNote);
                if (result == null) result = caseAdapter(contextmeBasicNote);
                if (result == null) result = caseNameable(contextmeBasicNote);
                if (result == null) result = caseIdentifier(contextmeBasicNote);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }  
            
            
            case IArchimatePackage.GOAL: {
                IGoal goal = (IGoal)theEObject;
                T result = caseGoal(goal);
                if (result == null) result = caseMotivationElement(goal);
                if (result == null) result = caseArchimateElement(goal);
                if (result == null) result = caseArchimateConcept(goal);
                if (result == null) result = caseArchimateModelObject(goal);
                if (result == null) result = caseCloneable(goal);
                if (result == null) result = caseDocumentable(goal);
                if (result == null) result = caseProperties(goal);
                if (result == null) result = caseAdapter(goal);
                if (result == null) result = caseNameable(goal);
                if (result == null) result = caseIdentifier(goal);
                if (result == null) result = caseFeatures(goal);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
   
            case IArchimatePackage.VSM_DATA_DATABASE: {
                IVsmDataDatabase vSMDataDatabase = (IVsmDataDatabase)theEObject;
                T result = caseVsmDataDatabase(vSMDataDatabase);
                if (result == null) result = caseVsmDataElement(vSMDataDatabase);
                if (result == null) result = casePassiveStructureElement(vSMDataDatabase);
                if (result == null) result = caseArchimateElement(vSMDataDatabase);
                if (result == null) result = caseArchimateConcept(vSMDataDatabase);
                if (result == null) result = caseArchimateModelObject(vSMDataDatabase);
                if (result == null) result = caseCloneable(vSMDataDatabase);
                if (result == null) result = caseDocumentable(vSMDataDatabase);
                if (result == null) result = caseProperties(vSMDataDatabase);
                if (result == null) result = caseAdapter(vSMDataDatabase);
                if (result == null) result = caseNameable(vSMDataDatabase);
                if (result == null) result = caseIdentifier(vSMDataDatabase);
                if (result == null) result = caseFeatures(vSMDataDatabase);
                if (result == null) result = defaultCase(theEObject);
                return result;
            } 
            case IArchimatePackage.VSM_DATA_SCHEMA: {
                IVsmDataSchema vSMDataSchema = (IVsmDataSchema)theEObject;
                T result = caseVsmDataSchema(vSMDataSchema);
                if (result == null) result = caseVsmDataElement(vSMDataSchema);
                if (result == null) result = casePassiveStructureElement(vSMDataSchema);
                if (result == null) result = caseArchimateElement(vSMDataSchema);
                if (result == null) result = caseArchimateConcept(vSMDataSchema);
                if (result == null) result = caseArchimateModelObject(vSMDataSchema);
                if (result == null) result = caseCloneable(vSMDataSchema);
                if (result == null) result = caseDocumentable(vSMDataSchema);
                if (result == null) result = caseProperties(vSMDataSchema);
                if (result == null) result = caseAdapter(vSMDataSchema);
                if (result == null) result = caseNameable(vSMDataSchema);
                if (result == null) result = caseIdentifier(vSMDataSchema);
                if (result == null) result = caseFeatures(vSMDataSchema);
                if (result == null) result = defaultCase(theEObject);
                return result;
            } 
            case IArchimatePackage.VSM_DATA_VIEW: {
                IVsmDataView vSMDataView = (IVsmDataView)theEObject;
                T result = caseVsmDataView(vSMDataView);
                if (result == null) result = caseVsmDataElement(vSMDataView);
                if (result == null) result = casePassiveStructureElement(vSMDataView);
                if (result == null) result = caseArchimateElement(vSMDataView);
                if (result == null) result = caseArchimateConcept(vSMDataView);
                if (result == null) result = caseArchimateModelObject(vSMDataView);
                if (result == null) result = caseCloneable(vSMDataView);
                if (result == null) result = caseDocumentable(vSMDataView);
                if (result == null) result = caseProperties(vSMDataView);
                if (result == null) result = caseAdapter(vSMDataView);
                if (result == null) result = caseNameable(vSMDataView);
                if (result == null) result = caseIdentifier(vSMDataView);
                if (result == null) result = caseFeatures(vSMDataView);
                if (result == null) result = defaultCase(theEObject);
                return result;
            } 
            case IArchimatePackage.VSM_DATA_TABLE: {
                IVsmDataTable vSMDataTable = (IVsmDataTable)theEObject;
                T result = caseVsmDataTable(vSMDataTable);
                if (result == null) result = caseVsmDataElement(vSMDataTable);
                if (result == null) result = casePassiveStructureElement(vSMDataTable);
                if (result == null) result = caseArchimateElement(vSMDataTable);
                if (result == null) result = caseArchimateConcept(vSMDataTable);
                if (result == null) result = caseArchimateModelObject(vSMDataTable);
                if (result == null) result = caseCloneable(vSMDataTable);
                if (result == null) result = caseDocumentable(vSMDataTable);
                if (result == null) result = caseProperties(vSMDataTable);
                if (result == null) result = caseAdapter(vSMDataTable);
                if (result == null) result = caseNameable(vSMDataTable);
                if (result == null) result = caseIdentifier(vSMDataTable);
                if (result == null) result = caseFeatures(vSMDataTable);
                if (result == null) result = defaultCase(theEObject);
                return result;
            } 
            case IArchimatePackage.VSM_DATA_COLUMN: {
                IVsmDataColumn vSMDataColumn = (IVsmDataColumn)theEObject;
                T result = caseVsmDataColumn(vSMDataColumn);
                if (result == null) result = caseVsmDataElement(vSMDataColumn);
                if (result == null) result = casePassiveStructureElement(vSMDataColumn);
                if (result == null) result = caseArchimateElement(vSMDataColumn);
                if (result == null) result = caseArchimateConcept(vSMDataColumn);
                if (result == null) result = caseArchimateModelObject(vSMDataColumn);
                if (result == null) result = caseCloneable(vSMDataColumn);
                if (result == null) result = caseDocumentable(vSMDataColumn);
                if (result == null) result = caseProperties(vSMDataColumn);
                if (result == null) result = caseAdapter(vSMDataColumn);
                if (result == null) result = caseNameable(vSMDataColumn);
                if (result == null) result = caseIdentifier(vSMDataColumn);
                if (result == null) result = caseFeatures(vSMDataColumn);
                if (result == null) result = defaultCase(theEObject);
                return result;
            } 
            case IArchimatePackage.VSM_DATA_TASK: {
                IVsmDataTask vSMDataTask = (IVsmDataTask)theEObject;
                T result = caseVsmDataTask(vSMDataTask);
                if (result == null) result = caseVsmDataElement(vSMDataTask);
                if (result == null) result = caseBehaviorElement(vSMDataTask);
                if (result == null) result = caseArchimateElement(vSMDataTask);
                if (result == null) result = caseArchimateConcept(vSMDataTask);
                if (result == null) result = caseArchimateModelObject(vSMDataTask);
                if (result == null) result = caseCloneable(vSMDataTask);
                if (result == null) result = caseDocumentable(vSMDataTask);
                if (result == null) result = caseProperties(vSMDataTask);
                if (result == null) result = caseAdapter(vSMDataTask);
                if (result == null) result = caseNameable(vSMDataTask);
                if (result == null) result = caseIdentifier(vSMDataTask);
                if (result == null) result = caseFeatures(vSMDataTask);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }            
            case IArchimatePackage.VSM_LANDSCAPE_INFRASTRUCTURE_OCTAGON: {
                IVsmLandscapeInfrastructureOctagon vSMLandscapeInfrastructureOctagon = (IVsmLandscapeInfrastructureOctagon)theEObject;
                T result = caseVsmLandscapeInfrastructureOctagon(vSMLandscapeInfrastructureOctagon);
                if (result == null) result = caseVsmLandscapeElement(vSMLandscapeInfrastructureOctagon);
                if (result == null) result = caseArchimateElement(vSMLandscapeInfrastructureOctagon);
                if (result == null) result = caseArchimateConcept(vSMLandscapeInfrastructureOctagon);
                if (result == null) result = caseArchimateModelObject(vSMLandscapeInfrastructureOctagon);
                if (result == null) result = caseCloneable(vSMLandscapeInfrastructureOctagon);
                if (result == null) result = caseDocumentable(vSMLandscapeInfrastructureOctagon);
                if (result == null) result = caseProperties(vSMLandscapeInfrastructureOctagon);
                if (result == null) result = caseAdapter(vSMLandscapeInfrastructureOctagon);
                if (result == null) result = caseNameable(vSMLandscapeInfrastructureOctagon);
                if (result == null) result = caseIdentifier(vSMLandscapeInfrastructureOctagon);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }  
            case IArchimatePackage.VSM_LANDSCAPE_PRODUCT_OCTAGON: {
                IVsmLandscapeProductOctagon vSMLandscapeProductOctagon = (IVsmLandscapeProductOctagon)theEObject;
                T result = caseVsmLandscapeProductOctagon(vSMLandscapeProductOctagon);
                if (result == null) result = caseVsmLandscapeElement(vSMLandscapeProductOctagon);
                if (result == null) result = caseArchimateElement(vSMLandscapeProductOctagon);
                if (result == null) result = caseArchimateConcept(vSMLandscapeProductOctagon);
                if (result == null) result = caseArchimateModelObject(vSMLandscapeProductOctagon);
                if (result == null) result = caseCloneable(vSMLandscapeProductOctagon);
                if (result == null) result = caseDocumentable(vSMLandscapeProductOctagon);
                if (result == null) result = caseProperties(vSMLandscapeProductOctagon);
                if (result == null) result = caseAdapter(vSMLandscapeProductOctagon);
                if (result == null) result = caseNameable(vSMLandscapeProductOctagon);
                if (result == null) result = caseIdentifier(vSMLandscapeProductOctagon);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }   
            case IArchimatePackage.VSM_LANDSCAPE_LAYER_GENERAL: {
                IVsmLandscapeLayerGeneral vSMLandscapeLayerGeneral = (IVsmLandscapeLayerGeneral)theEObject;
                T result = caseVsmLandscapeLayerGeneral(vSMLandscapeLayerGeneral);
                if (result == null) result = caseVsmLandscapeElement(vSMLandscapeLayerGeneral);
                if (result == null) result = caseArchimateElement(vSMLandscapeLayerGeneral);
                if (result == null) result = caseArchimateConcept(vSMLandscapeLayerGeneral);
                if (result == null) result = caseArchimateModelObject(vSMLandscapeLayerGeneral);
                if (result == null) result = caseCloneable(vSMLandscapeLayerGeneral);
                if (result == null) result = caseDocumentable(vSMLandscapeLayerGeneral);
                if (result == null) result = caseProperties(vSMLandscapeLayerGeneral);
                if (result == null) result = caseAdapter(vSMLandscapeLayerGeneral);
                if (result == null) result = caseNameable(vSMLandscapeLayerGeneral);
                if (result == null) result = caseIdentifier(vSMLandscapeLayerGeneral);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.VSM_LANDSCAPE_LAYER_BUSINESS: {
                IVsmLandscapeLayerBusiness vSMLandscapeLayerBusiness = (IVsmLandscapeLayerBusiness)theEObject;
                T result = caseVsmLandscapeLayerBusiness(vSMLandscapeLayerBusiness);
                if (result == null) result = caseVsmLandscapeElement(vSMLandscapeLayerBusiness);
                if (result == null) result = caseArchimateElement(vSMLandscapeLayerBusiness);
                if (result == null) result = caseArchimateConcept(vSMLandscapeLayerBusiness);
                if (result == null) result = caseArchimateModelObject(vSMLandscapeLayerBusiness);
                if (result == null) result = caseCloneable(vSMLandscapeLayerBusiness);
                if (result == null) result = caseDocumentable(vSMLandscapeLayerBusiness);
                if (result == null) result = caseProperties(vSMLandscapeLayerBusiness);
                if (result == null) result = caseAdapter(vSMLandscapeLayerBusiness);
                if (result == null) result = caseNameable(vSMLandscapeLayerBusiness);
                if (result == null) result = caseIdentifier(vSMLandscapeLayerBusiness);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }   
            case IArchimatePackage.VSM_LANDSCAPE_LAYER_APPLICATION: {
                IVsmLandscapeLayerApplication vSMLandscapeLayerApplication = (IVsmLandscapeLayerApplication)theEObject;
                T result = caseVsmLandscapeLayerApplication(vSMLandscapeLayerApplication);
                if (result == null) result = caseVsmLandscapeElement(vSMLandscapeLayerApplication);
                if (result == null) result = caseArchimateElement(vSMLandscapeLayerApplication);
                if (result == null) result = caseArchimateConcept(vSMLandscapeLayerApplication);
                if (result == null) result = caseArchimateModelObject(vSMLandscapeLayerApplication);
                if (result == null) result = caseCloneable(vSMLandscapeLayerApplication);
                if (result == null) result = caseDocumentable(vSMLandscapeLayerApplication);
                if (result == null) result = caseProperties(vSMLandscapeLayerApplication);
                if (result == null) result = caseAdapter(vSMLandscapeLayerApplication);
                if (result == null) result = caseNameable(vSMLandscapeLayerApplication);
                if (result == null) result = caseIdentifier(vSMLandscapeLayerApplication);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }   
            case IArchimatePackage.VSM_LANDSCAPE_LAYER_TECHNOLOGY: {
                IVsmLandscapeLayerTechnology vSMLandscapeLayerTechnology = (IVsmLandscapeLayerTechnology)theEObject;
                T result = caseVsmLandscapeLayerTechnology(vSMLandscapeLayerTechnology);
                if (result == null) result = caseVsmLandscapeElement(vSMLandscapeLayerTechnology);
                if (result == null) result = caseArchimateElement(vSMLandscapeLayerTechnology);
                if (result == null) result = caseArchimateConcept(vSMLandscapeLayerTechnology);
                if (result == null) result = caseArchimateModelObject(vSMLandscapeLayerTechnology);
                if (result == null) result = caseCloneable(vSMLandscapeLayerTechnology);
                if (result == null) result = caseDocumentable(vSMLandscapeLayerTechnology);
                if (result == null) result = caseProperties(vSMLandscapeLayerTechnology);
                if (result == null) result = caseAdapter(vSMLandscapeLayerTechnology);
                if (result == null) result = caseNameable(vSMLandscapeLayerTechnology);
                if (result == null) result = caseIdentifier(vSMLandscapeLayerTechnology);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }   
            case IArchimatePackage.VSM_LANDSCAPE_LAYER_DATA: {
                IVsmLandscapeLayerData vSMLandscapeLayerData = (IVsmLandscapeLayerData)theEObject;
                T result = caseVsmLandscapeLayerData(vSMLandscapeLayerData);
                if (result == null) result = caseVsmLandscapeElement(vSMLandscapeLayerData);
                if (result == null) result = caseArchimateElement(vSMLandscapeLayerData);
                if (result == null) result = caseArchimateConcept(vSMLandscapeLayerData);
                if (result == null) result = caseArchimateModelObject(vSMLandscapeLayerData);
                if (result == null) result = caseCloneable(vSMLandscapeLayerData);
                if (result == null) result = caseDocumentable(vSMLandscapeLayerData);
                if (result == null) result = caseProperties(vSMLandscapeLayerData);
                if (result == null) result = caseAdapter(vSMLandscapeLayerData);
                if (result == null) result = caseNameable(vSMLandscapeLayerData);
                if (result == null) result = caseIdentifier(vSMLandscapeLayerData);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }   
            case IArchimatePackage.VSM_LANDSCAPE_LAYER_B2A: {
                IVsmLandscapeLayerB2a vSMLandscapeLayerB2a = (IVsmLandscapeLayerB2a)theEObject;
                T result = caseVsmLandscapeLayerB2a(vSMLandscapeLayerB2a);
                if (result == null) result = caseVsmLandscapeElement(vSMLandscapeLayerB2a);
                if (result == null) result = caseArchimateElement(vSMLandscapeLayerB2a);
                if (result == null) result = caseArchimateConcept(vSMLandscapeLayerB2a);
                if (result == null) result = caseArchimateModelObject(vSMLandscapeLayerB2a);
                if (result == null) result = caseCloneable(vSMLandscapeLayerB2a);
                if (result == null) result = caseDocumentable(vSMLandscapeLayerB2a);
                if (result == null) result = caseProperties(vSMLandscapeLayerB2a);
                if (result == null) result = caseAdapter(vSMLandscapeLayerB2a);
                if (result == null) result = caseNameable(vSMLandscapeLayerB2a);
                if (result == null) result = caseIdentifier(vSMLandscapeLayerB2a);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.VSM_LANDSCAPE_LAYER_A2D: {
                IVsmLandscapeLayerA2d vSMLandscapeLayerA2d = (IVsmLandscapeLayerA2d)theEObject;
                T result = caseVsmLandscapeLayerA2d(vSMLandscapeLayerA2d);
                if (result == null) result = caseVsmLandscapeElement(vSMLandscapeLayerA2d);
                if (result == null) result = caseArchimateElement(vSMLandscapeLayerA2d);
                if (result == null) result = caseArchimateConcept(vSMLandscapeLayerA2d);
                if (result == null) result = caseArchimateModelObject(vSMLandscapeLayerA2d);
                if (result == null) result = caseCloneable(vSMLandscapeLayerA2d);
                if (result == null) result = caseDocumentable(vSMLandscapeLayerA2d);
                if (result == null) result = caseProperties(vSMLandscapeLayerA2d);
                if (result == null) result = caseAdapter(vSMLandscapeLayerA2d);
                if (result == null) result = caseNameable(vSMLandscapeLayerA2d);
                if (result == null) result = caseIdentifier(vSMLandscapeLayerA2d);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }   
            case IArchimatePackage.VSM_LANDSCAPE_LAYER_A2T: {
                IVsmLandscapeLayerA2t vSMLandscapeLayerA2t = (IVsmLandscapeLayerA2t)theEObject;
                T result = caseVsmLandscapeLayerA2t(vSMLandscapeLayerA2t);
                if (result == null) result = caseVsmLandscapeElement(vSMLandscapeLayerA2t);
                if (result == null) result = caseArchimateElement(vSMLandscapeLayerA2t);
                if (result == null) result = caseArchimateConcept(vSMLandscapeLayerA2t);
                if (result == null) result = caseArchimateModelObject(vSMLandscapeLayerA2t);
                if (result == null) result = caseCloneable(vSMLandscapeLayerA2t);
                if (result == null) result = caseDocumentable(vSMLandscapeLayerA2t);
                if (result == null) result = caseProperties(vSMLandscapeLayerA2t);
                if (result == null) result = caseAdapter(vSMLandscapeLayerA2t);
                if (result == null) result = caseNameable(vSMLandscapeLayerA2t);
                if (result == null) result = caseIdentifier(vSMLandscapeLayerA2t);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }   
            case IArchimatePackage.GROUPING: {
                IGrouping grouping = (IGrouping)theEObject;
                T result = caseGrouping(grouping);
                if (result == null) result = caseCompositeElement(grouping);
                if (result == null) result = caseArchimateElement(grouping);
                if (result == null) result = caseArchimateConcept(grouping);
                if (result == null) result = caseArchimateModelObject(grouping);
                if (result == null) result = caseCloneable(grouping);
                if (result == null) result = caseDocumentable(grouping);
                if (result == null) result = caseProperties(grouping);
                if (result == null) result = caseAdapter(grouping);
                if (result == null) result = caseNameable(grouping);
                if (result == null) result = caseIdentifier(grouping);
                if (result == null) result = caseFeatures(grouping);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.IMPLEMENTATION_EVENT: {
                IImplementationEvent implementationEvent = (IImplementationEvent)theEObject;
                T result = caseImplementationEvent(implementationEvent);
                if (result == null) result = caseImplementationMigrationElement(implementationEvent);
                if (result == null) result = caseArchimateElement(implementationEvent);
                if (result == null) result = caseArchimateConcept(implementationEvent);
                if (result == null) result = caseArchimateModelObject(implementationEvent);
                if (result == null) result = caseCloneable(implementationEvent);
                if (result == null) result = caseDocumentable(implementationEvent);
                if (result == null) result = caseProperties(implementationEvent);
                if (result == null) result = caseAdapter(implementationEvent);
                if (result == null) result = caseNameable(implementationEvent);
                if (result == null) result = caseIdentifier(implementationEvent);
                if (result == null) result = caseFeatures(implementationEvent);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.LOCATION: {
                ILocation location = (ILocation)theEObject;
                T result = caseLocation(location);
                if (result == null) result = caseCompositeElement(location);
                if (result == null) result = caseArchimateElement(location);
                if (result == null) result = caseArchimateConcept(location);
                if (result == null) result = caseArchimateModelObject(location);
                if (result == null) result = caseCloneable(location);
                if (result == null) result = caseDocumentable(location);
                if (result == null) result = caseProperties(location);
                if (result == null) result = caseAdapter(location);
                if (result == null) result = caseNameable(location);
                if (result == null) result = caseIdentifier(location);
                if (result == null) result = caseFeatures(location);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.MATERIAL: {
                IMaterial material = (IMaterial)theEObject;
                T result = caseMaterial(material);
                if (result == null) result = casePhysicalElement(material);
                if (result == null) result = caseTechnologyObject(material);
                if (result == null) result = caseTechnologyElement(material);
                if (result == null) result = casePassiveStructureElement(material);
                if (result == null) result = caseArchimateConcept(material);
                if (result == null) result = caseStructureElement(material);
                if (result == null) result = caseArchimateElement(material);
                if (result == null) result = caseArchimateModelObject(material);
                if (result == null) result = caseCloneable(material);
                if (result == null) result = caseDocumentable(material);
                if (result == null) result = caseProperties(material);
                if (result == null) result = caseAdapter(material);
                if (result == null) result = caseNameable(material);
                if (result == null) result = caseIdentifier(material);
                if (result == null) result = caseFeatures(material);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.MEANING: {
                IMeaning meaning = (IMeaning)theEObject;
                T result = caseMeaning(meaning);
                if (result == null) result = caseMotivationElement(meaning);
                if (result == null) result = caseArchimateElement(meaning);
                if (result == null) result = caseArchimateConcept(meaning);
                if (result == null) result = caseArchimateModelObject(meaning);
                if (result == null) result = caseCloneable(meaning);
                if (result == null) result = caseDocumentable(meaning);
                if (result == null) result = caseProperties(meaning);
                if (result == null) result = caseAdapter(meaning);
                if (result == null) result = caseNameable(meaning);
                if (result == null) result = caseIdentifier(meaning);
                if (result == null) result = caseFeatures(meaning);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.NODE: {
                INode node = (INode)theEObject;
                T result = caseNode(node);
                if (result == null) result = caseTechnologyElement(node);
                if (result == null) result = caseActiveStructureElement(node);
                if (result == null) result = caseStructureElement(node);
                if (result == null) result = caseArchimateElement(node);
                if (result == null) result = caseArchimateConcept(node);
                if (result == null) result = caseArchimateModelObject(node);
                if (result == null) result = caseCloneable(node);
                if (result == null) result = caseDocumentable(node);
                if (result == null) result = caseProperties(node);
                if (result == null) result = caseAdapter(node);
                if (result == null) result = caseNameable(node);
                if (result == null) result = caseIdentifier(node);
                if (result == null) result = caseFeatures(node);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.OUTCOME: {
                IOutcome outcome = (IOutcome)theEObject;
                T result = caseOutcome(outcome);
                if (result == null) result = caseMotivationElement(outcome);
                if (result == null) result = caseArchimateElement(outcome);
                if (result == null) result = caseArchimateConcept(outcome);
                if (result == null) result = caseArchimateModelObject(outcome);
                if (result == null) result = caseCloneable(outcome);
                if (result == null) result = caseDocumentable(outcome);
                if (result == null) result = caseProperties(outcome);
                if (result == null) result = caseAdapter(outcome);
                if (result == null) result = caseNameable(outcome);
                if (result == null) result = caseIdentifier(outcome);
                if (result == null) result = caseFeatures(outcome);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.PATH: {
                IPath path = (IPath)theEObject;
                T result = casePath(path);
                if (result == null) result = caseTechnologyElement(path);
                if (result == null) result = caseActiveStructureElement(path);
                if (result == null) result = caseStructureElement(path);
                if (result == null) result = caseArchimateElement(path);
                if (result == null) result = caseArchimateConcept(path);
                if (result == null) result = caseArchimateModelObject(path);
                if (result == null) result = caseCloneable(path);
                if (result == null) result = caseDocumentable(path);
                if (result == null) result = caseProperties(path);
                if (result == null) result = caseAdapter(path);
                if (result == null) result = caseNameable(path);
                if (result == null) result = caseIdentifier(path);
                if (result == null) result = caseFeatures(path);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.PLATEAU: {
                IPlateau plateau = (IPlateau)theEObject;
                T result = casePlateau(plateau);
                if (result == null) result = caseImplementationMigrationElement(plateau);
                if (result == null) result = caseCompositeElement(plateau);
                if (result == null) result = caseArchimateElement(plateau);
                if (result == null) result = caseArchimateConcept(plateau);
                if (result == null) result = caseArchimateModelObject(plateau);
                if (result == null) result = caseCloneable(plateau);
                if (result == null) result = caseDocumentable(plateau);
                if (result == null) result = caseProperties(plateau);
                if (result == null) result = caseAdapter(plateau);
                if (result == null) result = caseNameable(plateau);
                if (result == null) result = caseIdentifier(plateau);
                if (result == null) result = caseFeatures(plateau);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.PRINCIPLE: {
                IPrinciple principle = (IPrinciple)theEObject;
                T result = casePrinciple(principle);
                if (result == null) result = caseMotivationElement(principle);
                if (result == null) result = caseArchimateElement(principle);
                if (result == null) result = caseArchimateConcept(principle);
                if (result == null) result = caseArchimateModelObject(principle);
                if (result == null) result = caseCloneable(principle);
                if (result == null) result = caseDocumentable(principle);
                if (result == null) result = caseProperties(principle);
                if (result == null) result = caseAdapter(principle);
                if (result == null) result = caseNameable(principle);
                if (result == null) result = caseIdentifier(principle);
                if (result == null) result = caseFeatures(principle);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.PRODUCT: {
                IProduct product = (IProduct)theEObject;
                T result = caseProduct(product);
                if (result == null) result = caseBusinessElement(product);
                if (result == null) result = caseCompositeElement(product);
                if (result == null) result = caseArchimateElement(product);
                if (result == null) result = caseArchimateConcept(product);
                if (result == null) result = caseArchimateModelObject(product);
                if (result == null) result = caseCloneable(product);
                if (result == null) result = caseDocumentable(product);
                if (result == null) result = caseProperties(product);
                if (result == null) result = caseAdapter(product);
                if (result == null) result = caseNameable(product);
                if (result == null) result = caseIdentifier(product);
                if (result == null) result = caseFeatures(product);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.REPRESENTATION: {
                IRepresentation representation = (IRepresentation)theEObject;
                T result = caseRepresentation(representation);
                if (result == null) result = caseBusinessElement(representation);
                if (result == null) result = casePassiveStructureElement(representation);
                if (result == null) result = caseStructureElement(representation);
                if (result == null) result = caseArchimateElement(representation);
                if (result == null) result = caseArchimateConcept(representation);
                if (result == null) result = caseArchimateModelObject(representation);
                if (result == null) result = caseCloneable(representation);
                if (result == null) result = caseDocumentable(representation);
                if (result == null) result = caseProperties(representation);
                if (result == null) result = caseAdapter(representation);
                if (result == null) result = caseNameable(representation);
                if (result == null) result = caseIdentifier(representation);
                if (result == null) result = caseFeatures(representation);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.RESOURCE: {
                IResource resource = (IResource)theEObject;
                T result = caseResource(resource);
                if (result == null) result = caseStrategyElement(resource);
                if (result == null) result = caseStructureElement(resource);
                if (result == null) result = caseArchimateElement(resource);
                if (result == null) result = caseArchimateConcept(resource);
                if (result == null) result = caseArchimateModelObject(resource);
                if (result == null) result = caseCloneable(resource);
                if (result == null) result = caseDocumentable(resource);
                if (result == null) result = caseProperties(resource);
                if (result == null) result = caseAdapter(resource);
                if (result == null) result = caseNameable(resource);
                if (result == null) result = caseIdentifier(resource);
                if (result == null) result = caseFeatures(resource);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.REQUIREMENT: {
                IRequirement requirement = (IRequirement)theEObject;
                T result = caseRequirement(requirement);
                if (result == null) result = caseMotivationElement(requirement);
                if (result == null) result = caseArchimateElement(requirement);
                if (result == null) result = caseArchimateConcept(requirement);
                if (result == null) result = caseArchimateModelObject(requirement);
                if (result == null) result = caseCloneable(requirement);
                if (result == null) result = caseDocumentable(requirement);
                if (result == null) result = caseProperties(requirement);
                if (result == null) result = caseAdapter(requirement);
                if (result == null) result = caseNameable(requirement);
                if (result == null) result = caseIdentifier(requirement);
                if (result == null) result = caseFeatures(requirement);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.STAKEHOLDER: {
                IStakeholder stakeholder = (IStakeholder)theEObject;
                T result = caseStakeholder(stakeholder);
                if (result == null) result = caseMotivationElement(stakeholder);
                if (result == null) result = caseActiveStructureElement(stakeholder);
                if (result == null) result = caseStructureElement(stakeholder);
                if (result == null) result = caseArchimateElement(stakeholder);
                if (result == null) result = caseArchimateConcept(stakeholder);
                if (result == null) result = caseArchimateModelObject(stakeholder);
                if (result == null) result = caseCloneable(stakeholder);
                if (result == null) result = caseDocumentable(stakeholder);
                if (result == null) result = caseProperties(stakeholder);
                if (result == null) result = caseAdapter(stakeholder);
                if (result == null) result = caseNameable(stakeholder);
                if (result == null) result = caseIdentifier(stakeholder);
                if (result == null) result = caseFeatures(stakeholder);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.SYSTEM_SOFTWARE: {
                ISystemSoftware systemSoftware = (ISystemSoftware)theEObject;
                T result = caseSystemSoftware(systemSoftware);
                if (result == null) result = caseTechnologyElement(systemSoftware);
                if (result == null) result = caseActiveStructureElement(systemSoftware);
                if (result == null) result = caseStructureElement(systemSoftware);
                if (result == null) result = caseArchimateElement(systemSoftware);
                if (result == null) result = caseArchimateConcept(systemSoftware);
                if (result == null) result = caseArchimateModelObject(systemSoftware);
                if (result == null) result = caseCloneable(systemSoftware);
                if (result == null) result = caseDocumentable(systemSoftware);
                if (result == null) result = caseProperties(systemSoftware);
                if (result == null) result = caseAdapter(systemSoftware);
                if (result == null) result = caseNameable(systemSoftware);
                if (result == null) result = caseIdentifier(systemSoftware);
                if (result == null) result = caseFeatures(systemSoftware);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.TECHNOLOGY_COLLABORATION: {
                ITechnologyCollaboration technologyCollaboration = (ITechnologyCollaboration)theEObject;
                T result = caseTechnologyCollaboration(technologyCollaboration);
                if (result == null) result = caseTechnologyElement(technologyCollaboration);
                if (result == null) result = caseActiveStructureElement(technologyCollaboration);
                if (result == null) result = caseStructureElement(technologyCollaboration);
                if (result == null) result = caseArchimateElement(technologyCollaboration);
                if (result == null) result = caseArchimateConcept(technologyCollaboration);
                if (result == null) result = caseArchimateModelObject(technologyCollaboration);
                if (result == null) result = caseCloneable(technologyCollaboration);
                if (result == null) result = caseDocumentable(technologyCollaboration);
                if (result == null) result = caseProperties(technologyCollaboration);
                if (result == null) result = caseAdapter(technologyCollaboration);
                if (result == null) result = caseNameable(technologyCollaboration);
                if (result == null) result = caseIdentifier(technologyCollaboration);
                if (result == null) result = caseFeatures(technologyCollaboration);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.TECHNOLOGY_EVENT: {
                ITechnologyEvent technologyEvent = (ITechnologyEvent)theEObject;
                T result = caseTechnologyEvent(technologyEvent);
                if (result == null) result = caseTechnologyElement(technologyEvent);
                if (result == null) result = caseBehaviorElement(technologyEvent);
                if (result == null) result = caseArchimateElement(technologyEvent);
                if (result == null) result = caseArchimateConcept(technologyEvent);
                if (result == null) result = caseArchimateModelObject(technologyEvent);
                if (result == null) result = caseCloneable(technologyEvent);
                if (result == null) result = caseDocumentable(technologyEvent);
                if (result == null) result = caseProperties(technologyEvent);
                if (result == null) result = caseAdapter(technologyEvent);
                if (result == null) result = caseNameable(technologyEvent);
                if (result == null) result = caseIdentifier(technologyEvent);
                if (result == null) result = caseFeatures(technologyEvent);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.TECHNOLOGY_FUNCTION: {
                ITechnologyFunction technologyFunction = (ITechnologyFunction)theEObject;
                T result = caseTechnologyFunction(technologyFunction);
                if (result == null) result = caseTechnologyElement(technologyFunction);
                if (result == null) result = caseBehaviorElement(technologyFunction);
                if (result == null) result = caseArchimateElement(technologyFunction);
                if (result == null) result = caseArchimateConcept(technologyFunction);
                if (result == null) result = caseArchimateModelObject(technologyFunction);
                if (result == null) result = caseCloneable(technologyFunction);
                if (result == null) result = caseDocumentable(technologyFunction);
                if (result == null) result = caseProperties(technologyFunction);
                if (result == null) result = caseAdapter(technologyFunction);
                if (result == null) result = caseNameable(technologyFunction);
                if (result == null) result = caseIdentifier(technologyFunction);
                if (result == null) result = caseFeatures(technologyFunction);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.VSM_TECHNOLOGY_TASK: {
                IVsmTechnologyTask vSMTechnologyTask = (IVsmTechnologyTask)theEObject;
                T result = caseVsmTechnologyTask(vSMTechnologyTask);
                if (result == null) result = caseVsmTechnologyElement(vSMTechnologyTask);
                if (result == null) result = caseBehaviorElement(vSMTechnologyTask);
                if (result == null) result = caseArchimateElement(vSMTechnologyTask);
                if (result == null) result = caseArchimateConcept(vSMTechnologyTask);
                if (result == null) result = caseArchimateModelObject(vSMTechnologyTask);
                if (result == null) result = caseCloneable(vSMTechnologyTask);
                if (result == null) result = caseDocumentable(vSMTechnologyTask);
                if (result == null) result = caseProperties(vSMTechnologyTask);
                if (result == null) result = caseAdapter(vSMTechnologyTask);
                if (result == null) result = caseNameable(vSMTechnologyTask);
                if (result == null) result = caseIdentifier(vSMTechnologyTask);
                if (result == null) result = caseFeatures(vSMTechnologyTask);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.VSM_TECHNOLOGY_K8S_POD: {
                IVsmTechnologyK8sPod vSMTechnologyK8sPod = (IVsmTechnologyK8sPod)theEObject;
                T result = caseVsmTechnologyK8sPod(vSMTechnologyK8sPod);
                if (result == null) result = caseVsmTechnologyElement(vSMTechnologyK8sPod);
                if (result == null) result = caseBehaviorElement(vSMTechnologyK8sPod);
                if (result == null) result = caseArchimateElement(vSMTechnologyK8sPod);
                if (result == null) result = caseArchimateConcept(vSMTechnologyK8sPod);
                if (result == null) result = caseArchimateModelObject(vSMTechnologyK8sPod);
                if (result == null) result = caseCloneable(vSMTechnologyK8sPod);
                if (result == null) result = caseDocumentable(vSMTechnologyK8sPod);
                if (result == null) result = caseProperties(vSMTechnologyK8sPod);
                if (result == null) result = caseAdapter(vSMTechnologyK8sPod);
                if (result == null) result = caseNameable(vSMTechnologyK8sPod);
                if (result == null) result = caseIdentifier(vSMTechnologyK8sPod);
                if (result == null) result = caseFeatures(vSMTechnologyK8sPod);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }            
            case IArchimatePackage.VSM_TECHNOLOGY_K8S_CONTAINER: {
                IVsmTechnologyK8sContainer vSMTechnologyK8sContainer = (IVsmTechnologyK8sContainer)theEObject;
                T result = caseVsmTechnologyK8sContainer(vSMTechnologyK8sContainer);
                if (result == null) result = caseVsmTechnologyElement(vSMTechnologyK8sContainer);
                if (result == null) result = caseBehaviorElement(vSMTechnologyK8sContainer);
                if (result == null) result = caseArchimateElement(vSMTechnologyK8sContainer);
                if (result == null) result = caseArchimateConcept(vSMTechnologyK8sContainer);
                if (result == null) result = caseArchimateModelObject(vSMTechnologyK8sContainer);
                if (result == null) result = caseCloneable(vSMTechnologyK8sContainer);
                if (result == null) result = caseDocumentable(vSMTechnologyK8sContainer);
                if (result == null) result = caseProperties(vSMTechnologyK8sContainer);
                if (result == null) result = caseAdapter(vSMTechnologyK8sContainer);
                if (result == null) result = caseNameable(vSMTechnologyK8sContainer);
                if (result == null) result = caseIdentifier(vSMTechnologyK8sContainer);
                if (result == null) result = caseFeatures(vSMTechnologyK8sContainer);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.VSM_TECHNOLOGY_K8S_CONTROLLER: {
                IVsmTechnologyK8sController vSMTechnologyK8sController = (IVsmTechnologyK8sController)theEObject;
                T result = caseVsmTechnologyK8sController(vSMTechnologyK8sController);
                if (result == null) result = caseVsmTechnologyElement(vSMTechnologyK8sController);
                if (result == null) result = caseBehaviorElement(vSMTechnologyK8sController);
                if (result == null) result = caseArchimateElement(vSMTechnologyK8sController);
                if (result == null) result = caseArchimateConcept(vSMTechnologyK8sController);
                if (result == null) result = caseArchimateModelObject(vSMTechnologyK8sController);
                if (result == null) result = caseCloneable(vSMTechnologyK8sController);
                if (result == null) result = caseDocumentable(vSMTechnologyK8sController);
                if (result == null) result = caseProperties(vSMTechnologyK8sController);
                if (result == null) result = caseAdapter(vSMTechnologyK8sController);
                if (result == null) result = caseNameable(vSMTechnologyK8sController);
                if (result == null) result = caseIdentifier(vSMTechnologyK8sController);
                if (result == null) result = caseFeatures(vSMTechnologyK8sController);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.VSM_TECHNOLOGY_STORAGE: {
                IVsmTechnologyStorage vSMTechnologyStorage = (IVsmTechnologyStorage)theEObject;
                T result = caseVsmTechnologyStorage(vSMTechnologyStorage);
                if (result == null) result = caseVsmTechnologyElement(vSMTechnologyStorage);
                if (result == null) result = caseBehaviorElement(vSMTechnologyStorage);
                if (result == null) result = caseArchimateElement(vSMTechnologyStorage);
                if (result == null) result = caseArchimateConcept(vSMTechnologyStorage);
                if (result == null) result = caseArchimateModelObject(vSMTechnologyStorage);
                if (result == null) result = caseCloneable(vSMTechnologyStorage);
                if (result == null) result = caseDocumentable(vSMTechnologyStorage);
                if (result == null) result = caseProperties(vSMTechnologyStorage);
                if (result == null) result = caseAdapter(vSMTechnologyStorage);
                if (result == null) result = caseNameable(vSMTechnologyStorage);
                if (result == null) result = caseIdentifier(vSMTechnologyStorage);
                if (result == null) result = caseFeatures(vSMTechnologyStorage);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }            
            case IArchimatePackage.TECHNOLOGY_INTERFACE: {
                ITechnologyInterface technologyInterface = (ITechnologyInterface)theEObject;
                T result = caseTechnologyInterface(technologyInterface);
                if (result == null) result = caseTechnologyElement(technologyInterface);
                if (result == null) result = caseActiveStructureElement(technologyInterface);
                if (result == null) result = caseStructureElement(technologyInterface);
                if (result == null) result = caseArchimateElement(technologyInterface);
                if (result == null) result = caseArchimateConcept(technologyInterface);
                if (result == null) result = caseArchimateModelObject(technologyInterface);
                if (result == null) result = caseCloneable(technologyInterface);
                if (result == null) result = caseDocumentable(technologyInterface);
                if (result == null) result = caseProperties(technologyInterface);
                if (result == null) result = caseAdapter(technologyInterface);
                if (result == null) result = caseNameable(technologyInterface);
                if (result == null) result = caseIdentifier(technologyInterface);
                if (result == null) result = caseFeatures(technologyInterface);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.TECHNOLOGY_INTERACTION: {
                ITechnologyInteraction technologyInteraction = (ITechnologyInteraction)theEObject;
                T result = caseTechnologyInteraction(technologyInteraction);
                if (result == null) result = caseTechnologyElement(technologyInteraction);
                if (result == null) result = caseBehaviorElement(technologyInteraction);
                if (result == null) result = caseArchimateElement(technologyInteraction);
                if (result == null) result = caseArchimateConcept(technologyInteraction);
                if (result == null) result = caseArchimateModelObject(technologyInteraction);
                if (result == null) result = caseCloneable(technologyInteraction);
                if (result == null) result = caseDocumentable(technologyInteraction);
                if (result == null) result = caseProperties(technologyInteraction);
                if (result == null) result = caseAdapter(technologyInteraction);
                if (result == null) result = caseNameable(technologyInteraction);
                if (result == null) result = caseIdentifier(technologyInteraction);
                if (result == null) result = caseFeatures(technologyInteraction);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.TECHNOLOGY_PROCESS: {
                ITechnologyProcess technologyProcess = (ITechnologyProcess)theEObject;
                T result = caseTechnologyProcess(technologyProcess);
                if (result == null) result = caseTechnologyElement(technologyProcess);
                if (result == null) result = caseBehaviorElement(technologyProcess);
                if (result == null) result = caseArchimateElement(technologyProcess);
                if (result == null) result = caseArchimateConcept(technologyProcess);
                if (result == null) result = caseArchimateModelObject(technologyProcess);
                if (result == null) result = caseCloneable(technologyProcess);
                if (result == null) result = caseDocumentable(technologyProcess);
                if (result == null) result = caseProperties(technologyProcess);
                if (result == null) result = caseAdapter(technologyProcess);
                if (result == null) result = caseNameable(technologyProcess);
                if (result == null) result = caseIdentifier(technologyProcess);
                if (result == null) result = caseFeatures(technologyProcess);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.TECHNOLOGY_SERVICE: {
                ITechnologyService technologyService = (ITechnologyService)theEObject;
                T result = caseTechnologyService(technologyService);
                if (result == null) result = caseTechnologyElement(technologyService);
                if (result == null) result = caseBehaviorElement(technologyService);
                if (result == null) result = caseArchimateElement(technologyService);
                if (result == null) result = caseArchimateConcept(technologyService);
                if (result == null) result = caseArchimateModelObject(technologyService);
                if (result == null) result = caseCloneable(technologyService);
                if (result == null) result = caseDocumentable(technologyService);
                if (result == null) result = caseProperties(technologyService);
                if (result == null) result = caseAdapter(technologyService);
                if (result == null) result = caseNameable(technologyService);
                if (result == null) result = caseIdentifier(technologyService);
                if (result == null) result = caseFeatures(technologyService);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.VALUE: {
                IValue value = (IValue)theEObject;
                T result = caseValue(value);
                if (result == null) result = caseMotivationElement(value);
                if (result == null) result = caseArchimateElement(value);
                if (result == null) result = caseArchimateConcept(value);
                if (result == null) result = caseArchimateModelObject(value);
                if (result == null) result = caseCloneable(value);
                if (result == null) result = caseDocumentable(value);
                if (result == null) result = caseProperties(value);
                if (result == null) result = caseAdapter(value);
                if (result == null) result = caseNameable(value);
                if (result == null) result = caseIdentifier(value);
                if (result == null) result = caseFeatures(value);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.VALUE_STREAM: {
                IValueStream valueStream = (IValueStream)theEObject;
                T result = caseValueStream(valueStream);
                if (result == null) result = caseStrategyBehaviorElement(valueStream);
                if (result == null) result = caseBehaviorElement(valueStream);
                if (result == null) result = caseStrategyElement(valueStream);
                if (result == null) result = caseArchimateElement(valueStream);
                if (result == null) result = caseArchimateConcept(valueStream);
                if (result == null) result = caseArchimateModelObject(valueStream);
                if (result == null) result = caseCloneable(valueStream);
                if (result == null) result = caseDocumentable(valueStream);
                if (result == null) result = caseProperties(valueStream);
                if (result == null) result = caseAdapter(valueStream);
                if (result == null) result = caseNameable(valueStream);
                if (result == null) result = caseIdentifier(valueStream);
                if (result == null) result = caseFeatures(valueStream);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.WORK_PACKAGE: {
                IWorkPackage workPackage = (IWorkPackage)theEObject;
                T result = caseWorkPackage(workPackage);
                if (result == null) result = caseImplementationMigrationElement(workPackage);
                if (result == null) result = caseBehaviorElement(workPackage);
                if (result == null) result = caseArchimateElement(workPackage);
                if (result == null) result = caseArchimateConcept(workPackage);
                if (result == null) result = caseArchimateModelObject(workPackage);
                if (result == null) result = caseCloneable(workPackage);
                if (result == null) result = caseDocumentable(workPackage);
                if (result == null) result = caseProperties(workPackage);
                if (result == null) result = caseAdapter(workPackage);
                if (result == null) result = caseNameable(workPackage);
                if (result == null) result = caseIdentifier(workPackage);
                if (result == null) result = caseFeatures(workPackage);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.ACCESS_RELATIONSHIP: {
                IAccessRelationship accessRelationship = (IAccessRelationship)theEObject;
                T result = caseAccessRelationship(accessRelationship);
                if (result == null) result = caseDependendencyRelationship(accessRelationship);
                if (result == null) result = caseArchimateRelationship(accessRelationship);
                if (result == null) result = caseArchimateConcept(accessRelationship);
                if (result == null) result = caseArchimateModelObject(accessRelationship);
                if (result == null) result = caseCloneable(accessRelationship);
                if (result == null) result = caseDocumentable(accessRelationship);
                if (result == null) result = caseProperties(accessRelationship);
                if (result == null) result = caseAdapter(accessRelationship);
                if (result == null) result = caseNameable(accessRelationship);
                if (result == null) result = caseIdentifier(accessRelationship);
                if (result == null) result = caseFeatures(accessRelationship);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.AGGREGATION_RELATIONSHIP: {
                IAggregationRelationship aggregationRelationship = (IAggregationRelationship)theEObject;
                T result = caseAggregationRelationship(aggregationRelationship);
                if (result == null) result = caseStructuralRelationship(aggregationRelationship);
                if (result == null) result = caseArchimateRelationship(aggregationRelationship);
                if (result == null) result = caseArchimateConcept(aggregationRelationship);
                if (result == null) result = caseArchimateModelObject(aggregationRelationship);
                if (result == null) result = caseCloneable(aggregationRelationship);
                if (result == null) result = caseDocumentable(aggregationRelationship);
                if (result == null) result = caseProperties(aggregationRelationship);
                if (result == null) result = caseAdapter(aggregationRelationship);
                if (result == null) result = caseNameable(aggregationRelationship);
                if (result == null) result = caseIdentifier(aggregationRelationship);
                if (result == null) result = caseFeatures(aggregationRelationship);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.ASSIGNMENT_RELATIONSHIP: {
                IAssignmentRelationship assignmentRelationship = (IAssignmentRelationship)theEObject;
                T result = caseAssignmentRelationship(assignmentRelationship);
                if (result == null) result = caseStructuralRelationship(assignmentRelationship);
                if (result == null) result = caseArchimateRelationship(assignmentRelationship);
                if (result == null) result = caseArchimateConcept(assignmentRelationship);
                if (result == null) result = caseArchimateModelObject(assignmentRelationship);
                if (result == null) result = caseCloneable(assignmentRelationship);
                if (result == null) result = caseDocumentable(assignmentRelationship);
                if (result == null) result = caseProperties(assignmentRelationship);
                if (result == null) result = caseAdapter(assignmentRelationship);
                if (result == null) result = caseNameable(assignmentRelationship);
                if (result == null) result = caseIdentifier(assignmentRelationship);
                if (result == null) result = caseFeatures(assignmentRelationship);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.ASSOCIATION_RELATIONSHIP: {
                IAssociationRelationship associationRelationship = (IAssociationRelationship)theEObject;
                T result = caseAssociationRelationship(associationRelationship);
                if (result == null) result = caseDependendencyRelationship(associationRelationship);
                if (result == null) result = caseArchimateRelationship(associationRelationship);
                if (result == null) result = caseArchimateConcept(associationRelationship);
                if (result == null) result = caseArchimateModelObject(associationRelationship);
                if (result == null) result = caseCloneable(associationRelationship);
                if (result == null) result = caseDocumentable(associationRelationship);
                if (result == null) result = caseProperties(associationRelationship);
                if (result == null) result = caseAdapter(associationRelationship);
                if (result == null) result = caseNameable(associationRelationship);
                if (result == null) result = caseIdentifier(associationRelationship);
                if (result == null) result = caseFeatures(associationRelationship);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.COMPOSITION_RELATIONSHIP: {
                ICompositionRelationship compositionRelationship = (ICompositionRelationship)theEObject;
                T result = caseCompositionRelationship(compositionRelationship);
                if (result == null) result = caseStructuralRelationship(compositionRelationship);
                if (result == null) result = caseArchimateRelationship(compositionRelationship);
                if (result == null) result = caseArchimateConcept(compositionRelationship);
                if (result == null) result = caseArchimateModelObject(compositionRelationship);
                if (result == null) result = caseCloneable(compositionRelationship);
                if (result == null) result = caseDocumentable(compositionRelationship);
                if (result == null) result = caseProperties(compositionRelationship);
                if (result == null) result = caseAdapter(compositionRelationship);
                if (result == null) result = caseNameable(compositionRelationship);
                if (result == null) result = caseIdentifier(compositionRelationship);
                if (result == null) result = caseFeatures(compositionRelationship);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.FLOW_RELATIONSHIP: {
                IFlowRelationship flowRelationship = (IFlowRelationship)theEObject;
                T result = caseFlowRelationship(flowRelationship);
                if (result == null) result = caseDynamicRelationship(flowRelationship);
                if (result == null) result = caseArchimateRelationship(flowRelationship);
                if (result == null) result = caseArchimateConcept(flowRelationship);
                if (result == null) result = caseArchimateModelObject(flowRelationship);
                if (result == null) result = caseCloneable(flowRelationship);
                if (result == null) result = caseDocumentable(flowRelationship);
                if (result == null) result = caseProperties(flowRelationship);
                if (result == null) result = caseAdapter(flowRelationship);
                if (result == null) result = caseNameable(flowRelationship);
                if (result == null) result = caseIdentifier(flowRelationship);
                if (result == null) result = caseFeatures(flowRelationship);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.INFLUENCE_RELATIONSHIP: {
                IInfluenceRelationship influenceRelationship = (IInfluenceRelationship)theEObject;
                T result = caseInfluenceRelationship(influenceRelationship);
                if (result == null) result = caseDependendencyRelationship(influenceRelationship);
                if (result == null) result = caseArchimateRelationship(influenceRelationship);
                if (result == null) result = caseArchimateConcept(influenceRelationship);
                if (result == null) result = caseArchimateModelObject(influenceRelationship);
                if (result == null) result = caseCloneable(influenceRelationship);
                if (result == null) result = caseDocumentable(influenceRelationship);
                if (result == null) result = caseProperties(influenceRelationship);
                if (result == null) result = caseAdapter(influenceRelationship);
                if (result == null) result = caseNameable(influenceRelationship);
                if (result == null) result = caseIdentifier(influenceRelationship);
                if (result == null) result = caseFeatures(influenceRelationship);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.REALIZATION_RELATIONSHIP: {
                IRealizationRelationship realizationRelationship = (IRealizationRelationship)theEObject;
                T result = caseRealizationRelationship(realizationRelationship);
                if (result == null) result = caseStructuralRelationship(realizationRelationship);
                if (result == null) result = caseArchimateRelationship(realizationRelationship);
                if (result == null) result = caseArchimateConcept(realizationRelationship);
                if (result == null) result = caseArchimateModelObject(realizationRelationship);
                if (result == null) result = caseCloneable(realizationRelationship);
                if (result == null) result = caseDocumentable(realizationRelationship);
                if (result == null) result = caseProperties(realizationRelationship);
                if (result == null) result = caseAdapter(realizationRelationship);
                if (result == null) result = caseNameable(realizationRelationship);
                if (result == null) result = caseIdentifier(realizationRelationship);
                if (result == null) result = caseFeatures(realizationRelationship);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.SERVING_RELATIONSHIP: {
                IServingRelationship servingRelationship = (IServingRelationship)theEObject;
                T result = caseServingRelationship(servingRelationship);
                if (result == null) result = caseDependendencyRelationship(servingRelationship);
                if (result == null) result = caseArchimateRelationship(servingRelationship);
                if (result == null) result = caseArchimateConcept(servingRelationship);
                if (result == null) result = caseArchimateModelObject(servingRelationship);
                if (result == null) result = caseCloneable(servingRelationship);
                if (result == null) result = caseDocumentable(servingRelationship);
                if (result == null) result = caseProperties(servingRelationship);
                if (result == null) result = caseAdapter(servingRelationship);
                if (result == null) result = caseNameable(servingRelationship);
                if (result == null) result = caseIdentifier(servingRelationship);
                if (result == null) result = caseFeatures(servingRelationship);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.SPECIALIZATION_RELATIONSHIP: {
                ISpecializationRelationship specializationRelationship = (ISpecializationRelationship)theEObject;
                T result = caseSpecializationRelationship(specializationRelationship);
                if (result == null) result = caseOtherRelationship(specializationRelationship);
                if (result == null) result = caseArchimateRelationship(specializationRelationship);
                if (result == null) result = caseArchimateConcept(specializationRelationship);
                if (result == null) result = caseArchimateModelObject(specializationRelationship);
                if (result == null) result = caseCloneable(specializationRelationship);
                if (result == null) result = caseDocumentable(specializationRelationship);
                if (result == null) result = caseProperties(specializationRelationship);
                if (result == null) result = caseAdapter(specializationRelationship);
                if (result == null) result = caseNameable(specializationRelationship);
                if (result == null) result = caseIdentifier(specializationRelationship);
                if (result == null) result = caseFeatures(specializationRelationship);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.TRIGGERING_RELATIONSHIP: {
                ITriggeringRelationship triggeringRelationship = (ITriggeringRelationship)theEObject;
                T result = caseTriggeringRelationship(triggeringRelationship);
                if (result == null) result = caseDynamicRelationship(triggeringRelationship);
                if (result == null) result = caseArchimateRelationship(triggeringRelationship);
                if (result == null) result = caseArchimateConcept(triggeringRelationship);
                if (result == null) result = caseArchimateModelObject(triggeringRelationship);
                if (result == null) result = caseCloneable(triggeringRelationship);
                if (result == null) result = caseDocumentable(triggeringRelationship);
                if (result == null) result = caseProperties(triggeringRelationship);
                if (result == null) result = caseAdapter(triggeringRelationship);
                if (result == null) result = caseNameable(triggeringRelationship);
                if (result == null) result = caseIdentifier(triggeringRelationship);
                if (result == null) result = caseFeatures(triggeringRelationship);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            
            case IArchimatePackage.PRODUCTAPPOWNS_RELATIONSHIP: {
                IProductAppOwnsRelationship productAppOwnsRelationship = (IProductAppOwnsRelationship)theEObject;
                T result = caseProductAppOwnsRelationship(productAppOwnsRelationship);
                if (result == null) result = caseDynamicRelationship(productAppOwnsRelationship);
                if (result == null) result = caseArchimateRelationship(productAppOwnsRelationship);
                if (result == null) result = caseArchimateConcept(productAppOwnsRelationship);
                if (result == null) result = caseArchimateModelObject(productAppOwnsRelationship);
                if (result == null) result = caseCloneable(productAppOwnsRelationship);
                if (result == null) result = caseDocumentable(productAppOwnsRelationship);
                if (result == null) result = caseProperties(productAppOwnsRelationship);
                if (result == null) result = caseAdapter(productAppOwnsRelationship);
                if (result == null) result = caseNameable(productAppOwnsRelationship);
                if (result == null) result = caseIdentifier(productAppOwnsRelationship);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            
            case IArchimatePackage.PRODUCTBIZOWNS_RELATIONSHIP: {
                IProductBizOwnsRelationship productBizOwnsRelationship = (IProductBizOwnsRelationship)theEObject;
                T result = caseProductBizOwnsRelationship(productBizOwnsRelationship);
                if (result == null) result = caseDynamicRelationship(productBizOwnsRelationship);
                if (result == null) result = caseArchimateRelationship(productBizOwnsRelationship);
                if (result == null) result = caseArchimateConcept(productBizOwnsRelationship);
                if (result == null) result = caseArchimateModelObject(productBizOwnsRelationship);
                if (result == null) result = caseCloneable(productBizOwnsRelationship);
                if (result == null) result = caseDocumentable(productBizOwnsRelationship);
                if (result == null) result = caseProperties(productBizOwnsRelationship);
                if (result == null) result = caseAdapter(productBizOwnsRelationship);
                if (result == null) result = caseNameable(productBizOwnsRelationship);
                if (result == null) result = caseIdentifier(productBizOwnsRelationship);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            
            case IArchimatePackage.PRODUCTDATAOWNS_RELATIONSHIP: {
                IProductDataOwnsRelationship productDataOwnsRelationship = (IProductDataOwnsRelationship)theEObject;
                T result = caseProductDataOwnsRelationship(productDataOwnsRelationship);
                if (result == null) result = caseDynamicRelationship(productDataOwnsRelationship);
                if (result == null) result = caseArchimateRelationship(productDataOwnsRelationship);
                if (result == null) result = caseArchimateConcept(productDataOwnsRelationship);
                if (result == null) result = caseArchimateModelObject(productDataOwnsRelationship);
                if (result == null) result = caseCloneable(productDataOwnsRelationship);
                if (result == null) result = caseDocumentable(productDataOwnsRelationship);
                if (result == null) result = caseProperties(productDataOwnsRelationship);
                if (result == null) result = caseAdapter(productDataOwnsRelationship);
                if (result == null) result = caseNameable(productDataOwnsRelationship);
                if (result == null) result = caseIdentifier(productDataOwnsRelationship);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            
            case IArchimatePackage.PRODUCTTECHOWNS_RELATIONSHIP: {
                IProductTechOwnsRelationship productTechOwnsRelationship = (IProductTechOwnsRelationship)theEObject;
                T result = caseProductTechOwnsRelationship(productTechOwnsRelationship);
                if (result == null) result = caseDynamicRelationship(productTechOwnsRelationship);
                if (result == null) result = caseArchimateRelationship(productTechOwnsRelationship);
                if (result == null) result = caseArchimateConcept(productTechOwnsRelationship);
                if (result == null) result = caseArchimateModelObject(productTechOwnsRelationship);
                if (result == null) result = caseCloneable(productTechOwnsRelationship);
                if (result == null) result = caseDocumentable(productTechOwnsRelationship);
                if (result == null) result = caseProperties(productTechOwnsRelationship);
                if (result == null) result = caseAdapter(productTechOwnsRelationship);
                if (result == null) result = caseNameable(productTechOwnsRelationship);
                if (result == null) result = caseIdentifier(productTechOwnsRelationship);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            
            
            case IArchimatePackage.PRODUCTTECHOSI_RELATIONSHIP: {
                IProductTechOSIRelationship productTechOSIRelationship = (IProductTechOSIRelationship)theEObject;
                T result = caseProductTechOSIRelationship(productTechOSIRelationship);
                if (result == null) result = caseDynamicRelationship(productTechOSIRelationship);
                if (result == null) result = caseArchimateRelationship(productTechOSIRelationship);
                if (result == null) result = caseArchimateConcept(productTechOSIRelationship);
                if (result == null) result = caseArchimateModelObject(productTechOSIRelationship);
                if (result == null) result = caseCloneable(productTechOSIRelationship);
                if (result == null) result = caseDocumentable(productTechOSIRelationship);
                if (result == null) result = caseProperties(productTechOSIRelationship);
                if (result == null) result = caseAdapter(productTechOSIRelationship);
                if (result == null) result = caseNameable(productTechOSIRelationship);
                if (result == null) result = caseIdentifier(productTechOSIRelationship);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.DIAGRAM_MODEL_COMPONENT: {
                IDiagramModelComponent diagramModelComponent = (IDiagramModelComponent)theEObject;
                T result = caseDiagramModelComponent(diagramModelComponent);
                if (result == null) result = caseCloneable(diagramModelComponent);
                if (result == null) result = caseArchimateModelObject(diagramModelComponent);
                if (result == null) result = caseIdentifier(diagramModelComponent);
                if (result == null) result = caseAdapter(diagramModelComponent);
                if (result == null) result = caseNameable(diagramModelComponent);
                if (result == null) result = caseFeatures(diagramModelComponent);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.CONNECTABLE: {
                IConnectable connectable = (IConnectable)theEObject;
                T result = caseConnectable(connectable);
                if (result == null) result = caseDiagramModelComponent(connectable);
                if (result == null) result = caseCloneable(connectable);
                if (result == null) result = caseArchimateModelObject(connectable);
                if (result == null) result = caseIdentifier(connectable);
                if (result == null) result = caseAdapter(connectable);
                if (result == null) result = caseNameable(connectable);
                if (result == null) result = caseFeatures(connectable);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.DIAGRAM_MODEL_CONTAINER: {
                IDiagramModelContainer diagramModelContainer = (IDiagramModelContainer)theEObject;
                T result = caseDiagramModelContainer(diagramModelContainer);
                if (result == null) result = caseDiagramModelComponent(diagramModelContainer);
                if (result == null) result = caseCloneable(diagramModelContainer);
                if (result == null) result = caseArchimateModelObject(diagramModelContainer);
                if (result == null) result = caseIdentifier(diagramModelContainer);
                if (result == null) result = caseAdapter(diagramModelContainer);
                if (result == null) result = caseNameable(diagramModelContainer);
                if (result == null) result = caseFeatures(diagramModelContainer);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.DIAGRAM_MODEL: {
                IDiagramModel diagramModel = (IDiagramModel)theEObject;
                T result = caseDiagramModel(diagramModel);
                if (result == null) result = caseDiagramModelContainer(diagramModel);
                if (result == null) result = caseDocumentable(diagramModel);
                if (result == null) result = caseProperties(diagramModel);
                if (result == null) result = caseFeatures(diagramModel);
                if (result == null) result = caseDiagramModelComponent(diagramModel);
                if (result == null) result = caseArchimateModelObject(diagramModel);
                if (result == null) result = caseAdapter(diagramModel);
                if (result == null) result = caseNameable(diagramModel);
                if (result == null) result = caseIdentifier(diagramModel);
                if (result == null) result = caseCloneable(diagramModel);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.DIAGRAM_MODEL_REFERENCE: {
                IDiagramModelReference diagramModelReference = (IDiagramModelReference)theEObject;
                T result = caseDiagramModelReference(diagramModelReference);
                if (result == null) result = caseDiagramModelObject(diagramModelReference);
                if (result == null) result = caseTextPosition(diagramModelReference);
                if (result == null) result = caseConnectable(diagramModelReference);
                if (result == null) result = caseFontAttribute(diagramModelReference);
                if (result == null) result = caseLineObject(diagramModelReference);
                if (result == null) result = caseTextAlignment(diagramModelReference);
                if (result == null) result = caseDiagramModelComponent(diagramModelReference);
                if (result == null) result = caseCloneable(diagramModelReference);
                if (result == null) result = caseArchimateModelObject(diagramModelReference);
                if (result == null) result = caseIdentifier(diagramModelReference);
                if (result == null) result = caseAdapter(diagramModelReference);
                if (result == null) result = caseNameable(diagramModelReference);
                if (result == null) result = caseFeatures(diagramModelReference);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.DIAGRAM_MODEL_OBJECT: {
                IDiagramModelObject diagramModelObject = (IDiagramModelObject)theEObject;
                T result = caseDiagramModelObject(diagramModelObject);
                if (result == null) result = caseConnectable(diagramModelObject);
                if (result == null) result = caseFontAttribute(diagramModelObject);
                if (result == null) result = caseLineObject(diagramModelObject);
                if (result == null) result = caseTextAlignment(diagramModelObject);
                if (result == null) result = caseDiagramModelComponent(diagramModelObject);
                if (result == null) result = caseCloneable(diagramModelObject);
                if (result == null) result = caseArchimateModelObject(diagramModelObject);
                if (result == null) result = caseIdentifier(diagramModelObject);
                if (result == null) result = caseAdapter(diagramModelObject);
                if (result == null) result = caseNameable(diagramModelObject);
                if (result == null) result = caseFeatures(diagramModelObject);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.DIAGRAM_MODEL_GROUP: {
                IDiagramModelGroup diagramModelGroup = (IDiagramModelGroup)theEObject;
                T result = caseDiagramModelGroup(diagramModelGroup);
                if (result == null) result = caseDiagramModelObject(diagramModelGroup);
                if (result == null) result = caseDiagramModelContainer(diagramModelGroup);
                if (result == null) result = caseDocumentable(diagramModelGroup);
                if (result == null) result = caseProperties(diagramModelGroup);
                if (result == null) result = caseTextPosition(diagramModelGroup);
                if (result == null) result = caseBorderType(diagramModelGroup);
                if (result == null) result = caseConnectable(diagramModelGroup);
                if (result == null) result = caseFontAttribute(diagramModelGroup);
                if (result == null) result = caseLineObject(diagramModelGroup);
                if (result == null) result = caseTextAlignment(diagramModelGroup);
                if (result == null) result = caseDiagramModelComponent(diagramModelGroup);
                if (result == null) result = caseCloneable(diagramModelGroup);
                if (result == null) result = caseArchimateModelObject(diagramModelGroup);
                if (result == null) result = caseIdentifier(diagramModelGroup);
                if (result == null) result = caseAdapter(diagramModelGroup);
                if (result == null) result = caseNameable(diagramModelGroup);
                if (result == null) result = caseFeatures(diagramModelGroup);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.DIAGRAM_MODEL_NOTE: {
                IDiagramModelNote diagramModelNote = (IDiagramModelNote)theEObject;
                T result = caseDiagramModelNote(diagramModelNote);
                if (result == null) result = caseDiagramModelObject(diagramModelNote);
                if (result == null) result = caseTextContent(diagramModelNote);
                if (result == null) result = caseTextPosition(diagramModelNote);
                if (result == null) result = caseProperties(diagramModelNote);
                if (result == null) result = caseBorderType(diagramModelNote);
                if (result == null) result = caseConnectable(diagramModelNote);
                if (result == null) result = caseFontAttribute(diagramModelNote);
                if (result == null) result = caseLineObject(diagramModelNote);
                if (result == null) result = caseTextAlignment(diagramModelNote);
                if (result == null) result = caseDiagramModelComponent(diagramModelNote);
                if (result == null) result = caseCloneable(diagramModelNote);
                if (result == null) result = caseArchimateModelObject(diagramModelNote);
                if (result == null) result = caseIdentifier(diagramModelNote);
                if (result == null) result = caseAdapter(diagramModelNote);
                if (result == null) result = caseNameable(diagramModelNote);
                if (result == null) result = caseFeatures(diagramModelNote);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.DIAGRAM_MODEL_IMAGE: {
                IDiagramModelImage diagramModelImage = (IDiagramModelImage)theEObject;
                T result = caseDiagramModelImage(diagramModelImage);
                if (result == null) result = caseDiagramModelObject(diagramModelImage);
                if (result == null) result = caseBorderObject(diagramModelImage);
                if (result == null) result = caseDiagramModelImageProvider(diagramModelImage);
                if (result == null) result = caseProperties(diagramModelImage);
                if (result == null) result = caseDocumentable(diagramModelImage);
                if (result == null) result = caseConnectable(diagramModelImage);
                if (result == null) result = caseFontAttribute(diagramModelImage);
                if (result == null) result = caseLineObject(diagramModelImage);
                if (result == null) result = caseTextAlignment(diagramModelImage);
                if (result == null) result = caseDiagramModelComponent(diagramModelImage);
                if (result == null) result = caseCloneable(diagramModelImage);
                if (result == null) result = caseArchimateModelObject(diagramModelImage);
                if (result == null) result = caseIdentifier(diagramModelImage);
                if (result == null) result = caseAdapter(diagramModelImage);
                if (result == null) result = caseNameable(diagramModelImage);
                if (result == null) result = caseFeatures(diagramModelImage);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.DIAGRAM_MODEL_CONNECTION: {
                IDiagramModelConnection diagramModelConnection = (IDiagramModelConnection)theEObject;
                T result = caseDiagramModelConnection(diagramModelConnection);
                if (result == null) result = caseConnectable(diagramModelConnection);
                if (result == null) result = caseFontAttribute(diagramModelConnection);
                if (result == null) result = caseProperties(diagramModelConnection);
                if (result == null) result = caseDocumentable(diagramModelConnection);
                if (result == null) result = caseLineObject(diagramModelConnection);
                if (result == null) result = caseDiagramModelComponent(diagramModelConnection);
                if (result == null) result = caseCloneable(diagramModelConnection);
                if (result == null) result = caseArchimateModelObject(diagramModelConnection);
                if (result == null) result = caseIdentifier(diagramModelConnection);
                if (result == null) result = caseAdapter(diagramModelConnection);
                if (result == null) result = caseNameable(diagramModelConnection);
                if (result == null) result = caseFeatures(diagramModelConnection);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.DIAGRAM_MODEL_BENDPOINT: {
                IDiagramModelBendpoint diagramModelBendpoint = (IDiagramModelBendpoint)theEObject;
                T result = caseDiagramModelBendpoint(diagramModelBendpoint);
                if (result == null) result = caseCloneable(diagramModelBendpoint);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.LINE_OBJECT: {
                ILineObject lineObject = (ILineObject)theEObject;
                T result = caseLineObject(lineObject);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.FONT_ATTRIBUTE: {
                IFontAttribute fontAttribute = (IFontAttribute)theEObject;
                T result = caseFontAttribute(fontAttribute);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.TEXT_POSITION: {
                ITextPosition textPosition = (ITextPosition)theEObject;
                T result = caseTextPosition(textPosition);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.TEXT_ALIGNMENT: {
                ITextAlignment textAlignment = (ITextAlignment)theEObject;
                T result = caseTextAlignment(textAlignment);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.BORDER_OBJECT: {
                IBorderObject borderObject = (IBorderObject)theEObject;
                T result = caseBorderObject(borderObject);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.BORDER_TYPE: {
                IBorderType borderType = (IBorderType)theEObject;
                T result = caseBorderType(borderType);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.DIAGRAM_MODEL_IMAGE_PROVIDER: {
                IDiagramModelImageProvider diagramModelImageProvider = (IDiagramModelImageProvider)theEObject;
                T result = caseDiagramModelImageProvider(diagramModelImageProvider);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.BOUNDS: {
                IBounds bounds = (IBounds)theEObject;
                T result = caseBounds(bounds);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.LOCKABLE: {
                ILockable lockable = (ILockable)theEObject;
                T result = caseLockable(lockable);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.ARCHIMATE_DIAGRAM_MODEL: {
                IArchimateDiagramModel archimateDiagramModel = (IArchimateDiagramModel)theEObject;
                T result = caseArchimateDiagramModel(archimateDiagramModel);
                if (result == null) result = caseDiagramModel(archimateDiagramModel);
                if (result == null) result = caseDiagramModelContainer(archimateDiagramModel);
                if (result == null) result = caseDocumentable(archimateDiagramModel);
                if (result == null) result = caseProperties(archimateDiagramModel);
                if (result == null) result = caseFeatures(archimateDiagramModel);
                if (result == null) result = caseDiagramModelComponent(archimateDiagramModel);
                if (result == null) result = caseArchimateModelObject(archimateDiagramModel);
                if (result == null) result = caseAdapter(archimateDiagramModel);
                if (result == null) result = caseNameable(archimateDiagramModel);
                if (result == null) result = caseIdentifier(archimateDiagramModel);
                if (result == null) result = caseCloneable(archimateDiagramModel);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.DIAGRAM_MODEL_ARCHIMATE_COMPONENT: {
                IDiagramModelArchimateComponent diagramModelArchimateComponent = (IDiagramModelArchimateComponent)theEObject;
                T result = caseDiagramModelArchimateComponent(diagramModelArchimateComponent);
                if (result == null) result = caseConnectable(diagramModelArchimateComponent);
                if (result == null) result = caseDiagramModelComponent(diagramModelArchimateComponent);
                if (result == null) result = caseCloneable(diagramModelArchimateComponent);
                if (result == null) result = caseArchimateModelObject(diagramModelArchimateComponent);
                if (result == null) result = caseIdentifier(diagramModelArchimateComponent);
                if (result == null) result = caseAdapter(diagramModelArchimateComponent);
                if (result == null) result = caseNameable(diagramModelArchimateComponent);
                if (result == null) result = caseFeatures(diagramModelArchimateComponent);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.DIAGRAM_MODEL_ARCHIMATE_OBJECT: {
                IDiagramModelArchimateObject diagramModelArchimateObject = (IDiagramModelArchimateObject)theEObject;
                T result = caseDiagramModelArchimateObject(diagramModelArchimateObject);
                if (result == null) result = caseDiagramModelObject(diagramModelArchimateObject);
                if (result == null) result = caseDiagramModelContainer(diagramModelArchimateObject);
                if (result == null) result = caseDiagramModelArchimateComponent(diagramModelArchimateObject);
                if (result == null) result = caseTextPosition(diagramModelArchimateObject);
                if (result == null) result = caseConnectable(diagramModelArchimateObject);
                if (result == null) result = caseFontAttribute(diagramModelArchimateObject);
                if (result == null) result = caseLineObject(diagramModelArchimateObject);
                if (result == null) result = caseTextAlignment(diagramModelArchimateObject);
                if (result == null) result = caseDiagramModelComponent(diagramModelArchimateObject);
                if (result == null) result = caseCloneable(diagramModelArchimateObject);
                if (result == null) result = caseArchimateModelObject(diagramModelArchimateObject);
                if (result == null) result = caseIdentifier(diagramModelArchimateObject);
                if (result == null) result = caseAdapter(diagramModelArchimateObject);
                if (result == null) result = caseNameable(diagramModelArchimateObject);
                if (result == null) result = caseFeatures(diagramModelArchimateObject);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.DIAGRAM_MODEL_ARCHIMATE_CONNECTION: {
                IDiagramModelArchimateConnection diagramModelArchimateConnection = (IDiagramModelArchimateConnection)theEObject;
                T result = caseDiagramModelArchimateConnection(diagramModelArchimateConnection);
                if (result == null) result = caseDiagramModelConnection(diagramModelArchimateConnection);
                if (result == null) result = caseDiagramModelArchimateComponent(diagramModelArchimateConnection);
                if (result == null) result = caseConnectable(diagramModelArchimateConnection);
                if (result == null) result = caseFontAttribute(diagramModelArchimateConnection);
                if (result == null) result = caseProperties(diagramModelArchimateConnection);
                if (result == null) result = caseDocumentable(diagramModelArchimateConnection);
                if (result == null) result = caseLineObject(diagramModelArchimateConnection);
                if (result == null) result = caseDiagramModelComponent(diagramModelArchimateConnection);
                if (result == null) result = caseCloneable(diagramModelArchimateConnection);
                if (result == null) result = caseArchimateModelObject(diagramModelArchimateConnection);
                if (result == null) result = caseIdentifier(diagramModelArchimateConnection);
                if (result == null) result = caseAdapter(diagramModelArchimateConnection);
                if (result == null) result = caseNameable(diagramModelArchimateConnection);
                if (result == null) result = caseFeatures(diagramModelArchimateConnection);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.SKETCH_MODEL: {
                ISketchModel sketchModel = (ISketchModel)theEObject;
                T result = caseSketchModel(sketchModel);
                if (result == null) result = caseDiagramModel(sketchModel);
                if (result == null) result = caseDiagramModelContainer(sketchModel);
                if (result == null) result = caseDocumentable(sketchModel);
                if (result == null) result = caseProperties(sketchModel);
                if (result == null) result = caseFeatures(sketchModel);
                if (result == null) result = caseDiagramModelComponent(sketchModel);
                if (result == null) result = caseArchimateModelObject(sketchModel);
                if (result == null) result = caseAdapter(sketchModel);
                if (result == null) result = caseNameable(sketchModel);
                if (result == null) result = caseIdentifier(sketchModel);
                if (result == null) result = caseCloneable(sketchModel);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.SKETCH_MODEL_STICKY: {
                ISketchModelSticky sketchModelSticky = (ISketchModelSticky)theEObject;
                T result = caseSketchModelSticky(sketchModelSticky);
                if (result == null) result = caseDiagramModelObject(sketchModelSticky);
                if (result == null) result = caseDiagramModelContainer(sketchModelSticky);
                if (result == null) result = caseTextContent(sketchModelSticky);
                if (result == null) result = caseProperties(sketchModelSticky);
                if (result == null) result = caseTextPosition(sketchModelSticky);
                if (result == null) result = caseConnectable(sketchModelSticky);
                if (result == null) result = caseFontAttribute(sketchModelSticky);
                if (result == null) result = caseLineObject(sketchModelSticky);
                if (result == null) result = caseTextAlignment(sketchModelSticky);
                if (result == null) result = caseDiagramModelComponent(sketchModelSticky);
                if (result == null) result = caseCloneable(sketchModelSticky);
                if (result == null) result = caseArchimateModelObject(sketchModelSticky);
                if (result == null) result = caseIdentifier(sketchModelSticky);
                if (result == null) result = caseAdapter(sketchModelSticky);
                if (result == null) result = caseNameable(sketchModelSticky);
                if (result == null) result = caseFeatures(sketchModelSticky);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case IArchimatePackage.SKETCH_MODEL_ACTOR: {
                ISketchModelActor sketchModelActor = (ISketchModelActor)theEObject;
                T result = caseSketchModelActor(sketchModelActor);
                if (result == null) result = caseDiagramModelObject(sketchModelActor);
                if (result == null) result = caseDocumentable(sketchModelActor);
                if (result == null) result = caseProperties(sketchModelActor);
                if (result == null) result = caseConnectable(sketchModelActor);
                if (result == null) result = caseFontAttribute(sketchModelActor);
                if (result == null) result = caseLineObject(sketchModelActor);
                if (result == null) result = caseTextAlignment(sketchModelActor);
                if (result == null) result = caseDiagramModelComponent(sketchModelActor);
                if (result == null) result = caseCloneable(sketchModelActor);
                if (result == null) result = caseArchimateModelObject(sketchModelActor);
                if (result == null) result = caseIdentifier(sketchModelActor);
                if (result == null) result = caseAdapter(sketchModelActor);
                if (result == null) result = caseNameable(sketchModelActor);
                if (result == null) result = caseFeatures(sketchModelActor);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            default: return defaultCase(theEObject);
        }
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Adapter</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Adapter</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseAdapter(IAdapter object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Identifier</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Identifier</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseIdentifier(IIdentifier object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Property</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Property</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseProperty(IProperty object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Properties</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Properties</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseProperties(IProperties object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Feature</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Feature</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseFeature(IFeature object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Features</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Features</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseFeatures(IFeatures object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Metadata</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Metadata</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseMetadata(IMetadata object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Nameable</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Nameable</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseNameable(INameable object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Text Content</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Text Content</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseTextContent(ITextContent object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Documentable</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Documentable</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseDocumentable(IDocumentable object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Cloneable</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Cloneable</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseCloneable(ICloneable object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Folder Container</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Folder Container</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseFolderContainer(IFolderContainer object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Model</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Model</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseArchimateModel(IArchimateModel object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Junction</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Junction</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseJunction(IJunction object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Resource</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Resource</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseResource(IResource object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Folder</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Folder</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseFolder(IFolder object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Model Object</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Model Object</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseArchimateModelObject(IArchimateModelObject object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Concept</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Concept</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseArchimateConcept(IArchimateConcept object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Element</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Element</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseArchimateElement(IArchimateElement object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Artifact</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Artifact</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseArtifact(IArtifact object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Node</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Node</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseNode(INode object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Outcome</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Outcome</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseOutcome(IOutcome object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>System Software</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>System Software</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseSystemSoftware(ISystemSoftware object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Technology Collaboration</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Technology Collaboration</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseTechnologyCollaboration(ITechnologyCollaboration object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Technology Event</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Technology Event</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseTechnologyEvent(ITechnologyEvent object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Technology Function</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Technology Function</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseTechnologyFunction(ITechnologyFunction object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>VsmTechnologyTask</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>VsmTechnologyTask</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseVsmTechnologyTask(IVsmTechnologyTask object) {
        return null;
    }
    
    /**
     * Returns the result of interpreting the object as an instance of '<em>VsmTechnologyK8sPod</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>VsmTechnologyK8sPod</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseVsmTechnologyK8sPod(IVsmTechnologyK8sPod object) {
        return null;
    }
    
    /**
     * Returns the result of interpreting the object as an instance of '<em>VsmTechnologyK8sContainer</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>VsmTechnologyK8sContainer</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseVsmTechnologyK8sContainer(IVsmTechnologyK8sContainer object) {
        return null;
    }
    
    /**
     * Returns the result of interpreting the object as an instance of '<em>VsmTechnologyK8sController</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>VsmTechnologyK8sController</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseVsmTechnologyK8sController(IVsmTechnologyK8sController object) {
        return null;
    }
    
    /**
     * Returns the result of interpreting the object as an instance of '<em>VsmTechnologyStorage</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>VsmTechnologyStorage</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseVsmTechnologyStorage(IVsmTechnologyStorage object) {
        return null;
    }
    
    /**
     * Returns the result of interpreting the object as an instance of '<em>Technology Interface</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Technology Interface</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseTechnologyInterface(ITechnologyInterface object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Technology Interaction</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Technology Interaction</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseTechnologyInteraction(ITechnologyInteraction object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Technology Object</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Technology Object</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseTechnologyObject(ITechnologyObject object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Technology Process</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Technology Process</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseTechnologyProcess(ITechnologyProcess object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Technology Service</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Technology Service</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseTechnologyService(ITechnologyService object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Device</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Device</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseDevice(IDevice object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Distribution Network</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Distribution Network</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseDistributionNetwork(IDistributionNetwork object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Equipment</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Equipment</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseEquipment(IEquipment object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Facility</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Facility</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseFacility(IFacility object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Motivation Element</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Motivation Element</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseMotivationElement(IMotivationElement object) {
        return null;
    } 
    
    /**
     * Returns the result of interpreting the object as an instance of '<em>VsmData Element</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>VsmData Element</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseVsmDataElement(IVsmDataElement object) {
        return null;
    }     

    
    /**
     * Returns the result of interpreting the object as an instance of '<em>Data Element</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Data Element</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseDataElement(IDataElement object) {
        return null;
    }     

    /**
     * Returns the result of interpreting the object as an instance of '<em>VsmLandscape Element</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>VsmLandscape Element</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseVsmLandscapeElement(IVsmLandscapeElement object) {
        return null;
    } 

    /**
     * Returns the result of interpreting the object as an instance of '<em>DynamicsPersonal Element</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>DynamicsPersonal Element</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseDynamicsPersonalElement(IDynamicsPersonalElement object) {
        return null;
    }   
    

    /**
     * Returns the result of interpreting the object as an instance of '<em>DynamicsTeam Element</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>DynamicsTeam Element</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseDynamicsTeamElement(IDynamicsTeamElement object) {
        return null;
    }          

    /**
     * Returns the result of interpreting the object as an instance of '<em>DynamicsOrg Element</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>DynamicsOrg Element</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseDynamicsOrgElement(IDynamicsOrgElement object) {
        return null;
    }    
    
    /**
     * Returns the result of interpreting the object as an instance of '<em>DynamicsEnv Element</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>DynamicsEnv Element</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseDynamicsEnvElement(IDynamicsEnvElement object) {
        return null;
    }    
    
    public T caseVsmSrmElement(IVsmSrmElement object) {
        return null;
    }       
    
    
    public T caseContextmeBasicElement(IContextmeBasicElement object) {
        return null;
    }        
    /**
     * Returns the result of interpreting the object as an instance of '<em>Stakeholder</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Stakeholder</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseStakeholder(IStakeholder object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Driver</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Driver</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseDriver(IDriver object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Assessment</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Assessment</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseAssessment(IAssessment object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Goal</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Goal</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseGoal(IGoal object) {
        return null;
    }

    
    /**
     * Returns the result of interpreting the object as an instance of '<em>PersonalFeeling</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>PersonalFeeling</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseDynamicsPersonalFeeling(IDynamicsPersonalFeeling object) {
        return null;
    }    
    
    /**
     * Returns the result of interpreting the object as an instance of '<em>PersonalPrediction</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>PersonalPrediction</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseDynamicsPersonalPrediction(IDynamicsPersonalPrediction object) {
        return null;
    }
    
    /**
     * Returns the result of interpreting the object as an instance of '<em>PersonalCorrection</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>PersonalCorrection</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseDynamicsPersonalCorrection(IDynamicsPersonalCorrection object) {
        return null;
    }
    
    
    /**
     * Returns the result of interpreting the object as an instance of '<em>PersonalByproduct</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>PersonalByproduct</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseDynamicsPersonalByproduct(IDynamicsPersonalByproduct object) {
        return null;
    }
    
    /**
     * Returns the result of interpreting the object as an instance of '<em>TeamPrediction</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>TeamPrediction</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseDynamicsTeamPrediction(IDynamicsTeamPrediction object) {
        return null;
    }
    
    
    /**
     * Returns the result of interpreting the object as an instance of '<em>TeamFeeling</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>TeamFeeling</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseDynamicsTeamFeeling(IDynamicsTeamFeeling object) {
        return null;
    }    
    
    /**
     * Returns the result of interpreting the object as an instance of '<em>OrgPrediction</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>OrgPrediction</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseDynamicsOrgPrediction(IDynamicsOrgPrediction object) {
        return null;
    }
    
    /**
     * Returns the result of interpreting the object as an instance of '<em>EnvPrediction</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>EnvPrediction</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseDynamicsEnvPrediction(IDynamicsEnvPrediction object) {
        return null;
    }    
    
    
    public T caseVsmSrmStatus(IVsmSrmStatus object) {
        return null;
    }       
    
    public T caseVsmSrmAction(IVsmSrmAction object) {
        return null;
    }   
    
    public T caseVsmSrmRoleFunction(IVsmSrmRoleFunction object) {
        return null;
    }   
    
    public T caseVsmSrmRoleUser(IVsmSrmRoleUser object) {
        return null;
    }   
    
    public T caseVsmSrmUser(IVsmSrmUser object) {
        return null;
    }   
    
    
    public T caseContextmeBasicConcept(IContextmeBasicConcept object) {
        return null;
    }      
    
    public T caseContextmeBasicSituation(IContextmeBasicSituation object) {
        return null;
    }
    
    public T caseContextmeBasicQuestioning(IContextmeBasicQuestioning object) {
        return null;
    }
    
    public T caseContextmeBasicCurious(IContextmeBasicCurious object) {
        return null;
    }
    
    public T caseContextmeBasicLink(IContextmeBasicLink object) {
        return null;
    }
    
    public T caseContextmeBasicClarify(IContextmeBasicClarify object) {
        return null;
    }
    
    public T caseContextmeBasicExpress(IContextmeBasicExpress object) {
        return null;
    }
    
    public T caseContextmeBasicDiscuss(IContextmeBasicDiscuss object) {
        return null;
    }
    
    public T caseContextmeBasicNote(IContextmeBasicNote object) {
        return null;
    }
  
    
    /**
     * Returns the result of interpreting the object as an instance of '<em>VsmDataDatabase</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>VsmDataDatabase</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseVsmDataDatabase(IVsmDataDatabase object) {
        return null;
    }
    
    /**
     * Returns the result of interpreting the object as an instance of '<em>VsmDataSchema</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>VsmDataSchema</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseVsmDataSchema(IVsmDataSchema object) {
        return null;
    }
    
    /**
     * Returns the result of interpreting the object as an instance of '<em>VsmDataView</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>VsmDataView</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseVsmDataView(IVsmDataView object) {
        return null;
    }
    
    /**
     * Returns the result of interpreting the object as an instance of '<em>VsmDataTable</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>VsmDataTable</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseVsmDataTable(IVsmDataTable object) {
        return null;
    }
    
    /**
     * Returns the result of interpreting the object as an instance of '<em>VsmDataColumn</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>VsmDataColumn</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseVsmDataColumn(IVsmDataColumn object) {
        return null;
    }
    
    /**
     * Returns the result of interpreting the object as an instance of '<em>VsmDataTask</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>VsmDataTask</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseVsmDataTask(IVsmDataTask object) {
        return null;
    }

    
    /**
     * Returns the result of interpreting the object as an instance of '<em>VsmLandscapeInfrastructureOctagon</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>VsmLandscapeInfrastructureOctagon</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseVsmLandscapeInfrastructureOctagon(IVsmLandscapeInfrastructureOctagon object) {
        return null;
    }  
        
    
    /**
     * Returns the result of interpreting the object as an instance of '<em>VsmLandscapeProductOctagon</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>VsmLandscapeProductOctagon</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseVsmLandscapeProductOctagon(IVsmLandscapeProductOctagon object) {
        return null;
    }  
    
    /**
     * Returns the result of interpreting the object as an instance of '<em>VsmLandscapeLayerGeneral</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>VsmLandscapeLayerGeneral</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseVsmLandscapeLayerGeneral(IVsmLandscapeLayerGeneral object) {
        return null;
    }  
    
    public T caseVsmLandscapeLayerBusiness(IVsmLandscapeLayerBusiness object) {
        return null;
    }  
    
    public T caseVsmLandscapeLayerApplication(IVsmLandscapeLayerApplication object) {
        return null;
    }  
    
    public T caseVsmLandscapeLayerTechnology(IVsmLandscapeLayerTechnology object) {
        return null;
    }  
    
    public T caseVsmLandscapeLayerData(IVsmLandscapeLayerData object) {
        return null;
    }
    
    public T caseVsmLandscapeLayerB2a(IVsmLandscapeLayerB2a object) {
        return null;
    }  
    
    public T caseVsmLandscapeLayerA2d(IVsmLandscapeLayerA2d object) {
        return null;
    }
    
    public T caseVsmLandscapeLayerA2t(IVsmLandscapeLayerA2t object) {
        return null;
    }  
 
    
    /**
     * Returns the result of interpreting the object as an instance of '<em>Grouping</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Grouping</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseGrouping(IGrouping object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Implementation Event</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Implementation Event</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseImplementationEvent(IImplementationEvent object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Requirement</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Requirement</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseRequirement(IRequirement object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Constraint</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Constraint</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseConstraint(IConstraint object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Course Of Action</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Course Of Action</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseCourseOfAction(ICourseOfAction object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Principle</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Principle</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T casePrinciple(IPrinciple object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Implementation Migration Element</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Implementation Migration Element</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseImplementationMigrationElement(IImplementationMigrationElement object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Composite Element</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Composite Element</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseCompositeElement(ICompositeElement object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Behavior Element</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Behavior Element</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseBehaviorElement(IBehaviorElement object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Strategy Behavior Element</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Strategy Behavior Element</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseStrategyBehaviorElement(IStrategyBehaviorElement object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Structure Element</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Structure Element</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseStructureElement(IStructureElement object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Active Structure Element</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Active Structure Element</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseActiveStructureElement(IActiveStructureElement object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Passive Structure Element</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Passive Structure Element</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T casePassiveStructureElement(IPassiveStructureElement object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Structural Relationship</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Structural Relationship</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseStructuralRelationship(IStructuralRelationship object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Dependendency Relationship</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Dependendency Relationship</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseDependendencyRelationship(IDependendencyRelationship object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Dynamic Relationship</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Dynamic Relationship</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseDynamicRelationship(IDynamicRelationship object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Other Relationship</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Other Relationship</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseOtherRelationship(IOtherRelationship object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Strategy Element</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Strategy Element</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseStrategyElement(IStrategyElement object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Work Package</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Work Package</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseWorkPackage(IWorkPackage object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Deliverable</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Deliverable</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseDeliverable(IDeliverable object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Plateau</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Plateau</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T casePlateau(IPlateau object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Gap</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Gap</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseGap(IGap object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Relationship</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Relationship</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseArchimateRelationship(IArchimateRelationship object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Diagram Model</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Diagram Model</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseDiagramModel(IDiagramModel object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Diagram Model</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Diagram Model</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseArchimateDiagramModel(IArchimateDiagramModel object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Diagram Model Archimate Component</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Diagram Model Archimate Component</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseDiagramModelArchimateComponent(IDiagramModelArchimateComponent object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Diagram Model Reference</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Diagram Model Reference</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseDiagramModelReference(IDiagramModelReference object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Diagram Model Component</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Diagram Model Component</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseDiagramModelComponent(IDiagramModelComponent object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Connectable</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Connectable</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseConnectable(IConnectable object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Diagram Model Object</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Diagram Model Object</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseDiagramModelObject(IDiagramModelObject object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Diagram Model Archimate Object</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Diagram Model Archimate Object</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseDiagramModelArchimateObject(IDiagramModelArchimateObject object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Diagram Model Container</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Diagram Model Container</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseDiagramModelContainer(IDiagramModelContainer object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Diagram Model Group</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Diagram Model Group</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseDiagramModelGroup(IDiagramModelGroup object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Diagram Model Note</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Diagram Model Note</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseDiagramModelNote(IDiagramModelNote object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Diagram Model Image</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Diagram Model Image</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseDiagramModelImage(IDiagramModelImage object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Diagram Model Connection</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Diagram Model Connection</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseDiagramModelConnection(IDiagramModelConnection object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Diagram Model Archimate Connection</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Diagram Model Archimate Connection</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseDiagramModelArchimateConnection(IDiagramModelArchimateConnection object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Diagram Model Bendpoint</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Diagram Model Bendpoint</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseDiagramModelBendpoint(IDiagramModelBendpoint object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Line Object</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Line Object</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseLineObject(ILineObject object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Font Attribute</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Font Attribute</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseFontAttribute(IFontAttribute object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Text Position</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Text Position</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseTextPosition(ITextPosition object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Text Alignment</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Text Alignment</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseTextAlignment(ITextAlignment object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Border Object</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Border Object</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseBorderObject(IBorderObject object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Border Type</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Border Type</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseBorderType(IBorderType object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Diagram Model Image Provider</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Diagram Model Image Provider</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseDiagramModelImageProvider(IDiagramModelImageProvider object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Bounds</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Bounds</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseBounds(IBounds object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Lockable</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Lockable</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseLockable(ILockable object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Sketch Model</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Sketch Model</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseSketchModel(ISketchModel object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Sketch Model Sticky</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Sketch Model Sticky</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseSketchModelSticky(ISketchModelSticky object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Sketch Model Actor</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Sketch Model Actor</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseSketchModelActor(ISketchModelActor object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Business Actor</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Business Actor</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseBusinessActor(IBusinessActor object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Business Collaboration</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Business Collaboration</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseBusinessCollaboration(IBusinessCollaboration object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Contract</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Contract</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseContract(IContract object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Business Event</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Business Event</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseBusinessEvent(IBusinessEvent object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Business Function</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Business Function</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseBusinessFunction(IBusinessFunction object) {
        return null;
    }
    
    /**
     * Returns the result of interpreting the object as an instance of '<em>VsmBusinessEpic</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>VsmBusinessEpic</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseVsmBusinessEpic(IVsmBusinessEpic object) {
        return null;
    }
    
    /**
     * Returns the result of interpreting the object as an instance of '<em>VsmBusinessUs</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>VsmBusinessUs</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseVsmBusinessUs(IVsmBusinessUs object) {
        return null;
    }
    
    /**
     * Returns the result of interpreting the object as an instance of '<em>VsmBusinessTask</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>VsmBusinessTask</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseVsmBusinessTask(IVsmBusinessTask object) {
        return null;
    }    

    /**
     * Returns the result of interpreting the object as an instance of '<em>VsmBusinessAc</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>VsmBusinessAc</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseVsmBusinessAc(IVsmBusinessAc object) {
        return null;
    }
    
    /**
     * Returns the result of interpreting the object as an instance of '<em>VsmBusinessUc</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>VsmBusinessUc</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseVsmBusinessUc(IVsmBusinessUc object) {
        return null;
    }
    
    /**
     * Returns the result of interpreting the object as an instance of '<em>VsmBusinessFr</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>VsmBusinessFr</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseVsmBusinessFr(IVsmBusinessFr object) {
        return null;
    }
    
    /**
     * Returns the result of interpreting the object as an instance of '<em>VsmBusinessNfr</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>VsmBusinessNfr</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseVsmBusinessNfr(IVsmBusinessNfr object) {
        return null;
    }
    
    /**
     * Returns the result of interpreting the object as an instance of '<em>Business Interaction</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Business Interaction</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseBusinessInteraction(IBusinessInteraction object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Business Interface</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Business Interface</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseBusinessInterface(IBusinessInterface object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Meaning</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Meaning</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseMeaning(IMeaning object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Business Object</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Business Object</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseBusinessObject(IBusinessObject object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Business Process</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Business Process</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseBusinessProcess(IBusinessProcess object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Product</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Product</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseProduct(IProduct object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Representation</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Representation</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseRepresentation(IRepresentation object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Business Role</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Business Role</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseBusinessRole(IBusinessRole object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Business Service</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Business Service</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseBusinessService(IBusinessService object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Capability</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Capability</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseCapability(ICapability object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Communication Network</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Communication Network</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseCommunicationNetwork(ICommunicationNetwork object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Value</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Value</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseValue(IValue object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Value Stream</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Value Stream</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseValueStream(IValueStream object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Location</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Location</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseLocation(ILocation object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Material</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Material</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseMaterial(IMaterial object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Application Component</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Application Component</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseApplicationComponent(IApplicationComponent object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Application Event</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Application Event</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseApplicationEvent(IApplicationEvent object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Application Function</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Application Function</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseApplicationFunction(IApplicationFunction object) {
        return null;
    }
    
    /**
     * Returns the result of interpreting the object as an instance of '<em>VsmApplicationTask</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>VsmApplicationTask</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseVsmApplicationTask(IVsmApplicationTask object) {
        return null;
    }
    
    /**
     * Returns the result of interpreting the object as an instance of '<em>VsmDataGitRepo</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>VsmDataGitRepo</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseVsmDataGitRepo(IVsmDataGitRepo object) {
        return null;
    }
    
    /**
     * Returns the result of interpreting the object as an instance of '<em>VsmDataGitBranch</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>VsmDataGitBranch</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseVsmDataGitBranch(IVsmDataGitBranch object) {
        return null;
    }
    
    /**
     * Returns the result of interpreting the object as an instance of '<em>VsmDataSrcDir</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>VsmDataSrcDir</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseVsmDataSrcDir(IVsmDataSrcDir object) {
        return null;
    }
    
    /**
     * Returns the result of interpreting the object as an instance of '<em>VsmDataSrcFile</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>VsmDataSrcFile</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseVsmDataSrcFile(IVsmDataSrcFile object) {
        return null;
    }
    
    /**
     * Returns the result of interpreting the object as an instance of '<em>VsmApplicationClass</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>VsmApplicationClass</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseVsmApplicationClass(IVsmApplicationClass object) {
        return null;
    }
    
    /**
     * Returns the result of interpreting the object as an instance of '<em>VsmApplicationModule</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>VsmApplicationModule</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseVsmApplicationModule(IVsmApplicationModule object) {
        return null;
    }
    
    /**
     * Returns the result of interpreting the object as an instance of '<em>VsmApplicationMethod</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>VsmApplicationMethod</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseVsmApplicationMethod(IVsmApplicationMethod object) {
        return null;
    }
    
    /**
     * Returns the result of interpreting the object as an instance of '<em>Application Interaction</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Application Interaction</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseApplicationInteraction(IApplicationInteraction object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Application Interface</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Application Interface</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseApplicationInterface(IApplicationInterface object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Application Process</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Application Process</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseApplicationProcess(IApplicationProcess object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Data Object</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Data Object</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseDataObject(IDataObject object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Application Service</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Application Service</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseApplicationService(IApplicationService object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Application Collaboration</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Application Collaboration</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseApplicationCollaboration(IApplicationCollaboration object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Access Relationship</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Access Relationship</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseAccessRelationship(IAccessRelationship object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Aggregation Relationship</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Aggregation Relationship</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseAggregationRelationship(IAggregationRelationship object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Assignment Relationship</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Assignment Relationship</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseAssignmentRelationship(IAssignmentRelationship object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Association Relationship</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Association Relationship</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseAssociationRelationship(IAssociationRelationship object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Composition Relationship</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Composition Relationship</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseCompositionRelationship(ICompositionRelationship object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Flow Relationship</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Flow Relationship</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseFlowRelationship(IFlowRelationship object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Triggering Relationship</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Triggering Relationship</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseTriggeringRelationship(ITriggeringRelationship object) {
        return null;
    }

    

    
    public T caseProductAppOwnsRelationship(IProductAppOwnsRelationship object) {
        return null;
    }
    
    public T caseProductBizOwnsRelationship(IProductBizOwnsRelationship object) {
        return null;
    }
    
    public T caseProductDataOwnsRelationship(IProductDataOwnsRelationship object) {
        return null;
    }
    
    public T caseProductTechOwnsRelationship(IProductTechOwnsRelationship object) {
        return null;
    }
    
    
    
    /**
     * Returns the result of interpreting the object as an instance of '<em>OSI Relationship</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>OSI Relationship</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
     
     
    public T caseProductTechOSIRelationship(IProductTechOSIRelationship object) {
        return null;
    }
    
    
    /**
     * Returns the result of interpreting the object as an instance of '<em>Influence Relationship</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Influence Relationship</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseInfluenceRelationship(IInfluenceRelationship object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Realization Relationship</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Realization Relationship</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseRealizationRelationship(IRealizationRelationship object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Serving Relationship</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Serving Relationship</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseServingRelationship(IServingRelationship object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Specialization Relationship</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Specialization Relationship</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseSpecializationRelationship(ISpecializationRelationship object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>VsmBusiness Element</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>VsmBusiness Element</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseVsmBusinessElement(IVsmBusinessElement object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Business Element</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Business Element</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseBusinessElement(IBusinessElement object) {
        return null;
    }
    
    /**
     * Returns the result of interpreting the object as an instance of '<em>VsmVia Element</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>VsmVia Element</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseVsmViaElement(IVsmViaElement object) {
        return null;
    }    
    
    /**
     * Returns the result of interpreting the object as an instance of '<em>VsmViaB2aUi Element</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>VsmViaB2aUi Element</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseVsmViaB2aUi(IVsmViaB2aUi object) {
        return null;
    }
    
    /**
     * Returns the result of interpreting the object as an instance of '<em>VsmViaB2aIntegration Element</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>VsmViaB2aIntegration Element</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseVsmViaB2aIntegration(IVsmViaB2aIntegration object) {
        return null;
    }
    
    /**
     * Returns the result of interpreting the object as an instance of '<em>VsmViaB2aDs Element</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>VsmViaB2aDs Element</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseVsmViaB2aDs(IVsmViaB2aDs object) {
        return null;
    }
    
    /**     * Returns the result of interpreting the object as an instance of '<em>VsmViaA2dData Element</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>VsmViaA2dData Element</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseVsmViaA2dData(IVsmViaA2dData object) {
        return null;
    }
    
    /**
     * Returns the result of interpreting the object as an instance of '<em>VsmViaA2tYaml Element</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>VsmViaA2tYaml Element</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseVsmViaA2tYaml(IVsmViaA2tYaml object) {
        return null;
    }
    
    
    /**
     * Returns the result of interpreting the object as an instance of '<em>VsmApplication Element</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>VsmApplication Element</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseVsmApplicationElement(IVsmApplicationElement object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Application Element</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Application Element</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseApplicationElement(IApplicationElement object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>VsmTechnology Element</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>VsmTechnology Element</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseVsmTechnologyElement(IVsmTechnologyElement object) {
        return null;
    }
    
    /**
     * Returns the result of interpreting the object as an instance of '<em>Technology Element</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Technology Element</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseTechnologyElement(ITechnologyElement object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Physical Element</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Physical Element</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T casePhysicalElement(IPhysicalElement object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Path</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Path</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T casePath(IPath object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch, but this is the last case anyway.
     * <!-- end-user-doc -->
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject)
     * @generated
     */
    @Override
    public T defaultCase(EObject object) {
        return null;
    }

} //ArchimateSwitch
