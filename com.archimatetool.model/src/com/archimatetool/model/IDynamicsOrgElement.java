/**
 * This program and the accompanying materials
 * are made available under the terms of the License
 * which accompanies this distribution in the file LICENSE.txt
 */
package com.archimatetool.model;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>DynamicsOrg </b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.archimatetool.model.IArchimatePackage#getDynamicsOrgElement()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface IDynamicsOrgElement extends IArchimateElement {
} // IDynamicsOrgElement
