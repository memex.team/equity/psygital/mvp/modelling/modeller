/**
 * This program and the accompanying materials
 * are made available under the terms of the License
 * which accompanies this distribution in the file LICENSE.txt
 */
package com.archimatetool.model;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ContextmeBasicConcept</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.archimatetool.model.IArchimatePackage#getContextmeBasicConcept()
 * @model
 * @generated
 */
public interface IContextmeBasicConcept extends IContextmeBasicElement, IBehaviorElement {
} // IContextmeBasicConcept
