package com.archimatetool.model;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {

    private static final String BUNDLE_NAME = "com.archimatetool.model.messages"; //$NON-NLS-1$

    public static String FolderType_0;

    public static String FolderType_1;

    public static String FolderType_2;

    public static String FolderType_3;

    public static String FolderType_4;

    public static String FolderType_5;

    public static String FolderType_6;

    public static String FolderType_7;

    public static String FolderType_8;

    public static String FolderType_9;
    
    public static String FolderType_20;

    public static String FolderType_30;

    public static String FolderType_40;
    
    public static String FolderType_50;
    
    public static String FolderType_60;
    
    public static String FolderType_70;
    
    public static String FolderType_81;
    
    public static String FolderType_82;
    
    public static String FolderType_83;
    
    public static String FolderType_84;
    
    public static String FolderType_85;
    
    public static String FolderType_90;
    
    public static String FolderType_100;
    
    
    static {
        // initialize resource bundle
        NLS.initializeMessages(BUNDLE_NAME, Messages.class);
    }

    private Messages() {
    }
}
