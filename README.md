# Archestry™ VSM Modeller

Archestry™ VSM Modeller is a Value Stream Management modeller.

Archestry™ VSM Modeller is based on the Archi® modelling tool ([https://www.archimatetool.com](https://www.archimatetool.com)), ArchiMate® language modeller.

### License versioning:
    Archestry™ version: as in "Memex.Team Licensing policy"
    Original version: Copyright (c) 2013-2020 Phillip Beauvoir, Jean-Baptiste Sarrodie, The Open Group under The MIT License (MIT)
