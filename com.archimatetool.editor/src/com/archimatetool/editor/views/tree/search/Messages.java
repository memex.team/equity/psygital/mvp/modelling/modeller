/**
 * This program and the accompanying materials
 * are made available under the terms of the License
 * which accompanies this distribution in the file LICENSE.txt
 */
package com.archimatetool.editor.views.tree.search;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {

    private static final String BUNDLE_NAME = "com.archimatetool.editor.views.tree.search.messages"; //$NON-NLS-1$

    public static String SearchWidget_0;

    public static String SearchWidget_1;

    public static String SearchWidget_10;

    public static String SearchWidget_11;

    public static String SearchWidget_12;

    public static String SearchWidget_13;

    public static String SearchWidget_14;

    public static String SearchWidget_15;

    public static String SearchWidget_2;

    public static String SearchWidget_3;

    public static String SearchWidget_4;

    public static String SearchWidget_5;

    public static String SearchWidget_6;

    public static String SearchWidget_7;

    public static String SearchWidget_8;

    public static String SearchWidget_9;
    
    public static String SearchWidget_20;

    public static String SearchWidget_30;

    public static String SearchWidget_40;
    
    public static String SearchWidget_50;
    
    public static String SearchWidget_60;
    
    public static String SearchWidget_70;
    
    public static String SearchWidget_81;
    
    public static String SearchWidget_82;
    
    public static String SearchWidget_83;
    
    public static String SearchWidget_84;
    
    public static String SearchWidget_90;
    
    public static String SearchWidget_100;
    
    public static String SearchWidget_301;
    
    public static String SearchWidget_302;

    static {
        // initialize resource bundle
        NLS.initializeMessages(BUNDLE_NAME, Messages.class);
    }

    private Messages() {
    }
}
