/**
 * This program and the accompanying materials
 * are made available under the terms of the License
 * which accompanies this distribution in the file LICENSE.txt
 */
package com.archimatetool.editor.propertysections;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.PlatformUI;

import com.archimatetool.model.IArchimatePackage;



/**
 * Property Section for a Archestry element
 * 
 * @author Phillip Beauvoir
 */
public abstract class AbstractArchestrySection extends AbstractECorePropertySection {
    
    private static final String HELP_ID = "com.archimatetool.help.elementPropertySection"; //$NON-NLS-1$

    private PropertySectionTextControl fTextArchestry;
    
    @Override
    protected void createControls(Composite parent) {
        fTextArchestry = createArchestryRefIDControl(parent, Messages.ArchestryRefID_1);

        // Help
        PlatformUI.getWorkbench().getHelpSystem().setHelp(parent, HELP_ID);
    }
    
    @Override
    protected void notifyChanged(Notification msg) {
        if(msg.getNotifier() == getFirstSelectedObject()) {
            Object feature = msg.getFeature();
            
            if(feature == IArchimatePackage.Literals.ARCHESTRY__REFID_LABEL ||
                    feature == IArchimatePackage.Literals.LOCKABLE__LOCKED) {
                update();
            }
        }
    }

    @Override
    protected void update() {
        if(fIsExecutingCommand) {
            return; 
        }
        
        fTextArchestry.refresh(getFirstSelectedObject());
        fTextArchestry.setEditable(!isLocked(getFirstSelectedObject()));
    }
    
    @Override
    public boolean shouldUseExtraSpace() {
        return true;
    }

}
