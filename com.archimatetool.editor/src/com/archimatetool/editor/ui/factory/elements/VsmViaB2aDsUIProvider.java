/**
 * This program and the accompanying materials
 * are made available under the terms of the License
 * which accompanies this distribution in the file LICENSE.txt
 */
package com.archimatetool.editor.ui.factory.elements;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.gef.EditPart;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;

import com.archimatetool.editor.diagram.editparts.ArchimateElementEditPart;
import com.archimatetool.editor.diagram.figures.elements.VsmFigure;
import com.archimatetool.editor.ui.ColorFactory;
import com.archimatetool.editor.ui.IArchiImages;
import com.archimatetool.model.IArchimatePackage;



/**
 * Application Function UI Provider
 * 
 * @author Phillip Beauvoir
 */
public class VsmViaB2aDsUIProvider extends AbstractArchimateElementUIProvider {

    @Override
    public EClass providerFor() {
        return IArchimatePackage.eINSTANCE.getVsmViaB2aDs();
    }
    
    @Override
    public EditPart createEditPart() {
        return new ArchimateElementEditPart(VsmFigure.class);
    }

    @Override
    public String getDefaultName() {
        return Messages.VsmViaB2aDsUIProvider_0;
    }

    @Override
    public Image getImage() {
        return IArchiImages.ImageFactory.getImage(IArchiImages.ICON_VSM_VIA_B2A_DS);
    }

    @Override
    public ImageDescriptor getImageDescriptor() {
        return IArchiImages.ImageFactory.getImageDescriptor(IArchiImages.ICON_VSM_VIA_B2A_DS);
    }
    
    @Override
    public Color getDefaultColor() {
        return ColorFactory.get(216, 240, 243);
    }
}
