package com.archimatetool.editor.ui.factory.elements;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {

    private static final String BUNDLE_NAME = "com.archimatetool.editor.ui.factory.elements.messages"; //$NON-NLS-1$

    public static String ApplicationCollaborationUIProvider_0;

    public static String ApplicationComponentUIProvider_0;

    public static String ApplicationEventUIProvider_0;

    public static String ApplicationFunctionUIProvider_0;

    public static String VsmViaB2aUiUIProvider_0;
    
    public static String VsmViaB2aIntegrationUIProvider_0;
    
    public static String VsmViaB2aDsUIProvider_0;
    
    public static String VsmViaA2dDataUIProvider_0;
    
    public static String VsmViaA2tYamlUIProvider_0;
    
    public static String VsmApplicationTaskUIProvider_0;
    
    public static String VsmDataGitRepoUIProvider_0;
    
    public static String VsmDataGitBranchUIProvider_0;
    
    public static String VsmDataSrcDirUIProvider_0;
    
    public static String VsmDataSrcFileUIProvider_0;
    
    public static String VsmApplicationClassUIProvider_0;
    
    public static String VsmApplicationModuleUIProvider_0;
    
    public static String VsmApplicationMethodUIProvider_0;
    
    public static String ApplicationInteractionUIProvider_0;

    public static String ApplicationInterfaceUIProvider_0;

    public static String ApplicationProcessUIProvider_0;

    public static String ApplicationServiceUIProvider_0;

    public static String ArtifactUIProvider_0;

    public static String AssessmentUIProvider_0;

    public static String BusinessActorUIProvider_0;

    public static String BusinessCollaborationUIProvider_0;

    public static String BusinessEventUIProvider_0;

    public static String BusinessFunctionUIProvider_0;

    public static String VsmBusinessEpicUIProvider_0;
    
    public static String VsmBusinessUsUIProvider_0;
    
    public static String VsmBusinessTaskUIProvider_0;
    
    public static String VsmBusinessAcUIProvider_0;
    
    public static String VsmBusinessUcUIProvider_0;
    
    public static String VsmBusinessFrUIProvider_0;
    
    public static String VsmBusinessNfrUIProvider_0;

    public static String BusinessInteractionUIProvider_0;

    public static String BusinessInterfaceUIProvider_0;

    public static String BusinessObjectUIProvider_0;

    public static String BusinessProcessUIProvider_0;

    public static String BusinessRoleUIProvider_0;

    public static String BusinessServiceUIProvider_0;

    public static String CapabilityUIProvider_0;

    public static String CommunicationNetworkUIProvider_0;

    public static String ConstraintUIProvider_0;

    public static String ContractUIProvider_0;

    public static String CourseOfActionUIProvider_0;

    public static String DataObjectUIProvider_0;

    public static String DeliverableUIProvider_0;

    public static String DeviceUIProvider_0;

    public static String DistributionNetworkUIProvider_0;

    public static String DriverUIProvider_0;

    public static String EquipmentUIProvider_0;

    public static String FacilityUIProvider_0;

    public static String GapUIProvider_0;

    public static String GoalUIProvider_0;

    public static String DynamicsPersonalFeelingUIProvider_0;
    
    public static String DynamicsPersonalPredictionUIProvider_0;
    
    public static String DynamicsPersonalCorrectionUIProvider_0;
    
    public static String DynamicsPersonalByproductUIProvider_0;
    
    public static String DynamicsTeamPredictionUIProvider_0;
    
    public static String DynamicsTeamFeelingUIProvider_0;
    
    public static String DynamicsOrgPredictionUIProvider_0;
    
    public static String DynamicsEnvPredictionUIProvider_0;
    
    public static String VsmDataDatabaseUIProvider_0;
    
    public static String VsmDataSchemaUIProvider_0;
    
    public static String VsmDataViewUIProvider_0;
    
    public static String VsmDataTableUIProvider_0;
    
    public static String VsmDataColumnUIProvider_0;
    
    public static String VsmDataTaskUIProvider_0;
    
    public static String VsmLandscapeInfrastructureOctagonUIProvider_0;
    
    public static String VsmLandscapeProductOctagonUIProvider_0;
    
    public static String VsmLandscapeLayerGeneralUIProvider_0;
    public static String VsmLandscapeLayerBusinessUIProvider_0;
    public static String VsmLandscapeLayerApplicationUIProvider_0;
    public static String VsmLandscapeLayerTechnologyUIProvider_0;
    public static String VsmLandscapeLayerDataUIProvider_0;
    public static String VsmLandscapeLayerB2aUIProvider_0;
    public static String VsmLandscapeLayerA2dUIProvider_0;
    public static String VsmLandscapeLayerA2tUIProvider_0;
    
    
    public static String VsmSrmActionUIProvider_0;
    
    public static String VsmSrmStatusUIProvider_0;
    
    public static String VsmSrmRoleFunctionUIProvider_0;
    
    public static String VsmSrmRoleUserUIProvider_0;
    
    public static String VsmSrmUserUIProvider_0;
    
    
    public static String ContextmeBasicConceptUIProvider_0;
    
    public static String ContextmeBasicSituationUIProvider_0;
    
    public static String ContextmeBasicQuestioningUIProvider_0;
    
    public static String ContextmeBasicCuriousUIProvider_0;
    
    public static String ContextmeBasicLinkUIProvider_0;
    
    public static String ContextmeBasicClarifyUIProvider_0;
    
    public static String ContextmeBasicExpressUIProvider_0;
    
    public static String ContextmeBasicDiscussUIProvider_0;
    
    public static String ContextmeBasicNoteUIProvider_0;

    public static String GroupingUIProvider_0;

    public static String ImplementationEventUIProvider_0;

    public static String JunctionUIProvider_0;

    public static String LocationUIProvider_0;

    public static String MaterialUIProvider_0;

    public static String MeaningUIProvider_0;

    public static String NodeUIProvider_0;

    public static String OutcomeUIProvider_0;

    public static String PathUIProvider_0;

    public static String PlateauUIProvider_0;

    public static String PrincipleUIProvider_0;

    public static String ProductUIProvider_0;

    public static String RepresentationUIProvider_0;

    public static String RequirementUIProvider_0;

    public static String ResourceUIProvider_0;

    public static String StakeholderUIProvider_0;

    public static String SystemSoftwareUIProvider_0;

    public static String TechnologyCollaborationUIProvider_0;

    public static String TechnologyEventUIProvider_0;

    public static String TechnologyFunctionUIProvider_0;
    
    public static String VsmTechnologyTaskUIProvider_0;
    
    public static String VsmTechnologyK8sPodUIProvider_0;
    
    public static String VsmTechnologyK8sContainerUIProvider_0;
    
    public static String VsmTechnologyK8sControllerUIProvider_0;
    
    public static String VsmTechnologyStorageUIProvider_0; 

    public static String TechnologyInteractionUIProvider_0;

    public static String TechnologyInterfaceUIProvider_0;

    public static String TechnologyProcessUIProvider_0;

    public static String TechnologyServiceUIProvider_0;

    public static String ValueStreamUIProvider_0;

    public static String ValueUIProvider_0;

    public static String WorkPackageUIProvider_0;
    static {
        // initialize resource bundle
        NLS.initializeMessages(BUNDLE_NAME, Messages.class);
    }

    private Messages() {
    }
}
