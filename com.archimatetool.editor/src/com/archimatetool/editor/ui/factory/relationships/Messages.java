package com.archimatetool.editor.ui.factory.relationships;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {

    private static final String BUNDLE_NAME = "com.archimatetool.editor.ui.factory.relationships.messages"; //$NON-NLS-1$

    public static String AccessRelationshipUIProvider_0;

    public static String AggregationRelationshipUIProvider_0;

    public static String AssignmentRelationshipUIProvider_0;

    public static String AssociationRelationshipUIProvider_0;

    public static String CompositionRelationshipUIProvider_0;

    public static String FlowRelationshipUIProvider_0;

    public static String InfluenceRelationshipUIProvider_0;

    public static String RealizationRelationshipUIProvider_0;

    public static String SpecializationRelationshipUIProvider_0;

    public static String TriggeringRelationshipUIProvider_0;

    public static String ServingRelationshipUIProvider_0;
    
    public static String ProductBizOwnsRelationshipUIProvider_0;
    
    public static String ProductAppOwnsRelationshipUIProvider_0;
    
    public static String ProductDataOwnsRelationshipUIProvider_0;
    
    public static String ProductTechOwnsRelationshipUIProvider_0;
    
    
    public static String DynamicsPersonalPredictionRelationshipUIProvider_0;
    
    public static String DynamicsTeamPredictionRelationshipUIProvider_0;
    
    public static String DynamicsOrgPredictionRelationshipUIProvider_0;
    
    public static String DynamicsEnvPredictionRelationshipUIProvider_0;
    
    
    public static String ProductTechOSIRelationshipUIProvider_0;     
    
    static {
        // initialize resource bundle
        NLS.initializeMessages(BUNDLE_NAME, Messages.class);
    }

    private Messages() {
    }
}
