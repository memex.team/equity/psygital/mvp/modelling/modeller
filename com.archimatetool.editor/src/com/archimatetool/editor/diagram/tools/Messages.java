/**
 * This program and the accompanying materials
 * are made available under the terms of the License
 * which accompanies this distribution in the file LICENSE.txt
 */
package com.archimatetool.editor.diagram.tools;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {

    private static final String BUNDLE_NAME = "com.archimatetool.editor.diagram.tools.messages"; //$NON-NLS-1$

    public static String FormatPainterTool_0;

    public static String FormatPainterToolEntry_0;

    public static String FormatPainterToolEntry_1;

    public static String FormatPainterToolEntry_2;

    public static String FormatPainterToolEntry_3;

    public static String FormatPainterToolEntry_4;

    public static String FormatPainterToolEntry_5;

    public static String MagicConnectionCreationTool_0;

    public static String MagicConnectionCreationTool_1;

    public static String MagicConnectionCreationTool_2;

    public static String MagicConnectionCreationTool_3;

    public static String MagicConnectionCreationTool_4;

    public static String MagicConnectionCreationTool_6;

    public static String MagicConnectionCreationTool_7;

    public static String MagicConnectionCreationTool_8;

    public static String MagicConnectionCreationTool_9;

    public static String MagicConnectionCreationTool_20;
    
    public static String MagicConnectionCreationTool_30;
    
    public static String MagicConnectionCreationTool_40;
    
    public static String MagicConnectionCreationTool_50;
    
    public static String MagicConnectionCreationTool_60;
    
    public static String MagicConnectionCreationTool_70;
    
    public static String MagicConnectionCreationTool_81;
    
    public static String MagicConnectionCreationTool_82;
    
    public static String MagicConnectionCreationTool_83;
    
    public static String MagicConnectionCreationTool_84;
    
    public static String MagicConnectionCreationTool_90;
    
    static {
        // initialize resource bundle
        NLS.initializeMessages(BUNDLE_NAME, Messages.class);
    }

    private Messages() {
    }
}
