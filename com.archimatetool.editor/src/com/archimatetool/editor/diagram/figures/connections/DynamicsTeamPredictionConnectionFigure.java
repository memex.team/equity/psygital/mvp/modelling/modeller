/**
 * This program and the accompanying materials
 * are made available under the terms of the License
 * which accompanies this distribution in the file LICENSE.txt
 */
package com.archimatetool.editor.diagram.figures.connections;

import org.eclipse.draw2d.PolygonDecoration;
import org.eclipse.draw2d.RotatableDecoration;
import org.eclipse.draw2d.geometry.PointList;
import org.eclipse.swt.SWT;



/**
 * DynamicsTeamPrediction Connection Figure class
 * 
 * @author Phillip Beauvoir
 */
public class DynamicsTeamPredictionConnectionFigure extends AbstractArchimateConnectionFigure {
	
    /**
     * @return Decoration to use on Target Node
     */
    public static RotatableDecoration createFigureSourceDecoration() {
        return new PolygonDecoration() {
            {
                setScale(5, 3);
                PointList decorationPointList = new PointList();
                decorationPointList.addPoint( 1, 0);
                decorationPointList.addPoint( 1, 1);
                decorationPointList.addPoint( 0, 2);                
                decorationPointList.addPoint(-1, 1);
                decorationPointList.addPoint(-1, 0);
                decorationPointList.addPoint( 0,-1);
                setTemplate(decorationPointList);
            }
        };
    }
    
    public static RotatableDecoration createFigureTargetDecoration() {
        return new PolygonDecoration();
    }    
    
    public DynamicsTeamPredictionConnectionFigure() {
    }
    

    private float[] getLineDashes(double zoomLevel) {
        return new float[] { (float)(6 * zoomLevel), (float)(3 * zoomLevel) }; 
    }    

    @Override
    protected void setFigureProperties() {
        setLineStyle(SWT.LINE_CUSTOM); // We have to explitly set this otherwise dashes/dots don't show
        setLineDash(getLineDashes(1.0));    	
        setSourceDecoration(createFigureSourceDecoration());
        setTargetDecoration(createFigureTargetDecoration()); 
    }
}
