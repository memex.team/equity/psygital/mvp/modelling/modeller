/**
 * This program and the accompanying materials
 * are made available under the terms of the License
 * which accompanies this distribution in the file LICENSE.txt
 */
package com.archimatetool.editor.diagram.figures;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.DelegatingLayout;
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.GridData;
import org.eclipse.draw2d.GridLayout;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.Locator;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.draw2d.text.BlockFlow;
import org.eclipse.draw2d.text.FlowPage;
import org.eclipse.draw2d.text.ParagraphTextLayout;
import org.eclipse.draw2d.text.TextFlow;
import org.eclipse.swt.SWT;
import org.eclipse.emf.ecore.EClass;

import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.geometry.PointList;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontMetrics;
import com.archimatetool.editor.ui.ColorFactory;
import com.archimatetool.editor.ui.FontFactory;

import com.archimatetool.editor.preferences.IPreferenceConstants;
import com.archimatetool.editor.preferences.Preferences;
import com.archimatetool.editor.utils.StringUtils;
import com.archimatetool.model.IDiagramModelObject;
import com.archimatetool.model.ITextAlignment;
import com.archimatetool.model.ITextPosition;
import com.archimatetool.model.IDiagramModelArchimateObject;
import com.archimatetool.model.impl.DiagramModelArchimateObject;
import com.archimatetool.model.IArchestry;
import com.archimatetool.model.IDiagramModelArchimateComponent;

/**
 * Abstract Container Figure with Text Control
 * 
 * @author Phillip Beauvoir
 */
public abstract class AbstractTextControlContainerFigureVSM extends AbstractContainerFigure {
    
    private IFigure fTextControl;
    private int fTextControlType = TEXT_FLOW_CONTROL;
    
    public static final int TEXT_FLOW_CONTROL = 0;
    public static final int LABEL_CONTROL = 1;
    
    private TextPositionDelegate fTextPositionDelegate;
    
    protected AbstractTextControlContainerFigureVSM(int textControlType) {
        fTextControlType = textControlType;
    }
    
    protected AbstractTextControlContainerFigureVSM(IDiagramModelObject diagramModelObject, int textControlType) {
        fTextControlType = textControlType;
        setDiagramModelObject(diagramModelObject);
    }
    
    @Override
    protected void setUI() {
        setLayoutManager(new DelegatingLayout());
        
        Locator textLocator = new Locator() {
            @Override
            public void relocate(IFigure target) {
                Rectangle bounds = calculateTextControlBounds();
                if(bounds == null) {
                    bounds = getBounds().getCopy();
                }
                translateFromParent(bounds);
                target.setBounds(bounds);
            }
        };
        
        fTextControl = createTextControl(textLocator);
        
        Locator mainLocator = new Locator() {
            @Override
            public void relocate(IFigure target) {
                Rectangle bounds = getBounds().getCopy();
                translateFromParent(bounds);
                target.setBounds(bounds);
            }
        };
        
        add(getMainFigure(), mainLocator);
    }
    
    @Override
    public void refreshVisuals() {
        // Text
        setText();
        
        // Font
        setFont();
        
        // Fill Color
        setFillColor();
        
        // Font Color
        setFontColor();
        
        // Line Color
        setLineColor();
        
        // Text Position
        if(getTextControl() instanceof TextFlow) {
            int alignment = getDiagramModelObject().getTextAlignment();
            ((BlockFlow)getTextControl().getParent()).setHorizontalAligment(alignment);
            
            if(fTextPositionDelegate != null) {
                fTextPositionDelegate.updateTextPosition();
            }
        }
        
        repaint(); // repaint when figure changes
    }
    
    @Override
    public void setEnabled(boolean value) {
        super.setEnabled(value);
        
        getTextControl().setEnabled(value);
        
        if(getFigureDelegate() != null) {
            getFigureDelegate().setEnabled(value);
        }
    }
    
    protected void setText() {
        String text = StringUtils.safeString(getDiagramModelObject().getName());
        
        if(getTextControl() instanceof TextFlow) {
            ((TextFlow)getTextControl()).setText(text);
        }
        else if(getTextControl() instanceof Label) {
            ((Label)getTextControl()).setText(text);
        }
    }
    
    public String getText() {
        if(getTextControl() instanceof TextFlow) {
            return ((TextFlow)getTextControl()).getText();
        }
        else {
            return ((Label)getTextControl()).getText();
        }
    }
    
    @Override
    public IFigure getTextControl() {
        return fTextControl;
    }

    protected IFigure createTextControl(Locator textLocator) {
        if(fTextControlType == TEXT_FLOW_CONTROL) {
            return createTextFlowControl(textLocator);
        }
        else {
            return createLabelControl(textLocator);
        }
    }
    
    protected TextFlow createTextFlowControl(Locator textLocator) {
        TextFlow textFlow = new TextFlow();
        
        int wordWrapStyle = Preferences.STORE.getInt(IPreferenceConstants.ARCHIMATE_FIGURE_WORD_WRAP_STYLE);
        textFlow.setLayoutManager(new ParagraphTextLayout(textFlow, wordWrapStyle));
        
        BlockFlow block = new BlockFlow();
        block.add(textFlow);

        FlowPage page = new FlowPage();
        page.add(block);
        
        Figure textWrapperFigure = new Figure();
        GridLayout layout = new GridLayout();
        layout.marginWidth = getTextControlMarginWidth();
        layout.marginHeight = 5;
        textWrapperFigure.setLayoutManager(layout);
        GridData gd = new GridData(SWT.CENTER, SWT.TOP, true, true);
        textWrapperFigure.add(page, gd);
        
        if(getDiagramModelObject() instanceof ITextPosition) {
            fTextPositionDelegate = new TextPositionDelegate(textWrapperFigure, page, (ITextPosition)getDiagramModelObject());
        }
        
        add(textWrapperFigure, textLocator);
        //add(page, textLocator);
        
        return textFlow;
    }

    protected Label createLabelControl(Locator textLocator) {
        Label label = new Label(""); //$NON-NLS-1$
        add(label, textLocator);
        return label;
    }

    /**
     * @return the left and right margin width for text
     */
    protected int getTextControlMarginWidth() {
        return 20;
    }
    
    protected int getIconOffset() {
        return 10;
    }
    
    protected void metaMarkup(Graphics graphics, int halfOfQuoter, PointList areaL1, PointList areaL2, PointList areaL3, PointList areaL4, PointList areaR1, PointList areaR2, PointList areaR3, PointList areaR4, PointList areaC1, PointList areaC2, Color areaC2Color, PointList areaC3) {
    
        
          graphics.pushState();
          graphics.setLineWidth(1);
          graphics.setForegroundColor(isEnabled() ? ColorConstants.black : ColorConstants.gray);
          Rectangle bounds = getBounds();
          
          
          String archestryName =  (((DiagramModelArchimateObject)getDiagramModelObject()).getArchimateElement()).getName();
          String archestryClass = (((DiagramModelArchimateObject)getDiagramModelObject()).getArchimateElement()).eClass().getName();
          String archestryOrderLabel = StringUtils.safeString(((IArchestry)((DiagramModelArchimateObject)getDiagramModelObject()).getArchimateElement()).getArchestry_OrderLabel());
          String archestryFamilyLabel = StringUtils.safeString(((IArchestry)((DiagramModelArchimateObject)getDiagramModelObject()).getArchimateElement()).getArchestry_FamilyLabel());
          String archestryGenusLabel = StringUtils.safeString(((IArchestry)((DiagramModelArchimateObject)getDiagramModelObject()).getArchimateElement()).getArchestry_GenusLabel());
          String archestryExtUUIDLabel = StringUtils.safeString(((IArchestry)((DiagramModelArchimateObject)getDiagramModelObject()).getArchimateElement()).getArchestry_ExtUUIDLabel());        
          String archestryRefIDLabel = StringUtils.safeString(((IArchestry)((DiagramModelArchimateObject)getDiagramModelObject()).getArchimateElement()).getArchestry_RefIDLabel());
          String archestryStageLabel = StringUtils.safeString(((IArchestry)((DiagramModelArchimateObject)getDiagramModelObject()).getArchimateElement()).getArchestry_StageLabel());
          String archestryStatusLabel = StringUtils.safeString(((IArchestry)((DiagramModelArchimateObject)getDiagramModelObject()).getArchimateElement()).getArchestry_StatusLabel());
          String archestryCommWithLabel = StringUtils.safeString(((IArchestry)((DiagramModelArchimateObject)getDiagramModelObject()).getArchimateElement()).getArchestry_CommWithLabel());
          String archestryVersionLabel = StringUtils.safeString(((IArchestry)((DiagramModelArchimateObject)getDiagramModelObject()).getArchimateElement()).getArchestry_VersionLabel());
          String archestryDateCLabel = StringUtils.safeString(((IArchestry)((DiagramModelArchimateObject)getDiagramModelObject()).getArchimateElement()).getArchestry_DateLabel());
          String archestryDateMLabel = "-";
          
          String archestryOrderFontColors = StringUtils.safeString(((IArchestry)((DiagramModelArchimateObject)getDiagramModelObject()).getArchimateElement()).getArchestry_OrderFontColor());
          String archestryFamilyFontColors = StringUtils.safeString(((IArchestry)((DiagramModelArchimateObject)getDiagramModelObject()).getArchimateElement()).getArchestry_FamilyFontColor());
          String archestryGenusFontColors = StringUtils.safeString(((IArchestry)((DiagramModelArchimateObject)getDiagramModelObject()).getArchimateElement()).getArchestry_GenusFontColor());
          String archestryExtUUIDFontColors = StringUtils.safeString(((IArchestry)((DiagramModelArchimateObject)getDiagramModelObject()).getArchimateElement()).getArchestry_ExtUUIDFontColor()); 
          String archestryRefIDFontColors = StringUtils.safeString(((IArchestry)((DiagramModelArchimateObject)getDiagramModelObject()).getArchimateElement()).getArchestry_RefIDFontColor());
          String archestryStageFontColors = StringUtils.safeString(((IArchestry)((DiagramModelArchimateObject)getDiagramModelObject()).getArchimateElement()).getArchestry_StageFontColor());
          String archestryStatusFontColors = StringUtils.safeString(((IArchestry)((DiagramModelArchimateObject)getDiagramModelObject()).getArchimateElement()).getArchestry_StatusFontColor());
          String archestryCommWithFontColors = StringUtils.safeString(((IArchestry)((DiagramModelArchimateObject)getDiagramModelObject()).getArchimateElement()).getArchestry_CommWithFontColor());
          String archestryVersionFontColors = StringUtils.safeString(((IArchestry)((DiagramModelArchimateObject)getDiagramModelObject()).getArchimateElement()).getArchestry_VersionFontColor());
          String archestryDateCFontColors = StringUtils.safeString(((IArchestry)((DiagramModelArchimateObject)getDiagramModelObject()).getArchimateElement()).getArchestry_DateFontColor());
          String archestryDateMFontColors = "1";            
          
          String archestryOrderAreaColors;
          String archestryFamilyAreaColors = StringUtils.safeString(((IArchestry)((DiagramModelArchimateObject)getDiagramModelObject()).getArchimateElement()).getArchestry_FamilyAreaColor());
          String archestryGenusAreaColors = StringUtils.safeString(((IArchestry)((DiagramModelArchimateObject)getDiagramModelObject()).getArchimateElement()).getArchestry_GenusAreaColor());
          String archestryExtUUIDAreaColors = StringUtils.safeString(((IArchestry)((DiagramModelArchimateObject)getDiagramModelObject()).getArchimateElement()).getArchestry_ExtUUIDAreaColor()); 
          String archestryRefIDAreaColors = StringUtils.safeString(((IArchestry)((DiagramModelArchimateObject)getDiagramModelObject()).getArchimateElement()).getArchestry_RefIDAreaColor());
          String archestryStageAreaColors = StringUtils.safeString(((IArchestry)((DiagramModelArchimateObject)getDiagramModelObject()).getArchimateElement()).getArchestry_StageAreaColor());
          String archestryStatusAreaColors = StringUtils.safeString(((IArchestry)((DiagramModelArchimateObject)getDiagramModelObject()).getArchimateElement()).getArchestry_StatusAreaColor());
          String archestryCommWithAreaColors = StringUtils.safeString(((IArchestry)((DiagramModelArchimateObject)getDiagramModelObject()).getArchimateElement()).getArchestry_CommWithAreaColor());
          String archestryVersionAreaColors = StringUtils.safeString(((IArchestry)((DiagramModelArchimateObject)getDiagramModelObject()).getArchimateElement()).getArchestry_VersionAreaColor());
          String archestryDateCAreaColors = StringUtils.safeString(((IArchestry)((DiagramModelArchimateObject)getDiagramModelObject()).getArchimateElement()).getArchestry_DateAreaColor());
          String archestryDateMAreaColors = "1";        
        

          if(archestryClass.startsWith("VsmApplication")) {
            archestryOrderAreaColors = "200,255,255";
          }
          else if(archestryClass.startsWith("VsmBusiness")) {
            archestryOrderAreaColors = "255,255,215";
          }
          else if(archestryClass.startsWith("VsmData")) {
            archestryOrderAreaColors = "255,226,192";
          }
          else if(archestryClass.startsWith("VsmSrm")) {
            archestryOrderAreaColors = "220,210,255";
          }
          else if(archestryClass.startsWith("VsmTechnology")) {
            archestryOrderAreaColors = "190,255,190";
          }
          else if(archestryClass.startsWith("VsmViaB2a")) {
            archestryOrderAreaColors = "230,230,255";
          }
          else if(archestryClass.startsWith("ContextmeBasic")) {
            archestryOrderAreaColors = "255,220,180";
          }
          else { 
            archestryOrderAreaColors = "255,255,255";
          }
                  

          
          int okNum = 0;
          int nonOkNum = 0;                    
                    
          String textL2;
          String[] areaColorsL2;
          if (archestryDateCLabel.length() > 1) {
            textL2 =  archestryDateCLabel;
            areaColorsL2 = archestryDateCAreaColors.split(",");
            okNum = okNum + 1;
          } else {
            textL2 =  "  CDate: ?";
            areaColorsL2 = "255,240,240".split(",");
            nonOkNum = nonOkNum + 1;
          }
          String[] fontColorsL2 = archestryDateCFontColors.split(",");
          
          
          String textL3;
          String[] areaColorsL3;
          if (archestryDateMLabel.length() > 1) {
            textL3 =  archestryDateMLabel;
            areaColorsL3 = archestryDateMAreaColors.split(",");
            okNum = okNum + 1;
          } else {
            textL3 =  "  MDate: ?";
            areaColorsL3 = "255,240,240".split(",");
            nonOkNum = nonOkNum + 1;
          }
          String[] fontColorsL3 = archestryDateMFontColors.split(",");         
                    
                    
          String textL4;
          String[] areaColorsL4;
          if (archestryCommWithLabel.length() > 1) {
            textL4 =  archestryCommWithLabel;
            areaColorsL4 = archestryCommWithAreaColors.split(",");
            okNum = okNum + 1;
          } else {
            textL4 =  "  CW: ?";
            areaColorsL4 = "255,240,240".split(",");
            nonOkNum = nonOkNum + 1;
          }          
          String[] fontColorsL4 = archestryCommWithFontColors.split(",");
          
          
          String textR1 = "";
          String[] areaColorsR1 = "0".split(",");
          String[] fontColorsR1 = "0".split(",");

          
          String textR2;
          String[] areaColorsR2;
          if (archestryVersionLabel.length() > 1) {
            textR2 =  archestryVersionLabel;
            areaColorsR2 = archestryStageAreaColors.split(",");
            okNum = okNum + 1;
          } else {
            textR2 =  "  Stage: ?";
            areaColorsR2 = "255,240,240".split(",");
            nonOkNum = nonOkNum + 1;
          }
          String[] fontColorsR2 = archestryStageFontColors.split(",");          
          
                    
          String textR3;
          String[] areaColorsR3;
          if (archestryVersionLabel.length() > 1) {
            textR3 =  archestryVersionLabel;
            areaColorsR3 = archestryVersionAreaColors.split(",");
            okNum = okNum + 1;
          } else {
            textR3 =  "  V: ?";
            areaColorsR3 = "255,240,240".split(",");
            nonOkNum = nonOkNum + 1;
          }
          String[] fontColorsR3 = archestryVersionFontColors.split(",");
          
          
          String textR4;
          String[] areaColorsR4;
          if (archestryStatusLabel.length() > 1) {
            textR4 =  archestryStatusLabel;
            areaColorsR4 = archestryStatusAreaColors.split(",");
            okNum = okNum + 1;
          } else {
            textR4 =  "  S: ?";
            areaColorsR4 = "255,240,240".split(",");
            nonOkNum = nonOkNum + 1;
          }          
          String[] fontColorsR4 = archestryStatusAreaColors.split(",");
          
          
          //String textC1 = archestryOrderLabel + " - " + archestryFamilyLabel + " - " + archestryGenusLabel;
          String textC1 = archestryClass;
          String[] areaColorsC1 = archestryOrderAreaColors.split(",");
          String[] fontColorsC1 = archestryOrderFontColors.split(",");
          
          String textC2 = archestryName;
          
          String textC3;
          String[] areaColorsC3;
          if (archestryRefIDLabel.length() > 1) {
            textC3 =  "RefID: " + archestryRefIDLabel;
            areaColorsC3 = archestryRefIDAreaColors.split(",");
            okNum = okNum + 1;
          } else {
            textC3 =  "";  
            areaColorsC3 = "255,240,240".split(",");    
            nonOkNum = nonOkNum + 1;
          }
          String[] fontColorsC3 = archestryRefIDFontColors.split(",");
          
          

          Integer fontIndex = bounds.width/60;
          Font font = new Font (null, "Latin Modern Sans", fontIndex, SWT.NORMAL);
          graphics.setFont (font);          
          
          String newLine = System.getProperty("line.separator");
          String textL1 = "OK:" + okNum +  ", NonOk:" + nonOkNum;
          String[] areaColorsL1;
          if (okNum == 0) { areaColorsL1 = "255,240,240".split(",");}
          else if (okNum == 1) { areaColorsL1 = "173,255,47".split(",");}
          else if (okNum == 2) { areaColorsL1 = "89,255,47".split(",");}
          else { areaColorsL1 = "173,58,47".split(",");}

        
          String[] fontColorsL1 = "0".split(",");
          
          if (areaColorsL1.length == 3) {
            Color areaColorL1 = new Color(null, Integer.parseInt(areaColorsL1[0]), Integer.parseInt(areaColorsL1[1]), Integer.parseInt(areaColorsL1[2]));
            graphics.setBackgroundColor(areaColorL1);  
            graphics.fillPolygon(areaL1);
          }
          if (fontColorsL1.length == 3) {
            Color fontColorL1 = new Color(null, Integer.parseInt(fontColorsL1[0]), Integer.parseInt(fontColorsL1[1]), Integer.parseInt(fontColorsL1[2]));
            graphics.setForegroundColor(fontColorL1);  
          }
          else {
            graphics.setForegroundColor(ColorConstants.gray);
          }
          graphics.drawString(textL1, areaL1.getPoint(2).x - halfOfQuoter/6, areaL1.getPoint(2).y + halfOfQuoter/4);          
          
          
          fontIndex = bounds.width/40;
          font = new Font (null, "Latin Modern Sans", fontIndex, SWT.NORMAL);
          graphics.setFont (font);
          
          
          //С1
          if (fontColorsC1.length == 3) {
            Color fontColorC1 = new Color(null, Integer.parseInt(fontColorsC1[0]), Integer.parseInt(fontColorsC1[1]), Integer.parseInt(fontColorsC1[2]));
            graphics.setForegroundColor(fontColorC1);  
          }
          else {
            graphics.setForegroundColor(ColorConstants.gray);
          }         
          
          //С3
          if (fontColorsC3.length == 3) {
            Color fontColorC3 = new Color(null, Integer.parseInt(fontColorsC3[0]), Integer.parseInt(fontColorsC3[1]), Integer.parseInt(fontColorsC3[2]));
            graphics.setForegroundColor(fontColorC3);  
          }
          else {
            graphics.setForegroundColor(ColorConstants.gray);
          }
          graphics.drawString(textC3, areaC3.getPoint(0).x + 1, areaC3.getPoint(0).y + halfOfQuoter/3 + halfOfQuoter/9);
                    
          

          if (areaColorsL2.length == 3) {
            Color areaColorL2 = new Color(null, Integer.parseInt(areaColorsL2[0]), Integer.parseInt(areaColorsL2[1]), Integer.parseInt(areaColorsL2[2]));
            graphics.setBackgroundColor(areaColorL2);  
            graphics.fillPolygon(areaL2);
          }
          if (fontColorsL2.length == 3) {
            Color fontColorL2 = new Color(null, Integer.parseInt(fontColorsL2[0]), Integer.parseInt(fontColorsL2[1]), Integer.parseInt(fontColorsL2[2]));
            graphics.setForegroundColor(fontColorL2);  
          }
          else {
            graphics.setForegroundColor(ColorConstants.gray);
          }
          graphics.drawString(textL2, areaL2.getPoint(2).x + 1, areaL2.getPoint(0).y + halfOfQuoter/3);


          
          if (areaColorsL3.length == 3) {
            Color areaColorL3 = new Color(null, Integer.parseInt(areaColorsL3[0]), Integer.parseInt(areaColorsL3[1]), Integer.parseInt(areaColorsL3[2]));
            graphics.setBackgroundColor(areaColorL3);  
            graphics.fillPolygon(areaL3);
          }
          if (fontColorsL3.length == 3) {
            Color fontColorL3 = new Color(null, Integer.parseInt(fontColorsL3[0]), Integer.parseInt(fontColorsL3[1]), Integer.parseInt(fontColorsL3[2]));
            graphics.setForegroundColor(fontColorL3);  
          }
          else {
            graphics.setForegroundColor(ColorConstants.gray);
          }
          graphics.drawString(textL3, areaL3.getPoint(2).x + 1, areaL3.getPoint(2).y + halfOfQuoter/3);
          

          if (areaColorsL4.length == 3) {
            Color areaColorL4 = new Color(null, Integer.parseInt(areaColorsL4[0]), Integer.parseInt(areaColorsL4[1]), Integer.parseInt(areaColorsL4[2]));
            graphics.setBackgroundColor(areaColorL4);  
            graphics.fillPolygon(areaL4);
          }
          if (fontColorsL4.length == 3) {
            Color fontColorL4 = new Color(null, Integer.parseInt(fontColorsL4[0]), Integer.parseInt(fontColorsL4[1]), Integer.parseInt(fontColorsL4[2]));
            graphics.setForegroundColor(fontColorL4);  
          }
          else {
            graphics.setForegroundColor(ColorConstants.gray);
          }
          graphics.drawString(textL4, areaL4.getPoint(2).x + 1, areaL4.getPoint(1).y + halfOfQuoter/4);
          

          if (areaColorsR1.length == 3) {
            Color areaColorR1 = new Color(null, Integer.parseInt(areaColorsR1[0]), Integer.parseInt(areaColorsR1[1]), Integer.parseInt(areaColorsR1[2]));
            graphics.setBackgroundColor(areaColorR1);  
            graphics.fillPolygon(areaR1);
          }
          if (fontColorsR1.length == 3) {
            Color fontColorR1 = new Color(null, Integer.parseInt(fontColorsR1[0]), Integer.parseInt(fontColorsR1[1]), Integer.parseInt(fontColorsR1[2]));
            graphics.setForegroundColor(fontColorR1);  
          }
          else {
            graphics.setForegroundColor(ColorConstants.gray);
          }
          graphics.drawString(textR1, areaR1.getPoint(0).x + 1, areaR1.getPoint(1).y);
          

          if (areaColorsR2.length == 3) {
            Color areaColorR2 = new Color(null, Integer.parseInt(areaColorsR2[0]), Integer.parseInt(areaColorsR2[1]), Integer.parseInt(areaColorsR2[2]));
            graphics.setBackgroundColor(areaColorR2);  
            graphics.fillPolygon(areaR2);
          }
          if (fontColorsR2.length == 3) {
            Color fontColorR2 = new Color(null, Integer.parseInt(fontColorsR2[0]), Integer.parseInt(fontColorsR2[1]), Integer.parseInt(fontColorsR2[2]));
            graphics.setForegroundColor(fontColorR2);  
          }
          else {
            graphics.setForegroundColor(ColorConstants.gray);
          }
          graphics.drawString(textR2, areaR2.getPoint(0).x + 1, areaR2.getPoint(0).y + halfOfQuoter/3);
          

          if (areaColorsR3.length == 3) {
            Color areaColorR3 = new Color(null, Integer.parseInt(areaColorsR3[0]), Integer.parseInt(areaColorsR3[1]), Integer.parseInt(areaColorsR3[2]));
            graphics.setBackgroundColor(areaColorR3);  
            graphics.fillPolygon(areaR3);
          }
          if (fontColorsR3.length == 3) {
            Color fontColorR3 = new Color(null, Integer.parseInt(fontColorsR3[0]), Integer.parseInt(fontColorsR3[1]), Integer.parseInt(fontColorsR3[2]));
            graphics.setForegroundColor(fontColorR3);  
          }
          else {
            graphics.setForegroundColor(ColorConstants.gray);
          }
          graphics.drawString(textR3, areaR3.getPoint(0).x + 1, areaR3.getPoint(1).y + halfOfQuoter/3);
          

          if (areaColorsR4.length == 3) {
            Color areaColorR4 = new Color(null, Integer.parseInt(areaColorsR4[0]), Integer.parseInt(areaColorsR4[1]), Integer.parseInt(areaColorsR4[2]));
            graphics.setBackgroundColor(areaColorR4);  
            graphics.fillPolygon(areaR4);
          }
          
          if (fontColorsR4.length == 3) {
            Color fontColorR4 = new Color(null, Integer.parseInt(fontColorsR4[0]), Integer.parseInt(fontColorsR4[1]), Integer.parseInt(fontColorsR4[2]));
            graphics.setForegroundColor(fontColorR4);  
          }
          else {
            graphics.setForegroundColor(ColorConstants.gray);
          }          
          graphics.drawString(textR4, areaR4.getPoint(0).x + 1, areaR4.getPoint(0).y + halfOfQuoter/4);          

                  
          if (areaColorsC1.length == 3) {
            Color areaColorC1 = new Color(null, Integer.parseInt(areaColorsC1[0]), Integer.parseInt(areaColorsC1[1]), Integer.parseInt(areaColorsC1[2]));
            graphics.setBackgroundColor(areaC2Color);  
            graphics.fillPolygon(areaC1);
          }
          graphics.drawString(textC1, areaC1.getPoint(0).x + 1 + halfOfQuoter, areaC1.getPoint(0).y - halfOfQuoter/2 - halfOfQuoter/4);

          Color areaColorC2 = new Color(null, Integer.parseInt(areaColorsC1[0]), Integer.parseInt(areaColorsC1[1]), Integer.parseInt(areaColorsC1[2]));
          graphics.setBackgroundColor(areaColorC2);            
          graphics.fillPolygon(areaC2);
          
          if (areaColorsC3.length == 3) {
            Color areaColorC3 = new Color(null, Integer.parseInt(areaColorsC3[0]), Integer.parseInt(areaColorsC3[1]), Integer.parseInt(areaColorsC3[2]));
            graphics.setBackgroundColor(areaColorC3);  
            graphics.fillPolygon(areaC3);
          }          
          
          fontIndex = bounds.height/12;
          font = new Font (null, "Source Code Pro", fontIndex, SWT.NORMAL);
          graphics.setFont (font);
          graphics.setForegroundColor(ColorConstants.black);          
          
          //CENTER CENTER     
//           int CenterCenterX = bounds.x + fontIndex*8;
          int CenterCenterX = bounds.x + bounds.width/4;
          int CenterCenterY = bounds.y + bounds.height/4 + bounds.height/20;
          graphics.drawText(textC2, CenterCenterX, CenterCenterY);   
          //fontIndex.toString()          
          
          graphics.popState();
      }     

    /**
     * Calculate the Text Control Bounds or null if none.
     */
    protected Rectangle calculateTextControlBounds() {
       
        Rectangle bounds = getBounds().getCopy();
        bounds.x += 1000;

        return bounds;
    }
    

}
