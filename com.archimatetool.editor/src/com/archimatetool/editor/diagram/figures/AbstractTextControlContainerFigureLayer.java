/**
 * This program and the accompanying materials
 * are made available under the terms of the License
 * which accompanies this distribution in the file LICENSE.txt
 */
package com.archimatetool.editor.diagram.figures;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.DelegatingLayout;
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.GridData;
import org.eclipse.draw2d.GridLayout;
import org.eclipse.draw2d.FigureUtilities;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.Locator;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.draw2d.text.BlockFlow;
import org.eclipse.draw2d.text.FlowPage;
import org.eclipse.draw2d.text.ParagraphTextLayout;
import org.eclipse.draw2d.text.TextFlow;
import org.eclipse.swt.SWT;

import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.geometry.PointList;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Transform;
import org.eclipse.swt.graphics.FontMetrics;
import org.eclipse.swt.graphics.Transform;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Canvas;
import com.archimatetool.editor.ui.ColorFactory;
import com.archimatetool.editor.ui.FontFactory;

import com.archimatetool.editor.preferences.IPreferenceConstants;
import com.archimatetool.editor.preferences.Preferences;
import com.archimatetool.editor.utils.StringUtils;
import com.archimatetool.model.IDiagramModelObject;
import com.archimatetool.model.ITextAlignment;
import com.archimatetool.model.ITextPosition;
import com.archimatetool.model.IDiagramModelArchimateObject;
import com.archimatetool.model.impl.DiagramModelArchimateObject;
import com.archimatetool.model.IArchestry;
/**
 * Abstract Container Figure with Text Control
 * 
 * @author Phillip Beauvoir
 */
public abstract class AbstractTextControlContainerFigureLayer extends AbstractContainerFigure {
    
    private IFigure fTextControl;
    private int fTextControlType = TEXT_FLOW_CONTROL;
    
    public static final int TEXT_FLOW_CONTROL = 0;
    public static final int LABEL_CONTROL = 1;
    
    private TextPositionDelegate fTextPositionDelegate;
    
    protected AbstractTextControlContainerFigureLayer(int textControlType) {
        fTextControlType = textControlType;
    }
    
    protected AbstractTextControlContainerFigureLayer(IDiagramModelObject diagramModelObject, int textControlType) {
        fTextControlType = textControlType;
        setDiagramModelObject(diagramModelObject);
    }
    
    @Override
    protected void setUI() {
        setLayoutManager(new DelegatingLayout());
        
        Locator textLocator = new Locator() {
            @Override
            public void relocate(IFigure target) {
                Rectangle bounds = calculateTextControlBounds();
                if(bounds == null) {
                    bounds = getBounds().getCopy();
                }
                translateFromParent(bounds);
                target.setBounds(bounds);
            }
        };
        
        fTextControl = createTextControl(textLocator);
        
        Locator mainLocator = new Locator() {
            @Override
            public void relocate(IFigure target) {
                Rectangle bounds = getBounds().getCopy();
                translateFromParent(bounds);
                target.setBounds(bounds);
            }
        };
        
        add(getMainFigure(), mainLocator);
    }
    
    @Override
    public void refreshVisuals() {

        // Font
        setFont();
        
        // Fill Color
        setFillColor();
        
        // Font Color
        setFontColor();
        
        // Line Color
        setLineColor();
        
        // Text Position
        if(getTextControl() instanceof TextFlow) {
            int alignment = getDiagramModelObject().getTextAlignment();
            ((BlockFlow)getTextControl().getParent()).setHorizontalAligment(alignment);
            
            if(fTextPositionDelegate != null) {
                fTextPositionDelegate.updateTextPosition();
            }
        }
        
        repaint(); // repaint when figure changes
    }
    
    @Override
    public void setEnabled(boolean value) {
        super.setEnabled(value);
        
        getTextControl().setEnabled(value);
        
        if(getFigureDelegate() != null) {
            getFigureDelegate().setEnabled(value);
        }
    }
    
    protected void setText() {
        String text = StringUtils.safeString(getDiagramModelObject().getName());
        
        if(getTextControl() instanceof TextFlow) {
            ((TextFlow)getTextControl()).setText(text);
        }
        else if(getTextControl() instanceof Label) {
            ((Label)getTextControl()).setText(text);
        }
    }
    
    public String getText() {
        if(getTextControl() instanceof TextFlow) {
            return ((TextFlow)getTextControl()).getText();
        }
        else {
            return ((Label)getTextControl()).getText();
        }
    }
    
    @Override
    public IFigure getTextControl() {
        return fTextControl;
    }

    protected IFigure createTextControl(Locator textLocator) {
        if(fTextControlType == TEXT_FLOW_CONTROL) {
            return createTextFlowControl(textLocator);
        }
        else {
            return createLabelControl(textLocator);
        }
    }
    
    protected TextFlow createTextFlowControl(Locator textLocator) {
        TextFlow textFlow = new TextFlow();
        
        int wordWrapStyle = Preferences.STORE.getInt(IPreferenceConstants.ARCHIMATE_FIGURE_WORD_WRAP_STYLE);
        textFlow.setLayoutManager(new ParagraphTextLayout(textFlow, wordWrapStyle));
        
        BlockFlow block = new BlockFlow();
        block.add(textFlow);

        FlowPage page = new FlowPage();
        page.add(block);
        
        Figure textWrapperFigure = new Figure();
        GridLayout layout = new GridLayout();
        layout.marginWidth = getTextControlMarginWidth();
        layout.marginHeight = 5;
        textWrapperFigure.setLayoutManager(layout);
        GridData gd = new GridData(SWT.CENTER, SWT.TOP, true, true);
        textWrapperFigure.add(page, gd);
        
        if(getDiagramModelObject() instanceof ITextPosition) {
            fTextPositionDelegate = new TextPositionDelegate(textWrapperFigure, page, (ITextPosition)getDiagramModelObject());
        }
        
        add(textWrapperFigure, textLocator);
        //add(page, textLocator);
        
        return textFlow;
    }

    protected Label createLabelControl(Locator textLocator) {
        Label label = new Label(""); //$NON-NLS-1$
        add(label, textLocator);
        return label;
    }

    /**
     * @return the left and right margin width for text
     */
    protected int getTextControlMarginWidth() {
        return 20;
    }
    
    protected int getIconOffset() {
        return 10;
    }
    
    protected void layerMarkup(Graphics graphics) {
    
        
          graphics.pushState();
          graphics.setLineWidth(1);
          graphics.setForegroundColor(isEnabled() ? ColorConstants.black : ColorConstants.gray);
          
          Rectangle bounds = getBounds();
          
          PointList points = new PointList();

          int x = bounds.x;
          int y = bounds.y;
          int width = bounds.width - 1;
          int height = bounds.height - 1;
          int centerX = x + bounds.width/2;
          int centerY = y + bounds.height/2;        
          int realWidth = bounds.width/3;
          int realHeigth = bounds.height/2;
              
          Integer fontIndex = bounds.height/20;
          Font font = new Font (null, "Noto", fontIndex, SWT.NORMAL);
          graphics.setFont (font);

          String text = StringUtils.safeString(getDiagramModelObject().getName());
          
          int textWidth = FigureUtilities.getTextWidth(text, font);
          
           graphics.translate(width / 2, height / 2);
           graphics.rotate(-90);
           graphics.drawText(text, (int)-(bounds.y + height/4 + height/5), (int)bounds.x - width / 2 + width / 60); 
           graphics.drawLine((int)-(bounds.y + height/2), bounds.x - width / 2 + width / 20, (int)-(bounds.y - height/2), bounds.x - width / 2 + width / 20);

           graphics.rotate(90);
           graphics.translate(-(height / 2), -(width / 2));

          graphics.popState();
      }     

    /**
     * Calculate the Text Control Bounds or null if none.
     */
    protected Rectangle calculateTextControlBounds() {
       
        Rectangle bounds = getBounds().getCopy();
        bounds.x += 1000;

        return bounds;
    }
    

}
