/**
 * This program and the accompanying materials
 * are made available under the terms of the License
 * which accompanies this distribution in the file LICENSE.txt
 */
package com.archimatetool.editor.diagram.figures.elements;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.ConnectionAnchor;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.draw2d.geometry.PointList;
import org.eclipse.swt.graphics.Pattern;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.RGBA;
import org.eclipse.draw2d.IFigure;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.SWT;


import com.archimatetool.editor.ui.IArchiImages;
import com.archimatetool.editor.diagram.editparts.RoundedRectangleAnchor;
import com.archimatetool.editor.diagram.figures.AbstractTextControlContainerFigureVSM;
import com.archimatetool.editor.diagram.figures.VsmFigureDelegate;
import com.archimatetool.editor.diagram.figures.FigureUtils;
import com.archimatetool.editor.preferences.IPreferenceConstants;
import com.archimatetool.editor.preferences.Preferences;
import com.archimatetool.editor.utils.StringUtils;

import com.archimatetool.editor.diagram.figures.AbstractDiagramModelObjectFigure;
import com.archimatetool.model.IDiagramModelArchimateObject;
import com.archimatetool.model.impl.DiagramModelArchimateObject;

import com.archimatetool.model.IArchimateConcept;
import com.archimatetool.model.impl.ArchimateConcept;
import com.archimatetool.model.IDocumentable;
import com.archimatetool.model.IArchestry;
import com.archimatetool.model.IDiagramModelObject;
import com.archimatetool.editor.diagram.figures.FigureUtils.Direction;

/**
 * VSM Figure 
 * 
 * @author Phillip Beauvoir
 */
public class VsmFigure extends AbstractTextControlContainerFigureVSM {

    public VsmFigure() {
      super(TEXT_FLOW_CONTROL); 
        setFigureDelegate(new VsmFigureDelegate(this, 1));
    }
    
    
    @Override
    public void drawFigure(Graphics graphics) {
    
        super.drawFigure(graphics);
        graphics.pushState();
        
        Rectangle bounds = getBounds();
        
        PointList points = new PointList();
 
        int x = bounds.x;
        int y = bounds.y;
        int width = bounds.width - 1;
        int height = bounds.height - 1;
        int centerX = x + bounds.width/2;
        int centerY = y + bounds.height/2;        
        int realWidth = bounds.width/3;
        int realHeigth = bounds.height/2;
        
        for (int i = 0; i < 12; i++){
          points.addPoint((int) (centerX + realWidth * Math.cos(i * 2 * Math.PI / 12)),
				  (int) (centerY + realHeigth * Math.sin(i * 2 * Math.PI / 12)));
        }        
        
        
        if(!isEnabled()) {
            setDisabledState(graphics);
        }
        
        // Fill
        graphics.setAlpha(getAlpha());
        //graphics.setBackgroundColor(getFillColor());    

        
        PointList areaC2 = new PointList();
        areaC2.addPoint(points.getPoint(1));
        areaC2.addPoint(points.getPoint(5));
        areaC2.addPoint(points.getPoint(7));
        areaC2.addPoint(points.getPoint(11));
        graphics.fillPolygon(areaC2);
        
        int halfOfQuoter = (areaC2.getPoint(1).y - areaC2.getPoint(2).y)/2;
        
        // Line
        graphics.setForegroundColor(getLineColor());
        graphics.drawPolygon(areaC2);
        
        Color grey = new Color(null, 222, 225, 207);
        PointList pointsLine = new PointList();
        graphics.setForegroundColor(grey);        
        
        // R1
        PointList areaR1 = new PointList();
        areaR1.addPoint(areaC2.getPoint(3));
        areaR1.addPoint((int) x + width - width/20, (int) areaC2.getPoint(3).y);
        areaR1.addPoint((int) x + width - width/20, (int) y + height/20);        
        areaR1.addPoint((int) areaC2.getPoint(3).x, y + height/20);

        pointsLine = new PointList();
        pointsLine.addPoint(areaR1.getPoint(0));
        pointsLine.addPoint((int) areaR1.getPoint(0).x, y);
        graphics.drawPolyline(pointsLine);
        
        pointsLine = new PointList();
        pointsLine.addPoint(areaR1.getPoint(0));
        pointsLine.addPoint((int) x + width , areaR1.getPoint(0).y);
        graphics.drawPolyline(pointsLine);       
        
        // R2
        PointList areaR2 = new PointList();
        areaR2.addPoint((int) areaC2.getPoint(3).x + 1, (int) areaC2.getPoint(3).y + 1);
        areaR2.addPoint((int) areaC2.getPoint(3).x + 1, (int) areaC2.getPoint(3).y + 1 + (areaC2.getPoint(0).y - areaC2.getPoint(3).y)/2);
        areaR2.addPoint((int) x + width - 1, (int) areaC2.getPoint(3).y + 1 + (areaC2.getPoint(0).y - areaC2.getPoint(3).y)/2);            
        areaR2.addPoint((int) x + width - 1, areaC2.getPoint(3).y + 1);

        pointsLine = new PointList();
        pointsLine.addPoint((int) areaR2.getPoint(1).x, (int) areaR2.getPoint(1).y + 1);
        pointsLine.addPoint((int) areaR2.getPoint(2).x, (int) areaR2.getPoint(2).y + 1);
        graphics.drawPolyline(pointsLine);
        
        // R3
        PointList areaR3 = new PointList();
        areaR3.addPoint((int) areaC2.getPoint(0).x + 1, (int) areaC2.getPoint(0).y - 1);
        areaR3.addPoint((int) areaC2.getPoint(0).x + 1, (int) areaC2.getPoint(0).y + 1 - (areaC2.getPoint(0).y - areaC2.getPoint(3).y)/2);
        areaR3.addPoint((int) x + width - 1, (int) areaC2.getPoint(0).y + 1 - (areaC2.getPoint(0).y - areaC2.getPoint(3).y)/2);            
        areaR3.addPoint((int) x + width - 1, areaC2.getPoint(0).y - 1);
        
        // R4
        PointList areaR4 = new PointList();
        areaR4.addPoint((int) areaC2.getPoint(0).x + 1, (int) areaC2.getPoint(0).y + 1);
        areaR4.addPoint((int) x + width - width/20, (int) areaC2.getPoint(0).y + 1);
        areaR4.addPoint((int) x + width - width/20, (int) y + height - height/20);        
        areaR4.addPoint((int) areaC2.getPoint(0).x + 1, y + height - height/20);

        pointsLine = new PointList();
        pointsLine.addPoint((int) areaC2.getPoint(0).x, (int) areaC2.getPoint(0).y);
        pointsLine.addPoint((int) areaR4.getPoint(0).x - 1, y + height);
        graphics.drawPolyline(pointsLine);
        
        pointsLine = new PointList();
        pointsLine.addPoint((int) areaC2.getPoint(0).x, (int) areaC2.getPoint(0).y);
        pointsLine.addPoint((int) x + width, areaR4.getPoint(0).y - 1);
        graphics.drawPolyline(pointsLine);   
        
        
        
////////////////////// AreaL

        // L1
        PointList areaL1 = new PointList();
        areaL1.addPoint((int) areaC2.getPoint(2).x - 1, (int) areaC2.getPoint(2).y - 1);
        areaL1.addPoint((int) x + width/20, (int) areaC2.getPoint(2).y - 1);
        areaL1.addPoint((int) x + width/20, (int) y + height/20);        
        areaL1.addPoint((int) areaC2.getPoint(2).x - 1, y + height/20);

        pointsLine = new PointList();
        pointsLine.addPoint((int) areaL1.getPoint(0).x + 1, (int) areaL1.getPoint(0).y + 1);
        pointsLine.addPoint((int) areaL1.getPoint(0).x + 1, y);
        graphics.drawPolyline(pointsLine);
        
        pointsLine = new PointList();
        pointsLine.addPoint((int) areaL1.getPoint(0).x + 1, (int) areaL1.getPoint(0).y + 1);
        pointsLine.addPoint((int) x , areaL1.getPoint(0).y + 1);
        graphics.drawPolyline(pointsLine);       
        
        // L2
        PointList areaL2 = new PointList();
        areaL2.addPoint((int) areaC2.getPoint(2).x - 1, (int) areaC2.getPoint(2).y + 1);
        areaL2.addPoint((int) areaC2.getPoint(2).x - 1, (int) areaC2.getPoint(2).y + 1 + halfOfQuoter);
        areaL2.addPoint((int) x + 1, (int) areaC2.getPoint(2).y + 1 + halfOfQuoter);            
        areaL2.addPoint((int) x + 1, areaC2.getPoint(2).y + 1);

        pointsLine = new PointList();
        pointsLine.addPoint(areaL2.getPoint(1));
        pointsLine.addPoint(areaL2.getPoint(2));
        graphics.drawPolyline(pointsLine);
        
        // L3
        PointList areaL3 = new PointList();
        areaL3.addPoint((int) areaC2.getPoint(1).x - 1, (int) areaC2.getPoint(1).y - 1);
        areaL3.addPoint((int) areaC2.getPoint(1).x - 1, (int) areaC2.getPoint(1).y - 1 - halfOfQuoter);
        areaL3.addPoint((int) x + 1, (int) areaC2.getPoint(1).y - 1 - halfOfQuoter);            
        areaL3.addPoint((int) x + 1, areaC2.getPoint(1).y - 1);
        
        // L4
        PointList areaL4 = new PointList();
        areaL4.addPoint((int) areaC2.getPoint(1).x - 1, (int) areaC2.getPoint(1).y + 1);
        areaL4.addPoint((int) x + width/20, (int) areaC2.getPoint(1).y + 1);
        areaL4.addPoint((int) x + width/20, (int) y + height - height/20);        
        areaL4.addPoint((int) areaC2.getPoint(1).x - 1, y + height - height/20);

        pointsLine = new PointList();
        pointsLine.addPoint((int) areaL4.getPoint(0).x + 1, (int) areaL4.getPoint(0).y - 1);
        pointsLine.addPoint((int) areaL4.getPoint(0).x + 1, y + height);
        graphics.drawPolyline(pointsLine);
        
        pointsLine = new PointList();
        pointsLine.addPoint((int) areaL4.getPoint(0).x + 1, (int) areaL4.getPoint(0).y - 1);
        pointsLine.addPoint((int) x , areaL4.getPoint(0).y - 1);
        graphics.drawPolyline(pointsLine);   
        
        
////////////////////// AreaC

        // C1
        PointList areaC1 = new PointList();
        
        areaC1.addPoint((int) areaC2.getPoint(2).x + 1, (int) areaC2.getPoint(2).y - 1);
        areaC1.addPoint((int) areaC2.getPoint(2).x + 1, (int) y + 1);
        areaC1.addPoint((int) areaC2.getPoint(3).x - 1, (int) y + 1);
        areaC1.addPoint((int) areaC2.getPoint(3).x - 1, (int) areaC2.getPoint(3).y - 1);
//         graphics.fillPolygon(areaC1);
        
        // C3
        PointList areaC3 = new PointList();
        areaC3.addPoint((int) areaC2.getPoint(1).x + 1, (int) areaC2.getPoint(1).y + 1);
        areaC3.addPoint((int) areaC2.getPoint(1).x + 1, (int) y + height - 1);
        areaC3.addPoint((int) areaC2.getPoint(0).x - 1, (int) y + height - 1);
        areaC3.addPoint((int) areaC2.getPoint(0).x - 1, (int) areaC2.getPoint(0).y + 1);
//         graphics.fillPolygon(areaC3);        
                
        if(!isEnabled()) {
            setDisabledState(graphics);
        }
        

        
        metaMarkup(graphics, halfOfQuoter, areaL1, areaL2, areaL3, areaL4, areaR1, areaR2, areaR3, areaR4, areaC1, areaC2, getFillColor(), areaC3);
        graphics.popState();
    }
                                                                  

    @Override
    public IDiagramModelArchimateObject getDiagramModelObject() {
        return (IDiagramModelArchimateObject)super.getDiagramModelObject();
    }  

    
    @Override
    public ConnectionAnchor getDefaultConnectionAnchor() {
        return new RoundedRectangleAnchor(this);
    }
    



}
